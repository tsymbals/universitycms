package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateUserRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateUserRequest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserService {

    @Transactional
    UserDto save(CreateUserRequest createUserRequest);

    @Transactional(readOnly = true)
    List<UserDto> getAll();

    @Transactional(readOnly = true)
    List<UserDto> getAllFreeAdminUsers();

    @Transactional(readOnly = true)
    List<UserDto> getAllFreeEmployeeUsers();

    @Transactional(readOnly = true)
    List<UserDto> getAllFreeTeacherUsers();

    @Transactional(readOnly = true)
    List<UserDto> getAllFreeStudentUsers();

    @Transactional(readOnly = true)
    UserDto getById(Long id);

    @Transactional(readOnly = true)
    UserDto getByUsername(String username);

    @Transactional
    UserDto updateUsername(Long id, UpdateUserRequest updateUserRequest);

    @Transactional
    UserDto updatePassword(Long id, UpdateUserRequest updateUserRequest);

    @Transactional
    UserDto updateEnabledStatus(Long id, UpdateUserRequest updateUserRequest);

    @Transactional
    UserDto addRole(Long id, UpdateUserRequest updateUserRequest);

    @Transactional
    UserDto removeRole(Long id, UpdateUserRequest updateUserRequest);

    @Transactional
    void deleteById(Long id);
}
