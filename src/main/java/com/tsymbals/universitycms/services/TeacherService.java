package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.TeacherDto;
import com.tsymbals.universitycms.models.dto.requests.CreateTeacherRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateTeacherRequest;
import com.tsymbals.universitycms.models.entities.Teacher;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface TeacherService {

    @Transactional
    TeacherDto save(CreateTeacherRequest createTeacherRequest);

    @Transactional(readOnly = true)
    List<TeacherDto> getAll();

    @Transactional(readOnly = true)
    List<TeacherDto> getAllNoneMatching(Teacher comparisonTeacher);

    @Transactional(readOnly = true)
    TeacherDto getById(Long id);

    @Transactional(readOnly = true)
    TeacherDto getByUsername(String username);

    @Transactional(readOnly = true)
    TeacherDto getByFirstNameAndLastName(String firstName, String lastName);

    @Transactional
    TeacherDto updateUser(Long id, UpdateTeacherRequest updateTeacherRequest);

    @Transactional
    TeacherDto updateLastName(Long id, UpdateTeacherRequest updateTeacherRequest);

    @Transactional
    TeacherDto addCourse(Long id, UpdateTeacherRequest updateTeacherRequest);

    @Transactional
    TeacherDto removeCourse(Long id, UpdateTeacherRequest updateTeacherRequest);

    @Transactional
    TeacherDto addGroup(Long id, UpdateTeacherRequest updateTeacherRequest);

    @Transactional
    TeacherDto removeGroup(Long id, UpdateTeacherRequest updateTeacherRequest);

    @Transactional
    void deleteById(Long id);
}
