package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.UniversityDto;
import com.tsymbals.universitycms.models.dto.requests.CreateUniversityRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateUniversityRequest;
import com.tsymbals.universitycms.models.entities.University;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UniversityService {

    @Transactional
    UniversityDto save(CreateUniversityRequest createUniversityRequest);

    @Transactional(readOnly = true)
    List<UniversityDto> getAll();

    @Transactional(readOnly = true)
    List<UniversityDto> getAllNoneMatching(University comparisonUniversity);

    @Transactional(readOnly = true)
    UniversityDto getById(Long id);

    @Transactional(readOnly = true)
    UniversityDto getByAbbreviationName(String abbreviationName);

    @Transactional
    UniversityDto updateName(Long id, UpdateUniversityRequest updateUniversityRequest);

    @Transactional
    UniversityDto updateAbbreviationName(Long id, UpdateUniversityRequest updateUniversityRequest);

    @Transactional
    UniversityDto updateCity(Long id, UpdateUniversityRequest updateUniversityRequest);

    @Transactional
    UniversityDto updatePhoneNumber(Long id, UpdateUniversityRequest updateUniversityRequest);

    @Transactional
    void deleteById(Long id);
}
