package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.EmployeeDto;
import com.tsymbals.universitycms.models.dto.requests.CreateEmployeeRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateEmployeeRequest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface EmployeeService {

    @Transactional
    EmployeeDto save(CreateEmployeeRequest createEmployeeRequest);

    @Transactional(readOnly = true)
    List<EmployeeDto> getAll();

    @Transactional(readOnly = true)
    EmployeeDto getById(Long id);

    @Transactional(readOnly = true)
    EmployeeDto getByFirstNameAndLastName(String firstName, String lastName);

    @Transactional
    EmployeeDto updateUser(Long id, UpdateEmployeeRequest updateEmployeeRequest);

    @Transactional
    EmployeeDto updateLastName(Long id, UpdateEmployeeRequest updateEmployeeRequest);

    @Transactional
    void deleteById(Long id);
}
