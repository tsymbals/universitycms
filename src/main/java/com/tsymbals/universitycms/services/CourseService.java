package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.CourseDto;
import com.tsymbals.universitycms.models.dto.requests.CreateCourseRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateCourseRequest;
import com.tsymbals.universitycms.models.entities.Course;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public interface CourseService {

    @Transactional
    CourseDto save(CreateCourseRequest createCourseRequest);

    @Transactional(readOnly = true)
    List<CourseDto> getAll();

    @Transactional(readOnly = true)
    List<CourseDto> getAllNoneMatching(Course comparisonCourse);

    @Transactional(readOnly = true)
    List<CourseDto> getAllNoneMatching(Set<Course> comparisonCourses);

    @Transactional(readOnly = true)
    CourseDto getById(Long id);

    @Transactional(readOnly = true)
    CourseDto getByName(String name);

    @Transactional
    CourseDto updateName(Long id, UpdateCourseRequest updateCourseRequest);

    @Transactional
    CourseDto updateDescription(Long id, UpdateCourseRequest updateCourseRequest);

    @Transactional
    void deleteById(Long id);
}
