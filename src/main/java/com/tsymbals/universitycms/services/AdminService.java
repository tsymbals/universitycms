package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.AdminDto;
import com.tsymbals.universitycms.models.dto.requests.CreateAdminRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateAdminRequest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AdminService {

    @Transactional
    AdminDto save(CreateAdminRequest createAdminRequest);

    @Transactional(readOnly = true)
    List<AdminDto> getAll();

    @Transactional(readOnly = true)
    AdminDto getById(Long id);

    @Transactional
    AdminDto updateUser(Long id, UpdateAdminRequest updateAdminRequest);

    @Transactional
    void deleteById(Long id);
}
