package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.LessonTimeDto;
import com.tsymbals.universitycms.models.dto.requests.CreateLessonTimeRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateLessonTimeRequest;
import com.tsymbals.universitycms.models.entities.LessonTime;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalTime;
import java.util.List;

public interface LessonTimeService {

    @Transactional
    LessonTimeDto save(CreateLessonTimeRequest createLessonTimeRequest);

    @Transactional(readOnly = true)
    List<LessonTimeDto> getAll();

    @Transactional(readOnly = true)
    List<LessonTimeDto> getAllNoneMatching(LessonTime comparisonLessonTime);

    @Transactional(readOnly = true)
    LessonTimeDto getById(Long id);

    @Transactional(readOnly = true)
    LessonTimeDto getByStartTimeAndEndTime(LocalTime startTime, LocalTime endTime);

    @Transactional
    LessonTimeDto updateStartTime(Long id, UpdateLessonTimeRequest updateLessonTimeRequest);

    @Transactional
    LessonTimeDto updateEndTime(Long id, UpdateLessonTimeRequest updateLessonTimeRequest);

    @Transactional
    void deleteById(Long id);
}
