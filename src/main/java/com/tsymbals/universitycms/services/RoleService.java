package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.RoleDto;
import com.tsymbals.universitycms.models.dto.requests.CreateRoleRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateRoleRequest;
import com.tsymbals.universitycms.models.entities.Role;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public interface RoleService {

    @Transactional
    RoleDto save(CreateRoleRequest createRoleRequest);

    @Transactional(readOnly = true)
    List<RoleDto> getAll();

    @Transactional(readOnly = true)
    List<RoleDto> getAllNoneMatching(Set<Role> comparisonRoles);

    @Transactional(readOnly = true)
    RoleDto getById(Long id);

    @Transactional(readOnly = true)
    RoleDto getByName(String name);

    @Transactional
    RoleDto updateName(Long id, UpdateRoleRequest updateRoleRequest);

    @Transactional
    void deleteById(Long id);
}
