package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.LessonDto;
import com.tsymbals.universitycms.models.dto.StudentDto;
import com.tsymbals.universitycms.models.dto.TeacherDto;
import com.tsymbals.universitycms.models.dto.requests.CreateLessonRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateLessonRequest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public interface LessonService {

    @Transactional
    LessonDto save(CreateLessonRequest createLessonRequest);

    @Transactional(readOnly = true)
    List<LessonDto> getAll();

    @Transactional(readOnly = true)
    List<LessonDto> getAllByDate(LocalDate date);

    @Transactional(readOnly = true)
    List<LessonDto> getAllByDate(LocalDate date, TeacherDto teacherDto);

    @Transactional(readOnly = true)
    List<LessonDto> getAllByDate(LocalDate date, StudentDto studentDto);

    @Transactional(readOnly = true)
    List<LessonDto> getAllByWeek(LocalDate startWeekDate, LocalDate endWeekDate, TeacherDto teacherDto);

    @Transactional(readOnly = true)
    List<LessonDto> getAllByWeek(LocalDate startWeekDate, LocalDate endWeekDate, StudentDto studentDto);

    @Transactional(readOnly = true)
    LessonDto getById(Long id);

    @Transactional(readOnly = true)
    LessonDto getByDateAndStartTimeAndEndTime(LocalDate date, LocalTime startTime, LocalTime endTime);

    @Transactional
    LessonDto updateDate(Long id, UpdateLessonRequest updateLessonRequest);

    @Transactional
    LessonDto updateLessonTime(Long id, UpdateLessonRequest updateLessonRequest);

    @Transactional
    LessonDto updateAuditorium(Long id, UpdateLessonRequest updateLessonRequest);

    @Transactional
    LessonDto updateCourse(Long id, UpdateLessonRequest updateLessonRequest);

    @Transactional
    LessonDto removeCourse(Long id, UpdateLessonRequest updateLessonRequest);

    @Transactional
    LessonDto updateTeacher(Long id, UpdateLessonRequest updateLessonRequest);

    @Transactional
    LessonDto removeTeacher(Long id, UpdateLessonRequest updateLessonRequest);

    @Transactional
    LessonDto addGroup(Long id, UpdateLessonRequest updateLessonRequest);

    @Transactional
    LessonDto removeGroup(Long id, UpdateLessonRequest updateLessonRequest);

    @Transactional
    void deleteById(Long id);
}
