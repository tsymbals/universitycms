package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.StudentDto;
import com.tsymbals.universitycms.models.dto.requests.CreateStudentRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateStudentRequest;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface StudentService {

    @Transactional
    StudentDto save(CreateStudentRequest createStudentRequest);

    @Transactional(readOnly = true)
    List<StudentDto> getAll();

    @Transactional(readOnly = true)
    StudentDto getById(Long id);

    @Transactional(readOnly = true)
    StudentDto getByUsername(String username);

    @Transactional(readOnly = true)
    StudentDto getByFirstNameAndLastName(String firstName, String lastName);

    @Transactional
    StudentDto updateUser(Long id, UpdateStudentRequest updateStudentRequest);

    @Transactional
    StudentDto updateGroup(Long id, UpdateStudentRequest updateStudentRequest);

    @Transactional
    StudentDto removeGroup(Long id, UpdateStudentRequest updateStudentRequest);

    @Transactional
    StudentDto updateLastName(Long id, UpdateStudentRequest updateStudentRequest);

    @Transactional
    StudentDto addCourse(Long id, UpdateStudentRequest updateStudentRequest);

    @Transactional
    StudentDto removeCourse(Long id, UpdateStudentRequest updateStudentRequest);

    @Transactional
    void deleteById(Long id);
}
