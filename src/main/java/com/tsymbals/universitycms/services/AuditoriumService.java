package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.AuditoriumDto;
import com.tsymbals.universitycms.models.dto.LessonDto;
import com.tsymbals.universitycms.models.dto.requests.CreateAuditoriumRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateAuditoriumRequest;
import com.tsymbals.universitycms.models.entities.Auditorium;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface AuditoriumService {

    @Transactional
    AuditoriumDto save(CreateAuditoriumRequest createAuditoriumRequest);

    @Transactional(readOnly = true)
    List<AuditoriumDto> getAll();

    @Transactional(readOnly = true)
    List<AuditoriumDto> getAllNoneMatching(Auditorium comparisonAuditorium);

    @Transactional(readOnly = true)
    List<AuditoriumDto> getAllByDate(List<LessonDto> lessonDtoList);

    @Transactional(readOnly = true)
    AuditoriumDto getById(Long id);

    @Transactional(readOnly = true)
    AuditoriumDto getByName(String name);

    @Transactional
    AuditoriumDto updateDepartment(Long id, UpdateAuditoriumRequest updateAuditoriumRequest);

    @Transactional
    AuditoriumDto updateName(Long id, UpdateAuditoriumRequest updateAuditoriumRequest);

    @Transactional
    void deleteById(Long id);
}
