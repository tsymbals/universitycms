package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.GroupDto;
import com.tsymbals.universitycms.models.dto.requests.CreateGroupRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateGroupRequest;
import com.tsymbals.universitycms.models.entities.Group;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

public interface GroupService {

    @Transactional
    GroupDto save(CreateGroupRequest createGroupRequest);

    @Transactional(readOnly = true)
    List<GroupDto> getAll();

    @Transactional(readOnly = true)
    List<GroupDto> getAllNoneMatching(Group comparisonGroup);

    @Transactional(readOnly = true)
    List<GroupDto> getAllNoneMatching(Set<Group> comparisonGroups);

    @Transactional(readOnly = true)
    GroupDto getById(Long id);

    @Transactional(readOnly = true)
    GroupDto getByName(String name);

    @Transactional
    GroupDto updateName(Long id, UpdateGroupRequest updateGroupRequest);

    @Transactional
    void deleteById(Long id);
}
