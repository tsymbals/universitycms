package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.EmployeeDto;
import com.tsymbals.universitycms.models.dto.requests.CreateEmployeeRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateEmployeeRequest;
import com.tsymbals.universitycms.models.entities.Employee;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.repositories.EmployeeRepository;
import com.tsymbals.universitycms.repositories.UserRepository;
import com.tsymbals.universitycms.services.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private static final String USER_WITH_USERNAME_DOES_NOT_EXIST = "User with username='%s' doesn't exist";
    private static final String EMPLOYEE_WITH_ID_DOES_NOT_EXIST = "Employee with id=%d doesn't exist";
    private static final String EMPLOYEE_WITH_FIRST_NAME_AND_LAST_NAME_DOES_NOT_EXIST = "Employee with firstName='%s' and lastName='%s' doesn't exist";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final UserRepository userRepository;
    private final EmployeeRepository employeeRepository;

    @Override
    public EmployeeDto save(CreateEmployeeRequest createEmployeeRequest) {
        UserEntity user = userRepository.findByUsername(createEmployeeRequest.getUsername())
                .orElseThrow(() -> new EntityNotFoundException(USER_WITH_USERNAME_DOES_NOT_EXIST.formatted(createEmployeeRequest.getUsername())));
        userRepository.updateFreeStatus(user.getId(), Boolean.FALSE);

        return MODEL_MAPPER.map(employeeRepository.save(
                new Employee(
                        user,
                        createEmployeeRequest.getFirstName(),
                        createEmployeeRequest.getLastName())),
                EmployeeDto.class);
    }

    @Override
    public List<EmployeeDto> getAll() {
        return employeeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(employee -> MODEL_MAPPER.map(employee, EmployeeDto.class))
                .toList();
    }

    @Override
    public EmployeeDto getById(Long id) {
        return employeeRepository.findById(id)
                .map(employee -> MODEL_MAPPER.map(employee, EmployeeDto.class))
                .orElseThrow(() -> new EntityNotFoundException(EMPLOYEE_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public EmployeeDto getByFirstNameAndLastName(String firstName, String lastName) {
        return employeeRepository.findByFirstNameAndLastName(firstName, lastName)
                .map(employee -> MODEL_MAPPER.map(employee, EmployeeDto.class))
                .orElseThrow(() -> new EntityNotFoundException(EMPLOYEE_WITH_FIRST_NAME_AND_LAST_NAME_DOES_NOT_EXIST.formatted(firstName, lastName)));
    }

    @Override
    public EmployeeDto updateUser(Long id, UpdateEmployeeRequest updateEmployeeRequest) {
        employeeRepository.findById(id)
                .ifPresentOrElse(employee -> userRepository.findByUsername(updateEmployeeRequest.getUsername())
                        .ifPresentOrElse(user -> {
                            userRepository.updateFreeStatus(employee.getUser().getId(), Boolean.TRUE);
                            userRepository.updateFreeStatus(user.getId(), Boolean.FALSE);
                            employee.setUser(user);
                            employeeRepository.save(employee);
                        }, () -> {
                            throw new EntityNotFoundException(USER_WITH_USERNAME_DOES_NOT_EXIST.formatted(updateEmployeeRequest.getUsername()));
                }), () -> {
                    throw new EntityNotFoundException(EMPLOYEE_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(employeeRepository.getReferenceById(id), EmployeeDto.class);
    }

    @Override
    public EmployeeDto updateLastName(Long id, UpdateEmployeeRequest updateEmployeeRequest) {
        if (!employeeRepository.existsById(id)) {
            throw new EntityNotFoundException(EMPLOYEE_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        employeeRepository.updateLastName(id, updateEmployeeRequest.getLastName());

        return MODEL_MAPPER.map(employeeRepository.getReferenceById(id), EmployeeDto.class);
    }

    @Override
    public void deleteById(Long id) {
        employeeRepository.findById(id)
                .ifPresentOrElse(employee -> {
                    userRepository.updateFreeStatus(employee.getUser().getId(), Boolean.TRUE);
                    employeeRepository.deleteById(id);
                }, () -> {
                    throw new EntityNotFoundException(EMPLOYEE_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });
    }
}
