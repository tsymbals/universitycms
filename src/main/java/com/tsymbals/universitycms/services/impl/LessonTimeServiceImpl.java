package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.LessonTimeDto;
import com.tsymbals.universitycms.models.dto.requests.CreateLessonTimeRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateLessonTimeRequest;
import com.tsymbals.universitycms.models.dto.utils.DtoSortingCollections;
import com.tsymbals.universitycms.models.entities.Lesson;
import com.tsymbals.universitycms.models.entities.LessonTime;
import com.tsymbals.universitycms.repositories.LessonTimeRepository;
import com.tsymbals.universitycms.services.LessonTimeService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LessonTimeServiceImpl implements LessonTimeService {

    private static final String LESSON_TIME_WITH_ID_DOES_NOT_EXIST = "LessonTime with id=%d doesn't exist";
    private static final String LESSON_TIME_WITH_START_TIME_AND_END_TIME_DOES_NOT_EXIST = "LessonTime with startTime=%s and endTime=%s doesn't exist";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final LessonTimeRepository lessonTimeRepository;

    @Override
    public LessonTimeDto save(CreateLessonTimeRequest createLessonTimeRequest) {
        return MODEL_MAPPER.map(lessonTimeRepository.save(
                new LessonTime(
                        createLessonTimeRequest.getStartTime(),
                        createLessonTimeRequest.getEndTime())),
                LessonTimeDto.class);
    }

    @Override
    public List<LessonTimeDto> getAll() {
        return lessonTimeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(LessonTimeServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<LessonTimeDto> getAllNoneMatching(LessonTime comparisonLessonTime) {
        return lessonTimeRepository.findAllByIdNotOrderByIdAsc(comparisonLessonTime.getId())
                .stream()
                .map(LessonTimeServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public LessonTimeDto getById(Long id) {
        return lessonTimeRepository.findById(id)
                .map(LessonTimeServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(LESSON_TIME_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public LessonTimeDto getByStartTimeAndEndTime(LocalTime startTime, LocalTime endTime) {
        return lessonTimeRepository.findByStartTimeAndEndTime(startTime, endTime)
                .map(LessonTimeServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(LESSON_TIME_WITH_START_TIME_AND_END_TIME_DOES_NOT_EXIST.formatted(startTime, endTime)));
    }

    @Override
    public LessonTimeDto updateStartTime(Long id, UpdateLessonTimeRequest updateLessonTimeRequest) {
        if (!lessonTimeRepository.existsById(id)) {
            throw new EntityNotFoundException(LESSON_TIME_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        lessonTimeRepository.updateStartTime(id, updateLessonTimeRequest.getStartTime());

        return MODEL_MAPPER.map(lessonTimeRepository.getReferenceById(id), LessonTimeDto.class);
    }

    @Override
    public LessonTimeDto updateEndTime(Long id, UpdateLessonTimeRequest updateLessonTimeRequest) {
        if (!lessonTimeRepository.existsById(id)) {
            throw new EntityNotFoundException(LESSON_TIME_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        lessonTimeRepository.updateEndTime(id, updateLessonTimeRequest.getEndTime());

        return MODEL_MAPPER.map(lessonTimeRepository.getReferenceById(id), LessonTimeDto.class);
    }

    @Override
    public void deleteById(Long id) {
        if (!lessonTimeRepository.existsById(id)) {
            throw new EntityNotFoundException(LESSON_TIME_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        lessonTimeRepository.deleteById(id);
    }

    private static LessonTimeDto mapToDto(LessonTime lessonTime) {
        LessonTimeDto lessonTimeDto = MODEL_MAPPER.map(lessonTime, LessonTimeDto.class);
        lessonTimeDto.setLessons(DtoSortingCollections.sort(lessonTimeDto.getLessons(), Comparator.comparing(Lesson::getId)));

        return lessonTimeDto;
    }
}
