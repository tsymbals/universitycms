package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.GroupDto;
import com.tsymbals.universitycms.models.dto.requests.CreateGroupRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateGroupRequest;
import com.tsymbals.universitycms.models.dto.utils.DtoSortingCollections;
import com.tsymbals.universitycms.models.entities.Group;
import com.tsymbals.universitycms.models.entities.Lesson;
import com.tsymbals.universitycms.models.entities.Student;
import com.tsymbals.universitycms.models.entities.Teacher;
import com.tsymbals.universitycms.repositories.GroupRepository;
import com.tsymbals.universitycms.services.GroupService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private static final String GROUP_WITH_ID_DOES_NOT_EXIST = "Group with id=%d doesn't exist";
    private static final String GROUP_WITH_NAME_DOES_NOT_EXIST = "Group with name='%s' doesn't exist";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final GroupRepository groupRepository;

    @Override
    public GroupDto save(CreateGroupRequest createGroupRequest) {
        return MODEL_MAPPER.map(groupRepository.save(
                new Group(
                        createGroupRequest.getName())),
                GroupDto.class);
    }

    @Override
    public List<GroupDto> getAll() {
        return findAll();
    }

    @Override
    public List<GroupDto> getAllNoneMatching(Group comparisonGroup) {
        if (Objects.isNull(comparisonGroup)) {
            return findAll();
        }

        return groupRepository.findAllByIdNotOrderByIdAsc(comparisonGroup.getId())
                .stream()
                .map(GroupServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<GroupDto> getAllNoneMatching(Set<Group> comparisonGroups) {
        if (comparisonGroups.isEmpty()) {
            return findAll();
        }

        return groupRepository.findAllByIdNotInOrderByIdAsc(comparisonGroups.stream()
                        .map(Group::getId)
                        .collect(Collectors.toSet()))
                .stream()
                .map(GroupServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public GroupDto getById(Long id) {
        return groupRepository.findById(id)
                .map(GroupServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(GROUP_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public GroupDto getByName(String name) {
        return groupRepository.findByName(name)
                .map(GroupServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(GROUP_WITH_NAME_DOES_NOT_EXIST.formatted(name)));
    }

    @Override
    public GroupDto updateName(Long id, UpdateGroupRequest updateGroupRequest) {
        if (!groupRepository.existsById(id)) {
            throw new EntityNotFoundException(GROUP_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        groupRepository.updateName(id, updateGroupRequest.getName());

        return MODEL_MAPPER.map(groupRepository.getReferenceById(id), GroupDto.class);
    }

    @Override
    public void deleteById(Long id) {
        if (!groupRepository.existsById(id)) {
            throw new EntityNotFoundException(GROUP_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        groupRepository.deleteById(id);
    }

    private static GroupDto mapToDto(Group group) {
        GroupDto groupDto = MODEL_MAPPER.map(group, GroupDto.class);
        groupDto.setTeachers(DtoSortingCollections.sort(groupDto.getTeachers(), Comparator.comparing(Teacher::getId)));
        groupDto.setStudents(DtoSortingCollections.sort(groupDto.getStudents(), Comparator.comparing(Student::getId)));
        groupDto.setLessons(DtoSortingCollections.sort(groupDto.getLessons(), Comparator.comparing(Lesson::getId)));

        return groupDto;
    }

    private List<GroupDto> findAll() {
        return groupRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(GroupServiceImpl::mapToDto)
                .toList();
    }
}
