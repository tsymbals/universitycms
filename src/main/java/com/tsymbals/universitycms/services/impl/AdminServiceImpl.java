package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.AdminDto;
import com.tsymbals.universitycms.models.dto.requests.CreateAdminRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateAdminRequest;
import com.tsymbals.universitycms.models.entities.Admin;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.repositories.AdminRepository;
import com.tsymbals.universitycms.repositories.UserRepository;
import com.tsymbals.universitycms.services.AdminService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {

    private static final String USER_WITH_USERNAME_DOES_NOT_EXIST = "User with username='%s' doesn't exist";
    private static final String ADMIN_WITH_ID_DOES_NOT_EXIST = "Admin with id=%d doesn't exist";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final UserRepository userRepository;
    private final AdminRepository adminRepository;

    @Override
    public AdminDto save(CreateAdminRequest createAdminRequest) {
        UserEntity user = userRepository.findByUsername(createAdminRequest.getUsername())
                .orElseThrow(() -> new EntityNotFoundException(USER_WITH_USERNAME_DOES_NOT_EXIST.formatted(createAdminRequest.getUsername())));
        userRepository.updateFreeStatus(user.getId(), Boolean.FALSE);

        return MODEL_MAPPER.map(adminRepository.save(new Admin(user)), AdminDto.class);
    }

    @Override
    public List<AdminDto> getAll() {
        return adminRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(admin -> MODEL_MAPPER.map(admin, AdminDto.class))
                .toList();
    }

    @Override
    public AdminDto getById(Long id) {
        return adminRepository.findById(id)
                .map(admin -> MODEL_MAPPER.map(admin, AdminDto.class))
                .orElseThrow(() -> new EntityNotFoundException(ADMIN_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public AdminDto updateUser(Long id, UpdateAdminRequest updateAdminRequest) {
        adminRepository.findById(id)
                .ifPresentOrElse(admin -> userRepository.findByUsername(updateAdminRequest.getUsername())
                        .ifPresentOrElse(user -> {
                            userRepository.updateFreeStatus(admin.getUser().getId(), Boolean.TRUE);
                            userRepository.updateFreeStatus(user.getId(), Boolean.FALSE);
                            admin.setUser(user);
                            adminRepository.save(admin);
                        }, () -> {
                            throw new EntityNotFoundException(USER_WITH_USERNAME_DOES_NOT_EXIST.formatted(updateAdminRequest.getUsername()));
                }), () -> {
                    throw new EntityNotFoundException(ADMIN_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(adminRepository.getReferenceById(id), AdminDto.class);
    }

    @Override
    public void deleteById(Long id) {
        adminRepository.findById(id)
                .ifPresentOrElse(admin -> {
                    userRepository.updateFreeStatus(admin.getUser().getId(), Boolean.TRUE);
                    adminRepository.deleteById(id);
                }, () -> {
                    throw new EntityNotFoundException(ADMIN_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });
    }
}
