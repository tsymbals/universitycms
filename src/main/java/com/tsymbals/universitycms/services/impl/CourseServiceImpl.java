package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.CourseDto;
import com.tsymbals.universitycms.models.dto.requests.CreateCourseRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateCourseRequest;
import com.tsymbals.universitycms.models.dto.utils.DtoSortingCollections;
import com.tsymbals.universitycms.models.entities.Course;
import com.tsymbals.universitycms.models.entities.Student;
import com.tsymbals.universitycms.models.entities.Teacher;
import com.tsymbals.universitycms.repositories.CourseRepository;
import com.tsymbals.universitycms.services.CourseService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

    private static final String COURSE_WITH_ID_DOES_NOT_EXIST = "Course with id=%d doesn't exist";
    private static final String COURSE_WITH_NAME_DOES_NOT_EXIST = "Course with name='%s' doesn't exist";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final CourseRepository courseRepository;

    @Override
    public CourseDto save(CreateCourseRequest createCourseRequest) {
        return MODEL_MAPPER.map(courseRepository.save(
                new Course(
                        createCourseRequest.getName(),
                        createCourseRequest.getDescription())),
                CourseDto.class);
    }

    @Override
    public List<CourseDto> getAll() {
        return findAll();
    }

    @Override
    public List<CourseDto> getAllNoneMatching(Course comparisonCourse) {
        if (Objects.isNull(comparisonCourse)) {
            return findAll();
        }

        return courseRepository.findAllByIdNotOrderByIdAsc(comparisonCourse.getId())
                .stream()
                .map(CourseServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<CourseDto> getAllNoneMatching(Set<Course> comparisonCourses) {
        if (comparisonCourses.isEmpty()) {
            return findAll();
        }

        return courseRepository.findAllByIdNotInOrderByIdAsc(comparisonCourses.stream()
                        .map(Course::getId)
                        .collect(Collectors.toSet()))
                .stream()
                .map(CourseServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public CourseDto getById(Long id) {
        return courseRepository.findById(id)
                .map(CourseServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(COURSE_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public CourseDto getByName(String name) {
        return courseRepository.findByName(name)
                .map(CourseServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(COURSE_WITH_NAME_DOES_NOT_EXIST.formatted(name)));
    }

    @Override
    public CourseDto updateName(Long id, UpdateCourseRequest updateCourseRequest) {
        if (!courseRepository.existsById(id)) {
            throw new EntityNotFoundException(COURSE_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        courseRepository.updateName(id, updateCourseRequest.getName());

        return MODEL_MAPPER.map(courseRepository.getReferenceById(id), CourseDto.class);
    }

    @Override
    public CourseDto updateDescription(Long id, UpdateCourseRequest updateCourseRequest) {
        if (!courseRepository.existsById(id)) {
            throw new EntityNotFoundException(COURSE_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        courseRepository.updateDescription(id, updateCourseRequest.getDescription());

        return MODEL_MAPPER.map(courseRepository.getReferenceById(id), CourseDto.class);
    }

    @Override
    public void deleteById(Long id) {
        if (!courseRepository.existsById(id)) {
            throw new EntityNotFoundException(COURSE_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        courseRepository.deleteById(id);
    }

    private static CourseDto mapToDto(Course course) {
        CourseDto courseDto = MODEL_MAPPER.map(course, CourseDto.class);
        courseDto.setTeachers(DtoSortingCollections.sort(courseDto.getTeachers(), Comparator.comparing(Teacher::getId)));
        courseDto.setStudents(DtoSortingCollections.sort(courseDto.getStudents(), Comparator.comparing(Student::getId)));

        return courseDto;
    }

    private List<CourseDto> findAll() {
        return courseRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(CourseServiceImpl::mapToDto)
                .toList();
    }
}
