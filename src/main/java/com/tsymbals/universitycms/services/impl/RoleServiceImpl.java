package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.RoleDto;
import com.tsymbals.universitycms.models.dto.requests.CreateRoleRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateRoleRequest;
import com.tsymbals.universitycms.models.entities.Role;
import com.tsymbals.universitycms.repositories.RoleRepository;
import com.tsymbals.universitycms.services.RoleService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private static final String ROLE_WITH_ID_DOES_NOT_EXIST = "Role with id=%d doesn't exist";
    private static final String ROLE_WITH_NAME_DOES_NOT_EXIST = "Role with name='%s' doesn't exist";
    private static final Integer NUMBER_OF_PREFIX_CHARACTERS_IN_ROLE_NAME = 5;
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final RoleRepository roleRepository;

    @Override
    public RoleDto save(CreateRoleRequest createRoleRequest) {
        return MODEL_MAPPER.map(roleRepository.save(
                new Role(
                        createRoleRequest.getName())),
                RoleDto.class);
    }

    @Override
    public List<RoleDto> getAll() {
        return findAll();
    }

    @Override
    public List<RoleDto> getAllNoneMatching(Set<Role> comparisonRoles) {
        if (comparisonRoles.isEmpty()) {
            return findAll();
        }

        return roleRepository.findAllByIdNotInOrderByIdAsc(comparisonRoles.stream()
                        .map(Role::getId)
                        .collect(Collectors.toSet()))
                .stream()
                .map(role -> MODEL_MAPPER.map(role, RoleDto.class))
                .toList();
    }

    @Override
    public RoleDto getById(Long id) {
        return roleRepository.findById(id)
                .map(role -> MODEL_MAPPER.map(role, RoleDto.class))
                .orElseThrow(() -> new EntityNotFoundException(ROLE_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public RoleDto getByName(String name) {
        return roleRepository.findByName(name)
                .map(role -> MODEL_MAPPER.map(role, RoleDto.class))
                .orElseThrow(() -> new EntityNotFoundException(ROLE_WITH_NAME_DOES_NOT_EXIST.formatted(name.substring(NUMBER_OF_PREFIX_CHARACTERS_IN_ROLE_NAME))));
    }

    @Override
    public RoleDto updateName(Long id, UpdateRoleRequest updateRoleRequest) {
        if (!roleRepository.existsById(id)) {
            throw new EntityNotFoundException(ROLE_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        roleRepository.updateName(id, updateRoleRequest.getName());

        return MODEL_MAPPER.map(roleRepository.getReferenceById(id), RoleDto.class);
    }

    @Override
    public void deleteById(Long id) {
        if (!roleRepository.existsById(id)) {
            throw new EntityNotFoundException(ROLE_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        roleRepository.deleteById(id);
    }

    private List<RoleDto> findAll() {
        return roleRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(role -> MODEL_MAPPER.map(role, RoleDto.class))
                .toList();
    }
}
