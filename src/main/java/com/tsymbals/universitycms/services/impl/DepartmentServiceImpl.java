package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.DepartmentDto;
import com.tsymbals.universitycms.models.dto.requests.CreateDepartmentRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateDepartmentRequest;
import com.tsymbals.universitycms.models.dto.utils.DtoSortingCollections;
import com.tsymbals.universitycms.models.entities.Auditorium;
import com.tsymbals.universitycms.models.entities.Department;
import com.tsymbals.universitycms.repositories.DepartmentRepository;
import com.tsymbals.universitycms.repositories.UniversityRepository;
import com.tsymbals.universitycms.services.DepartmentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

    private static final String UNIVERSITY_WITH_ABBREVIATION_NAME_DOES_NOT_EXIST = "University with abbreviationName='%s' doesn't exist";
    private static final String DEPARTMENT_WITH_ID_DOES_NOT_EXIST = "Department with id=%d doesn't exist";
    private static final String DEPARTMENT_WITH_NAME_DOES_NOT_EXIST = "Department with name='%s' doesn't exist";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final DepartmentRepository departmentRepository;
    private final UniversityRepository universityRepository;

    @Override
    public DepartmentDto save(CreateDepartmentRequest createDepartmentRequest) {
        return MODEL_MAPPER.map(departmentRepository.save(
                new Department(
                        universityRepository.findByAbbreviationName(createDepartmentRequest.getUniversityAbbreviationName())
                                .orElseThrow(() -> new EntityNotFoundException(UNIVERSITY_WITH_ABBREVIATION_NAME_DOES_NOT_EXIST.formatted(createDepartmentRequest.getUniversityAbbreviationName()))),
                        createDepartmentRequest.getName())),
                DepartmentDto.class);
    }

    @Override
    public List<DepartmentDto> getAll() {
        return departmentRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(DepartmentServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<DepartmentDto> getAllNoneMatching(Department comparisonDepartment) {
        return departmentRepository.findAllByIdNotOrderByIdAsc(comparisonDepartment.getId())
                .stream()
                .map(DepartmentServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public DepartmentDto getById(Long id) {
        return departmentRepository.findById(id)
                .map(DepartmentServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(DEPARTMENT_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public DepartmentDto getByName(String name) {
        return departmentRepository.findByName(name)
                .map(DepartmentServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(DEPARTMENT_WITH_NAME_DOES_NOT_EXIST.formatted(name)));
    }

    @Override
    public DepartmentDto updateUniversity(Long id, UpdateDepartmentRequest updateDepartmentRequest) {
        departmentRepository.findById(id)
                .ifPresentOrElse(department -> universityRepository.findByAbbreviationName(updateDepartmentRequest.getUniversityAbbreviationName())
                        .ifPresentOrElse(university -> {
                            department.setUniversity(university);
                            departmentRepository.save(department);
                        }, () -> {
                            throw new EntityNotFoundException(UNIVERSITY_WITH_ABBREVIATION_NAME_DOES_NOT_EXIST.formatted(updateDepartmentRequest.getUniversityAbbreviationName()));
                }), () -> {
                    throw new EntityNotFoundException(DEPARTMENT_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(departmentRepository.getReferenceById(id), DepartmentDto.class);
    }

    @Override
    public DepartmentDto updateName(Long id, UpdateDepartmentRequest updateDepartmentRequest) {
        if (!departmentRepository.existsById(id)) {
            throw new EntityNotFoundException(DEPARTMENT_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        departmentRepository.updateName(id, updateDepartmentRequest.getName());

        return MODEL_MAPPER.map(departmentRepository.getReferenceById(id), DepartmentDto.class);
    }

    @Override
    public void deleteById(Long id) {
        if (!departmentRepository.existsById(id)) {
            throw new EntityNotFoundException(DEPARTMENT_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        departmentRepository.deleteById(id);
    }

    private static DepartmentDto mapToDto(Department department) {
        DepartmentDto departmentDto = MODEL_MAPPER.map(department, DepartmentDto.class);
        departmentDto.setAuditoriums(DtoSortingCollections.sort(departmentDto.getAuditoriums(), Comparator.comparing(Auditorium::getId)));

        return departmentDto;
    }
}
