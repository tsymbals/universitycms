package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.TeacherDto;
import com.tsymbals.universitycms.models.dto.requests.CreateTeacherRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateTeacherRequest;
import com.tsymbals.universitycms.models.dto.utils.DtoSortingCollections;
import com.tsymbals.universitycms.models.entities.Course;
import com.tsymbals.universitycms.models.entities.Group;
import com.tsymbals.universitycms.models.entities.Teacher;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.repositories.CourseRepository;
import com.tsymbals.universitycms.repositories.GroupRepository;
import com.tsymbals.universitycms.repositories.TeacherRepository;
import com.tsymbals.universitycms.repositories.UserRepository;
import com.tsymbals.universitycms.services.TeacherService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {

    private static final String USER_WITH_USERNAME_DOES_NOT_EXIST = "User with username='%s' doesn't exist";
    private static final String TEACHER_WITH_ID_DOES_NOT_EXIST = "Teacher with id=%d doesn't exist";
    private static final String TEACHER_WITH_USERNAME_DOES_NOT_EXIST = "Teacher with username='%s' doesn't exist";
    private static final String TEACHER_WITH_FIRST_NAME_AND_LAST_NAME_DOES_NOT_EXIST = "Teacher with firstName='%s' and lastName='%s' doesn't exist";
    private static final String TEACHER_ALREADY_HAS_COURSE = "Teacher with id=%d already has course with name='%s'";
    private static final String COURSE_WITH_NAME_DOES_NOT_EXIST = "Course with name='%s' doesn't exist";
    private static final String TEACHER_DOES_NOT_HAVE_COURSE = "Teacher with id=%d doesn't have course with name='%s'";
    private static final String TEACHER_ALREADY_HAS_GROUP = "Teacher with id=%d already has group with name='%s'";
    private static final String GROUP_WITH_NAME_DOES_NOT_EXIST = "Group with name='%s' doesn't exist";
    private static final String TEACHER_DOES_NOT_HAVE_GROUP = "Teacher with id=%d doesn't have group with name='%s'";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final UserRepository userRepository;
    private final TeacherRepository teacherRepository;
    private final CourseRepository courseRepository;
    private final GroupRepository groupRepository;

    @Override
    public TeacherDto save(CreateTeacherRequest createTeacherRequest) {
        UserEntity user = userRepository.findByUsername(createTeacherRequest.getUsername())
                .orElseThrow(() -> new EntityNotFoundException(USER_WITH_USERNAME_DOES_NOT_EXIST.formatted(createTeacherRequest.getUsername())));
        userRepository.updateFreeStatus(user.getId(), Boolean.FALSE);

        return MODEL_MAPPER.map(teacherRepository.save(
                new Teacher(
                        user,
                        createTeacherRequest.getFirstName(),
                        createTeacherRequest.getLastName())),
                TeacherDto.class);
    }

    @Override
    public List<TeacherDto> getAll() {
        return findAll();
    }

    @Override
    public List<TeacherDto> getAllNoneMatching(Teacher comparisonTeacher) {
        if (Objects.isNull(comparisonTeacher)) {
            return findAll();
        }

        return teacherRepository.findAllByIdNotOrderByIdAsc(comparisonTeacher.getId())
                .stream()
                .map(TeacherServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public TeacherDto getById(Long id) {
        return teacherRepository.findById(id)
                .map(TeacherServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(TEACHER_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public TeacherDto getByUsername(String username) {
        return teacherRepository.findByUser_Username(username)
                .map(TeacherServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(TEACHER_WITH_USERNAME_DOES_NOT_EXIST.formatted(username)));
    }

    @Override
    public TeacherDto getByFirstNameAndLastName(String firstName, String lastName) {
        return teacherRepository.findByFirstNameAndLastName(firstName, lastName)
                .map(TeacherServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(TEACHER_WITH_FIRST_NAME_AND_LAST_NAME_DOES_NOT_EXIST.formatted(firstName, lastName)));
    }

    @Override
    public TeacherDto updateUser(Long id, UpdateTeacherRequest updateTeacherRequest) {
        teacherRepository.findById(id)
                .ifPresentOrElse(teacher -> userRepository.findByUsername(updateTeacherRequest.getUsername())
                        .ifPresentOrElse(user -> {
                            userRepository.updateFreeStatus(teacher.getUser().getId(), Boolean.TRUE);
                            userRepository.updateFreeStatus(user.getId(), Boolean.FALSE);
                            teacher.setUser(user);
                            teacherRepository.save(teacher);
                        }, () -> {
                            throw new EntityNotFoundException(USER_WITH_USERNAME_DOES_NOT_EXIST.formatted(updateTeacherRequest.getUsername()));
                }), () -> {
                    throw new EntityNotFoundException(TEACHER_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(teacherRepository.getReferenceById(id), TeacherDto.class);
    }

    @Override
    public TeacherDto updateLastName(Long id, UpdateTeacherRequest updateTeacherRequest) {
        if (!teacherRepository.existsById(id)) {
            throw new EntityNotFoundException(TEACHER_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        teacherRepository.updateLastName(id, updateTeacherRequest.getLastName());

        return MODEL_MAPPER.map(teacherRepository.getReferenceById(id), TeacherDto.class);
    }

    @Override
    public TeacherDto addCourse(Long id, UpdateTeacherRequest updateTeacherRequest) {
        teacherRepository.findById(id)
                .ifPresentOrElse(teacher -> courseRepository.findByName(updateTeacherRequest.getCourseNameToAdd())
                        .ifPresentOrElse(course -> {
                            if (teacher.getCourses().contains(course)) {
                                throw new IllegalArgumentException(TEACHER_ALREADY_HAS_COURSE.formatted(teacher.getId(), course.getName()));
                            } else {
                                teacher.getCourses().add(course);
                                teacherRepository.save(teacher);
                            }
                        }, () -> {
                            throw new EntityNotFoundException(COURSE_WITH_NAME_DOES_NOT_EXIST.formatted(updateTeacherRequest.getCourseNameToAdd()));
                }), () -> {
                    throw new EntityNotFoundException(TEACHER_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(teacherRepository.getReferenceById(id), TeacherDto.class);
    }

    @Override
    public TeacherDto removeCourse(Long id, UpdateTeacherRequest updateTeacherRequest) {
        teacherRepository.findById(id)
                .ifPresentOrElse(teacher -> courseRepository.findByName(updateTeacherRequest.getCourseNameToRemove())
                        .ifPresentOrElse(course -> {
                            if (!teacher.getCourses().contains(course)) {
                                throw new IllegalArgumentException(TEACHER_DOES_NOT_HAVE_COURSE.formatted(teacher.getId(), course.getName()));
                            } else {
                                teacher.getCourses().remove(course);
                                teacherRepository.save(teacher);
                            }
                        }, () -> {
                            throw new EntityNotFoundException(COURSE_WITH_NAME_DOES_NOT_EXIST.formatted(updateTeacherRequest.getCourseNameToRemove()));
                }), () -> {
                    throw new EntityNotFoundException(TEACHER_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(teacherRepository.getReferenceById(id), TeacherDto.class);
    }

    @Override
    public TeacherDto addGroup(Long id, UpdateTeacherRequest updateTeacherRequest) {
        teacherRepository.findById(id)
                .ifPresentOrElse(teacher -> groupRepository.findByName(updateTeacherRequest.getGroupNameToAdd())
                        .ifPresentOrElse(group -> {
                            if (teacher.getGroups().contains(group)) {
                                throw new IllegalArgumentException(TEACHER_ALREADY_HAS_GROUP.formatted(teacher.getId(), group.getName()));
                            } else {
                                teacher.getGroups().add(group);
                                teacherRepository.save(teacher);
                            }
                        }, () -> {
                            throw new EntityNotFoundException(GROUP_WITH_NAME_DOES_NOT_EXIST.formatted(updateTeacherRequest.getGroupNameToAdd()));
                }), () -> {
                    throw new EntityNotFoundException(TEACHER_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(teacherRepository.getReferenceById(id), TeacherDto.class);
    }

    @Override
    public TeacherDto removeGroup(Long id, UpdateTeacherRequest updateTeacherRequest) {
        teacherRepository.findById(id)
                .ifPresentOrElse(teacher -> groupRepository.findByName(updateTeacherRequest.getGroupNameToRemove())
                        .ifPresentOrElse(group -> {
                            if (!teacher.getGroups().contains(group)) {
                                throw new IllegalArgumentException(TEACHER_DOES_NOT_HAVE_GROUP.formatted(teacher.getId(), group.getName()));
                            } else {
                                teacher.getGroups().remove(group);
                                teacherRepository.save(teacher);
                            }
                        }, () -> {
                            throw new EntityNotFoundException(GROUP_WITH_NAME_DOES_NOT_EXIST.formatted(updateTeacherRequest.getGroupNameToRemove()));
                }), () -> {
                    throw new EntityNotFoundException(TEACHER_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(teacherRepository.getReferenceById(id), TeacherDto.class);
    }

    @Override
    public void deleteById(Long id) {
        teacherRepository.findById(id)
                .ifPresentOrElse(teacher -> {
                    userRepository.updateFreeStatus(teacher.getUser().getId(), Boolean.TRUE);
                    teacherRepository.deleteById(id);
                }, () -> {
                    throw new EntityNotFoundException(TEACHER_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });
    }

    private static TeacherDto mapToDto(Teacher teacher) {
        TeacherDto teacherDto = MODEL_MAPPER.map(teacher, TeacherDto.class);
        teacherDto.setCourses(DtoSortingCollections.sort(teacherDto.getCourses(), Comparator.comparing(Course::getId)));
        teacherDto.setGroups(DtoSortingCollections.sort(teacherDto.getGroups(), Comparator.comparing(Group::getId)));

        return teacherDto;
    }

    private List<TeacherDto> findAll() {
        return teacherRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(TeacherServiceImpl::mapToDto)
                .toList();
    }
}
