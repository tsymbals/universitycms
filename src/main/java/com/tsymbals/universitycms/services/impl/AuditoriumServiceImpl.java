package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.AuditoriumDto;
import com.tsymbals.universitycms.models.dto.LessonDto;
import com.tsymbals.universitycms.models.dto.requests.CreateAuditoriumRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateAuditoriumRequest;
import com.tsymbals.universitycms.models.dto.utils.DtoSortingCollections;
import com.tsymbals.universitycms.models.entities.Auditorium;
import com.tsymbals.universitycms.models.entities.Lesson;
import com.tsymbals.universitycms.repositories.AuditoriumRepository;
import com.tsymbals.universitycms.repositories.DepartmentRepository;
import com.tsymbals.universitycms.services.AuditoriumService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuditoriumServiceImpl implements AuditoriumService {

    private static final String DEPARTMENT_WITH_NAME_DOES_NOT_EXIST = "Department with name='%s' doesn't exist";
    private static final String AUDITORIUM_WITH_ID_DOES_NOT_EXIST = "Auditorium with id=%d doesn't exist";
    private static final String AUDITORIUM_WITH_NAME_DOES_NOT_EXIST = "Auditorium with name='%s' doesn't exist";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final AuditoriumRepository auditoriumRepository;
    private final DepartmentRepository departmentRepository;

    @Override
    public AuditoriumDto save(CreateAuditoriumRequest createAuditoriumRequest) {
        return MODEL_MAPPER.map(auditoriumRepository.save(
                new Auditorium(
                        departmentRepository.findByName(createAuditoriumRequest.getDepartmentName())
                                .orElseThrow(() -> new EntityNotFoundException(DEPARTMENT_WITH_NAME_DOES_NOT_EXIST.formatted(createAuditoriumRequest.getDepartmentName()))),
                        createAuditoriumRequest.getName())),
                AuditoriumDto.class);
    }

    @Override
    public List<AuditoriumDto> getAll() {
        return auditoriumRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(AuditoriumServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<AuditoriumDto> getAllNoneMatching(Auditorium comparisonAuditorium) {
        return auditoriumRepository.findAllByIdNotOrderByIdAsc(comparisonAuditorium.getId())
                .stream()
                .map(AuditoriumServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<AuditoriumDto> getAllByDate(List<LessonDto> lessonDtoList) {
        return auditoriumRepository.findAllByLessonsIsNotEmptyOrderByIdAsc()
                .stream()
                .map(auditorium -> mapToDto(auditorium, lessonDtoList))
                .toList();
    }

    @Override
    public AuditoriumDto getById(Long id) {
        return auditoriumRepository.findById(id)
                .map(AuditoriumServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(AUDITORIUM_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public AuditoriumDto getByName(String name) {
        return auditoriumRepository.findByName(name)
                .map(AuditoriumServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(AUDITORIUM_WITH_NAME_DOES_NOT_EXIST.formatted(name)));
    }

    @Override
    public AuditoriumDto updateDepartment(Long id, UpdateAuditoriumRequest updateAuditoriumRequest) {
        auditoriumRepository.findById(id)
                .ifPresentOrElse(auditorium -> departmentRepository.findByName(updateAuditoriumRequest.getDepartmentName())
                        .ifPresentOrElse(department -> {
                            auditorium.setDepartment(department);
                            auditoriumRepository.save(auditorium);
                        }, () -> {
                            throw new EntityNotFoundException(DEPARTMENT_WITH_NAME_DOES_NOT_EXIST.formatted(updateAuditoriumRequest.getDepartmentName()));
                }), () -> {
                    throw new EntityNotFoundException(AUDITORIUM_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(auditoriumRepository.getReferenceById(id), AuditoriumDto.class);
    }

    @Override
    public AuditoriumDto updateName(Long id, UpdateAuditoriumRequest updateAuditoriumRequest) {
        if (!auditoriumRepository.existsById(id)) {
            throw new EntityNotFoundException(AUDITORIUM_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        auditoriumRepository.updateName(id, updateAuditoriumRequest.getName());

        return MODEL_MAPPER.map(auditoriumRepository.getReferenceById(id), AuditoriumDto.class);
    }

    @Override
    public void deleteById(Long id) {
        if (!auditoriumRepository.existsById(id)) {
            throw new EntityNotFoundException(AUDITORIUM_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        auditoriumRepository.deleteById(id);
    }

    private static AuditoriumDto mapToDto(Auditorium auditorium) {
        AuditoriumDto auditoriumDto = MODEL_MAPPER.map(auditorium, AuditoriumDto.class);
        auditoriumDto.setLessons(DtoSortingCollections.sort(auditoriumDto.getLessons(), Comparator.comparing(Lesson::getId)));

        return auditoriumDto;
    }

    private static AuditoriumDto mapToDto(Auditorium auditorium, List<LessonDto> lessonDtoList) {
        auditorium.setLessons(lessonDtoList
                .stream()
                .filter(lessonDto -> lessonDto.getAuditorium().getId().equals(auditorium.getId()))
                .map(lessonDto -> MODEL_MAPPER.map(lessonDto, Lesson.class))
                .collect(Collectors.toSet())
        );

        return mapToDto(auditorium);
    }
}
