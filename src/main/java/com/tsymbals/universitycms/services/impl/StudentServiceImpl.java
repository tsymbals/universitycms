package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.StudentDto;
import com.tsymbals.universitycms.models.dto.requests.CreateStudentRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateStudentRequest;
import com.tsymbals.universitycms.models.dto.utils.DtoSortingCollections;
import com.tsymbals.universitycms.models.entities.Course;
import com.tsymbals.universitycms.models.entities.Student;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.repositories.CourseRepository;
import com.tsymbals.universitycms.repositories.GroupRepository;
import com.tsymbals.universitycms.repositories.StudentRepository;
import com.tsymbals.universitycms.repositories.UserRepository;
import com.tsymbals.universitycms.services.StudentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private static final String USER_WITH_USERNAME_DOES_NOT_EXIST = "User with username='%s' doesn't exist";
    private static final String STUDENT_WITH_ID_DOES_NOT_EXIST = "Student with id=%d doesn't exist";
    private static final String STUDENT_WITH_USERNAME_DOES_NOT_EXIST = "Student with username='%s' doesn't exist";
    private static final String STUDENT_WITH_FIRST_NAME_AND_LAST_NAME_DOES_NOT_EXIST = "Student with firstName='%s' and lastName='%s' doesn't exist";
    private static final String GROUP_WITH_NAME_DOES_NOT_EXIST = "Group with name='%s' doesn't exist";
    private static final String STUDENT_DOES_NOT_HAVE_ANY_GROUP = "Student with id=%d doesn't have any group";
    private static final String STUDENT_DOES_NOT_HAVE_GROUP = "Student with id=%d doesn't have group with name='%s'";
    private static final String STUDENT_ALREADY_HAS_COURSE = "Student with id=%d already has course with name='%s'";
    private static final String COURSE_WITH_NAME_DOES_NOT_EXIST = "Course with name='%s' doesn't exist";
    private static final String STUDENT_DOES_NOT_HAVE_COURSE = "Student with id=%d doesn't have course with name='%s'";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final UserRepository userRepository;
    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;
    private final CourseRepository courseRepository;

    @Override
    public StudentDto save(CreateStudentRequest createStudentRequest) {
        UserEntity user = userRepository.findByUsername(createStudentRequest.getUsername())
                .orElseThrow(() -> new EntityNotFoundException(USER_WITH_USERNAME_DOES_NOT_EXIST.formatted(createStudentRequest.getUsername())));
        userRepository.updateFreeStatus(user.getId(), Boolean.FALSE);

        return MODEL_MAPPER.map(studentRepository.save(
                new Student(
                        user,
                        createStudentRequest.getFirstName(),
                        createStudentRequest.getLastName())),
                StudentDto.class);
    }

    @Override
    public List<StudentDto> getAll() {
        return studentRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(StudentServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public StudentDto getById(Long id) {
        return studentRepository.findById(id)
                .map(StudentServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(STUDENT_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public StudentDto getByUsername(String username) {
        return studentRepository.findByUser_Username(username)
                .map(StudentServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(STUDENT_WITH_USERNAME_DOES_NOT_EXIST.formatted(username)));
    }

    @Override
    public StudentDto getByFirstNameAndLastName(String firstName, String lastName) {
        return studentRepository.findByFirstNameAndLastName(firstName, lastName)
                .map(StudentServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(STUDENT_WITH_FIRST_NAME_AND_LAST_NAME_DOES_NOT_EXIST.formatted(firstName, lastName)));
    }

    @Override
    public StudentDto updateUser(Long id, UpdateStudentRequest updateStudentRequest) {
        studentRepository.findById(id)
                .ifPresentOrElse(student -> userRepository.findByUsername(updateStudentRequest.getUsername())
                        .ifPresentOrElse(user -> {
                            userRepository.updateFreeStatus(student.getUser().getId(), Boolean.TRUE);
                            userRepository.updateFreeStatus(user.getId(), Boolean.FALSE);
                            student.setUser(user);
                            studentRepository.save(student);
                        }, () -> {
                            throw new EntityNotFoundException(USER_WITH_USERNAME_DOES_NOT_EXIST.formatted(updateStudentRequest.getUsername()));
                }), () -> {
                    throw new EntityNotFoundException(STUDENT_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(studentRepository.getReferenceById(id), StudentDto.class);
    }

    @Override
    public StudentDto updateGroup(Long id, UpdateStudentRequest updateStudentRequest) {
        studentRepository.findById(id)
                .ifPresentOrElse(student -> groupRepository.findByName(updateStudentRequest.getGroupNameToUpdate())
                        .ifPresentOrElse(group -> {
                            student.setGroup(group);
                            studentRepository.save(student);
                        }, () -> {
                            throw new EntityNotFoundException(GROUP_WITH_NAME_DOES_NOT_EXIST.formatted(updateStudentRequest.getGroupNameToUpdate()));
                }), () -> {
                    throw new EntityNotFoundException(STUDENT_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(studentRepository.getReferenceById(id), StudentDto.class);
    }

    @Override
    public StudentDto removeGroup(Long id, UpdateStudentRequest updateStudentRequest) {
        studentRepository.findById(id)
                .ifPresentOrElse(student -> {
                    if (Objects.isNull(student.getGroup())) {
                        throw new IllegalArgumentException(STUDENT_DOES_NOT_HAVE_ANY_GROUP.formatted(student.getId()));
                    } else {
                        groupRepository.findByName(updateStudentRequest.getGroupNameToRemove())
                                .ifPresentOrElse(group -> {
                                    if (!student.getGroup().getName().equals(group.getName())) {
                                        throw new IllegalArgumentException(STUDENT_DOES_NOT_HAVE_GROUP.formatted(student.getId(), group.getName()));
                                    } else {
                                        student.setGroup(null);
                                        studentRepository.save(student);
                                    }
                                }, () -> {
                                    throw new EntityNotFoundException(GROUP_WITH_NAME_DOES_NOT_EXIST.formatted(updateStudentRequest.getGroupNameToRemove()));
                                });
                    }
                }, () -> {
                    throw new EntityNotFoundException(STUDENT_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(studentRepository.getReferenceById(id), StudentDto.class);
    }

    @Override
    public StudentDto updateLastName(Long id, UpdateStudentRequest updateStudentRequest) {
        if (!studentRepository.existsById(id)) {
            throw new EntityNotFoundException(STUDENT_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        studentRepository.updateLastName(id, updateStudentRequest.getLastName());

        return MODEL_MAPPER.map(studentRepository.getReferenceById(id), StudentDto.class);
    }

    @Override
    public StudentDto addCourse(Long id, UpdateStudentRequest updateStudentRequest) {
        studentRepository.findById(id)
                .ifPresentOrElse(student -> courseRepository.findByName(updateStudentRequest.getCourseNameToAdd())
                        .ifPresentOrElse(course -> {
                            if (student.getCourses().contains(course)) {
                                throw new IllegalArgumentException(STUDENT_ALREADY_HAS_COURSE.formatted(student.getId(), course.getName()));
                            } else {
                                student.getCourses().add(course);
                                studentRepository.save(student);
                            }
                        }, () -> {
                            throw new EntityNotFoundException(COURSE_WITH_NAME_DOES_NOT_EXIST.formatted(updateStudentRequest.getCourseNameToAdd()));
                }), () -> {
                    throw new EntityNotFoundException(STUDENT_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(studentRepository.getReferenceById(id), StudentDto.class);
    }

    @Override
    public StudentDto removeCourse(Long id, UpdateStudentRequest updateStudentRequest) {
        studentRepository.findById(id)
                .ifPresentOrElse(student -> courseRepository.findByName(updateStudentRequest.getCourseNameToRemove())
                        .ifPresentOrElse(course -> {
                            if (!student.getCourses().contains(course)) {
                                throw new IllegalArgumentException(STUDENT_DOES_NOT_HAVE_COURSE.formatted(student.getId(), course.getName()));
                            } else {
                                student.getCourses().remove(course);
                                studentRepository.save(student);
                            }
                        }, () -> {
                            throw new EntityNotFoundException(COURSE_WITH_NAME_DOES_NOT_EXIST.formatted(updateStudentRequest.getCourseNameToRemove()));
                }), () -> {
                    throw new EntityNotFoundException(STUDENT_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(studentRepository.getReferenceById(id), StudentDto.class);
    }

    @Override
    public void deleteById(Long id) {
        studentRepository.findById(id)
                .ifPresentOrElse(student -> {
                    userRepository.updateFreeStatus(student.getUser().getId(), Boolean.TRUE);
                    studentRepository.deleteById(id);
                }, () -> {
                    throw new EntityNotFoundException(STUDENT_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });
    }

    private static StudentDto mapToDto(Student student) {
        StudentDto studentDto = MODEL_MAPPER.map(student, StudentDto.class);
        studentDto.setCourses(DtoSortingCollections.sort(studentDto.getCourses(), Comparator.comparing(Course::getId)));

        return studentDto;
    }
}
