package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.LessonDto;
import com.tsymbals.universitycms.models.dto.StudentDto;
import com.tsymbals.universitycms.models.dto.TeacherDto;
import com.tsymbals.universitycms.models.dto.requests.CreateLessonRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateLessonRequest;
import com.tsymbals.universitycms.models.dto.utils.DtoSortingCollections;
import com.tsymbals.universitycms.models.entities.Group;
import com.tsymbals.universitycms.models.entities.Lesson;
import com.tsymbals.universitycms.repositories.*;
import com.tsymbals.universitycms.services.LessonService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

@Service
@RequiredArgsConstructor
public class LessonServiceImpl implements LessonService {

    private static final String LESSON_TIME_WITH_ID_DOES_NOT_EXIST = "LessonTime with id=%d doesn't exist";
    private static final String AUDITORIUM_WITH_NAME_DOES_NOT_EXIST = "Auditorium with name='%s' doesn't exist";
    private static final String LESSON_WITH_ID_DOES_NOT_EXIST = "Lesson with id=%d doesn't exist";
    private static final String LESSON_WITH_DATE_AND_TIME_DOES_NOT_EXIST = "Lesson with date=%s, startTime=%s and endTime=%s doesn't exist";
    private static final String COURSE_WITH_NAME_DOES_NOT_EXIST = "Course with name='%s' doesn't exist";
    private static final String LESSON_DOES_NOT_HAVE_ANY_COURSE = "Lesson with id=%d doesn't have any course";
    private static final String LESSON_DOES_NOT_HAVE_COURSE = "Lesson with id=%d doesn't have course with name='%s'";
    private static final String TEACHER_WITH_ID_DOES_NOT_EXIST = "Teacher with id=%d doesn't exist";
    private static final String LESSON_DOES_NOT_HAVE_ANY_TEACHER = "Lesson with id=%d doesn't have any teacher";
    private static final String LESSON_DOES_NOT_HAVE_TEACHER = "Lesson with id=%d doesn't have teacher with id=%d";
    private static final String GROUP_WITH_NAME_DOES_NOT_EXIST = "Group with name='%s' doesn't exist";
    private static final String LESSON_ALREADY_HAS_GROUP = "Lesson with id=%d already has group with name='%s'";
    private static final String LESSON_DOES_NOT_HAVE_GROUP = "Lesson with id=%d doesn't have group with name='%s'";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final LessonRepository lessonRepository;
    private final LessonTimeRepository lessonTimeRepository;
    private final AuditoriumRepository auditoriumRepository;
    private final CourseRepository courseRepository;
    private final TeacherRepository teacherRepository;
    private final GroupRepository groupRepository;

    @Override
    public LessonDto save(CreateLessonRequest createLessonRequest) {
        return MODEL_MAPPER.map(lessonRepository.save(
                new Lesson(
                        createLessonRequest.getDate(),
                        lessonTimeRepository.findById(createLessonRequest.getLessonTimeId())
                                .orElseThrow(() -> new EntityNotFoundException(LESSON_TIME_WITH_ID_DOES_NOT_EXIST.formatted(createLessonRequest.getLessonTimeId()))),
                        auditoriumRepository.findByName(createLessonRequest.getAuditoriumName())
                                .orElseThrow(() -> new EntityNotFoundException(AUDITORIUM_WITH_NAME_DOES_NOT_EXIST.formatted(createLessonRequest.getAuditoriumName()))))),
                LessonDto.class);
    }

    @Override
    public List<LessonDto> getAll() {
        return lessonRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(LessonServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<LessonDto> getAllByDate(LocalDate date) {
        return lessonRepository.findAllByDateOrderByIdAsc(date)
                .stream()
                .map(LessonServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<LessonDto> getAllByDate(LocalDate date, TeacherDto teacherDto) {
        return lessonRepository.findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc(date, teacherDto.getId())
                .stream()
                .map(LessonServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<LessonDto> getAllByDate(LocalDate date, StudentDto studentDto) {
        if (Objects.isNull(studentDto.getGroup())) {
            return List.of();
        }

        return lessonRepository.findAllByDateAndGroups_NameOrderByIdAsc(date, studentDto.getGroup().getName())
                .stream()
                .map(LessonServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<LessonDto> getAllByWeek(LocalDate startWeekDate, LocalDate endWeekDate, TeacherDto teacherDto) {
        return lessonRepository.findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc(startWeekDate, endWeekDate, teacherDto.getId())
                .stream()
                .map(LessonServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<LessonDto> getAllByWeek(LocalDate startWeekDate, LocalDate endWeekDate, StudentDto studentDto) {
        if (Objects.isNull(studentDto.getGroup())) {
            return List.of();
        }

        return lessonRepository.findAllByDateBetweenAndGroups_NameOrderByIdAsc(startWeekDate, endWeekDate, studentDto.getGroup().getName())
                .stream()
                .map(LessonServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public LessonDto getById(Long id) {
        return lessonRepository.findById(id)
                .map(LessonServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public LessonDto getByDateAndStartTimeAndEndTime(LocalDate date, LocalTime startTime, LocalTime endTime) {
        return lessonRepository.findByDateAndLessonTime_StartTimeAndLessonTime_EndTime(date, startTime, endTime)
                .map(LessonServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(LESSON_WITH_DATE_AND_TIME_DOES_NOT_EXIST.formatted(date, startTime, endTime)));
    }

    @Override
    public LessonDto updateDate(Long id, UpdateLessonRequest updateLessonRequest) {
        if (!lessonRepository.existsById(id)) {
            throw new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        lessonRepository.updateDate(id, updateLessonRequest.getDate());

        return MODEL_MAPPER.map(lessonRepository.getReferenceById(id), LessonDto.class);
    }

    @Override
    public LessonDto updateLessonTime(Long id, UpdateLessonRequest updateLessonRequest) {
        lessonRepository.findById(id)
                .ifPresentOrElse(lesson -> lessonTimeRepository.findById(updateLessonRequest.getLessonTimeId())
                        .ifPresentOrElse(lessonTime -> {
                            lesson.setLessonTime(lessonTime);
                            lessonRepository.save(lesson);
                        }, () -> {
                            throw new EntityNotFoundException(LESSON_TIME_WITH_ID_DOES_NOT_EXIST.formatted(updateLessonRequest.getLessonTimeId()));
                }), () -> {
                    throw new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(lessonRepository.getReferenceById(id), LessonDto.class);
    }

    @Override
    public LessonDto updateAuditorium(Long id, UpdateLessonRequest updateLessonRequest) {
        lessonRepository.findById(id)
                .ifPresentOrElse(lesson -> auditoriumRepository.findByName(updateLessonRequest.getAuditoriumName())
                        .ifPresentOrElse(auditorium -> {
                            lesson.setAuditorium(auditorium);
                            lessonRepository.save(lesson);
                        }, () -> {
                            throw new EntityNotFoundException(AUDITORIUM_WITH_NAME_DOES_NOT_EXIST.formatted(updateLessonRequest.getAuditoriumName()));
                }), () -> {
                    throw new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(lessonRepository.getReferenceById(id), LessonDto.class);
    }

    @Override
    public LessonDto updateCourse(Long id, UpdateLessonRequest updateLessonRequest) {
        lessonRepository.findById(id)
                .ifPresentOrElse(lesson -> courseRepository.findByName(updateLessonRequest.getCourseNameToUpdate())
                        .ifPresentOrElse(course -> {
                            lesson.setCourse(course);
                            lessonRepository.save(lesson);
                        }, () -> {
                            throw new EntityNotFoundException(COURSE_WITH_NAME_DOES_NOT_EXIST.formatted(updateLessonRequest.getCourseNameToUpdate()));
                }), () -> {
                    throw new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(lessonRepository.getReferenceById(id), LessonDto.class);
    }

    @Override
    public LessonDto removeCourse(Long id, UpdateLessonRequest updateLessonRequest) {
        lessonRepository.findById(id)
                .ifPresentOrElse(lesson -> {
                    if (Objects.isNull(lesson.getCourse())) {
                        throw new IllegalArgumentException(LESSON_DOES_NOT_HAVE_ANY_COURSE.formatted(lesson.getId()));
                    } else {
                        courseRepository.findByName(updateLessonRequest.getCourseNameToRemove())
                                .ifPresentOrElse(course -> {
                                    if (!lesson.getCourse().getName().equals(course.getName())) {
                                        throw new IllegalArgumentException(LESSON_DOES_NOT_HAVE_COURSE.formatted(lesson.getId(), course.getName()));
                                    } else {
                                        lesson.setCourse(null);
                                        lessonRepository.save(lesson);
                                    }
                                }, () -> {
                                    throw new EntityNotFoundException(COURSE_WITH_NAME_DOES_NOT_EXIST.formatted(updateLessonRequest.getCourseNameToRemove()));
                                });
                    }
                }, () -> {
                    throw new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(lessonRepository.getReferenceById(id), LessonDto.class);
    }

    @Override
    public LessonDto updateTeacher(Long id, UpdateLessonRequest updateLessonRequest) {
        lessonRepository.findById(id)
                .ifPresentOrElse(lesson -> teacherRepository.findById(updateLessonRequest.getTeacherIdToUpdate())
                        .ifPresentOrElse(teacher -> {
                            lesson.setTeacher(teacher);
                            lessonRepository.save(lesson);
                        }, () -> {
                            throw new EntityNotFoundException(TEACHER_WITH_ID_DOES_NOT_EXIST.formatted(updateLessonRequest.getTeacherIdToUpdate()));
                }), () -> {
                    throw new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(lessonRepository.getReferenceById(id), LessonDto.class);
    }

    @Override
    public LessonDto removeTeacher(Long id, UpdateLessonRequest updateLessonRequest) {
        lessonRepository.findById(id)
                .ifPresentOrElse(lesson -> {
                    if (Objects.isNull(lesson.getTeacher())) {
                        throw new IllegalArgumentException(LESSON_DOES_NOT_HAVE_ANY_TEACHER.formatted(lesson.getId()));
                    } else {
                        teacherRepository.findById(updateLessonRequest.getTeacherIdToRemove())
                                .ifPresentOrElse(teacher -> {
                                    if (!lesson.getTeacher().getId().equals(teacher.getId())) {
                                        throw new IllegalArgumentException(LESSON_DOES_NOT_HAVE_TEACHER.formatted(lesson.getId(), teacher.getId()));
                                    } else {
                                        lesson.setTeacher(null);
                                        lessonRepository.save(lesson);
                                    }
                                }, () -> {
                                    throw new EntityNotFoundException(TEACHER_WITH_ID_DOES_NOT_EXIST.formatted(updateLessonRequest.getTeacherIdToRemove()));
                                });
                    }
                }, () -> {
                    throw new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(lessonRepository.getReferenceById(id), LessonDto.class);
    }

    @Override
    public LessonDto addGroup(Long id, UpdateLessonRequest updateLessonRequest) {
        lessonRepository.findById(id)
                .ifPresentOrElse(lesson -> groupRepository.findByName(updateLessonRequest.getGroupNameToAdd())
                        .ifPresentOrElse(group -> {
                            if (lesson.getGroups().contains(group)) {
                                throw new IllegalArgumentException(LESSON_ALREADY_HAS_GROUP.formatted(lesson.getId(), group.getName()));
                            } else {
                                lesson.getGroups().add(group);
                                lessonRepository.save(lesson);
                            }
                        }, () -> {
                            throw new EntityNotFoundException(GROUP_WITH_NAME_DOES_NOT_EXIST.formatted(updateLessonRequest.getGroupNameToAdd()));
                }), () -> {
                    throw new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(lessonRepository.getReferenceById(id), LessonDto.class);
    }

    @Override
    public LessonDto removeGroup(Long id, UpdateLessonRequest updateLessonRequest) {
        lessonRepository.findById(id)
                .ifPresentOrElse(lesson -> groupRepository.findByName(updateLessonRequest.getGroupNameToRemove())
                        .ifPresentOrElse(group -> {
                            if (!lesson.getGroups().contains(group)) {
                                throw new IllegalArgumentException(LESSON_DOES_NOT_HAVE_GROUP.formatted(lesson.getId(), group.getName()));
                            } else {
                                lesson.getGroups().remove(group);
                                lessonRepository.save(lesson);
                            }
                        }, () -> {
                            throw new EntityNotFoundException(GROUP_WITH_NAME_DOES_NOT_EXIST.formatted(updateLessonRequest.getGroupNameToRemove()));
                }), () -> {
                    throw new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(lessonRepository.getReferenceById(id), LessonDto.class);
    }

    @Override
    public void deleteById(Long id) {
        if (!lessonRepository.existsById(id)) {
            throw new EntityNotFoundException(LESSON_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        lessonRepository.deleteById(id);
    }

    private static LessonDto mapToDto(Lesson lesson) {
        LessonDto lessonDto = MODEL_MAPPER.map(lesson, LessonDto.class);
        lessonDto.setGroups(DtoSortingCollections.sort(lessonDto.getGroups(), Comparator.comparing(Group::getId)));

        return lessonDto;
    }
}
