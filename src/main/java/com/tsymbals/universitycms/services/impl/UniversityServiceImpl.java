package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.UniversityDto;
import com.tsymbals.universitycms.models.dto.requests.CreateUniversityRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateUniversityRequest;
import com.tsymbals.universitycms.models.dto.utils.DtoSortingCollections;
import com.tsymbals.universitycms.models.entities.Department;
import com.tsymbals.universitycms.models.entities.University;
import com.tsymbals.universitycms.repositories.UniversityRepository;
import com.tsymbals.universitycms.services.UniversityService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UniversityServiceImpl implements UniversityService {

    private static final String UNIVERSITY_WITH_ID_DOES_NOT_EXIST = "University with id=%d doesn't exist";
    private static final String UNIVERSITY_WITH_ABBREVIATION_NAME_DOES_NOT_EXIST = "University with abbreviationName='%s' doesn't exist";
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final UniversityRepository universityRepository;

    @Override
    public UniversityDto save(CreateUniversityRequest createUniversityRequest) {
        return MODEL_MAPPER.map(universityRepository.save(
                new University(
                        createUniversityRequest.getName(),
                        createUniversityRequest.getAbbreviationName(),
                        createUniversityRequest.getCity(),
                        createUniversityRequest.getPhoneNumber())),
                UniversityDto.class);
    }

    @Override
    public List<UniversityDto> getAll() {
        return universityRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(UniversityServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<UniversityDto> getAllNoneMatching(University comparisonUniversity) {
        return universityRepository.findAllByIdNotOrderByIdAsc(comparisonUniversity.getId())
                .stream()
                .map(UniversityServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public UniversityDto getById(Long id) {
        return universityRepository.findById(id)
                .map(UniversityServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(UNIVERSITY_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public UniversityDto getByAbbreviationName(String abbreviationName) {
        return universityRepository.findByAbbreviationName(abbreviationName)
                .map(UniversityServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(UNIVERSITY_WITH_ABBREVIATION_NAME_DOES_NOT_EXIST.formatted(abbreviationName)));
    }

    @Override
    public UniversityDto updateName(Long id, UpdateUniversityRequest updateUniversityRequest) {
        if (!universityRepository.existsById(id)) {
            throw new EntityNotFoundException(UNIVERSITY_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        universityRepository.updateName(id, updateUniversityRequest.getName());

        return MODEL_MAPPER.map(universityRepository.getReferenceById(id), UniversityDto.class);
    }

    @Override
    public UniversityDto updateAbbreviationName(Long id, UpdateUniversityRequest updateUniversityRequest) {
        if (!universityRepository.existsById(id)) {
            throw new EntityNotFoundException(UNIVERSITY_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        universityRepository.updateAbbreviationName(id, updateUniversityRequest.getAbbreviationName());

        return MODEL_MAPPER.map(universityRepository.getReferenceById(id), UniversityDto.class);
    }

    @Override
    public UniversityDto updateCity(Long id, UpdateUniversityRequest updateUniversityRequest) {
        if (!universityRepository.existsById(id)) {
            throw new EntityNotFoundException(UNIVERSITY_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        universityRepository.updateCity(id, updateUniversityRequest.getCity());

        return MODEL_MAPPER.map(universityRepository.getReferenceById(id), UniversityDto.class);
    }

    @Override
    public UniversityDto updatePhoneNumber(Long id, UpdateUniversityRequest updateUniversityRequest) {
        if (!universityRepository.existsById(id)) {
            throw new EntityNotFoundException(UNIVERSITY_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        universityRepository.updatePhoneNumber(id, updateUniversityRequest.getPhoneNumber());

        return MODEL_MAPPER.map(universityRepository.getReferenceById(id), UniversityDto.class);
    }

    @Override
    public void deleteById(Long id) {
        if (!universityRepository.existsById(id)) {
            throw new EntityNotFoundException(UNIVERSITY_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        universityRepository.deleteById(id);
    }

    private static UniversityDto mapToDto(University university) {
        UniversityDto universityDto = MODEL_MAPPER.map(university, UniversityDto.class);
        universityDto.setDepartments(DtoSortingCollections.sort(universityDto.getDepartments(), Comparator.comparing(Department::getId)));

        return universityDto;
    }
}
