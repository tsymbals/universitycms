package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateUserRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateUserRequest;
import com.tsymbals.universitycms.models.dto.utils.DtoSortingCollections;
import com.tsymbals.universitycms.models.entities.Role;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.repositories.RoleRepository;
import com.tsymbals.universitycms.repositories.UserRepository;
import com.tsymbals.universitycms.services.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private static final String USER_WITH_ID_DOES_NOT_EXIST = "User with id=%d doesn't exist";
    private static final String USER_WITH_USERNAME_DOES_NOT_EXIST = "User with username='%s' doesn't exist";
    private static final String USER_ALREADY_HAS_ROLE = "User with id=%d already has role with name='%s'";
    private static final String ROLE_WITH_NAME_DOES_NOT_EXIST = "Role with name='%s' doesn't exist";
    private static final String USER_DOES_NOT_HAVE_ROLE = "User with id=%d doesn't have role with name='%s'";
    private static final String ADMIN_ROLE_NAME = "role_admin";
    private static final String STAFF_ROLE_NAME = "role_staff";
    private static final String TEACHER_ROLE_NAME = "role_teacher";
    private static final String STUDENT_ROLE_NAME = "role_student";
    private static final Integer NUMBER_OF_PREFIX_CHARACTERS_IN_ROLE_NAME = 5;
    private static final ModelMapper MODEL_MAPPER = new ModelMapper();

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDto save(CreateUserRequest createUserRequest) {
        return MODEL_MAPPER.map(userRepository.save(
                new UserEntity(
                        createUserRequest.getUsername(),
                        bCryptPasswordEncoder.encode(createUserRequest.getPassword()))),
                UserDto.class);
    }

    @Override
    public List<UserDto> getAll() {
        return userRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))
                .stream()
                .map(UserServiceImpl::mapToDto)
                .toList();
    }

    @Override
    public List<UserDto> getAllFreeAdminUsers() {
        return getAllFreeUsers(ADMIN_ROLE_NAME);
    }

    @Override
    public List<UserDto> getAllFreeEmployeeUsers() {
        return getAllFreeUsers(STAFF_ROLE_NAME);
    }

    @Override
    public List<UserDto> getAllFreeTeacherUsers() {
        return getAllFreeUsers(TEACHER_ROLE_NAME);
    }

    @Override
    public List<UserDto> getAllFreeStudentUsers() {
        return getAllFreeUsers(STUDENT_ROLE_NAME);
    }

    @Override
    public UserDto getById(Long id) {
        return userRepository.findById(id)
                .map(UserServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(USER_WITH_ID_DOES_NOT_EXIST.formatted(id)));
    }

    @Override
    public UserDto getByUsername(String username) {
        return userRepository.findByUsername(username)
                .map(UserServiceImpl::mapToDto)
                .orElseThrow(() -> new EntityNotFoundException(USER_WITH_USERNAME_DOES_NOT_EXIST.formatted(username)));
    }

    @Override
    public UserDto updateUsername(Long id, UpdateUserRequest updateUserRequest) {
        if (!userRepository.existsById(id)) {
            throw new EntityNotFoundException(USER_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        userRepository.updateUsername(id, updateUserRequest.getUsername());

        return MODEL_MAPPER.map(userRepository.getReferenceById(id), UserDto.class);
    }

    @Override
    public UserDto updatePassword(Long id, UpdateUserRequest updateUserRequest) {
        if (!userRepository.existsById(id)) {
            throw new EntityNotFoundException(USER_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        userRepository.updatePassword(id, bCryptPasswordEncoder.encode(updateUserRequest.getPassword()));

        return MODEL_MAPPER.map(userRepository.getReferenceById(id), UserDto.class);
    }

    @Override
    public UserDto updateEnabledStatus(Long id, UpdateUserRequest updateUserRequest) {
        if (!userRepository.existsById(id)) {
            throw new EntityNotFoundException(USER_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        userRepository.updateEnabledStatus(id, updateUserRequest.getEnabledStatus());

        return MODEL_MAPPER.map(userRepository.getReferenceById(id), UserDto.class);
    }

    @Override
    public UserDto addRole(Long id, UpdateUserRequest updateUserRequest) {
        userRepository.findById(id)
                .ifPresentOrElse(user -> roleRepository.findByName(updateUserRequest.getRoleNameToAdd())
                        .ifPresentOrElse(role -> {
                            if (user.getRoles().contains(role)) {
                                throw new IllegalArgumentException(USER_ALREADY_HAS_ROLE.formatted(user.getId(), role.getName().substring(NUMBER_OF_PREFIX_CHARACTERS_IN_ROLE_NAME)));
                            } else {
                                user.getRoles().add(role);
                                userRepository.save(user);
                            }
                        }, () -> {
                            throw new EntityNotFoundException(ROLE_WITH_NAME_DOES_NOT_EXIST.formatted(updateUserRequest.getRoleNameToAdd().substring(NUMBER_OF_PREFIX_CHARACTERS_IN_ROLE_NAME)));
                }), () -> {
                    throw new EntityNotFoundException(USER_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(userRepository.getReferenceById(id), UserDto.class);
    }

    @Override
    public UserDto removeRole(Long id, UpdateUserRequest updateUserRequest) {
        userRepository.findById(id)
                .ifPresentOrElse(user -> roleRepository.findByName(updateUserRequest.getRoleNameToRemove())
                        .ifPresentOrElse(role -> {
                            if (!user.getRoles().contains(role)) {
                                throw new IllegalArgumentException(USER_DOES_NOT_HAVE_ROLE.formatted(user.getId(), role.getName().substring(NUMBER_OF_PREFIX_CHARACTERS_IN_ROLE_NAME)));
                            } else {
                                user.getRoles().remove(role);
                                userRepository.save(user);
                            }
                        }, () -> {
                            throw new EntityNotFoundException(ROLE_WITH_NAME_DOES_NOT_EXIST.formatted(updateUserRequest.getRoleNameToRemove().substring(NUMBER_OF_PREFIX_CHARACTERS_IN_ROLE_NAME)));
                }), () -> {
                    throw new EntityNotFoundException(USER_WITH_ID_DOES_NOT_EXIST.formatted(id));
                });

        return MODEL_MAPPER.map(userRepository.getReferenceById(id), UserDto.class);
    }

    @Override
    public void deleteById(Long id) {
        if (!userRepository.existsById(id)) {
            throw new EntityNotFoundException(USER_WITH_ID_DOES_NOT_EXIST.formatted(id));
        }
        userRepository.deleteById(id);
    }

    private static UserDto mapToDto(UserEntity user) {
        UserDto userDto = MODEL_MAPPER.map(user, UserDto.class);
        userDto.setRoles(DtoSortingCollections.sort(userDto.getRoles(), Comparator.comparing(Role::getId)));

        return userDto;
    }

    private List<UserDto> getAllFreeUsers(String roleName) {
        return userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc(roleName)
                .stream()
                .map(UserServiceImpl::mapToDto)
                .toList();
    }
}
