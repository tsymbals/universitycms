package com.tsymbals.universitycms.services;

import com.tsymbals.universitycms.models.dto.DepartmentDto;
import com.tsymbals.universitycms.models.dto.requests.CreateDepartmentRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateDepartmentRequest;
import com.tsymbals.universitycms.models.entities.Department;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface DepartmentService {

    @Transactional
    DepartmentDto save(CreateDepartmentRequest createDepartmentRequest);

    @Transactional(readOnly = true)
    List<DepartmentDto> getAll();

    @Transactional(readOnly = true)
    List<DepartmentDto> getAllNoneMatching(Department comparisonDepartment);

    @Transactional(readOnly = true)
    DepartmentDto getById(Long id);

    @Transactional(readOnly = true)
    DepartmentDto getByName(String name);

    @Transactional
    DepartmentDto updateUniversity(Long id, UpdateDepartmentRequest updateDepartmentRequest);

    @Transactional
    DepartmentDto updateName(Long id, UpdateDepartmentRequest updateDepartmentRequest);

    @Transactional
    void deleteById(Long id);
}
