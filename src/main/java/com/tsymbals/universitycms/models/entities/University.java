package com.tsymbals.universitycms.models.entities;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "universities")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class University {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    @ToString.Include
    private Long id;

    @Column
    @ToString.Include
    private String name;

    @Column(name = "abbreviation_name")
    @ToString.Include
    private String abbreviationName;

    @Column
    @ToString.Include
    private String city;

    @Column(name = "phone_number")
    @ToString.Include
    private String phoneNumber;

    @OneToMany(mappedBy = "university", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Department> departments = new HashSet<>();

    public University(String name, String abbreviationName, String city, String phoneNumber) {
        this.name = name;
        this.abbreviationName = abbreviationName;
        this.city = city;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        University that = (University) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}
