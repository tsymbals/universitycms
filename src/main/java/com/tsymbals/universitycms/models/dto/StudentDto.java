package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.Course;
import com.tsymbals.universitycms.models.entities.Group;
import com.tsymbals.universitycms.models.entities.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class StudentDto implements Serializable {

    private static final String STUDENT_DTO_TEMPLATE = "StudentDto{id=%d, user=%s, group=%s, firstName='%s', lastName='%s', courses=%s}";

    private Long id;
    private UserEntity user;
    private Group group;
    private String firstName;
    private String lastName;
    private Set<Course> courses;

    @Override
    public String toString() {
        return STUDENT_DTO_TEMPLATE.formatted(
                id,
                DtoStringViewer.toString(user),
                DtoStringViewer.toString(group),
                firstName,
                lastName,
                DtoStringViewer.toString(courses, Comparator.comparing(Course::getId), Course::toString));
    }
}
