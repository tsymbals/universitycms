package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class CreateDepartmentRequest implements Serializable {

    @NotEmpty(message = "Ensure the university is selected")
    private String universityAbbreviationName;

    @NotEmpty(message = "The name is required")
    @Size(max = 64, message = "Ensure the name do not exceed 64 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Ensure the name contains only letters and words separated whitespaces")
    private String name;
}
