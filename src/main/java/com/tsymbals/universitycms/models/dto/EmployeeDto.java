package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class EmployeeDto implements Serializable {

    private static final String EMPLOYEE_DTO_TEMPLATE = "EmployeeDto{id=%d, user=%s, firstName='%s', lastName='%s'}";

    private Long id;
    private UserEntity user;
    private String firstName;
    private String lastName;

    @Override
    public String toString() {
        return EMPLOYEE_DTO_TEMPLATE.formatted(
                id,
                DtoStringViewer.toString(user),
                firstName,
                lastName);
    }
}
