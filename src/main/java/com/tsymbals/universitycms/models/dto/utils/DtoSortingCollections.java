package com.tsymbals.universitycms.models.dto.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DtoSortingCollections {

    public static <T> Set<T> sort(Set<T> set, Comparator<T> comparator) {
        if (set.isEmpty()) {
            return set;
        }
        List<T> list = new ArrayList<>(set);
        list.sort(comparator);

        return new LinkedHashSet<>(list);
    }
}
