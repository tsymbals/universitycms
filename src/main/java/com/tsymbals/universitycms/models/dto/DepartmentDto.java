package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.Auditorium;
import com.tsymbals.universitycms.models.entities.University;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class DepartmentDto implements Serializable {

    private static final String DEPARTMENT_DTO_TEMPLATE = "DepartmentDto{id=%d, university=%s, name='%s', auditoriums=%s}";

    private Long id;
    private University university;
    private String name;
    private Set<Auditorium> auditoriums;

    @Override
    public String toString() {
        return DEPARTMENT_DTO_TEMPLATE.formatted(
                id,
                DtoStringViewer.toString(university),
                name,
                DtoStringViewer.toString(auditoriums, Comparator.comparing(Auditorium::getId), Auditorium::toString));
    }
}
