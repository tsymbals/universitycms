package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UserDto implements Serializable {

    private static final String USER_DTO_TEMPLATE = "UserDto{id=%d, username='%s', password='%s', enabledStatus=%s, freeStatus=%s, roles=%s}";

    private Long id;
    private String username;
    private String password;
    private Boolean enabledStatus;
    private Boolean freeStatus;
    private Set<Role> roles;

    @Override
    public String toString() {
        return USER_DTO_TEMPLATE.formatted(
                id,
                username,
                password,
                enabledStatus,
                freeStatus,
                DtoStringViewer.toString(roles, Comparator.comparing(Role::getId), Role::toString));
    }
}
