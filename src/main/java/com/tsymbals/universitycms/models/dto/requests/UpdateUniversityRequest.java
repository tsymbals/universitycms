package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UpdateUniversityRequest implements Serializable {

    @NotEmpty(message = "The new name is required")
    @Size(max = 64, message = "Ensure the new name do not exceed 64 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Ensure the new name contains only letters and words separated whitespaces")
    private String name;

    @NotEmpty(message = "The new abbreviation name is required")
    @Size(max = 8, message = "Ensure the new abbreviation name do not exceed 8 characters")
    @Pattern(regexp = "^[A-Z]+$", message = "Ensure the new abbreviation name contains only capital letters")
    private String abbreviationName;

    @NotEmpty(message = "The new city is required")
    @Size(max = 32, message = "Ensure the new city do not exceed 32 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Ensure the new city contains only letters and words separated whitespaces")
    private String city;

    @NotEmpty(message = "The new phone number is required")
    @Size(min = 15, max = 15, message = "Ensure the new phone number has 15 characters")
    @Pattern(regexp = "^\\+380\\(\\d{2}\\)\\d{7}$", message = "Ensure the new phone number is in the format +380(12)3456789")
    private String phoneNumber;
}
