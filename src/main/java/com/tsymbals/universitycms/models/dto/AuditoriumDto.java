package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.Department;
import com.tsymbals.universitycms.models.entities.Lesson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class AuditoriumDto implements Serializable {

    private static final String AUDITORIUM_DTO_TEMPLATE = "AuditoriumDto{id=%d, department=%s, name='%s', lessons=%s}";

    private Long id;
    private Department department;
    private String name;
    private Set<Lesson> lessons;

    @Override
    public String toString() {
        return AUDITORIUM_DTO_TEMPLATE.formatted(
                id,
                DtoStringViewer.toString(department),
                name,
                DtoStringViewer.toString(lessons, Comparator.comparing(Lesson::getId), Lesson::toString));
    }
}
