package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UpdateRoleRequest implements Serializable {

    @NotEmpty(message = "The new name is required")
    @Size(max = 16, message = "Ensure the new name do not exceed 16 characters")
    @Pattern(regexp = "ROLE_[A-Z0-9_]*", message = "Ensure the new name starts with ROLE_, followed by uppercase letters, numbers, and underscores")
    private String name;
}
