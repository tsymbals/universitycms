package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.Lesson;
import com.tsymbals.universitycms.models.entities.Student;
import com.tsymbals.universitycms.models.entities.Teacher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class GroupDto implements Serializable {

    private static final String GROUP_DTO_TEMPLATE = "GroupDto{id=%d, name='%s', teachers=%s, students=%s, lessons=%s}";

    private Long id;
    private String name;
    private Set<Teacher> teachers;
    private Set<Student> students;
    private Set<Lesson> lessons;

    @Override
    public String toString() {
        return GROUP_DTO_TEMPLATE.formatted(
                id,
                name,
                DtoStringViewer.toString(teachers, Comparator.comparing(Teacher::getId), Teacher::toString),
                DtoStringViewer.toString(students, Comparator.comparing(Student::getId), Student::toString),
                DtoStringViewer.toString(lessons, Comparator.comparing(Lesson::getId), Lesson::toString));
    }
}
