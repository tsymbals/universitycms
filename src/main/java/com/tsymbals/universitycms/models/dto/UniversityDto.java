package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.Department;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UniversityDto implements Serializable {

    private static final String UNIVERSITY_DTO_TEMPLATE = "UniversityDto{id=%d, name='%s', abbreviationName='%s', city='%s', phoneNumber='%s', departments=%s}";

    private Long id;
    private String name;
    private String abbreviationName;
    private String city;
    private String phoneNumber;
    private Set<Department> departments;

    @Override
    public String toString() {
        return UNIVERSITY_DTO_TEMPLATE.formatted(
                id,
                name,
                abbreviationName,
                city,
                phoneNumber,
                DtoStringViewer.toString(departments, Comparator.comparing(Department::getId), Department::toString));
    }
}
