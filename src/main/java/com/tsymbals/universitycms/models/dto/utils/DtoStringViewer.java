package com.tsymbals.universitycms.models.dto.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Comparator;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DtoStringViewer {

    private static final String NULL = "null";
    private static final String SQUARE_BRACKETS = "[]";
    private static final String DELIMITER = ", ";
    private static final int MIN_COUNT_OF_SET = 1;

    public static <T> String toString(T object) {
        if (Objects.isNull(object)) {
            return NULL;
        }

        return object.toString();
    }

    public static <T> String toString(Set<T> set, Comparator<T> comparator, Function<T, String> function) {
        if (set.isEmpty()) {
            return SQUARE_BRACKETS;
        } else if (set.size() == MIN_COUNT_OF_SET) {
            return set.stream()
                    .map(function)
                    .collect(Collectors.joining());
        }

        return set.stream()
                .sorted(comparator)
                .map(function)
                .collect(Collectors.joining(DELIMITER));
    }
}
