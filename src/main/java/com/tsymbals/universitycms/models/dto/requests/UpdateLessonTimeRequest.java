package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UpdateLessonTimeRequest implements Serializable {

    @NotNull(message = "The new start time is required in the format HH:MM")
    private LocalTime startTime;

    @NotNull(message = "The new end time is required in the format HH:MM")
    private LocalTime endTime;
}
