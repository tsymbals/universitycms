package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class CreateUniversityRequest implements Serializable {

    @NotEmpty(message = "The name is required")
    @Size(max = 64, message = "Ensure the name do not exceed 64 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Ensure the name contains only letters and words separated whitespaces")
    private String name;

    @NotEmpty(message = "The abbreviation name is required")
    @Size(max = 8, message = "Ensure the abbreviation name do not exceed 8 characters")
    @Pattern(regexp = "^[A-Z]+$", message = "Ensure the abbreviation name contains only capital letters")
    private String abbreviationName;

    @NotEmpty(message = "The city is required")
    @Size(max = 32, message = "Ensure the city do not exceed 32 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Ensure the city contains only letters and words separated whitespaces")
    private String city;

    @NotEmpty(message = "The phone number is required")
    @Size(min = 15, max = 15, message = "Ensure the phone number has 15 characters")
    @Pattern(regexp = "^\\+380\\(\\d{2}\\)\\d{7}$", message = "Ensure the phone number is in the format +380(12)3456789")
    private String phoneNumber;
}
