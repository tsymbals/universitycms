package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class CreateCourseRequest implements Serializable {

    @NotEmpty(message = "The name is required")
    @Size(max = 128, message = "Ensure the name do not exceed 128 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Ensure the name contains only letters and words separated whitespaces")
    private String name;

    @NotEmpty(message = "The description is required")
    @Size(max = 256, message = "Ensure the description do not exceed 256 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Ensure the description contains only letters and words separated whitespaces")
    private String description;
}
