package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class CreateLessonRequest implements Serializable {

    @NotNull(message = "The date is required in the format YYYY-MM-DD")
    private LocalDate date;

    @NotNull(message = "Ensure the lesson time is selected")
    private Long lessonTimeId;

    @NotEmpty(message = "Ensure the auditorium is selected")
    private String auditoriumName;

}
