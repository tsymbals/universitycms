package com.tsymbals.universitycms.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class RoleDto implements Serializable {

    private static final String ROLE_DTO_TEMPLATE = "RoleDto{id=%d, name='%s'}";

    private Long id;
    private String name;

    @Override
    public String toString() {
        return ROLE_DTO_TEMPLATE.formatted(
                id,
                name);
    }
}
