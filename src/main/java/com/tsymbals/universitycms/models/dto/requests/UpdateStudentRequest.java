package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UpdateStudentRequest implements Serializable {
    private String username;
    private String groupNameToUpdate;
    private String groupNameToRemove;

    @NotEmpty(message = "The new last name is required")
    @Size(max = 32, message = "Ensure the new last name do not exceed 32 characters")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Ensure the new last name contains only letters")
    private String lastName;

    private String courseNameToAdd;
    private String courseNameToRemove;

    public UpdateStudentRequest(String lastName) {
        this.lastName = lastName;
    }
}
