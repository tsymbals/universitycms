package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.Lesson;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class LessonTimeDto implements Serializable {

    private static final String LESSON_TIME_DTO_TEMPLATE = "LessonTimeDto{id=%d, startTime=%s, endTime=%s, lessons=%s}";

    private Long id;
    private LocalTime startTime;
    private LocalTime endTime;
    private Set<Lesson> lessons;

    @Override
    public String toString() {
        return LESSON_TIME_DTO_TEMPLATE.formatted(
                id,
                startTime,
                endTime,
                DtoStringViewer.toString(lessons, Comparator.comparing(Lesson::getId), Lesson::toString));
    }
}
