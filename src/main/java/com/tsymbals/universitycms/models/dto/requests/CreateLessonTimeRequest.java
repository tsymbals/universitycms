package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class CreateLessonTimeRequest implements Serializable {

    @NotNull(message = "The start time is required in the format HH:MM")
    private LocalTime startTime;

    @NotNull(message = "The end time is required in the format HH:MM")
    private LocalTime endTime;
}
