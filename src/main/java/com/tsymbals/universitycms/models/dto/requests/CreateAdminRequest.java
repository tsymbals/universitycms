package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class CreateAdminRequest implements Serializable {

    @NotEmpty(message = "Ensure the user is selected and free users with admin role are available")
    private String username;
}
