package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UpdateLessonRequest implements Serializable {

    @NotNull(message = "The date is required in the format YYYY-MM-DD")
    private LocalDate date;

    private Long lessonTimeId;
    private String auditoriumName;
    private String courseNameToUpdate;
    private String courseNameToRemove;
    private Long teacherIdToUpdate;
    private Long teacherIdToRemove;
    private String groupNameToAdd;
    private String groupNameToRemove;

    public UpdateLessonRequest(LocalDate date) {
        this.date = date;
    }
}
