package com.tsymbals.universitycms.models.dto.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UpdateAdminRequest implements Serializable {
    private String username;
}
