package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.Course;
import com.tsymbals.universitycms.models.entities.Group;
import com.tsymbals.universitycms.models.entities.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class TeacherDto implements Serializable {

    private static final String TEACHER_DTO_TEMPLATE = "TeacherDto{id=%d, user=%s, firstName='%s', lastName='%s', courses=%s, groups=%s}";

    private Long id;
    private UserEntity user;
    private String firstName;
    private String lastName;
    private Set<Course> courses;
    private Set<Group> groups;

    @Override
    public String toString() {
        return TEACHER_DTO_TEMPLATE.formatted(
                id,
                DtoStringViewer.toString(user),
                firstName,
                lastName,
                DtoStringViewer.toString(courses, Comparator.comparing(Course::getId), Course::toString),
                DtoStringViewer.toString(groups, Comparator.comparing(Group::getId), Group::toString));
    }
}
