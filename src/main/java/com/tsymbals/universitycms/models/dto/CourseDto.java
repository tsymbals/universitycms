package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.Student;
import com.tsymbals.universitycms.models.entities.Teacher;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class CourseDto implements Serializable {

    private static final String COURSE_DTO_TEMPLATE = "CourseDto{id=%d, name='%s', description='%s', teachers=%s, students=%s}";

    private Long id;
    private String name;
    private String description;
    private Set<Teacher> teachers;
    private Set<Student> students;

    @Override
    public String toString() {
        return COURSE_DTO_TEMPLATE.formatted(
                id,
                name,
                description,
                DtoStringViewer.toString(teachers, Comparator.comparing(Teacher::getId), Teacher::toString),
                DtoStringViewer.toString(students, Comparator.comparing(Student::getId), Student::toString));
    }
}
