package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UpdateAuditoriumRequest implements Serializable {
    private String departmentName;

    @NotEmpty(message = "The new name is required")
    @Size(min = 3, max = 3, message = "Ensure the new name has 3 characters")
    @Pattern(regexp = "^\\d+$", message = "Ensure the new name contains only digits")
    private String name;

    public UpdateAuditoriumRequest(String name) {
        this.name = name;
    }
}
