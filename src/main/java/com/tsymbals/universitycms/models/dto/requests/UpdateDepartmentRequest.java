package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UpdateDepartmentRequest implements Serializable {
    private String universityAbbreviationName;

    @NotEmpty(message = "The new name is required")
    @Size(max = 64, message = "Ensure the new name do not exceed 64 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Ensure the new name contains only letters and words separated whitespaces")
    private String name;

    public UpdateDepartmentRequest(String name) {
        this.name = name;
    }
}
