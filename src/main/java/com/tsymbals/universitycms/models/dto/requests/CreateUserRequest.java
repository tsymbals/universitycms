package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class CreateUserRequest implements Serializable {

    @NotEmpty(message = "The username is required")
    @Size(max = 16, message = "Ensure the username do not exceed 16 characters")
    @Pattern(regexp = "^\\w+$", message = "Ensure the username contains only letters, numbers, and underscores")
    private String username;

    @NotEmpty(message = "The password is required")
    @Size(min = 8, max = 64, message = "Ensure the password is between 8 and 64 characters")
    @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$", message = "Ensure the password contains at least one uppercase letter, one lowercase letter, and one digit")
    private String password;
}
