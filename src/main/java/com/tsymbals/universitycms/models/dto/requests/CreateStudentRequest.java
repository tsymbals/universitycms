package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class CreateStudentRequest implements Serializable {

    @NotEmpty(message = "Ensure the user is selected and free users with student role are available")
    private String username;

    @NotEmpty(message = "The first name is required")
    @Size(max = 32, message = "Ensure the first name do not exceed 32 characters")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Ensure the first name contains only letters")
    private String firstName;

    @NotEmpty(message = "The last name is required")
    @Size(max = 32, message = "Ensure the last name do not exceed 32 characters")
    @Pattern(regexp = "^[a-zA-Z]+$", message = "Ensure the last name contains only letters")
    private String lastName;
}
