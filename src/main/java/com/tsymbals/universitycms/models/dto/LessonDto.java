package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class LessonDto implements Serializable {

    private static final String LESSON_DTO_TEMPLATE = "LessonDto{id=%d, date=%s, lessonTime=%s, auditorium=%s, course=%s, teacher=%s, groups=%s}";

    private Long id;
    private LocalDate date;
    private LessonTime lessonTime;
    private Auditorium auditorium;
    private Course course;
    private Teacher teacher;
    private Set<Group> groups;

    @Override
    public String toString() {
        return LESSON_DTO_TEMPLATE.formatted(
                id,
                date,
                DtoStringViewer.toString(lessonTime),
                DtoStringViewer.toString(auditorium),
                DtoStringViewer.toString(course),
                DtoStringViewer.toString(teacher),
                DtoStringViewer.toString(groups, Comparator.comparing(Group::getId), Group::toString));
    }
}
