package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class UpdateCourseRequest implements Serializable {

    @NotEmpty(message = "The new name is required")
    @Size(max = 128, message = "Ensure the new name do not exceed 128 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Ensure the new name contains only letters and words separated whitespaces")
    private String name;

    @NotEmpty(message = "The new description is required")
    @Size(max = 256, message = "Ensure the new description do not exceed 256 characters")
    @Pattern(regexp = "^[a-zA-Z\\s]+$", message = "Ensure the new description contains only letters and words separated whitespaces")
    private String description;
}
