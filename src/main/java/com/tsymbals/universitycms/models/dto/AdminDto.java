package com.tsymbals.universitycms.models.dto;

import com.tsymbals.universitycms.models.dto.utils.DtoStringViewer;
import com.tsymbals.universitycms.models.entities.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class AdminDto implements Serializable {

    private static final String ADMIN_DTO_TEMPLATE = "AdminDto{id=%d, user=%s}";

    private Long id;
    private UserEntity user;

    @Override
    public String toString() {
        return ADMIN_DTO_TEMPLATE.formatted(
                id,
                DtoStringViewer.toString(user));
    }
}
