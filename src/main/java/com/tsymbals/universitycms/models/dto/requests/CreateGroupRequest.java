package com.tsymbals.universitycms.models.dto.requests;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public final class CreateGroupRequest implements Serializable {

    @NotEmpty(message = "The name is required")
    @Size(min = 6, max = 6, message = "Ensure the name has 6 characters")
    @Pattern(regexp = "^\\d[A-Z]{2}-\\d{2}$", message = "Ensure the name is in the format DLL-DD, D - digit, L - capital letter")
    private String name;
}
