package com.tsymbals.universitycms.configs;

import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.LogoutConfigurer;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig {

    private static final String ROLE_ADMIN = "ROLE_ADMIN";
    private static final String ROLE_STAFF = "ROLE_STAFF";
    private static final String ROLE_TEACHER = "ROLE_TEACHER";
    private static final String ROLE_STUDENT = "ROLE_STUDENT";

    private final UserDetailsServiceImpl userDetailsService;

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());

        return authenticationProvider;
    }

    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration) throws Exception {
        return authenticationConfiguration.getAuthenticationManager();
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(request -> request
                        .requestMatchers("/css/**", "/webjars/**", "/pictures/**")
                        .permitAll()
                        .requestMatchers(
                                "/roles/**",
                                "/users/create", "/users", "/users/{id}", "/users/{id}/update", "/users/{id}/delete",
                                "/admins/**",
                                "/employees/create", "/employees/{id}/update", "/employees/{id}/delete",
                                "/teachers/create", "/teachers/{id}/update", "/teachers/{id}/delete",
                                "/students/create", "/students/{id}/update", "/students/{id}/delete",
                                "/universities/create", "/universities/{id}/update", "/universities/{id}/delete",
                                "/departments/create", "/departments/{id}/update", "/departments/{id}/delete",
                                "/auditoriums/create", "/auditoriums/{id}/update", "/auditoriums/{id}/delete",
                                "/lesson-times/create", "/lesson-times/{id}/update", "/lesson-times/{id}/delete",
                                "/courses/create", "/courses/{id}/update", "/courses/{id}/delete",
                                "/groups/create", "/groups/{id}/update", "/groups/{id}/delete",
                                "/lessons/create", "/lessons", "/lessons/{id}", "/lessons/{id}/update", "/lessons/{id}/delete")
                        .hasAuthority(ROLE_ADMIN)
                        .requestMatchers(
                                "/auditoriums/{username}/date-schedule")
                        .hasAuthority(ROLE_STAFF)
                        .requestMatchers(
                                "/employees", "/employees/{id}")
                        .hasAnyAuthority(ROLE_ADMIN, ROLE_STAFF)
                        .requestMatchers(
                                "/lessons/{username}/date-schedule",
                                "/lessons/{username}/week-schedule")
                        .hasAnyAuthority(ROLE_TEACHER, ROLE_STUDENT)
                        .requestMatchers(
                                "/teachers", "/teachers/{id}",
                                "/students", "/students/{id}",
                                "/courses", "/courses/{id}",
                                "/groups", "/groups/{id}")
                        .hasAnyAuthority(ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT)
                        .requestMatchers(
                                "/users/{username}/profile",
                                "/universities", "/universities/{id}",
                                "/departments", "/departments/{id}",
                                "/auditoriums", "auditoriums/{id}",
                                "/lesson-times", "/lesson-times/{id}")
                        .hasAnyAuthority(ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT)
                        .anyRequest()
                        .authenticated())
                .formLogin(form -> form
                        .loginPage("/login")
                        .permitAll())
                .logout(LogoutConfigurer::permitAll)
                .exceptionHandling(exceptionHandling -> exceptionHandling
                        .accessDeniedPage("/access-denied"));

        return http.build();
    }
}
