package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
    List<Department> findAllByIdNotOrderByIdAsc(Long id);
    Optional<Department> findByName(String name);

    @Modifying
    @Query("UPDATE Department d SET d.name = :name WHERE d.id = :id")
    void updateName(@Param(value = "id") Long id, @Param(value = "name") String name);
}
