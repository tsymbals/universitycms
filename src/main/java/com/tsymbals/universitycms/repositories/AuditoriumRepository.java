package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Auditorium;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AuditoriumRepository extends JpaRepository<Auditorium, Long> {
    List<Auditorium> findAllByIdNotOrderByIdAsc(Long id);
    List<Auditorium> findAllByLessonsIsNotEmptyOrderByIdAsc();
    Optional<Auditorium> findByName(String name);

    @Modifying
    @Query("UPDATE Auditorium a SET a.name = :name WHERE a.id = :id")
    void updateName(@Param(value = "id") Long id, @Param(value = "name") String name);
}
