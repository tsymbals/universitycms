package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student> findByUser_Username(String username);
    Optional<Student> findByFirstNameAndLastName(String firstName, String lastName);

    @Modifying
    @Query("UPDATE Student s SET s.lastName = :lastName WHERE s.id = :id")
    void updateLastName(@Param(value = "id") Long id, @Param(value = "lastName") String lastName);
}
