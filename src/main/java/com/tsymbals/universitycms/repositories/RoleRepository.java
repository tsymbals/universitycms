package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    List<Role> findAllByIdNotInOrderByIdAsc(Set<Long> ids);
    Optional<Role> findByName(String name);

    @Modifying
    @Query("UPDATE Role r SET r.name = :name WHERE r.id = :id")
    void updateName(@Param(value = "id") Long id, @Param(value = "name") String name);
}
