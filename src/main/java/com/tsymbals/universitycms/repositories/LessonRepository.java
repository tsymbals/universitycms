package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface LessonRepository extends JpaRepository<Lesson, Long> {
    List<Lesson> findAllByDateOrderByIdAsc(LocalDate date);
    List<Lesson> findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc(LocalDate date, Long teacherId);
    List<Lesson> findAllByDateAndGroups_NameOrderByIdAsc(LocalDate date, String groupName);
    List<Lesson> findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc(LocalDate dateBefore, LocalDate dateAfter, Long teacherId);
    List<Lesson> findAllByDateBetweenAndGroups_NameOrderByIdAsc(LocalDate dateBefore, LocalDate dateAfter, String groupName);
    Optional<Lesson> findByDateAndLessonTime_StartTimeAndLessonTime_EndTime(LocalDate date, LocalTime startTime, LocalTime endTime);

    @Modifying
    @Query("UPDATE Lesson l SET l.date = :date WHERE l.id = :id")
    void updateDate(@Param(value = "id") Long id, @Param(value = "date") LocalDate date);
}
