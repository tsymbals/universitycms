package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {
    List<Course> findAllByIdNotOrderByIdAsc(Long id);
    List<Course> findAllByIdNotInOrderByIdAsc(Set<Long> ids);
    Optional<Course> findByName(String name);

    @Modifying
    @Query("UPDATE Course c SET c.name = :name WHERE c.id = :id")
    void updateName(@Param(value = "id") Long id, @Param(value = "name") String name);

    @Modifying
    @Query("UPDATE Course c SET c.description = :description WHERE c.id = :id")
    void updateDescription(@Param(value = "id") Long id, @Param(value = "description") String description);
}
