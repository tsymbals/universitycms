package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    List<UserEntity> findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc(String roleName);
    Optional<UserEntity> findByUsername(String username);

    @Modifying
    @Query("UPDATE UserEntity u SET u.username = :username WHERE u.id = :id")
    void updateUsername(@Param(value = "id") Long id, @Param(value = "username") String username);

    @Modifying
    @Query("UPDATE UserEntity u SET u.password = :password WHERE u.id = :id")
    void updatePassword(@Param(value = "id") Long id, @Param(value = "password") String password);

    @Modifying
    @Query("UPDATE UserEntity u SET u.enabledStatus = :enabledStatus WHERE u.id = :id")
    void updateEnabledStatus(@Param(value = "id") Long id, @Param(value = "enabledStatus") Boolean enabledStatus);

    @Modifying
    @Query("UPDATE UserEntity u SET u.freeStatus = :freeStatus WHERE u.id = :id")
    void updateFreeStatus(@Param(value = "id") Long id, @Param(value = "freeStatus") Boolean freeStatus);
}
