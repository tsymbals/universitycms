package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.LessonTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface LessonTimeRepository extends JpaRepository<LessonTime, Long> {
    List<LessonTime> findAllByIdNotOrderByIdAsc(Long id);
    Optional<LessonTime> findByStartTimeAndEndTime(LocalTime startTime, LocalTime endTime);

    @Modifying
    @Query("UPDATE LessonTime lt SET lt.startTime = :startTime WHERE lt.id = :id")
    void updateStartTime(@Param(value = "id") Long id, @Param(value = "startTime") LocalTime startTime);

    @Modifying
    @Query("UPDATE LessonTime lt SET lt.endTime = :endTime WHERE lt.id = :id")
    void updateEndTime(@Param(value = "id") Long id, @Param(value = "endTime") LocalTime endTime);
}
