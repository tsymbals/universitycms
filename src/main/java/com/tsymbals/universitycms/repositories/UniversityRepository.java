package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.University;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UniversityRepository extends JpaRepository<University, Long> {
    List<University> findAllByIdNotOrderByIdAsc(Long id);
    Optional<University> findByAbbreviationName(String abbreviationName);

    @Modifying
    @Query("UPDATE University u SET u.name = :name WHERE u.id = :id")
    void updateName(@Param(value = "id") Long id, @Param(value = "name") String name);

    @Modifying
    @Query("UPDATE University u SET u.abbreviationName = :abbreviationName WHERE u.id = :id")
    void updateAbbreviationName(@Param(value = "id") Long id, @Param(value = "abbreviationName") String abbreviationName);

    @Modifying
    @Query("UPDATE University u SET u.city = :city WHERE u.id = :id")
    void updateCity(@Param(value = "id") Long id, @Param(value = "city") String city);

    @Modifying
    @Query("UPDATE University u SET u.phoneNumber = :phoneNumber WHERE u.id = :id")
    void updatePhoneNumber(@Param(value = "id") Long id, @Param(value = "phoneNumber") String phoneNumber);
}
