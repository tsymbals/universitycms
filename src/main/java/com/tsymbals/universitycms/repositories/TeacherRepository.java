package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long> {
    List<Teacher> findAllByIdNotOrderByIdAsc(Long id);
    Optional<Teacher> findByUser_Username(String username);
    Optional<Teacher> findByFirstNameAndLastName(String firstName, String lastName);

    @Modifying
    @Query("UPDATE Teacher t SET t.lastName = :lastName WHERE t.id = :id")
    void updateLastName(@Param(value = "id") Long id, @Param(value = "lastName") String lastName);
}
