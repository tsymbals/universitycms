package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    List<Group> findAllByIdNotOrderByIdAsc(Long id);
    List<Group> findAllByIdNotInOrderByIdAsc(Set<Long> ids);
    Optional<Group> findByName(String name);

    @Modifying
    @Query("UPDATE Group g SET g.name = :name WHERE g.id = :id")
    void updateName(@Param(value = "id") Long id, @Param(value = "name") String name);
}
