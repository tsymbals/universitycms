package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    Optional<Employee> findByFirstNameAndLastName(String firstName, String lastName);

    @Modifying
    @Query("UPDATE Employee e SET e.lastName = :lastName WHERE e.id = :id")
    void updateLastName(@Param(value = "id") Long id, @Param(value = "lastName") String lastName);
}
