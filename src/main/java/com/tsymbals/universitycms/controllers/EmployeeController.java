package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.EmployeeDto;
import com.tsymbals.universitycms.models.dto.requests.CreateEmployeeRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateEmployeeRequest;
import com.tsymbals.universitycms.services.EmployeeService;
import com.tsymbals.universitycms.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private static final String CREATE_EMPLOYEE_REQUEST = "createEmployeeRequest";
    private static final String UPDATE_EMPLOYEE_REQUEST = "updateEmployeeRequest";
    private static final String USERS = "users";
    private static final String EMPLOYEE = "employee";
    private static final String EMPLOYEES = "employees";

    private static final String CREATE_EMPLOYEE_FORM = "pages/employees/create-employee";
    private static final String ALL_EMPLOYEES_PAGE = "pages/employees/all-employees";
    private static final String ONE_EMPLOYEE_PAGE = "pages/employees/one-employee";
    private static final String UPDATE_EMPLOYEE_FORM = "pages/employees/update-employee";
    private static final String REDIRECT_EMPLOYEES = "redirect:/employees";

    private final UserService userService;
    private final EmployeeService employeeService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateEmployeeForm(Model model) {
        model.addAttribute(CREATE_EMPLOYEE_REQUEST, new CreateEmployeeRequest());
        model.addAttribute(USERS, userService.getAllFreeEmployeeUsers());

        return CREATE_EMPLOYEE_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveEmployee(@Valid @ModelAttribute CreateEmployeeRequest createEmployeeRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(USERS, userService.getAllFreeEmployeeUsers());

            return CREATE_EMPLOYEE_FORM;
        }
        employeeService.save(createEmployeeRequest);

        return REDIRECT_EMPLOYEES;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF')")
    public String getAllEmployees(Model model) {
        model.addAttribute(EMPLOYEES, employeeService.getAll());

        return ALL_EMPLOYEES_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF')")
    public String getEmployeeById(@PathVariable Long id, Model model) {
        model.addAttribute(EMPLOYEE, employeeService.getById(id));

        return ONE_EMPLOYEE_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateEmployeeForm(@PathVariable Long id, Model model) {
        EmployeeDto employee = employeeService.getById(id);
        model.addAttribute(UPDATE_EMPLOYEE_REQUEST, new UpdateEmployeeRequest(employee.getLastName()));
        model.addAttribute(EMPLOYEE, employee);
        model.addAttribute(USERS, userService.getAllFreeEmployeeUsers());

        return UPDATE_EMPLOYEE_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateEmployee(@PathVariable Long id, @Valid @ModelAttribute UpdateEmployeeRequest updateEmployeeRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(EMPLOYEE, employeeService.getById(id));
            model.addAttribute(USERS, userService.getAllFreeEmployeeUsers());

            return UPDATE_EMPLOYEE_FORM;
        }
        employeeService.updateUser(id, updateEmployeeRequest);
        employeeService.updateLastName(id, updateEmployeeRequest);

        return REDIRECT_EMPLOYEES;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteEmployee(@PathVariable Long id) {
        employeeService.deleteById(id);

        return REDIRECT_EMPLOYEES;
    }
}
