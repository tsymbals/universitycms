package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.AuditoriumDto;
import com.tsymbals.universitycms.models.dto.LessonDto;
import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateAuditoriumRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateAuditoriumRequest;
import com.tsymbals.universitycms.services.AuditoriumService;
import com.tsymbals.universitycms.services.DepartmentService;
import com.tsymbals.universitycms.services.LessonService;
import com.tsymbals.universitycms.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping(path = "/auditoriums")
@RequiredArgsConstructor
public class AuditoriumController {

    private static final String CREATE_AUDITORIUM_REQUEST = "createAuditoriumRequest";
    private static final String UPDATE_AUDITORIUM_REQUEST = "updateAuditoriumRequest";
    private static final String DEPARTMENTS = "departments";
    private static final String AUDITORIUM = "auditorium";
    private static final String AUDITORIUMS = "auditoriums";
    private static final String SELECTED_DATE = "selectedDate";
    private static final String USER = "user";

    private static final String CREATE_AUDITORIUM_FORM = "pages/auditoriums/create-auditorium";
    private static final String ALL_AUDITORIUMS_PAGE = "pages/auditoriums/all-auditoriums";
    private static final String ONE_AUDITORIUM_PAGE = "pages/auditoriums/one-auditorium";
    private static final String DATE_AUDITORIUMS_SCHEDULE_PAGE = "pages/auditoriums/date-auditoriums-schedule";
    private static final String UPDATE_AUDITORIUM_FORM = "pages/auditoriums/update-auditorium";
    private static final String REDIRECT_AUDITORIUMS = "redirect:/auditoriums";
    private static final String REDIRECT = "redirect:/";

    private final DepartmentService departmentService;
    private final AuditoriumService auditoriumService;
    private final UserService userService;
    private final LessonService lessonService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateAuditoriumForm(Model model) {
        model.addAttribute(CREATE_AUDITORIUM_REQUEST, new CreateAuditoriumRequest());
        model.addAttribute(DEPARTMENTS, departmentService.getAll());

        return CREATE_AUDITORIUM_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveAuditorium(@Valid @ModelAttribute CreateAuditoriumRequest createAuditoriumRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(DEPARTMENTS, departmentService.getAll());

            return CREATE_AUDITORIUM_FORM;
        }
        auditoriumService.save(createAuditoriumRequest);

        return REDIRECT_AUDITORIUMS;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getAllAuditoriums(Model model) {
        model.addAttribute(AUDITORIUMS, auditoriumService.getAll());

        return ALL_AUDITORIUMS_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getAuditoriumById(@PathVariable Long id, Model model) {
        model.addAttribute(AUDITORIUM, auditoriumService.getById(id));

        return ONE_AUDITORIUM_PAGE;
    }

    @GetMapping(path = "/{username}/date-schedule")
    @PreAuthorize("hasAuthority('ROLE_STAFF')")
    public String getDateAuditoriumsSchedule(@PathVariable String username, @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate selectedDate, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDto user = userService.getByUsername(authentication.getName());
        if (!user.getUsername().equals(username)) {
            return REDIRECT;
        }

        LocalDate scheduleDate = (selectedDate != null) ? selectedDate : LocalDate.now();
        List<LessonDto> lessons = lessonService.getAllByDate(scheduleDate);
        model.addAttribute(SELECTED_DATE, scheduleDate);
        model.addAttribute(USER, user);
        model.addAttribute(AUDITORIUMS, auditoriumService.getAllByDate(lessons));

        return DATE_AUDITORIUMS_SCHEDULE_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateAuditoriumForm(@PathVariable Long id, Model model) {
        AuditoriumDto auditorium = auditoriumService.getById(id);
        model.addAttribute(UPDATE_AUDITORIUM_REQUEST, new UpdateAuditoriumRequest(auditorium.getName()));
        model.addAttribute(AUDITORIUM, auditorium);
        model.addAttribute(DEPARTMENTS, departmentService.getAllNoneMatching(auditorium.getDepartment()));

        return UPDATE_AUDITORIUM_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateAuditorium(@PathVariable Long id, @Valid @ModelAttribute UpdateAuditoriumRequest updateAuditoriumRequest, BindingResult result, Model model) {
        AuditoriumDto auditorium = auditoriumService.getById(id);
        if (result.hasErrors()) {
            model.addAttribute(AUDITORIUM, auditorium);
            model.addAttribute(DEPARTMENTS, departmentService.getAllNoneMatching(auditorium.getDepartment()));

            return UPDATE_AUDITORIUM_FORM;
        }
        auditoriumService.updateDepartment(id, updateAuditoriumRequest);
        auditoriumService.updateName(id, updateAuditoriumRequest);

        return REDIRECT_AUDITORIUMS;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteAuditorium(@PathVariable Long id) {
        auditoriumService.deleteById(id);

        return REDIRECT_AUDITORIUMS;
    }
}
