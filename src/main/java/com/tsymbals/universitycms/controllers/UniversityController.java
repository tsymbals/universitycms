package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.UniversityDto;
import com.tsymbals.universitycms.models.dto.requests.CreateUniversityRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateUniversityRequest;
import com.tsymbals.universitycms.services.UniversityService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/universities")
@RequiredArgsConstructor
public class UniversityController {

    private static final String CREATE_UNIVERSITY_REQUEST = "createUniversityRequest";
    private static final String UPDATE_UNIVERSITY_REQUEST = "updateUniversityRequest";
    private static final String UNIVERSITY = "university";
    private static final String UNIVERSITIES = "universities";

    private static final String CREATE_UNIVERSITY_FORM = "pages/universities/create-university";
    private static final String ALL_UNIVERSITIES_PAGE = "pages/universities/all-universities";
    private static final String ONE_UNIVERSITY_PAGE = "pages/universities/one-university";
    private static final String UPDATE_UNIVERSITY_FORM = "pages/universities/update-university";
    private static final String REDIRECT_UNIVERSITIES = "redirect:/universities";

    private final UniversityService universityService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateUniversityForm(Model model) {
        model.addAttribute(CREATE_UNIVERSITY_REQUEST, new CreateUniversityRequest());

        return CREATE_UNIVERSITY_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveUniversity(@Valid @ModelAttribute CreateUniversityRequest createUniversityRequest, BindingResult result) {
        if (result.hasErrors()) {
            return CREATE_UNIVERSITY_FORM;
        }
        universityService.save(createUniversityRequest);

        return REDIRECT_UNIVERSITIES;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getAllUniversities(Model model) {
        model.addAttribute(UNIVERSITIES, universityService.getAll());

        return ALL_UNIVERSITIES_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getUniversityById(@PathVariable Long id, Model model) {
        model.addAttribute(UNIVERSITY, universityService.getById(id));

        return ONE_UNIVERSITY_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateUniversityForm(@PathVariable Long id, Model model) {
        UniversityDto university = universityService.getById(id);
        model.addAttribute(UPDATE_UNIVERSITY_REQUEST, new UpdateUniversityRequest(
                university.getName(),
                university.getAbbreviationName(),
                university.getCity(),
                university.getPhoneNumber()));
        model.addAttribute(UNIVERSITY, university);

        return UPDATE_UNIVERSITY_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateUniversity(@PathVariable Long id, @Valid @ModelAttribute UpdateUniversityRequest updateUniversityRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(UNIVERSITY, universityService.getById(id));

            return UPDATE_UNIVERSITY_FORM;
        }
        universityService.updateName(id, updateUniversityRequest);
        universityService.updateAbbreviationName(id, updateUniversityRequest);
        universityService.updateCity(id, updateUniversityRequest);
        universityService.updatePhoneNumber(id, updateUniversityRequest);

        return REDIRECT_UNIVERSITIES;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteUniversity(@PathVariable Long id) {
        universityService.deleteById(id);

        return REDIRECT_UNIVERSITIES;
    }
}
