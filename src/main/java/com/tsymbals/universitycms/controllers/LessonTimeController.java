package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.LessonTimeDto;
import com.tsymbals.universitycms.models.dto.requests.CreateLessonTimeRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateLessonTimeRequest;
import com.tsymbals.universitycms.services.LessonTimeService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/lesson-times")
@RequiredArgsConstructor
public class LessonTimeController {

    private static final String CREATE_LESSON_TIME_REQUEST = "createLessonTimeRequest";
    private static final String UPDATE_LESSON_TIME_REQUEST = "updateLessonTimeRequest";
    private static final String LESSON_TIME = "lessonTime";
    private static final String LESSON_TIMES = "lessonTimes";

    private static final String CREATE_LESSON_TIME_FORM = "pages/lesson-times/create-lesson-time";
    private static final String ALL_LESSON_TIMES_PAGE = "pages/lesson-times/all-lesson-times";
    private static final String ONE_LESSON_TIME_PAGE = "pages/lesson-times/one-lesson-time";
    private static final String UPDATE_LESSON_TIME_FORM = "pages/lesson-times/update-lesson-time";
    private static final String REDIRECT_LESSON_TIMES = "redirect:/lesson-times";

    private final LessonTimeService lessonTimeService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateLessonTimeForm(Model model) {
        model.addAttribute(CREATE_LESSON_TIME_REQUEST, new CreateLessonTimeRequest());

        return CREATE_LESSON_TIME_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveLessonTime(@Valid @ModelAttribute CreateLessonTimeRequest createLessonTimeRequest, BindingResult result) {
        if (result.hasErrors()) {
            return CREATE_LESSON_TIME_FORM;
        }
        lessonTimeService.save(createLessonTimeRequest);

        return REDIRECT_LESSON_TIMES;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getAllLessonTimes(Model model) {
        model.addAttribute(LESSON_TIMES, lessonTimeService.getAll());

        return ALL_LESSON_TIMES_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getLessonTimeById(@PathVariable Long id, Model model) {
        model.addAttribute(LESSON_TIME, lessonTimeService.getById(id));

        return ONE_LESSON_TIME_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateLessonTimeForm(@PathVariable Long id, Model model) {
        LessonTimeDto lessonTime = lessonTimeService.getById(id);
        model.addAttribute(UPDATE_LESSON_TIME_REQUEST, new UpdateLessonTimeRequest(lessonTime.getStartTime(), lessonTime.getEndTime()));
        model.addAttribute(LESSON_TIME, lessonTime);

        return UPDATE_LESSON_TIME_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateLessonTime(@PathVariable Long id, @Valid @ModelAttribute UpdateLessonTimeRequest updateLessonTimeRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(LESSON_TIME, lessonTimeService.getById(id));

            return UPDATE_LESSON_TIME_FORM;
        }
        lessonTimeService.updateStartTime(id, updateLessonTimeRequest);
        lessonTimeService.updateEndTime(id, updateLessonTimeRequest);

        return REDIRECT_LESSON_TIMES;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteLessonTime(@PathVariable Long id) {
        lessonTimeService.deleteById(id);

        return REDIRECT_LESSON_TIMES;
    }
}
