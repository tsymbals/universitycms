package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.RoleDto;
import com.tsymbals.universitycms.models.dto.requests.CreateRoleRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateRoleRequest;
import com.tsymbals.universitycms.services.RoleService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/roles")
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
public class RoleController {

    private static final String CREATE_ROLE_REQUEST = "createRoleRequest";
    private static final String UPDATE_ROLE_REQUEST = "updateRoleRequest";
    private static final String ROLE = "role";
    private static final String ROLES = "roles";

    private static final String CREATE_ROLE_FORM = "pages/roles/create-role";
    private static final String ALL_ROLES_PAGE = "pages/roles/all-roles";
    private static final String ONE_ROLE_PAGE = "pages/roles/one-role";
    private static final String UPDATE_ROLE_FORM = "pages/roles/update-role";
    private static final String REDIRECT_ROLES = "redirect:/roles";

    private final RoleService roleService;

    @GetMapping(path = "/create")
    public String getCreateRoleForm(Model model) {
        model.addAttribute(CREATE_ROLE_REQUEST, new CreateRoleRequest());

        return CREATE_ROLE_FORM;
    }

    @PostMapping(path = "/create")
    public String saveRole(@Valid @ModelAttribute CreateRoleRequest createRoleRequest, BindingResult result) {
        if (result.hasErrors()) {
            return CREATE_ROLE_FORM;
        }
        roleService.save(createRoleRequest);

        return REDIRECT_ROLES;
    }

    @GetMapping
    public String getAllRoles(Model model) {
        model.addAttribute(ROLES, roleService.getAll());

        return ALL_ROLES_PAGE;
    }

    @GetMapping(path = "/{id}")
    public String getRoleById(@PathVariable Long id, Model model) {
        model.addAttribute(ROLE, roleService.getById(id));

        return ONE_ROLE_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    public String getUpdateRoleForm(@PathVariable Long id, Model model) {
        RoleDto role = roleService.getById(id);
        model.addAttribute(UPDATE_ROLE_REQUEST, new UpdateRoleRequest(role.getName()));
        model.addAttribute(ROLE, role);

        return UPDATE_ROLE_FORM;
    }

    @PostMapping(path = "/{id}/update")
    public String updateRole(@PathVariable Long id, @Valid @ModelAttribute UpdateRoleRequest updateRoleRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(ROLE, roleService.getById(id));

            return UPDATE_ROLE_FORM;
        }
        roleService.updateName(id, updateRoleRequest);

        return REDIRECT_ROLES;
    }

    @GetMapping(path = "/{id}/delete")
    public String deleteRole(@PathVariable Long id) {
        roleService.deleteById(id);

        return REDIRECT_ROLES;
    }
}
