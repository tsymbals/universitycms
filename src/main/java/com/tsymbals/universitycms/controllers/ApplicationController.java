package com.tsymbals.universitycms.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Objects;

@Controller
@RequestMapping(path = "/")
public class ApplicationController {

    private static final String APP_NAME = "appName";

    private static final String LOGIN_FORM = "login";
    private static final String REDIRECT = "redirect:/";
    private static final String HOME_PAGE = "index";
    private static final String ACCESS_DENIED_PAGE = "pages/access-denied";

    @Value("${spring.application.name}")
    private String appName;

    @GetMapping(path = "/login")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getLoginForm() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (Objects.isNull(authentication) || authentication instanceof AnonymousAuthenticationToken) {
            return LOGIN_FORM;
        }

        return REDIRECT;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getHomePage(Model model) {
        model.addAttribute(APP_NAME, appName);

        return HOME_PAGE;
    }

    @GetMapping(path = "/access-denied")
    public String getAccessDeniedPage() {
        return ACCESS_DENIED_PAGE;
    }
}
