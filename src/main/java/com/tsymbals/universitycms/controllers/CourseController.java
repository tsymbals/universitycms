package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.CourseDto;
import com.tsymbals.universitycms.models.dto.requests.CreateCourseRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateCourseRequest;
import com.tsymbals.universitycms.services.CourseService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/courses")
@RequiredArgsConstructor
public class CourseController {

    private static final String CREATE_COURSE_REQUEST = "createCourseRequest";
    private static final String UPDATE_COURSE_REQUEST = "updateCourseRequest";
    private static final String COURSE = "course";
    private static final String COURSES = "courses";

    private static final String CREATE_COURSE_FORM = "pages/courses/create-course";
    private static final String ALL_COURSES_PAGE = "pages/courses/all-courses";
    private static final String ONE_COURSE_PAGE = "pages/courses/one-course";
    private static final String UPDATE_COURSE_FORM = "pages/courses/update-course";
    private static final String REDIRECT_COURSES = "redirect:/courses";

    private final CourseService courseService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateCourseForm(Model model) {
        model.addAttribute(CREATE_COURSE_REQUEST, new CreateCourseRequest());

        return CREATE_COURSE_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveCourse(@Valid @ModelAttribute CreateCourseRequest createCourseRequest, BindingResult result) {
        if (result.hasErrors()) {
            return CREATE_COURSE_FORM;
        }
        courseService.save(createCourseRequest);

        return REDIRECT_COURSES;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getAllCourses(Model model) {
        model.addAttribute(COURSES, courseService.getAll());

        return ALL_COURSES_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getCourseById(@PathVariable Long id, Model model) {
        model.addAttribute(COURSE, courseService.getById(id));

        return ONE_COURSE_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateCourseForm(@PathVariable Long id, Model model) {
        CourseDto course = courseService.getById(id);
        model.addAttribute(UPDATE_COURSE_REQUEST, new UpdateCourseRequest(course.getName(), course.getDescription()));
        model.addAttribute(COURSE, course);

        return UPDATE_COURSE_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateCourse(@PathVariable Long id, @Valid @ModelAttribute UpdateCourseRequest updateCourseRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(COURSE, courseService.getById(id));

            return UPDATE_COURSE_FORM;
        }
        courseService.updateName(id, updateCourseRequest);
        courseService.updateDescription(id, updateCourseRequest);

        return REDIRECT_COURSES;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteCourse(@PathVariable Long id) {
        courseService.deleteById(id);

        return REDIRECT_COURSES;
    }
}
