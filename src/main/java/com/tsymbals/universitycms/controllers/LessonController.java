package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.LessonDto;
import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateLessonRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateLessonRequest;
import com.tsymbals.universitycms.services.*;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

@Controller
@RequestMapping(path = "/lessons")
@RequiredArgsConstructor
public class LessonController {

    private static final String CREATE_LESSON_REQUEST = "createLessonRequest";
    private static final String UPDATE_LESSON_REQUEST = "updateLessonRequest";
    private static final String LESSON_TIMES = "lessonTimes";
    private static final String AUDITORIUMS = "auditoriums";
    private static final String LESSON = "lesson";
    private static final String LESSONS = "lessons";
    private static final String SELECTED_DATE = "selectedDate";
    private static final String START_WEEK_SCHEDULE_DATE = "startWeekScheduleDate";
    private static final String END_WEEK_SCHEDULE_DATE = "endWeekScheduleDate";
    private static final String USER = "user";
    private static final String COURSES_TO_UPDATE = "coursesToUpdate";
    private static final String COURSE_TO_REMOVE = "courseToRemove";
    private static final String TEACHERS_TO_UPDATE = "teachersToUpdate";
    private static final String TEACHER_TO_REMOVE = "teacherToRemove";
    private static final String GROUPS_TO_ADD = "groupsToAdd";
    private static final String GROUPS_TO_REMOVE = "groupsToRemove";

    private static final String ROLE_TEACHER = "ROLE_TEACHER";
    private static final String ROLE_STUDENT = "ROLE_STUDENT";

    private static final String CREATE_LESSON_FORM = "pages/lessons/create-lesson";
    private static final String ALL_LESSONS_PAGE = "pages/lessons/all-lessons";
    private static final String ONE_LESSON_PAGE = "pages/lessons/one-lesson";
    private static final String DATE_LESSONS_SCHEDULE_PAGE = "pages/lessons/date-lessons-schedule";
    private static final String WEEK_LESSONS_SCHEDULE_PAGE = "pages/lessons/week-lessons-schedule";
    private static final String UPDATE_LESSON_FORM = "pages/lessons/update-lesson";
    private static final String REDIRECT_LESSONS = "redirect:/lessons";
    private static final String REDIRECT = "redirect:/";

    private final LessonTimeService lessonTimeService;
    private final AuditoriumService auditoriumService;
    private final LessonService lessonService;
    private final UserService userService;
    private final TeacherService teacherService;
    private final StudentService studentService;
    private final CourseService courseService;
    private final GroupService groupService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateLessonForm(Model model) {
        model.addAttribute(CREATE_LESSON_REQUEST, new CreateLessonRequest());
        model.addAttribute(LESSON_TIMES, lessonTimeService.getAll());
        model.addAttribute(AUDITORIUMS, auditoriumService.getAll());

        return CREATE_LESSON_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveLesson(@Valid @ModelAttribute CreateLessonRequest createLessonRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(LESSON_TIMES, lessonTimeService.getAll());
            model.addAttribute(AUDITORIUMS, auditoriumService.getAll());

            return CREATE_LESSON_FORM;
        }
        lessonService.save(createLessonRequest);

        return REDIRECT_LESSONS;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getAllLessons(Model model) {
        model.addAttribute(LESSONS, lessonService.getAll());

        return ALL_LESSONS_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getLessonById(@PathVariable Long id, Model model) {
        model.addAttribute(LESSON, lessonService.getById(id));

        return ONE_LESSON_PAGE;
    }

    @GetMapping(path = "/{username}/date-schedule")
    @PreAuthorize("hasAnyAuthority('ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getDateLessonsSchedule(@PathVariable String username, @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate selectedDate, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDto user = userService.getByUsername(authentication.getName());
        if (!user.getUsername().equals(username)) {
            return REDIRECT;
        }

        LocalDate scheduleDate = (selectedDate != null) ? selectedDate : LocalDate.now();
        model.addAttribute(SELECTED_DATE, scheduleDate);
        model.addAttribute(USER, user);

        if (user.getRoles()
                .stream()
                .anyMatch(role -> role.getName().equals(ROLE_TEACHER))) {
            model.addAttribute(LESSONS, lessonService.getAllByDate(scheduleDate, teacherService.getByUsername(user.getUsername())));

            return DATE_LESSONS_SCHEDULE_PAGE;
        } else if (user.getRoles()
                .stream()
                .anyMatch(role -> role.getName().equals(ROLE_STUDENT))) {
            model.addAttribute(LESSONS, lessonService.getAllByDate(scheduleDate, studentService.getByUsername(user.getUsername())));

            return DATE_LESSONS_SCHEDULE_PAGE;
        }

        return REDIRECT;
    }

    @GetMapping(path = "/{username}/week-schedule")
    @PreAuthorize("hasAnyAuthority('ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getWeekLessonsSchedule(@PathVariable String username, @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate selectedDate, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDto user = userService.getByUsername(authentication.getName());
        if (!user.getUsername().equals(username)) {
            return REDIRECT;
        }

        LocalDate scheduleDate = (selectedDate != null) ? selectedDate : LocalDate.now();
        LocalDate startWeekScheduleDate = scheduleDate.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate endWeekScheduleDate = startWeekScheduleDate.plusDays(DayOfWeek.SATURDAY.getValue());
        model.addAttribute(SELECTED_DATE, scheduleDate);
        model.addAttribute(START_WEEK_SCHEDULE_DATE, startWeekScheduleDate);
        model.addAttribute(END_WEEK_SCHEDULE_DATE, endWeekScheduleDate);
        model.addAttribute(USER, user);

        if (user.getRoles()
                .stream()
                .anyMatch(role -> role.getName().equals(ROLE_TEACHER))) {
            model.addAttribute(LESSONS, lessonService.getAllByWeek(startWeekScheduleDate, endWeekScheduleDate, teacherService.getByUsername(user.getUsername())));

            return WEEK_LESSONS_SCHEDULE_PAGE;
        } else if (user.getRoles()
                .stream()
                .anyMatch(role -> role.getName().equals(ROLE_STUDENT))) {
            model.addAttribute(LESSONS, lessonService.getAllByWeek(startWeekScheduleDate, endWeekScheduleDate, studentService.getByUsername(user.getUsername())));

            return WEEK_LESSONS_SCHEDULE_PAGE;
        }

        return REDIRECT;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateLessonForm(@PathVariable Long id, Model model) {
        LessonDto lesson = lessonService.getById(id);
        model.addAttribute(UPDATE_LESSON_REQUEST, new UpdateLessonRequest(lesson.getDate()));
        model.addAttribute(LESSON, lesson);
        model.addAttribute(LESSON_TIMES, lessonTimeService.getAllNoneMatching(lesson.getLessonTime()));
        model.addAttribute(AUDITORIUMS, auditoriumService.getAllNoneMatching(lesson.getAuditorium()));
        model.addAttribute(COURSES_TO_UPDATE, courseService.getAllNoneMatching(lesson.getCourse()));
        model.addAttribute(COURSE_TO_REMOVE, lesson.getCourse());
        model.addAttribute(TEACHERS_TO_UPDATE, teacherService.getAllNoneMatching(lesson.getTeacher()));
        model.addAttribute(TEACHER_TO_REMOVE, lesson.getTeacher());
        model.addAttribute(GROUPS_TO_ADD, groupService.getAllNoneMatching(lesson.getGroups()));
        model.addAttribute(GROUPS_TO_REMOVE, lesson.getGroups());

        return UPDATE_LESSON_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateLesson(@PathVariable Long id, @Valid @ModelAttribute UpdateLessonRequest updateLessonRequest, BindingResult result, Model model) {
        LessonDto lesson = lessonService.getById(id);
        if (result.hasErrors()) {
            model.addAttribute(LESSON, lesson);
            model.addAttribute(LESSON_TIMES, lessonTimeService.getAllNoneMatching(lesson.getLessonTime()));
            model.addAttribute(AUDITORIUMS, auditoriumService.getAllNoneMatching(lesson.getAuditorium()));
            model.addAttribute(COURSES_TO_UPDATE, courseService.getAllNoneMatching(lesson.getCourse()));
            model.addAttribute(COURSE_TO_REMOVE, lesson.getCourse());
            model.addAttribute(TEACHERS_TO_UPDATE, teacherService.getAllNoneMatching(lesson.getTeacher()));
            model.addAttribute(TEACHER_TO_REMOVE, lesson.getTeacher());
            model.addAttribute(GROUPS_TO_ADD, groupService.getAllNoneMatching(lesson.getGroups()));
            model.addAttribute(GROUPS_TO_REMOVE, lesson.getGroups());

            return UPDATE_LESSON_FORM;
        }
        lessonService.updateDate(id, updateLessonRequest);
        lessonService.updateLessonTime(id, updateLessonRequest);
        lessonService.updateAuditorium(id, updateLessonRequest);
        if (!updateLessonRequest.getCourseNameToRemove().isEmpty()) {
            lessonService.removeCourse(id, updateLessonRequest);
        }
        if (!updateLessonRequest.getCourseNameToUpdate().isEmpty()) {
            lessonService.updateCourse(id, updateLessonRequest);
        }
        if (!updateLessonRequest.getTeacherIdToRemove().equals(Long.MIN_VALUE)) {
            lessonService.removeTeacher(id, updateLessonRequest);
        }
        if (!updateLessonRequest.getTeacherIdToUpdate().equals(Long.MIN_VALUE)) {
            lessonService.updateTeacher(id, updateLessonRequest);
        }
        if (!updateLessonRequest.getGroupNameToAdd().isEmpty()) {
            lessonService.addGroup(id, updateLessonRequest);
        }
        if (!updateLessonRequest.getGroupNameToRemove().isEmpty()) {
            lessonService.removeGroup(id, updateLessonRequest);
        }

        return REDIRECT_LESSONS;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteLesson(@PathVariable Long id) {
        lessonService.deleteById(id);

        return REDIRECT_LESSONS;
    }
}
