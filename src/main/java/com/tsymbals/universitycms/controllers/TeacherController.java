package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.TeacherDto;
import com.tsymbals.universitycms.models.dto.requests.CreateTeacherRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateTeacherRequest;
import com.tsymbals.universitycms.services.CourseService;
import com.tsymbals.universitycms.services.GroupService;
import com.tsymbals.universitycms.services.TeacherService;
import com.tsymbals.universitycms.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/teachers")
@RequiredArgsConstructor
public class TeacherController {

    private static final String CREATE_TEACHER_REQUEST = "createTeacherRequest";
    private static final String UPDATE_TEACHER_REQUEST = "updateTeacherRequest";
    private static final String USERS = "users";
    private static final String TEACHER = "teacher";
    private static final String TEACHERS = "teachers";
    private static final String COURSES_TO_ADD = "coursesToAdd";
    private static final String COURSES_TO_REMOVE = "coursesToRemove";
    private static final String GROUPS_TO_ADD = "groupsToAdd";
    private static final String GROUPS_TO_REMOVE = "groupsToRemove";

    private static final String CREATE_TEACHER_FORM = "pages/teachers/create-teacher";
    private static final String ALL_TEACHERS_PAGE = "pages/teachers/all-teachers";
    private static final String ONE_TEACHER_PAGE = "pages/teachers/one-teacher";
    private static final String UPDATE_TEACHER_FORM = "pages/teachers/update-teacher";
    private static final String REDIRECT_TEACHERS = "redirect:/teachers";

    private final UserService userService;
    private final TeacherService teacherService;
    private final CourseService courseService;
    private final GroupService groupService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateTeacherForm(Model model) {
        model.addAttribute(CREATE_TEACHER_REQUEST, new CreateTeacherRequest());
        model.addAttribute(USERS, userService.getAllFreeTeacherUsers());

        return CREATE_TEACHER_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveTeacher(@Valid @ModelAttribute CreateTeacherRequest createTeacherRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(USERS, userService.getAllFreeTeacherUsers());

            return CREATE_TEACHER_FORM;
        }
        teacherService.save(createTeacherRequest);

        return REDIRECT_TEACHERS;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getAllTeachers(Model model) {
        model.addAttribute(TEACHERS, teacherService.getAll());

        return ALL_TEACHERS_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getTeacherById(@PathVariable Long id, Model model) {
        model.addAttribute(TEACHER, teacherService.getById(id));

        return ONE_TEACHER_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateTeacherForm(@PathVariable Long id, Model model) {
        TeacherDto teacher = teacherService.getById(id);
        model.addAttribute(UPDATE_TEACHER_REQUEST, new UpdateTeacherRequest(teacher.getLastName()));
        model.addAttribute(TEACHER, teacher);
        model.addAttribute(USERS, userService.getAllFreeTeacherUsers());
        model.addAttribute(COURSES_TO_ADD, courseService.getAllNoneMatching(teacher.getCourses()));
        model.addAttribute(COURSES_TO_REMOVE, teacher.getCourses());
        model.addAttribute(GROUPS_TO_ADD, groupService.getAllNoneMatching(teacher.getGroups()));
        model.addAttribute(GROUPS_TO_REMOVE, teacher.getGroups());

        return UPDATE_TEACHER_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateTeacher(@PathVariable Long id, @Valid @ModelAttribute UpdateTeacherRequest updateTeacherRequest, BindingResult result, Model model) {
        TeacherDto teacher = teacherService.getById(id);
        if (result.hasErrors()) {
            model.addAttribute(TEACHER, teacher);
            model.addAttribute(USERS, userService.getAllFreeTeacherUsers());
            model.addAttribute(COURSES_TO_ADD, courseService.getAllNoneMatching(teacher.getCourses()));
            model.addAttribute(COURSES_TO_REMOVE, teacher.getCourses());
            model.addAttribute(GROUPS_TO_ADD, groupService.getAllNoneMatching(teacher.getGroups()));
            model.addAttribute(GROUPS_TO_REMOVE, teacher.getGroups());

            return UPDATE_TEACHER_FORM;
        }
        teacherService.updateUser(id, updateTeacherRequest);
        teacherService.updateLastName(id, updateTeacherRequest);
        if (!updateTeacherRequest.getCourseNameToAdd().isEmpty()) {
            teacherService.addCourse(id, updateTeacherRequest);
        }
        if (!updateTeacherRequest.getCourseNameToRemove().isEmpty()) {
            teacherService.removeCourse(id, updateTeacherRequest);
        }
        if (!updateTeacherRequest.getGroupNameToAdd().isEmpty()) {
            teacherService.addGroup(id, updateTeacherRequest);
        }
        if (!updateTeacherRequest.getGroupNameToRemove().isEmpty()) {
            teacherService.removeGroup(id, updateTeacherRequest);
        }

        return REDIRECT_TEACHERS;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteTeacher(@PathVariable Long id) {
        teacherService.deleteById(id);

        return REDIRECT_TEACHERS;
    }
}
