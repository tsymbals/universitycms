package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.GroupDto;
import com.tsymbals.universitycms.models.dto.requests.CreateGroupRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateGroupRequest;
import com.tsymbals.universitycms.services.GroupService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/groups")
@RequiredArgsConstructor
public class GroupController {

    private static final String CREATE_GROUP_REQUEST = "createGroupRequest";
    private static final String UPDATE_GROUP_REQUEST = "updateGroupRequest";
    private static final String GROUP = "group";
    private static final String GROUPS = "groups";

    private static final String CREATE_GROUP_FORM = "pages/groups/create-group";
    private static final String ALL_GROUPS_PAGE = "pages/groups/all-groups";
    private static final String ONE_GROUP_PAGE = "pages/groups/one-group";
    private static final String UPDATE_GROUP_FORM = "pages/groups/update-group";
    private static final String REDIRECT_GROUPS = "redirect:/groups";

    private final GroupService groupService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateGroupForm(Model model) {
        model.addAttribute(CREATE_GROUP_REQUEST, new CreateGroupRequest());

        return CREATE_GROUP_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveGroup(@Valid @ModelAttribute CreateGroupRequest createGroupRequest, BindingResult result) {
        if (result.hasErrors()) {
            return CREATE_GROUP_FORM;
        }
        groupService.save(createGroupRequest);

        return REDIRECT_GROUPS;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getAllGroups(Model model) {
        model.addAttribute(GROUPS, groupService.getAll());

        return ALL_GROUPS_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getGroupById(@PathVariable Long id, Model model) {
        model.addAttribute(GROUP, groupService.getById(id));

        return ONE_GROUP_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateGroupForm(@PathVariable Long id, Model model) {
        GroupDto group = groupService.getById(id);
        model.addAttribute(UPDATE_GROUP_REQUEST, new UpdateGroupRequest(group.getName()));
        model.addAttribute(GROUP, group);

        return UPDATE_GROUP_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateGroup(@PathVariable Long id, @Valid @ModelAttribute UpdateGroupRequest updateGroupRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(GROUP, groupService.getById(id));

            return UPDATE_GROUP_FORM;
        }
        groupService.updateName(id, updateGroupRequest);

        return REDIRECT_GROUPS;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteGroup(@PathVariable Long id) {
        groupService.deleteById(id);

        return REDIRECT_GROUPS;
    }
}
