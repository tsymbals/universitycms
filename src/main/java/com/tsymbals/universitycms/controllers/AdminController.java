package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.requests.CreateAdminRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateAdminRequest;
import com.tsymbals.universitycms.services.AdminService;
import com.tsymbals.universitycms.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/admins")
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
public class AdminController {

    private static final String CREATE_ADMIN_REQUEST = "createAdminRequest";
    private static final String UPDATE_ADMIN_REQUEST = "updateAdminRequest";
    private static final String USERS = "users";
    private static final String ADMIN = "admin";
    private static final String ADMINS = "admins";

    private static final String CREATE_ADMIN_FORM = "pages/admins/create-admin";
    private static final String ALL_ADMINS_PAGE = "pages/admins/all-admins";
    private static final String ONE_ADMIN_PAGE = "pages/admins/one-admin";
    private static final String UPDATE_ADMIN_FORM = "pages/admins/update-admin";
    private static final String REDIRECT_ADMINS = "redirect:/admins";

    private final UserService userService;
    private final AdminService adminService;

    @GetMapping(path = "/create")
    public String getCreateAdminForm(Model model) {
        model.addAttribute(CREATE_ADMIN_REQUEST, new CreateAdminRequest());
        model.addAttribute(USERS, userService.getAllFreeAdminUsers());

        return CREATE_ADMIN_FORM;
    }

    @PostMapping(path = "/create")
    public String saveAdmin(@Valid @ModelAttribute CreateAdminRequest createAdminRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(USERS, userService.getAllFreeAdminUsers());

            return CREATE_ADMIN_FORM;
        }
        adminService.save(createAdminRequest);

        return REDIRECT_ADMINS;
    }

    @GetMapping
    public String getAllAdmins(Model model) {
        model.addAttribute(ADMINS, adminService.getAll());

        return ALL_ADMINS_PAGE;
    }

    @GetMapping(path = "/{id}")
    public String getAdminById(@PathVariable Long id, Model model) {
        model.addAttribute(ADMIN, adminService.getById(id));

        return ONE_ADMIN_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    public String getUpdateAdminForm(@PathVariable Long id, Model model) {
        model.addAttribute(UPDATE_ADMIN_REQUEST, new UpdateAdminRequest());
        model.addAttribute(ADMIN, adminService.getById(id));
        model.addAttribute(USERS, userService.getAllFreeAdminUsers());

        return UPDATE_ADMIN_FORM;
    }

    @PostMapping(path = "/{id}/update")
    public String updateAdmin(@PathVariable Long id, @ModelAttribute UpdateAdminRequest updateAdminRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(ADMIN, adminService.getById(id));
            model.addAttribute(USERS, userService.getAllFreeAdminUsers());

            return UPDATE_ADMIN_FORM;
        }
        adminService.updateUser(id, updateAdminRequest);

        return REDIRECT_ADMINS;
    }

    @GetMapping(path = "/{id}/delete")
    public String deleteAdmin(@PathVariable Long id) {
        adminService.deleteById(id);

        return REDIRECT_ADMINS;
    }
}
