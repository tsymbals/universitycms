package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.StudentDto;
import com.tsymbals.universitycms.models.dto.requests.CreateStudentRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateStudentRequest;
import com.tsymbals.universitycms.services.CourseService;
import com.tsymbals.universitycms.services.GroupService;
import com.tsymbals.universitycms.services.StudentService;
import com.tsymbals.universitycms.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/students")
@RequiredArgsConstructor
public class StudentController {

    private static final String CREATE_STUDENT_REQUEST = "createStudentRequest";
    private static final String UPDATE_STUDENT_REQUEST = "updateStudentRequest";
    private static final String USERS = "users";
    private static final String STUDENT = "student";
    private static final String STUDENTS = "students";
    private static final String GROUPS_TO_UPDATE = "groupsToUpdate";
    private static final String GROUP_TO_REMOVE = "groupToRemove";
    private static final String COURSES_TO_ADD = "coursesToAdd";
    private static final String COURSES_TO_REMOVE = "coursesToRemove";

    private static final String CREATE_STUDENT_FORM = "pages/students/create-student";
    private static final String ALL_STUDENTS_PAGE = "pages/students/all-students";
    private static final String ONE_STUDENT_PAGE = "pages/students/one-student";
    private static final String UPDATE_STUDENT_FORM = "pages/students/update-student";
    private static final String REDIRECT_STUDENTS = "redirect:/students";

    private final UserService userService;
    private final StudentService studentService;
    private final GroupService groupService;
    private final CourseService courseService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateStudentForm(Model model) {
        model.addAttribute(CREATE_STUDENT_REQUEST, new CreateStudentRequest());
        model.addAttribute(USERS, userService.getAllFreeStudentUsers());

        return CREATE_STUDENT_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveStudent(@Valid @ModelAttribute CreateStudentRequest createStudentRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(USERS, userService.getAllFreeStudentUsers());

            return CREATE_STUDENT_FORM;
        }
        studentService.save(createStudentRequest);

        return REDIRECT_STUDENTS;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getAllStudents(Model model) {
        model.addAttribute(STUDENTS, studentService.getAll());

        return ALL_STUDENTS_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getStudentById(@PathVariable Long id, Model model) {
        model.addAttribute(STUDENT, studentService.getById(id));

        return ONE_STUDENT_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateStudentForm(@PathVariable Long id, Model model) {
        StudentDto student = studentService.getById(id);
        model.addAttribute(UPDATE_STUDENT_REQUEST, new UpdateStudentRequest(student.getLastName()));
        model.addAttribute(STUDENT, student);
        model.addAttribute(USERS, userService.getAllFreeStudentUsers());
        model.addAttribute(GROUPS_TO_UPDATE, groupService.getAllNoneMatching(student.getGroup()));
        model.addAttribute(GROUP_TO_REMOVE, student.getGroup());
        model.addAttribute(COURSES_TO_ADD, courseService.getAllNoneMatching(student.getCourses()));
        model.addAttribute(COURSES_TO_REMOVE, student.getCourses());

        return UPDATE_STUDENT_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateStudent(@PathVariable Long id, @Valid @ModelAttribute UpdateStudentRequest updateStudentRequest, BindingResult result, Model model) {
        StudentDto student = studentService.getById(id);
        if (result.hasErrors()) {
            model.addAttribute(STUDENT, student);
            model.addAttribute(USERS, userService.getAllFreeStudentUsers());
            model.addAttribute(GROUPS_TO_UPDATE, groupService.getAllNoneMatching(student.getGroup()));
            model.addAttribute(GROUP_TO_REMOVE, student.getGroup());
            model.addAttribute(COURSES_TO_ADD, courseService.getAllNoneMatching(student.getCourses()));
            model.addAttribute(COURSES_TO_REMOVE, student.getCourses());

            return UPDATE_STUDENT_FORM;
        }
        studentService.updateUser(id, updateStudentRequest);
        if (!updateStudentRequest.getGroupNameToRemove().isEmpty()) {
            studentService.removeGroup(id, updateStudentRequest);
        }
        if (!updateStudentRequest.getGroupNameToUpdate().isEmpty()) {
            studentService.updateGroup(id, updateStudentRequest);
        }
        studentService.updateLastName(id, updateStudentRequest);
        if (!updateStudentRequest.getCourseNameToAdd().isEmpty()) {
            studentService.addCourse(id, updateStudentRequest);
        }
        if (!updateStudentRequest.getCourseNameToRemove().isEmpty()) {
            studentService.removeCourse(id, updateStudentRequest);
        }

        return REDIRECT_STUDENTS;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteStudent(@PathVariable Long id) {
        studentService.deleteById(id);

        return REDIRECT_STUDENTS;
    }
}
