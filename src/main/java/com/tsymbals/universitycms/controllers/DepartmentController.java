package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.DepartmentDto;
import com.tsymbals.universitycms.models.dto.requests.CreateDepartmentRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateDepartmentRequest;
import com.tsymbals.universitycms.services.DepartmentService;
import com.tsymbals.universitycms.services.UniversityService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/departments")
@RequiredArgsConstructor
public class DepartmentController {

    private static final String CREATE_DEPARTMENT_REQUEST = "createDepartmentRequest";
    private static final String UPDATE_DEPARTMENT_REQUEST = "updateDepartmentRequest";
    private static final String UNIVERSITIES = "universities";
    private static final String DEPARTMENT = "department";
    private static final String DEPARTMENTS = "departments";

    private static final String CREATE_DEPARTMENT_FORM = "pages/departments/create-department";
    private static final String ALL_DEPARTMENTS_PAGE = "pages/departments/all-departments";
    private static final String ONE_DEPARTMENT_PAGE = "pages/departments/one-department";
    private static final String UPDATE_DEPARTMENT_FORM = "pages/departments/update-department";
    private static final String REDIRECT_DEPARTMENTS = "redirect:/departments";

    private final UniversityService universityService;
    private final DepartmentService departmentService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateDepartmentForm(Model model) {
        model.addAttribute(CREATE_DEPARTMENT_REQUEST, new CreateDepartmentRequest());
        model.addAttribute(UNIVERSITIES, universityService.getAll());

        return CREATE_DEPARTMENT_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveDepartment(@Valid @ModelAttribute CreateDepartmentRequest createDepartmentRequest, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute(UNIVERSITIES, universityService.getAll());

            return CREATE_DEPARTMENT_FORM;
        }
        departmentService.save(createDepartmentRequest);

        return REDIRECT_DEPARTMENTS;
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getAllDepartments(Model model) {
        model.addAttribute(DEPARTMENTS, departmentService.getAll());

        return ALL_DEPARTMENTS_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN', 'ROLE_STAFF', 'ROLE_TEACHER', 'ROLE_STUDENT')")
    public String getDepartmentById(@PathVariable Long id, Model model) {
        model.addAttribute(DEPARTMENT, departmentService.getById(id));

        return ONE_DEPARTMENT_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateDepartmentForm(@PathVariable Long id, Model model) {
        DepartmentDto department = departmentService.getById(id);
        model.addAttribute(UPDATE_DEPARTMENT_REQUEST, new UpdateDepartmentRequest(department.getName()));
        model.addAttribute(DEPARTMENT, department);
        model.addAttribute(UNIVERSITIES, universityService.getAllNoneMatching(department.getUniversity()));

        return UPDATE_DEPARTMENT_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateDepartment(@PathVariable Long id, @Valid @ModelAttribute UpdateDepartmentRequest updateDepartmentRequest, BindingResult result, Model model) {
        DepartmentDto department = departmentService.getById(id);
        if (result.hasErrors()) {
            model.addAttribute(DEPARTMENT, department);
            model.addAttribute(UNIVERSITIES, universityService.getAllNoneMatching(department.getUniversity()));

            return UPDATE_DEPARTMENT_FORM;
        }
        departmentService.updateUniversity(id, updateDepartmentRequest);
        departmentService.updateName(id, updateDepartmentRequest);

        return REDIRECT_DEPARTMENTS;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteDepartment(@PathVariable Long id) {
        departmentService.deleteById(id);

        return REDIRECT_DEPARTMENTS;
    }
}
