package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateUserRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateUserRequest;
import com.tsymbals.universitycms.services.RoleService;
import com.tsymbals.universitycms.services.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path = "/users")
@RequiredArgsConstructor
public class UserController {

    private static final String CREATE_USER_REQUEST = "createUserRequest";
    private static final String UPDATE_USER_REQUEST = "updateUserRequest";
    private static final String USER = "user";
    private static final String USERS = "users";
    private static final String ROLES_TO_ADD = "rolesToAdd";
    private static final String ROLES_TO_REMOVE = "rolesToRemove";

    private static final String CREATE_USER_FORM = "pages/users/create-user";
    private static final String ALL_USERS_PAGE = "pages/users/all-users";
    private static final String ONE_USER_PAGE = "pages/users/one-user";
    private static final String UPDATE_USER_FORM = "pages/users/update-user";
    private static final String REDIRECT_USERS = "redirect:/users";
    private static final String REDIRECT = "redirect:/";

    private final UserService userService;
    private final RoleService roleService;

    @GetMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getCreateUserForm(Model model) {
        model.addAttribute(CREATE_USER_REQUEST, new CreateUserRequest());

        return CREATE_USER_FORM;
    }

    @PostMapping(path = "/create")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String saveUser(@Valid @ModelAttribute CreateUserRequest createUserRequest, BindingResult result) {
        if (result.hasErrors()) {
            return CREATE_USER_FORM;
        }
        userService.save(createUserRequest);

        return REDIRECT_USERS;
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getAllUsers(Model model) {
        model.addAttribute(USERS, userService.getAll());

        return ALL_USERS_PAGE;
    }

    @GetMapping(path = "/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUserById(@PathVariable Long id, Model model) {
        model.addAttribute(USER, userService.getById(id));

        return ONE_USER_PAGE;
    }

    @GetMapping(path = "/{username}/profile")
    @PreAuthorize("#username == authentication.principal.username")
    public String getLoggedInUserProfile(@PathVariable String username, Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDto user = userService.getByUsername(authentication.getName());
        if (!user.getUsername().equals(username)) {
            return REDIRECT;
        }
        model.addAttribute(USER, user);

        return ONE_USER_PAGE;
    }

    @GetMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String getUpdateUserForm(@PathVariable Long id, Model model) {
        UserDto user = userService.getById(id);
        model.addAttribute(UPDATE_USER_REQUEST, new UpdateUserRequest(user.getUsername()));
        model.addAttribute(USER, user);
        model.addAttribute(ROLES_TO_ADD, roleService.getAllNoneMatching(user.getRoles()));
        model.addAttribute(ROLES_TO_REMOVE, user.getRoles());

        return UPDATE_USER_FORM;
    }

    @PostMapping(path = "/{id}/update")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String updateUser(@PathVariable Long id, @Valid @ModelAttribute UpdateUserRequest updateUserRequest, BindingResult result, Model model) {
        UserDto user = userService.getById(id);
        if (result.hasErrors()) {
            model.addAttribute(USER, user);
            model.addAttribute(ROLES_TO_ADD, roleService.getAllNoneMatching(user.getRoles()));
            model.addAttribute(ROLES_TO_REMOVE, user.getRoles());

            return UPDATE_USER_FORM;
        }
        userService.updateUsername(id, updateUserRequest);
        userService.updatePassword(id, updateUserRequest);
        userService.updateEnabledStatus(id, updateUserRequest);
        if (!updateUserRequest.getRoleNameToAdd().isEmpty()) {
            userService.addRole(id, updateUserRequest);
        }
        if (!updateUserRequest.getRoleNameToRemove().isEmpty()) {
            userService.removeRole(id, updateUserRequest);
        }

        return REDIRECT_USERS;
    }

    @GetMapping(path = "/{id}/delete")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public String deleteUser(@PathVariable Long id) {
        userService.deleteById(id);

        return REDIRECT_USERS;
    }
}
