INSERT INTO universities (name, abbreviation_name, city, phone_number) VALUES ('National Technical University', 'NTU', 'Vice City', '+380(12)3456789');

INSERT INTO departments (university_id, name) VALUES (1, 'Software engineering');
INSERT INTO departments (university_id, name) VALUES (1, 'Computer engineering');
INSERT INTO departments (university_id, name) VALUES (1, 'Security of information and communication systems');
INSERT INTO departments (university_id, name) VALUES (1, 'Professional education. Digital technologies');
INSERT INTO departments (university_id, name) VALUES (1, 'System programming');
INSERT INTO departments (university_id, name) VALUES (1, 'Cybersecurity of critical systems');

-- 'Software engineering' department
INSERT INTO auditoriums (department_id, name) VALUES (1, '101');
INSERT INTO auditoriums (department_id, name) VALUES (1, '102');
INSERT INTO auditoriums (department_id, name) VALUES (1, '103');
-- 'Computer engineering' department
INSERT INTO auditoriums (department_id, name) VALUES (2, '201');
INSERT INTO auditoriums (department_id, name) VALUES (2, '202');
INSERT INTO auditoriums (department_id, name) VALUES (2, '203');
-- 'Security of information and communication systems' department
INSERT INTO auditoriums (department_id, name) VALUES (3, '301');
INSERT INTO auditoriums (department_id, name) VALUES (3, '302');
INSERT INTO auditoriums (department_id, name) VALUES (3, '303');
-- 'Professional education. Digital technologies' department
INSERT INTO auditoriums (department_id, name) VALUES (4, '401');
INSERT INTO auditoriums (department_id, name) VALUES (4, '402');
INSERT INTO auditoriums (department_id, name) VALUES (4, '403');
-- 'System programming' department
INSERT INTO auditoriums (department_id, name) VALUES (5, '501');
INSERT INTO auditoriums (department_id, name) VALUES (5, '502');
INSERT INTO auditoriums (department_id, name) VALUES (5, '503');
-- 'Cybersecurity of critical systems' department
INSERT INTO auditoriums (department_id, name) VALUES (6, '601');
INSERT INTO auditoriums (department_id, name) VALUES (6, '602');
INSERT INTO auditoriums (department_id, name) VALUES (6, '603');

INSERT INTO lesson_times (start_time, end_time) VALUES ('08:15', '9:00');
INSERT INTO lesson_times (start_time, end_time) VALUES ('09:15', '10:00');
INSERT INTO lesson_times (start_time, end_time) VALUES ('10:15', '11:00');
INSERT INTO lesson_times (start_time, end_time) VALUES ('11:15', '12:00');
INSERT INTO lesson_times (start_time, end_time) VALUES ('12:15', '13:00');
INSERT INTO lesson_times (start_time, end_time) VALUES ('13:15', '14:00');

-- 'Software engineering' department
INSERT INTO courses (name, description) VALUES ('Computer architecture', 'This course covers the fundamental principles of computer architecture, including hardware components, instruction sets, memory hierarchy, and system performance optimization.');
INSERT INTO courses (name, description) VALUES ('Basics of programming', 'This course introduces fundamental programming concepts, including algorithms, data structures, and coding in a beginner-friendly language.');
INSERT INTO courses (name, description) VALUES ('Software design', 'This course explores software design principles, emphasizing modularity, design patterns, and best practices for creating efficient, scalable, and maintainable software systems.');
-- 'Computer engineering' department
INSERT INTO courses (name, description) VALUES ('Computer logic', 'This course introduces the principles of computer logic, covering topics like Boolean algebra, logic gates, digital and sequential circuits, and their role in digital system design.');
INSERT INTO courses (name, description) VALUES ('System software', 'This course focuses on the design and implementation of system software, including operating systems, compilers, and utilities that manage hardware and provide services to applications.');
INSERT INTO courses (name, description) VALUES ('Computer systems', 'This course examines the structure and functioning of computer systems, covering topics such as hardware-software interaction, system components, storage, and performance optimization.');
-- 'Security of information and communication systems' department
INSERT INTO courses (name, description) VALUES ('Basics of information security', 'This course introduces the fundamental concepts of information security, including encryption, access control, threat assessment, and best practices for protecting data and systems.');
INSERT INTO courses (name, description) VALUES ('Applied cryptology', 'This course explores practical applications of cryptology, including encryption algorithms, cryptographic protocols, and techniques for securing communication and data in real-world scenarios.');
INSERT INTO courses (name, description) VALUES ('Complex information protection systems', 'This course delves into the design and implementation of complex information protection systems, focusing on encryption, network security, threat detection, and risk management strategies.');
-- 'Professional education. Digital technologies' department
INSERT INTO courses (name, description) VALUES ('Management of professional education', 'This course focuses on the strategies and practices involved in managing professional education, including curriculum development, instructional design, and methods for assessing and improving educational programs.');
INSERT INTO courses (name, description) VALUES ('Methods of distance education', 'This course explores various methods of distance education, including online learning technologies, instructional design principles, and strategies for effective learner engagement and assessment.');
INSERT INTO courses (name, description) VALUES ('Organization of digital educational environment', 'This course covers the design and implementation of digital educational environments, focusing on creating effective online learning platforms, integrating educational technologies, and enhancing student engagement and learning outcomes.');
-- 'System programming' department
INSERT INTO courses (name, description) VALUES ('Basics of system programming', 'This course introduces the fundamentals of system programming, covering topics such as low-level programming, system calls, memory management, and the interaction between software and hardware.');
INSERT INTO courses (name, description) VALUES ('Database management systems', 'This course explores the principles and practices of database management systems, including database design, query languages, transaction management, and techniques for ensuring data integrity and performance.');
INSERT INTO courses (name, description) VALUES ('Digital system engineering', 'This course covers the principles and practices of digital system engineering, focusing on the design, analysis, and implementation of digital circuits and systems, including hardware description languages and system integration.');
-- 'Cybersecurity of critical systems' department
INSERT INTO courses (name, description) VALUES ('Basics of cybersecurity', 'This course introduces fundamental concepts of cybersecurity, including risk management, threat analysis, protective measures, and strategies for safeguarding digital information and systems.');
INSERT INTO courses (name, description) VALUES ('Cybersecurity monitoring and auditing', 'This course focuses on techniques and tools for cybersecurity monitoring and auditing, including real-time threat detection, log analysis, and assessment methods to ensure the integrity and security of information systems.');
INSERT INTO courses (name, description) VALUES ('Techniques for building a secure network application', 'This course covers techniques for developing secure network applications, including best practices for secure coding, encryption, authentication, and vulnerability assessment to protect against common threats and attacks.');

-- 'Software engineering' department
INSERT INTO groups (name) VALUES ('1SE-24');
INSERT INTO groups (name) VALUES ('2SE-24');
-- 'Computer engineering' department
INSERT INTO groups (name) VALUES ('1CE-24');
-- 'Security of information and communication systems' department
INSERT INTO groups (name) VALUES ('1SS-24');
-- 'Professional education. Digital technologies' department
INSERT INTO groups (name) VALUES ('1PE-24');
-- 'System programming' department
INSERT INTO groups (name) VALUES ('1SP-24');
INSERT INTO groups (name) VALUES ('2SP-24');
-- 'Cybersecurity of critical systems' department
INSERT INTO groups (name) VALUES ('1CS-24');

INSERT INTO roles (name) VALUES ('ROLE_ADMIN');
INSERT INTO roles (name) VALUES ('ROLE_STAFF');
INSERT INTO roles (name) VALUES ('ROLE_TEACHER');
INSERT INTO roles (name) VALUES ('ROLE_STUDENT');

-- Admins
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('admin', '$2a$10$lIKMLY.XiQcczZEkkfHdyOwQJLph/RjjW6QenmIXLZ5JIsbvytBsy', true, false); -- Admin001
-- Staff
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('employee', '$2a$10$951cWWf77K4gXeZCXoKvQOWN51TiK2gElsGRy/90zu2/ZWqRlxsdm', true, false); -- Employee1
-- Teachers
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher01', '$2a$10$PyMNgDxbF8rzc/64MvZVpuROlhTt4BAS59XR8plL7x8PvZeRN6nti', true, false); -- Teacher1
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher02', '$2a$10$occj07ZCZLz0WoLDRRlxHuNyvFK1wU27CFzCb0Tm1lMJO2RYErvIG', true, false); -- Teacher2
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher03', '$2a$10$Nfs14.5StTx71Gme8ZqSCucPbz5jQ6yZDF0iJ3rRMBkWkhu0E6n0u', true, false); -- Teacher3
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher04', '$2a$10$deLfc0eFHh7qVqujOj8w7e.bgwMpUpY8ybS0Vk9HMl4iXRU.SDYmC', true, false); -- Teacher4
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher05', '$2a$10$nN6vcY.1IngGSO/Le/jxj.LdMaMCRi5dz1xCafw.5TvxDuT1t.Y4q', true, false); -- Teacher5
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher06', '$2a$10$6zlEo0/UCyF/2eWnoBfcNezC5Ff56m65wUUN95jCZ0pMzj1hNOxQq', true, false); -- Teacher6
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher07', '$2a$10$aG6lscIpQzcpOehr6Fy3r.0TSaJTw.Y85KBx6vDqm28BmD5OuBbkK', true, false); -- Teacher7
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher08', '$2a$10$wzJatq4MFZiZvd98oieLrO6HOBzEai7C765/3gz7U98lFheM8Zga2', true, false); -- Teacher8
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher09', '$2a$10$U1ZTA5Fzox4NMxfSv5gGP.lhAu0AkuHi4jeEKjNnByvk7YH7kaJ.O', true, false); -- Teacher9
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher10', '$2a$10$cmr98C7HolaoOLAl6YBNMONu0XCSzNSxmteHvVAl68aykxP3m9hIe', true, false); -- Teacher10
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher11', '$2a$10$Zxr59DVcg0e9OhwLXbVa.urtP3KOYPqkmCn0lKJne8GU1AeCcwKqu', true, false); -- Teacher11
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher12', '$2a$10$ag1WowK7RmfL07ZMCClHk.7htK8zH02DivZZtn/cY9VRIb09t17Fq', true, false); -- Teacher12
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher13', '$2a$10$rCcb4uISPEOXcFpi8aXIr.t0zZO3bvyngUtUgrMPEMVJ/SIPmzlNO', true, false); -- Teacher13
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('teacher14', '$2a$10$YuDBb2AzZiHcTX12eqwUXOyJrngXCJdZgDWu7/rM1tg/2q2qDSFBG', true, false); -- Teacher14
-- Students
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student01', '$2a$10$hCOUgcUyrMw8N5n6.AFS6eO23vM9HvXw7pJuy3YVo/FPtTm04Bqtq', true, false); -- Student1
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student02', '$2a$10$/CxdHE7fSoG2Pgij2ITUkeyYtiLVZSUMgKy4QtSIc3M9EDEIL2NvC', true, false); -- Student2
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student03', '$2a$10$RQAfGip4dvb.Ozy6nlD50.1KCAo6ZHDVDLK2qF/4zCkq3zHrsOoHW', true, false); -- Student3
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student04', '$2a$10$ucAI3xlRiaPHitBxtu.lzeIJUTg44BoNzL3x8fDE957dkR8hfXTv.', true, false); -- Student4
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student05', '$2a$10$NYs8aKeuL2iq4V0VsHgJmekMEKx93M8nKmhnlKNgqx6jh3oEFQLfO', true, false); -- Student5
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student06', '$2a$10$IBypM4gjTkkNNK8aBMfCUeD50EHI0Aw20XvFYv2prxne3oCf./gF.', true, false); -- Student6
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student07', '$2a$10$U5utarHFNYokzyzbwIdCXOBiTo18BqjLXKFzZsCRIh1t8PSt8iB4q', true, false); -- Student7
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student08', '$2a$10$qK8kN76efSfOze8tsBa3..MoHy42jv6u6LaHV9oy2wKPyFXMpIgJO', true, false); -- Student8
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student09', '$2a$10$VHk4mP2wwlUErv/PfEleMuKWPFzyQbeBHtqHVQSA5GVpNW9r60nPa', true, false); -- Student9
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student10', '$2a$10$6f53S0VDy5R6rEqsDqi6.O6EVewpzpfnGBxv75bDviMNhp8Yk13oi', true, false); -- Student10
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student11', '$2a$10$7EhJ4cyuxgSguj2R9IzAPekjim7QPOUNEdHgfu/a.bEhZJGfcTE2y', true, false); -- Student11
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student12', '$2a$10$oAPsSzUkZabzMkP.H64L4OjrHRW0NBCs83Eth9wZEKGs2rwBJXMNS', true, false); -- Student12
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student13', '$2a$10$R1ls5yo9KkAaN9JsHY/CoO6vXPdW49LIgE/8xptIAxCjTHg6Q58BO', true, false); -- Student13
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student14', '$2a$10$HniPiY0k5.hcJjMc.NvHD.eH2syaraWzGCjVjvCPwB0ZavaRWtEp2', true, false); -- Student14
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student15', '$2a$10$tAjZITsNQCem.toUPYuCd.THAicfZSocH5o2e3MlUiI9DVjl9WLv6', true, false); -- Student15
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student16', '$2a$10$yImYTwHZjjWSdF.CULoX4OpvH8V0DB5UsY7DqObDQ27q1lxvamhJW', true, false); -- Student16
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student17', '$2a$10$ntqv7V7HaenwtxtTCOvcEeiNEKIN.819Kzz5BTwyuUkQdVaUlAcny', true, false); -- Student17
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student18', '$2a$10$VlnkJLa.HPbfow0E.JXZiuhWpzQDH4PrwFNHtpt8TB223im5tSLke', true, false); -- Student18
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student19', '$2a$10$OT24PoAtItmmSJkzKk/Weeas7Ct5lV7h9c0qr/1LY58Kfd3pybL12', true, false); -- Student19
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student20', '$2a$10$VdXm68lJ.vKxoqFoTUMQ2.ZsncSCTnzzZDeE4nprFXXlXnJ9M62J.', true, false); -- Student20
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student21', '$2a$10$CZhfQNQADI.mx4YybMGW9eRqlH6RvesDJ2Z8hyTUIob1t8irODESy', true, false); -- Student21
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student22', '$2a$10$tL/28OO/D37KH5gTXhAnX.TazXPiP4SPe7mbG0AJ9/PRD6OKc7N2a', true, false); -- Student22
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student23', '$2a$10$KaRofuMSg5V1JFNQZN6N1eUdZeHY9QRHJ8VHPwbUR5BgwX5FdWXDq', true, false); -- Student23
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student24', '$2a$10$pRwrXKbgcP2oE3s4SIGbn.nxfohVlP034EKqqP2..FiRUqNfwj9pG', true, false); -- Student24
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student25', '$2a$10$it.DgyerlG/K6klw91NbEeCbSeUL41BntU1KN6bVWkscTnOHli8jK', true, false); -- Student25
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student26', '$2a$10$QNEnpiCWFDy.5LkeLRw/qOtCG5Lo0spTzf.9e3/8GuCIaa3X8dakS', true, false); -- Student26
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student27', '$2a$10$wr6HL.ZOVYXS7o79PkA4.Opz3L8l7DQlvZmy.Jj9TnKB71hn5HV7.', true, false); -- Student27
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student28', '$2a$10$3uzG4FcQ5LlmoYm6SZdMhOPobz5wD11PLfgpsm9o9ehoNVvvdXzQ.', true, false); -- Student28
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student29', '$2a$10$KAxI9TsSAi7VF30E6GD9HOr/98EotKLyLvaqx2u6EFd1o/OWzH7AK', true, false); -- Student29
INSERT INTO users (username, password, enabled_status, free_status) VALUES ('student30', '$2a$10$wvp23QIFFDB40VPm2b/jp.V99NEGwDqguZOmv7TSBT711cek0DGsi', true, false); -- Student30

INSERT INTO admins (user_id) VALUES (1);

INSERT INTO employees (user_id, first_name, last_name) VALUES (2, 'Aimee', 'Simon');

-- 'Computer architecture' course
INSERT INTO teachers (user_id, first_name, last_name) VALUES (3, 'Tracie', 'Davenport');
-- 'Basics of programming' course
INSERT INTO teachers (user_id, first_name, last_name) VALUES (4, 'Deb', 'Simpson');
-- 'Software design' course
INSERT INTO teachers (user_id, first_name, last_name) VALUES (5, 'Tony', 'Robbins');
-- 'Computer logic', 'Computer systems' courses
INSERT INTO teachers (user_id, first_name, last_name) VALUES (6, 'Faith', 'Donahue');
-- 'System software' course
INSERT INTO teachers (user_id, first_name, last_name) VALUES (7, 'Loretta', 'Rowe');
-- 'Basics of information security', 'Applied cryptology' courses
INSERT INTO teachers (user_id, first_name, last_name) VALUES (8, 'Ingrid', 'Ryan');
-- 'Complex information protection systems' course
INSERT INTO teachers (user_id, first_name, last_name) VALUES (9, 'Marilyn', 'Phillips');
-- 'Management of professional education' course
INSERT INTO teachers (user_id, first_name, last_name) VALUES (10, 'Christina', 'Chaney');
-- 'Methods of distance education', 'Organization of digital educational environment' courses
INSERT INTO teachers (user_id, first_name, last_name) VALUES (11, 'Jay', 'Reid');
-- 'Basics of system programming' course
INSERT INTO teachers (user_id, first_name, last_name) VALUES (12, 'Jon', 'Gilbert');
-- 'Database management systems' course
INSERT INTO teachers (user_id, first_name, last_name) VALUES (13, 'Young', 'Howell');
-- 'Digital system engineering' course
INSERT INTO teachers (user_id, first_name, last_name) VALUES (14, 'Arthur', 'Barrett');
-- 'Basics of cybersecurity', 'Cybersecurity monitoring and auditing' courses
INSERT INTO teachers (user_id, first_name, last_name) VALUES (15, 'Bonnie', 'Jennings');
-- 'Techniques for building a secure network application' course
INSERT INTO teachers (user_id, first_name, last_name) VALUES (16, 'Mona', 'Santos');

-- '1SE-24' group
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (17, 1, 'Pamela', 'Kenney');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (18, 1, 'Johnnie', 'Roe');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (19, 1, 'Elisa', 'Barker');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (20, 1, 'Leona', 'Durham');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (21, 1, 'Lawrence', 'Ballard');
-- '2SE-24' group
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (22, 2, 'Marissa', 'Joyce');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (23, 2, 'Yvette', 'Zimmerman');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (24, 2, 'Santiago', 'Lockwood');
-- '1CE-24' group
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (25, 3, 'Jose', 'Andrade');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (26, 3, 'Regina', 'Alexander');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (27, 3, 'Paul', 'Orozco');
-- '1SS-24' group
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (28, 4, 'Ernest', 'Vance');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (29, 4, 'Candace', 'Walls');
-- '1PE-24' group
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (30, 5, 'Aimee', 'Baker');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (31, 5, 'Viola', 'William');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (32, 5, 'Norma', 'Ackerman');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (33, 5, 'Roberto', 'Wilkins');
-- '1SP-24' group
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (34, 6, 'Luz', 'Koch');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (35, 6, 'Tim', 'Farley');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (36, 6, 'Les', 'Stone');
-- '2SP-24' group
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (37, 7, 'Casey', 'Hooper');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (38, 7, 'Jen', 'Morse');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (39, 7, 'Evan', 'Burgess');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (40, 7, 'Allan', 'Henry');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (41, 7, 'Jennie', 'Gallagher');
-- '1CS-24' group
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (42, 8, 'Dan', 'House');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (43, 8, 'Krista', 'Cotton');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (44, 8, 'Troy', 'Sawyer');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (45, 8, 'Georgia', 'Zimmerman');
INSERT INTO students (user_id, group_id, first_name, last_name) VALUES (46, 8, 'Heather', 'Dixon');

-- '1SE-24', '2SE-24' groups for 25.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 1, 1, 1, 1);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 2, 1, 1, 1);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 3, 2, 2, 2);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 4, 2, 2, 2);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 5, 3, 3, 3);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 6, 3, 3, 3);
-- '1CE-24' group for 25.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 1, 6, 6, 4);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 2, 6, 6, 4);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 3, 5, 5, 5);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 4, 5, 5, 5);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 5, 4, 4, 4);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 6, 4, 4, 4);
-- '1SS-24' group for 25.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 1, 8, 7, 6);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 2, 8, 7, 6);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 3, 7, 9, 7);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 4, 7, 9, 7);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 5, 9, 8, 6);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 6, 9, 8, 6);
-- '1PE-24' group for 25.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 1, 10, 11, 9);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 2, 10, 11, 9);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 3, 12, 10, 8);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 4, 12, 10, 8);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 5, 11, 12, 9);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 6, 11, 12, 9);
-- '1SP-24', '2SP-24' groups for 25.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 1, 14, 15, 12);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 2, 14, 15, 12);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 3, 15, 13, 10);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 4, 15, 13, 10);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 5, 13, 14, 11);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 6, 13, 14, 11);
-- '1CS-24' group for 25.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 1, 18, 17, 13);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 2, 18, 17, 13);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 3, 16, 18, 14);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 4, 16, 18, 14);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 5, 17, 16, 13);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-25', 6, 17, 16, 13);
-- '1SE-24', '2SE-24' groups for 26.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 1, 1, 1, 1);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 2, 1, 1, 1);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 3, 2, 2, 2);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 4, 2, 2, 2);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 5, 3, 3, 3);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 6, 3, 3, 3);
-- '1CE-24' group for 26.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 1, 6, 6, 4);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 2, 6, 6, 4);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 3, 5, 5, 5);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 4, 5, 5, 5);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 5, 4, 4, 4);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 6, 4, 4, 4);
-- '1SS-24' group for 26.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 1, 8, 7, 6);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 2, 8, 7, 6);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 3, 7, 9, 7);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 4, 7, 9, 7);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 5, 9, 8, 6);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 6, 9, 8, 6);
-- '1PE-24' group for 26.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 1, 10, 11, 9);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 2, 10, 11, 9);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 3, 12, 10, 8);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 4, 12, 10, 8);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 5, 11, 12, 9);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 6, 11, 12, 9);
-- '1SP-24', '2SP-24' groups for 26.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 1, 14, 15, 12);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 2, 14, 15, 12);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 3, 15, 13, 10);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 4, 15, 13, 10);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 5, 13, 14, 11);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 6, 13, 14, 11);
-- '1CS-24' group for 26.11.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 1, 18, 17, 13);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 2, 18, 17, 13);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 3, 16, 18, 14);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 4, 16, 18, 14);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 5, 17, 16, 13);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-11-26', 6, 17, 16, 13);
-- '1SE-24', '2SE-24' groups for 02.12.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 1, 1, 1, 1);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 2, 1, 1, 1);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 3, 2, 2, 2);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 4, 2, 2, 2);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 5, 3, 3, 3);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 6, 3, 3, 3);
-- '1CE-24' group for 02.12.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 1, 6, 6, 4);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 2, 6, 6, 4);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 3, 5, 5, 5);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 4, 5, 5, 5);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 5, 4, 4, 4);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 6, 4, 4, 4);
-- '1SS-24' group for 02.12.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 1, 8, 7, 6);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 2, 8, 7, 6);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 3, 7, 9, 7);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 4, 7, 9, 7);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 5, 9, 8, 6);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 6, 9, 8, 6);
-- '1PE-24' group for 02.12.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 1, 10, 11, 9);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 2, 10, 11, 9);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 3, 12, 10, 8);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 4, 12, 10, 8);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 5, 11, 12, 9);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 6, 11, 12, 9);
-- '1SP-24', '2SP-24' groups for 02.12.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 1, 14, 15, 12);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 2, 14, 15, 12);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 3, 15, 13, 10);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 4, 15, 13, 10);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 5, 13, 14, 11);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 6, 13, 14, 11);
-- '1CS-24' group for 02.12.2024
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 1, 18, 17, 13);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 2, 18, 17, 13);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 3, 16, 18, 14);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 4, 16, 18, 14);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 5, 17, 16, 13);
INSERT INTO lessons (date, lesson_time_id, auditorium_id, course_id, teacher_id) VALUES ('2024-12-02', 6, 17, 16, 13);

INSERT INTO roles_users (role_id, user_id) VALUES (1, 1);
INSERT INTO roles_users (role_id, user_id) VALUES (2, 2);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 3);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 4);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 5);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 6);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 7);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 8);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 9);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 10);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 11);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 12);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 13);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 14);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 15);
INSERT INTO roles_users (role_id, user_id) VALUES (3, 16);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 17);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 18);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 19);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 20);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 21);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 22);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 23);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 24);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 25);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 26);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 27);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 28);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 29);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 30);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 31);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 32);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 33);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 34);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 35);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 36);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 37);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 38);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 39);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 40);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 41);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 42);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 43);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 44);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 45);
INSERT INTO roles_users (role_id, user_id) VALUES (4, 46);

INSERT INTO courses_students (course_id, student_id) VALUES (1, 1);
INSERT INTO courses_students (course_id, student_id) VALUES (1, 2);
INSERT INTO courses_students (course_id, student_id) VALUES (1, 3);
INSERT INTO courses_students (course_id, student_id) VALUES (1, 4);
INSERT INTO courses_students (course_id, student_id) VALUES (1, 5);
INSERT INTO courses_students (course_id, student_id) VALUES (1, 6);
INSERT INTO courses_students (course_id, student_id) VALUES (1, 7);
INSERT INTO courses_students (course_id, student_id) VALUES (1, 8);
INSERT INTO courses_students (course_id, student_id) VALUES (2, 1);
INSERT INTO courses_students (course_id, student_id) VALUES (2, 2);
INSERT INTO courses_students (course_id, student_id) VALUES (2, 3);
INSERT INTO courses_students (course_id, student_id) VALUES (2, 4);
INSERT INTO courses_students (course_id, student_id) VALUES (2, 5);
INSERT INTO courses_students (course_id, student_id) VALUES (2, 6);
INSERT INTO courses_students (course_id, student_id) VALUES (2, 7);
INSERT INTO courses_students (course_id, student_id) VALUES (2, 8);
INSERT INTO courses_students (course_id, student_id) VALUES (3, 1);
INSERT INTO courses_students (course_id, student_id) VALUES (3, 2);
INSERT INTO courses_students (course_id, student_id) VALUES (3, 3);
INSERT INTO courses_students (course_id, student_id) VALUES (3, 4);
INSERT INTO courses_students (course_id, student_id) VALUES (3, 5);
INSERT INTO courses_students (course_id, student_id) VALUES (3, 6);
INSERT INTO courses_students (course_id, student_id) VALUES (3, 7);
INSERT INTO courses_students (course_id, student_id) VALUES (3, 8);
INSERT INTO courses_students (course_id, student_id) VALUES (4, 9);
INSERT INTO courses_students (course_id, student_id) VALUES (4, 10);
INSERT INTO courses_students (course_id, student_id) VALUES (4, 11);
INSERT INTO courses_students (course_id, student_id) VALUES (5, 9);
INSERT INTO courses_students (course_id, student_id) VALUES (5, 10);
INSERT INTO courses_students (course_id, student_id) VALUES (5, 11);
INSERT INTO courses_students (course_id, student_id) VALUES (6, 9);
INSERT INTO courses_students (course_id, student_id) VALUES (6, 10);
INSERT INTO courses_students (course_id, student_id) VALUES (6, 11);
INSERT INTO courses_students (course_id, student_id) VALUES (7, 12);
INSERT INTO courses_students (course_id, student_id) VALUES (7, 13);
INSERT INTO courses_students (course_id, student_id) VALUES (8, 12);
INSERT INTO courses_students (course_id, student_id) VALUES (8, 13);
INSERT INTO courses_students (course_id, student_id) VALUES (9, 12);
INSERT INTO courses_students (course_id, student_id) VALUES (9, 13);
INSERT INTO courses_students (course_id, student_id) VALUES (10, 14);
INSERT INTO courses_students (course_id, student_id) VALUES (10, 15);
INSERT INTO courses_students (course_id, student_id) VALUES (10, 16);
INSERT INTO courses_students (course_id, student_id) VALUES (10, 17);
INSERT INTO courses_students (course_id, student_id) VALUES (11, 14);
INSERT INTO courses_students (course_id, student_id) VALUES (11, 15);
INSERT INTO courses_students (course_id, student_id) VALUES (11, 16);
INSERT INTO courses_students (course_id, student_id) VALUES (11, 17);
INSERT INTO courses_students (course_id, student_id) VALUES (12, 14);
INSERT INTO courses_students (course_id, student_id) VALUES (12, 15);
INSERT INTO courses_students (course_id, student_id) VALUES (12, 16);
INSERT INTO courses_students (course_id, student_id) VALUES (12, 17);
INSERT INTO courses_students (course_id, student_id) VALUES (13, 18);
INSERT INTO courses_students (course_id, student_id) VALUES (13, 19);
INSERT INTO courses_students (course_id, student_id) VALUES (13, 20);
INSERT INTO courses_students (course_id, student_id) VALUES (13, 21);
INSERT INTO courses_students (course_id, student_id) VALUES (13, 22);
INSERT INTO courses_students (course_id, student_id) VALUES (13, 23);
INSERT INTO courses_students (course_id, student_id) VALUES (13, 24);
INSERT INTO courses_students (course_id, student_id) VALUES (13, 25);
INSERT INTO courses_students (course_id, student_id) VALUES (14, 18);
INSERT INTO courses_students (course_id, student_id) VALUES (14, 19);
INSERT INTO courses_students (course_id, student_id) VALUES (14, 20);
INSERT INTO courses_students (course_id, student_id) VALUES (14, 21);
INSERT INTO courses_students (course_id, student_id) VALUES (14, 22);
INSERT INTO courses_students (course_id, student_id) VALUES (14, 23);
INSERT INTO courses_students (course_id, student_id) VALUES (14, 24);
INSERT INTO courses_students (course_id, student_id) VALUES (14, 25);
INSERT INTO courses_students (course_id, student_id) VALUES (15, 18);
INSERT INTO courses_students (course_id, student_id) VALUES (15, 19);
INSERT INTO courses_students (course_id, student_id) VALUES (15, 20);
INSERT INTO courses_students (course_id, student_id) VALUES (15, 21);
INSERT INTO courses_students (course_id, student_id) VALUES (15, 22);
INSERT INTO courses_students (course_id, student_id) VALUES (15, 23);
INSERT INTO courses_students (course_id, student_id) VALUES (15, 24);
INSERT INTO courses_students (course_id, student_id) VALUES (15, 25);
INSERT INTO courses_students (course_id, student_id) VALUES (16, 26);
INSERT INTO courses_students (course_id, student_id) VALUES (16, 27);
INSERT INTO courses_students (course_id, student_id) VALUES (16, 28);
INSERT INTO courses_students (course_id, student_id) VALUES (16, 29);
INSERT INTO courses_students (course_id, student_id) VALUES (16, 30);
INSERT INTO courses_students (course_id, student_id) VALUES (17, 26);
INSERT INTO courses_students (course_id, student_id) VALUES (17, 27);
INSERT INTO courses_students (course_id, student_id) VALUES (17, 28);
INSERT INTO courses_students (course_id, student_id) VALUES (17, 29);
INSERT INTO courses_students (course_id, student_id) VALUES (17, 30);
INSERT INTO courses_students (course_id, student_id) VALUES (18, 26);
INSERT INTO courses_students (course_id, student_id) VALUES (18, 27);
INSERT INTO courses_students (course_id, student_id) VALUES (18, 28);
INSERT INTO courses_students (course_id, student_id) VALUES (18, 29);
INSERT INTO courses_students (course_id, student_id) VALUES (18, 30);

INSERT INTO courses_teachers (course_id, teacher_id) VALUES (1, 1);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (2, 2);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (3, 3);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (4, 4);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (5, 5);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (6, 4);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (7, 6);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (8, 6);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (9, 7);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (10, 8);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (11, 9);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (12, 9);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (13, 10);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (14, 11);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (15, 12);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (16, 13);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (17, 13);
INSERT INTO courses_teachers (course_id, teacher_id) VALUES (18, 14);

INSERT INTO groups_teachers (group_id, teacher_id) VALUES (1, 1);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (1, 2);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (1, 3);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (2, 1);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (2, 2);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (2, 3);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (3, 4);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (3, 5);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (4, 6);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (4, 7);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (5, 8);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (5, 9);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (6, 10);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (6, 11);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (6, 12);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (7, 10);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (7, 11);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (7, 12);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (8, 13);
INSERT INTO groups_teachers (group_id, teacher_id) VALUES (8, 14);

INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 1);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 2);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 3);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 4);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 5);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 6);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 1);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 2);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 3);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 4);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 5);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 6);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 7);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 8);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 9);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 10);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 11);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 12);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 13);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 14);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 15);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 16);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 17);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 18);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 19);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 20);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 21);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 22);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 23);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 24);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 25);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 26);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 27);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 28);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 29);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 30);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 25);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 26);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 27);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 28);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 29);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 30);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 31);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 32);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 33);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 34);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 35);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 36);

INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 37);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 38);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 39);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 40);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 41);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 42);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 37);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 38);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 39);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 40);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 41);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 42);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 43);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 44);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 45);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 46);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 47);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 48);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 49);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 50);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 51);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 52);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 53);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 54);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 55);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 56);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 57);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 58);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 59);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 60);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 61);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 62);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 63);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 64);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 65);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 66);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 61);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 62);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 63);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 64);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 65);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 66);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 67);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 68);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 69);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 70);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 71);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 72);

INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 73);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 74);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 75);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 76);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 77);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1, 78);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 73);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 74);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 75);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 76);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 77);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (2, 78);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 79);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 80);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 81);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 82);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 83);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (3, 84);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 85);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 86);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 87);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 88);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 89);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (4, 90);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 91);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 92);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 93);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 94);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 95);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (5, 96);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 97);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 98);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 99);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 100);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 101);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (6, 102);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 97);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 98);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 99);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 100);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 101);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (7, 102);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 103);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 104);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 105);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 106);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 107);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (8, 108);
