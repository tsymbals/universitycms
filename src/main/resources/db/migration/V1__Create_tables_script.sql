DROP TABLE IF EXISTS
    universities, departments, auditoriums, lesson_times, courses, groups, roles, users, admins, employees, teachers, students, lessons,
    roles_users, courses_teachers, courses_students, groups_teachers, groups_lessons
CASCADE;

CREATE TABLE IF NOT EXISTS universities(
    id BIGSERIAL NOT NULL,
    name CHARACTER VARYING (64) NOT NULL,
    abbreviation_name CHARACTER VARYING (8) NOT NULL,
    city CHARACTER VARYING (32) NOT NULL,
    phone_number CHARACTER VARYING (16) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS departments(
    id BIGSERIAL NOT NULL,
    university_id BIGINT NOT NULL,
    name CHARACTER VARYING (64) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (university_id) REFERENCES universities (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS auditoriums(
    id BIGSERIAL NOT NULL,
    department_id BIGINT NOT NULL,
    name CHARACTER VARYING (8) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (department_id) REFERENCES departments (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS lesson_times(
    id BIGSERIAL NOT NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS courses(
    id BIGSERIAL NOT NULL,
    name CHARACTER VARYING (128) NOT NULL,
    description CHARACTER VARYING (256) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS groups(
    id BIGSERIAL NOT NULL,
    name CHARACTER VARYING (8) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS roles(
    id BIGSERIAL NOT NULL,
    name CHARACTER VARYING (16) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS users(
    id BIGSERIAL NOT NULL,
    username CHARACTER VARYING (16) NOT NULL,
    password CHARACTER VARYING (64) NOT NULL,
    enabled_status BOOLEAN NOT NULL,
    free_status BOOLEAN NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS admins(
    id BIGSERIAL NOT NULL,
    user_id BIGINT NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS employees(
    id BIGSERIAL NOT NULL,
    user_id BIGINT NOT NULL,
    first_name CHARACTER VARYING (32) NOT NULL,
    last_name CHARACTER VARYING (32) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS teachers(
    id BIGSERIAL NOT NULL,
    user_id BIGINT NOT NULL,
    first_name CHARACTER VARYING (32) NOT NULL,
    last_name CHARACTER VARYING (32) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS students(
    id BIGSERIAL NOT NULL,
    user_id BIGINT NOT NULL,
    group_id BIGINT,
    first_name CHARACTER VARYING (32) NOT NULL,
    last_name CHARACTER VARYING (32) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (group_id) REFERENCES groups (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS lessons(
    id BIGSERIAL NOT NULL,
    date DATE NOT NULL,
    lesson_time_id BIGINT NOT NULL,
    auditorium_id BIGINT NOT NULL,
    course_id BIGINT,
    teacher_id BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (lesson_time_id) REFERENCES lesson_times (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (auditorium_id) REFERENCES auditoriums (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (course_id) REFERENCES courses (id) ON DELETE CASCADE ON UPDATE CASCADE,
    FOREIGN KEY (teacher_id) REFERENCES teachers (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS roles_users(
    role_id BIGINT,
    user_id BIGINT,
    PRIMARY KEY (user_id, role_id),
    CONSTRAINT roles_users_fk_1 FOREIGN KEY (role_id) REFERENCES roles (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT roles_users_fk_2 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS courses_teachers(
    course_id BIGINT,
    teacher_id BIGINT,
    PRIMARY KEY (course_id, teacher_id),
    CONSTRAINT courses_teachers_fk_1 FOREIGN KEY (course_id) REFERENCES courses (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT courses_teachers_fk_2 FOREIGN KEY (teacher_id) REFERENCES teachers (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS courses_students(
    course_id BIGINT,
    student_id BIGINT,
    PRIMARY KEY (course_id, student_id),
    CONSTRAINT courses_students_fk_1 FOREIGN KEY (course_id) REFERENCES courses (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT courses_students_fk_2 FOREIGN KEY (student_id) REFERENCES students (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS groups_teachers(
    group_id BIGINT,
    teacher_id BIGINT,
    PRIMARY KEY (group_id, teacher_id),
    CONSTRAINT groups_teachers_fk_1 FOREIGN KEY (group_id) REFERENCES groups (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT groups_teachers_fk_2 FOREIGN KEY (teacher_id) REFERENCES teachers (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE IF NOT EXISTS groups_lessons(
    group_id BIGINT,
    lesson_id BIGINT,
    PRIMARY KEY (group_id, lesson_id),
    CONSTRAINT groups_lessons_fk_1 FOREIGN KEY (group_id) REFERENCES groups (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT groups_lessons_fk_2 FOREIGN KEY (lesson_id) REFERENCES lessons (id) ON DELETE CASCADE ON UPDATE CASCADE
);
