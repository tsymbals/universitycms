package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.AdminDto;
import com.tsymbals.universitycms.models.dto.requests.CreateAdminRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateAdminRequest;
import com.tsymbals.universitycms.models.entities.Admin;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.repositories.AdminRepository;
import com.tsymbals.universitycms.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = AdminServiceImpl.class)
class AdminServiceImplTest {

    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final UserEntity USER_DATA = new UserEntity(1000L, "admin1", ENCODED_PASSWORD, true, true, Set.of());

    @MockBean
    UserRepository userRepository;

    @MockBean
    AdminRepository adminRepository;

    @Autowired
    AdminServiceImpl adminService;

    @Test
    void save_should_UpdateUserFreeStatus_SaveNewAdmin_ReturnAdminDto_when_CreateAdminRequestIsValid_UserByUsernameExists() {
        CreateAdminRequest createAdminRequest = new CreateAdminRequest("admin1");
        Admin adminData = new Admin(1000L, USER_DATA);
        when(userRepository.findByUsername(createAdminRequest.getUsername())).thenReturn(Optional.of(USER_DATA));
        when(adminRepository.save(any(Admin.class))).thenReturn(adminData);

        AdminDto expected = new AdminDto(1000L, USER_DATA);
        AdminDto actual = adminService.save(createAdminRequest);
        assertEquals(expected, actual);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.FALSE);
    }

    @Test
    void save_should_ThrowEntityNotFoundException_when_CreateAdminRequestIsValid_UserByUsernameDoesNotExist() {
        CreateAdminRequest createAdminRequest = new CreateAdminRequest("employee1");
        when(userRepository.findByUsername(createAdminRequest.getUsername())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> adminService.save(createAdminRequest));
    }

    @Test
    void getAll_should_ReturnListOfAdminDto_when_AdminsExist() {
        List<Admin> adminDataList = List.of(new Admin(1000L, USER_DATA));
        when(adminRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(adminDataList);

        List<AdminDto> expected = List.of(new AdminDto(1000L, USER_DATA));
        List<AdminDto> actual = adminService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfAdminDto_when_AdminsDoNotExist() {
        List<Admin> adminDataList = List.of();
        when(adminRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(adminDataList);

        List<AdminDto> expected = List.of();
        List<AdminDto> actual = adminService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ReturnAdminDto_when_AdminByIdExists() {
        Admin adminData = new Admin(1000L, USER_DATA);
        when(adminRepository.findById(1000L)).thenReturn(Optional.of(adminData));

        AdminDto expected = new AdminDto(1000L, USER_DATA);
        AdminDto actual = adminService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_AdminByIdDoesNotExist() {
        when(adminRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> adminService.getById(9999L));
    }

    @Test
    void updateUser_should_UpdateUserOfAdmin_ReturnUpdatedAdminDto_when_AdminByIdExists_UserByUsernameExists() {
        UpdateAdminRequest updateAdminRequest = new UpdateAdminRequest("admin2");
        Admin adminData = new Admin(1000L, USER_DATA);
        UserEntity userData = new UserEntity(1001L, "admin2", ENCODED_PASSWORD, true, true, Set.of());
        Admin updatedAdmin = new Admin(1000L, userData);
        when(adminRepository.findById(1000L)).thenReturn(Optional.of(adminData));
        when(userRepository.findByUsername(updateAdminRequest.getUsername())).thenReturn(Optional.of(userData));
        when(adminRepository.getReferenceById(1000L)).thenReturn(updatedAdmin);

        AdminDto expected = new AdminDto(1000L, userData);
        AdminDto actual = adminService.updateUser(1000L, updateAdminRequest);
        assertEquals(expected, actual);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.TRUE);
        verify(userRepository).updateFreeStatus(userData.getId(), Boolean.FALSE);
        verify(adminRepository).save(updatedAdmin);
    }

    @Test
    void updateUser_should_ThrowEntityNotFoundException_when_AdminByIdDoesNotExist() {
        UpdateAdminRequest updateAdminRequest = new UpdateAdminRequest("admin2");
        when(adminRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> adminService.updateUser(9999L, updateAdminRequest));
    }

    @Test
    void updateUser_should_ThrowEntityNotFoundException_when_UserByUsernameDoesNotExist() {
        UpdateAdminRequest updateAdminRequest = new UpdateAdminRequest("admin3");
        Admin adminData = new Admin(1000L, USER_DATA);
        when(adminRepository.findById(1000L)).thenReturn(Optional.of(adminData));
        when(userRepository.findByUsername(updateAdminRequest.getUsername())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> adminService.updateUser(1000L, updateAdminRequest));
    }

    @Test
    void deleteById_should_UpdateUserFreeStatus_DeleteAdminById_when_AdminByIdExists() {
        Admin adminData = new Admin(1000L, USER_DATA);
        when(adminRepository.findById(1000L)).thenReturn(Optional.of(adminData));

        adminService.deleteById(1000L);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.TRUE);
        verify(adminRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_AdminByIdDoesNotExist() {
        when(adminRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> adminService.deleteById(9999L));
    }
}
