package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.StudentDto;
import com.tsymbals.universitycms.models.dto.requests.CreateStudentRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateStudentRequest;
import com.tsymbals.universitycms.models.entities.Course;
import com.tsymbals.universitycms.models.entities.Group;
import com.tsymbals.universitycms.models.entities.Student;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.repositories.CourseRepository;
import com.tsymbals.universitycms.repositories.GroupRepository;
import com.tsymbals.universitycms.repositories.StudentRepository;
import com.tsymbals.universitycms.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = StudentServiceImpl.class)
class StudentServiceImplTest {

    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final UserEntity USER_DATA = new UserEntity(1000L, "student1", ENCODED_PASSWORD, true, true, Set.of());

    @MockBean
    UserRepository userRepository;

    @MockBean
    StudentRepository studentRepository;

    @MockBean
    GroupRepository groupRepository;

    @MockBean
    CourseRepository courseRepository;

    @Autowired
    StudentServiceImpl studentService;

    @Test
    void save_should_UpdateUserFreeStatus_SaveNewStudent_ReturnStudentDto_when_CreateStudentRequestIsValid_UserByUsernameExists() {
        CreateStudentRequest createStudentRequest = new CreateStudentRequest("student1", "Emily", "Hensley");
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(userRepository.findByUsername(createStudentRequest.getUsername())).thenReturn(Optional.of(USER_DATA));
        when(studentRepository.save(any(Student.class))).thenReturn(studentData);

        StudentDto expected = new StudentDto(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        StudentDto actual = studentService.save(createStudentRequest);
        assertEquals(expected, actual);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.FALSE);
    }

    @Test
    void save_should_ThrowEntityNotFoundException_when_CreateStudentRequestIsValid_UserByUsernameDoesNotExist() {
        CreateStudentRequest createStudentRequest = new CreateStudentRequest("admin", "Emily", "Hensley");
        when(userRepository.findByUsername(createStudentRequest.getUsername())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.save(createStudentRequest));
    }

    @Test
    void getAll_should_ReturnListOfStudentDto_when_StudentsExist() {
        List<Student> studentDataList = List.of(new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of()));
        when(studentRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(studentDataList);

        List<StudentDto> expected = List.of(new StudentDto(1000L, USER_DATA, null, "Emily", "Hensley", Set.of()));
        List<StudentDto> actual = studentService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfStudentDto_when_StudentsDoNotExist() {
        List<Student> studentDataList = List.of();
        when(studentRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(studentDataList);

        List<StudentDto> expected = List.of();
        List<StudentDto> actual = studentService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ReturnStudentDto_when_StudentByIdExists() {
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));

        StudentDto expected = new StudentDto(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        StudentDto actual = studentService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_StudentByIdDoesNotExist() {
        when(studentRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.getById(9999L));
    }

    @Test
    void getByUsername_should_ReturnStudentDto_when_StudentByUsernameExists() {
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(studentRepository.findByUser_Username("student1")).thenReturn(Optional.of(studentData));

        StudentDto expected = new StudentDto(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        StudentDto actual = studentService.getByUsername("student1");
        assertEquals(expected, actual);
    }

    @Test
    void getByUsername_should_ThrowEntityNotFoundException_when_StudentByUsernameDoesNotExist() {
        when(studentRepository.findByUser_Username("admin")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.getByUsername("admin"));
    }

    @Test
    void getByFirstNameAndLastName_should_ReturnStudentDto_when_StudentByFirstNameAndLastNameExists() {
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(studentRepository.findByFirstNameAndLastName("Emily", "Hensley")).thenReturn(Optional.of(studentData));

        StudentDto expected = new StudentDto(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        StudentDto actual = studentService.getByFirstNameAndLastName("Emily", "Hensley");
        assertEquals(expected, actual);
    }

    @Test
    void getByFirstNameAndLastName_should_ThrowEntityNotFoundException_when_StudentByFirstNameAndLastNameDoesNotExist() {
        when(studentRepository.findByFirstNameAndLastName("Therese", "McConnell")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.getByFirstNameAndLastName("Therese", "McConnell"));
    }

    @Test
    void updateUser_should_UpdateUserOfStudent_ReturnUpdatedStudentDto_when_StudentByIdExists_UserByUsernameExists() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest("student2", null, null, null, null, null);
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        UserEntity userData = new UserEntity(1001L, "student2", ENCODED_PASSWORD, true, true, Set.of());
        Student updatedStudent = new Student(1000L, userData, null, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(userRepository.findByUsername(updateStudentRequest.getUsername())).thenReturn(Optional.of(userData));
        when(studentRepository.getReferenceById(1000L)).thenReturn(updatedStudent);

        StudentDto expected = new StudentDto(1000L, userData, null, "Emily", "Hensley", Set.of());
        StudentDto actual = studentService.updateUser(1000L, updateStudentRequest);
        assertEquals(expected, actual);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.TRUE);
        verify(userRepository).updateFreeStatus(userData.getId(), Boolean.FALSE);
        verify(studentRepository).save(updatedStudent);
    }

    @Test
    void updateUser_should_ThrowEntityNotFoundException_when_StudentByIdDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest("student2", null, null, null, null, null);
        when(studentRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.updateUser(9999L, updateStudentRequest));
    }

    @Test
    void updateUser_should_ThrowEntityNotFoundException_when_UserByUsernameDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest("student3", null, null, null, null, null);
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(userRepository.findByUsername(updateStudentRequest.getUsername())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.updateUser(1000L, updateStudentRequest));
    }

    @Test
    void updateGroup_should_UpdateGroupOfStudent_ReturnUpdatedStudentDto_when_StudentByIdExists_GroupByNameExists() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, "1SE-24", null, null, null, null);
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        Student updatedStudent = new Student(1000L, USER_DATA, groupData, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(groupRepository.findByName(updateStudentRequest.getGroupNameToUpdate())).thenReturn(Optional.of(groupData));
        when(studentRepository.getReferenceById(1000L)).thenReturn(updatedStudent);

        StudentDto expected = new StudentDto(1000L, USER_DATA, groupData, "Emily", "Hensley", Set.of());
        StudentDto actual = studentService.updateGroup(1000L, updateStudentRequest);
        assertEquals(expected, actual);
        verify(studentRepository).save(updatedStudent);
    }

    @Test
    void updateGroup_should_ThrowEntityNotFoundException_when_StudentByIdDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, "1SE-24", null, null, null, null);
        when(studentRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.updateGroup(9999L, updateStudentRequest));
    }

    @Test
    void updateGroup_should_ThrowEntityNotFoundException_when_GroupByNameDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, "9YY-99", null, null, null, null);
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(groupRepository.findByName(updateStudentRequest.getGroupNameToUpdate())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.updateGroup(1000L, updateStudentRequest));
    }

    @Test
    void removeGroup_should_RemoveGroupFromStudent_ReturnUpdatedStudentDto_when_StudentByIdExists_GroupOfStudentIsNotNull_GroupByNameExists_StudentContainsExistingGroup() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, "1SE-24", null, null, null);
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        Student studentData = new Student(1000L, USER_DATA, groupData, "Emily", "Hensley", Set.of());
        Student updatedStudent = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(groupRepository.findByName(updateStudentRequest.getGroupNameToRemove())).thenReturn(Optional.of(groupData));
        when(studentRepository.getReferenceById(1000L)).thenReturn(updatedStudent);

        StudentDto expected = new StudentDto(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        StudentDto actual = studentService.removeGroup(1000L, updateStudentRequest);
        assertEquals(expected, actual);
        verify(studentRepository).save(updatedStudent);
    }

    @Test
    void removeGroup_should_ThrowEntityNotFoundException_when_StudentByIdDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, "1SE-24", null, null, null);
        when(studentRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.removeGroup(9999L, updateStudentRequest));
    }

    @Test
    void removeGroup_should_ThrowIllegalArgumentException_when_GroupOfStudentIsNull() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, "1SE-24", null, null, null);
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));

        assertThrows(IllegalArgumentException.class, () -> studentService.removeGroup(1000L, updateStudentRequest));
    }

    @Test
    void removeGroup_should_ThrowEntityNotFoundException_when_GroupByNameDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, "9YY-99", null, null, null);
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        Student studentData = new Student(1000L, USER_DATA, groupData, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(groupRepository.findByName(updateStudentRequest.getGroupNameToRemove())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.removeGroup(1000L, updateStudentRequest));
    }

    @Test
    void removeGroup_should_ThrowIllegalArgumentException_when_StudentDoesNotContainExistingGroup() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, "1SE-24", null, null, null);
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        Group studentGroupData = new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of());
        Student studentData = new Student(1000L, USER_DATA, studentGroupData, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(groupRepository.findByName(updateStudentRequest.getGroupNameToRemove())).thenReturn(Optional.of(groupData));

        assertThrows(IllegalArgumentException.class, () -> studentService.removeGroup(1000L, updateStudentRequest));
    }

    @Test
    void updateLastName_should_UpdateLastNameOfStudent_ReturnUpdatedStudentDto_when_StudentByIdExists() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, null, "Maldonado", null, null);
        Student updatedStudent = new Student(1000L, USER_DATA, null, "Emily", "Maldonado", Set.of());
        when(!studentRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(studentRepository.getReferenceById(1000L)).thenReturn(updatedStudent);

        StudentDto expected = new StudentDto(1000L, USER_DATA, null, "Emily", "Maldonado", Set.of());
        StudentDto actual = studentService.updateLastName(1000L, updateStudentRequest);
        assertEquals(expected, actual);
        verify(studentRepository).updateLastName(1000L, updateStudentRequest.getLastName());
    }

    @Test
    void updateLastName_should_ThrowEntityNotFoundException_when_StudentByIdDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, null, "Maldonado", null, null);
        when(!studentRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> studentService.updateLastName(9999L, updateStudentRequest));
    }

    @Test
    void addCourse_should_AddCourseToStudent_ReturnUpdatedStudentDto_when_StudentByIdExists_CourseByNameExists_StudentDoesNotContainExistingCourse() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, null, null, "English", null);
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", new HashSet<>());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        Student updatedStudent = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of(courseData));
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(courseRepository.findByName(updateStudentRequest.getCourseNameToAdd())).thenReturn(Optional.of(courseData));
        when(studentRepository.getReferenceById(1000L)).thenReturn(updatedStudent);

        StudentDto expected = new StudentDto(1000L, USER_DATA, null, "Emily", "Hensley", Set.of(courseData));
        StudentDto actual = studentService.addCourse(1000L, updateStudentRequest);
        assertEquals(expected, actual);
        verify(studentRepository).save(updatedStudent);
    }

    @Test
    void addCourse_should_ThrowEntityNotFoundException_when_StudentByIdDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, null, null, "English", null);
        when(studentRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.addCourse(9999L, updateStudentRequest));
    }

    @Test
    void addCourse_should_ThrowEntityNotFoundException_when_CourseByNameDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, null, null, "Algebra", null);
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", new HashSet<>());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(courseRepository.findByName(updateStudentRequest.getCourseNameToAdd())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.addCourse(1000L, updateStudentRequest));
    }

    @Test
    void addCourse_should_ThrowIllegalArgumentException_when_StudentContainsExistingCourse() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, null, null, "English", null);
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", new HashSet<>());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        studentData.getCourses().add(courseData);
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(courseRepository.findByName(updateStudentRequest.getCourseNameToAdd())).thenReturn(Optional.of(courseData));

        assertThrows(IllegalArgumentException.class, () -> studentService.addCourse(1000L, updateStudentRequest));
    }

    @Test
    void removeCourse_should_RemoveCourseFromStudent_ReturnUpdatedStudentDto_when_StudentByIdExists_CourseByNameExists_StudentContainsExistingCourse() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, null, null, null, "English");
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", new HashSet<>());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        studentData.getCourses().add(courseData);
        Student updatedStudent = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(courseRepository.findByName(updateStudentRequest.getCourseNameToRemove())).thenReturn(Optional.of(courseData));
        when(studentRepository.getReferenceById(1000L)).thenReturn(updatedStudent);

        StudentDto expected = new StudentDto(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        StudentDto actual = studentService.removeCourse(1000L, updateStudentRequest);
        assertEquals(expected, actual);
        verify(studentRepository).save(updatedStudent);
    }

    @Test
    void removeCourse_should_ThrowEntityNotFoundException_when_StudentByIdDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, null, null, null, "English");
        when(studentRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.removeCourse(9999L, updateStudentRequest));
    }

    @Test
    void removeCourse_should_ThrowEntityNotFoundException_when_CourseByNameDoesNotExist() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, null, null, null, "Algebra");
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", new HashSet<>());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(courseRepository.findByName(updateStudentRequest.getCourseNameToRemove())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.removeCourse(1000L, updateStudentRequest));
    }

    @Test
    void removeCourse_should_ThrowIllegalArgumentException_when_StudentDoesNotContainExistingCourse() {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest(null, null, null, null, null, "English");
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", new HashSet<>());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));
        when(courseRepository.findByName(updateStudentRequest.getCourseNameToRemove())).thenReturn(Optional.of(courseData));

        assertThrows(IllegalArgumentException.class, () -> studentService.removeCourse(1000L, updateStudentRequest));
    }

    @Test
    void deleteById_should_UpdateUserFreeStatus_DeleteStudentById_when_StudentByIdExists() {
        Student studentData = new Student(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(studentRepository.findById(1000L)).thenReturn(Optional.of(studentData));

        studentService.deleteById(1000L);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.TRUE);
        verify(studentRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_StudentByIdDoesNotExist() {
        when(studentRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> studentService.deleteById(9999L));
    }
}
