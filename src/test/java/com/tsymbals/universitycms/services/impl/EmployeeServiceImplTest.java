package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.EmployeeDto;
import com.tsymbals.universitycms.models.dto.requests.CreateEmployeeRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateEmployeeRequest;
import com.tsymbals.universitycms.models.entities.Employee;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.repositories.EmployeeRepository;
import com.tsymbals.universitycms.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = EmployeeServiceImpl.class)
class EmployeeServiceImplTest {

    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final UserEntity USER_DATA = new UserEntity(1000L, "employee1", ENCODED_PASSWORD, true, true, Set.of());

    @MockBean
    UserRepository userRepository;

    @MockBean
    EmployeeRepository employeeRepository;

    @Autowired
    EmployeeServiceImpl employeeService;

    @Test
    void save_should_UpdateUserFreeStatus_SaveNewEmployee_ReturnEmployeeDto_when_CreateEmployeeRequestIsValid_UserByUsernameExists() {
        CreateEmployeeRequest createEmployeeRequest = new CreateEmployeeRequest("employee1", "Tasha", "Garcia");
        Employee employeeData = new Employee(1000L, USER_DATA, "Tasha", "Garcia");
        when(userRepository.findByUsername(createEmployeeRequest.getUsername())).thenReturn(Optional.of(USER_DATA));
        when(employeeRepository.save(any(Employee.class))).thenReturn(employeeData);

        EmployeeDto expected = new EmployeeDto(1000L, USER_DATA, "Tasha", "Garcia");
        EmployeeDto actual = employeeService.save(createEmployeeRequest);
        assertEquals(expected, actual);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.FALSE);
    }

    @Test
    void save_should_ThrowEntityNotFoundException_when_CreateEmployeeRequestIsValid_UserByUsernameDoesNotExist() {
        CreateEmployeeRequest createEmployeeRequest = new CreateEmployeeRequest("teacher", "Tasha", "Garcia");
        when(userRepository.findByUsername(createEmployeeRequest.getUsername())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> employeeService.save(createEmployeeRequest));
    }

    @Test
    void getAll_should_ReturnListOfEmployeeDto_when_EmployeesExist() {
        List<Employee> employeeDataList = List.of(new Employee(1000L, USER_DATA, "Tasha", "Garcia"));
        when(employeeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(employeeDataList);

        List<EmployeeDto> expected = List.of(new EmployeeDto(1000L, USER_DATA, "Tasha", "Garcia"));
        List<EmployeeDto> actual = employeeService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfEmployeeDto_when_EmployeesDoNotExist() {
        List<Employee> employeeDataList = List.of();
        when(employeeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(employeeDataList);

        List<EmployeeDto> expected = List.of();
        List<EmployeeDto> actual = employeeService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ReturnEmployeeDto_when_EmployeeByIdExists() {
        Employee employeeData = new Employee(1000L, USER_DATA, "Tasha", "Garcia");
        when(employeeRepository.findById(1000L)).thenReturn(Optional.of(employeeData));

        EmployeeDto expected = new EmployeeDto(1000L, USER_DATA, "Tasha", "Garcia");
        EmployeeDto actual = employeeService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_EmployeeByIdDoesNotExist() {
        when(employeeRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> employeeService.getById(9999L));
    }

    @Test
    void getByFirstNameAndLastName_should_ReturnEmployeeDto_when_EmployeeByFirstNameAndLastNameExists() {
        Employee employeeData = new Employee(1000L, USER_DATA, "Tasha", "Garcia");
        when(employeeRepository.findByFirstNameAndLastName("Tasha", "Garcia")).thenReturn(Optional.of(employeeData));

        EmployeeDto expected = new EmployeeDto(1000L, USER_DATA, "Tasha", "Garcia");
        EmployeeDto actual = employeeService.getByFirstNameAndLastName("Tasha", "Garcia");
        assertEquals(expected, actual);
    }

    @Test
    void getByFirstNameAndLastName_should_ThrowEntityNotFoundException_when_EmployeeByFirstNameAndLastNameDoesNotExist() {
        when(employeeRepository.findByFirstNameAndLastName("Sidney", "Wallace")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> employeeService.getByFirstNameAndLastName("Sidney", "Wallace"));
    }

    @Test
    void updateUser_should_UpdateUserOfEmployee_ReturnUpdatedEmployeeDto_when_EmployeeByIdExists_UserByUsernameExists() {
        UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest("employee2", null);
        Employee employeeData = new Employee(1000L, USER_DATA, "Tasha", "Garcia");
        UserEntity userData = new UserEntity(1001L, "employee2", ENCODED_PASSWORD, true, true, Set.of());
        Employee updatedEmployee = new Employee(1000L, userData, "Tasha", "Garcia");
        when(employeeRepository.findById(1000L)).thenReturn(Optional.of(employeeData));
        when(userRepository.findByUsername(updateEmployeeRequest.getUsername())).thenReturn(Optional.of(userData));
        when(employeeRepository.getReferenceById(1000L)).thenReturn(updatedEmployee);

        EmployeeDto expected = new EmployeeDto(1000L, userData, "Tasha", "Garcia");
        EmployeeDto actual = employeeService.updateUser(1000L, updateEmployeeRequest);
        assertEquals(expected, actual);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.TRUE);
        verify(userRepository).updateFreeStatus(userData.getId(), Boolean.FALSE);
        verify(employeeRepository).save(updatedEmployee);
    }

    @Test
    void updateUser_should_ThrowEntityNotFoundException_when_EmployeeByIdDoesNotExist() {
        UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest("employee2", null);
        when(employeeRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> employeeService.updateUser(9999L, updateEmployeeRequest));
    }

    @Test
    void updateUser_should_ThrowEntityNotFoundException_when_UserByUsernameDoesNotExist() {
        UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest("employee3", null);
        Employee employeeData = new Employee(1000L, USER_DATA, "Tasha", "Garcia");
        when(employeeRepository.findById(1000L)).thenReturn(Optional.of(employeeData));
        when(userRepository.findByUsername(updateEmployeeRequest.getUsername())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> employeeService.updateUser(1000L, updateEmployeeRequest));
    }

    @Test
    void updateLastName_should_UpdateLastNameOfEmployee_ReturnUpdatedEmployeeDto_when_EmployeeByIdExists() {
        UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest(null, "Parrish");
        Employee updatedEmployee = new Employee(1000L, USER_DATA, "Tasha", "Parrish");
        when(!employeeRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(employeeRepository.getReferenceById(1000L)).thenReturn(updatedEmployee);

        EmployeeDto expected = new EmployeeDto(1000L, USER_DATA, "Tasha", "Parrish");
        EmployeeDto actual = employeeService.updateLastName(1000L, updateEmployeeRequest);
        assertEquals(expected, actual);
        verify(employeeRepository).updateLastName(1000L, updateEmployeeRequest.getLastName());
    }

    @Test
    void updateLastName_should_ThrowEntityNotFoundException_when_EmployeeByIdDoesNotExist() {
        UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest(null, "Parrish");
        when(!employeeRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> employeeService.updateLastName(9999L, updateEmployeeRequest));
    }

    @Test
    void deleteById_should_UpdateUserFreeStatus_DeleteEmployeeById_when_EmployeeByIdExists() {
        Employee employeeData = new Employee(1000L, USER_DATA, "Tasha", "Garcia");
        when(employeeRepository.findById(1000L)).thenReturn(Optional.of(employeeData));

        employeeService.deleteById(1000L);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.TRUE);
        verify(employeeRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_EmployeeByIdDoesNotExist() {
        when(employeeRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> employeeService.deleteById(9999L));
    }
}
