package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.AuditoriumDto;
import com.tsymbals.universitycms.models.dto.LessonDto;
import com.tsymbals.universitycms.models.dto.requests.CreateAuditoriumRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateAuditoriumRequest;
import com.tsymbals.universitycms.models.entities.*;
import com.tsymbals.universitycms.repositories.AuditoriumRepository;
import com.tsymbals.universitycms.repositories.DepartmentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = AuditoriumServiceImpl.class)
class AuditoriumServiceImplTest {

    static final University UNIVERSITY_DATA = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
    static final Department DEPARTMENT_DATA = new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of());
    static final LessonTime LESSON_TIME_DATA = new LessonTime(1000L, LocalTime.of(0, 0), LocalTime.of(1, 0), Set.of());
    static final Auditorium AUDITORIUM_DATA = new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of());
    static final Lesson LESSON_DATA = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());

    @MockBean
    AuditoriumRepository auditoriumRepository;

    @MockBean
    DepartmentRepository departmentRepository;

    @Autowired
    AuditoriumServiceImpl auditoriumService;

    @Test
    void save_should_SaveNewAuditorium_ReturnAuditoriumDto_when_CreateAuditoriumRequestIsValid_DepartmentByNameExists() {
        CreateAuditoriumRequest createAuditoriumRequest = new CreateAuditoriumRequest("Technical", "101");
        Auditorium auditoriumData = new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of());
        when(departmentRepository.findByName(createAuditoriumRequest.getDepartmentName())).thenReturn(Optional.of(DEPARTMENT_DATA));
        when(auditoriumRepository.save(any(Auditorium.class))).thenReturn(auditoriumData);

        AuditoriumDto expected = new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of());
        AuditoriumDto actual = auditoriumService.save(createAuditoriumRequest);
        assertEquals(expected, actual);
    }

    @Test
    void save_should_ThrowEntityNotFoundException_when_CreateAuditoriumRequestIsValid_DepartmentByNameDoesNotExist() {
        CreateAuditoriumRequest createAuditoriumRequest = new CreateAuditoriumRequest("Humanitarian", "101");
        when(departmentRepository.findByName(createAuditoriumRequest.getDepartmentName())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> auditoriumService.save(createAuditoriumRequest));
    }

    @Test
    void getAll_should_ReturnListOfAuditoriumDto_when_AuditoriumsExist() {
        List<Auditorium> auditoriumDataList = List.of(new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of()));
        when(auditoriumRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(auditoriumDataList);

        List<AuditoriumDto> expected = List.of(new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of()));
        List<AuditoriumDto> actual = auditoriumService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfAuditoriumDto_when_AuditoriumsDoNotExist() {
        List<Auditorium> auditoriumDataList = List.of();
        when(auditoriumRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(auditoriumDataList);

        List<AuditoriumDto> expected = List.of();
        List<AuditoriumDto> actual = auditoriumService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_should_ReturnNoneMatchingListOfAuditoriumDto_when_AuditoriumsExist() {
        List<Auditorium> auditoriumDataList = List.of(new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of()));
        Auditorium comparisonAuditorium = new Auditorium(1001L, DEPARTMENT_DATA, "102", Set.of());
        when(auditoriumRepository.findAllByIdNotOrderByIdAsc(comparisonAuditorium.getId())).thenReturn(auditoriumDataList);

        List<AuditoriumDto> expected = List.of(new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of()));
        List<AuditoriumDto> actual = auditoriumService.getAllNoneMatching(comparisonAuditorium);
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_should_ReturnEmptyListOfAuditoriumDto_when_AuditoriumsDoNotExist() {
        List<Auditorium> auditoriumDataList = List.of();
        Auditorium comparisonAuditorium = new Auditorium(1002L, DEPARTMENT_DATA, "103", Set.of());
        when(auditoriumRepository.findAllByIdNotOrderByIdAsc(comparisonAuditorium.getId())).thenReturn(auditoriumDataList);

        List<AuditoriumDto> expected = List.of();
        List<AuditoriumDto> actual = auditoriumService.getAllNoneMatching(comparisonAuditorium);
        assertEquals(expected, actual);
    }

    @Test
    void getAllByDate_should_ReturnListOfAuditoriumDto_when_AuditoriumsExist_LessonsByDateExist() {
        List<Auditorium> auditoriumDataList = List.of(new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of(LESSON_DATA)));
        List<LessonDto> lessonDataList = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of()));
        when(auditoriumRepository.findAllByLessonsIsNotEmptyOrderByIdAsc()).thenReturn(auditoriumDataList);

        List<AuditoriumDto> expected = List.of(new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of(LESSON_DATA)));
        List<AuditoriumDto> actual = auditoriumService.getAllByDate(lessonDataList);
        assertEquals(expected, actual);
    }

    @Test
    void getAllByDate_should_ReturnEmptyListOfAuditoriumDto_when_AuditoriumsDoNotExist() {
        List<Auditorium> auditoriumDataList = List.of();
        List<LessonDto> lessonDataList = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of()));
        when(auditoriumRepository.findAllByLessonsIsNotEmptyOrderByIdAsc()).thenReturn(auditoriumDataList);

        List<AuditoriumDto> expected = List.of();
        List<AuditoriumDto> actual = auditoriumService.getAllByDate(lessonDataList);
        assertEquals(expected, actual);
    }

    @Test
    void getAllByDate_should_ReturnListOfAuditoriumDtoWithEmptyLessons_when_LessonsByDateDoNotExist() {
        List<Auditorium> auditoriumDataList = List.of(new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of(LESSON_DATA)));
        List<LessonDto> lessonDataList = List.of();
        when(auditoriumRepository.findAllByLessonsIsNotEmptyOrderByIdAsc()).thenReturn(auditoriumDataList);

        List<AuditoriumDto> expected = List.of(new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of()));
        List<AuditoriumDto> actual = auditoriumService.getAllByDate(lessonDataList);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ReturnAuditoriumDto_when_AuditoriumByIdExists() {
        Auditorium auditoriumData = new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of());
        when(auditoriumRepository.findById(1000L)).thenReturn(Optional.of(auditoriumData));

        AuditoriumDto expected = new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of());
        AuditoriumDto actual = auditoriumService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_AuditoriumByIdDoesNotExist() {
        when(auditoriumRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> auditoriumService.getById(9999L));
    }

    @Test
    void getByName_should_ReturnAuditoriumDto_when_AuditoriumByNameExists() {
        Auditorium auditoriumData = new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of());
        when(auditoriumRepository.findByName("101")).thenReturn(Optional.of(auditoriumData));

        AuditoriumDto expected = new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of());
        AuditoriumDto actual = auditoriumService.getByName("101");
        assertEquals(expected, actual);
    }

    @Test
    void getByName_should_ThrowEntityNotFoundException_when_AuditoriumByNameDoesNotExist() {
        when(auditoriumRepository.findByName("999")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> auditoriumService.getByName("999"));
    }

    @Test
    void updateDepartment_should_UpdateDepartmentOfAuditorium_ReturnUpdatedAuditoriumDto_when_AuditoriumByIdExists_DepartmentByNameExists() {
        UpdateAuditoriumRequest updateAuditoriumRequest = new UpdateAuditoriumRequest("Humanitarian", null);
        Auditorium auditoriumData = new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of());
        Department departmentData = new Department(1001L, UNIVERSITY_DATA, "Humanitarian", Set.of());
        Auditorium updatedAuditorium = new Auditorium(1000L, departmentData, "101", Set.of());
        when(auditoriumRepository.findById(1000L)).thenReturn(Optional.of(auditoriumData));
        when(departmentRepository.findByName(updateAuditoriumRequest.getDepartmentName())).thenReturn(Optional.of(departmentData));
        when(auditoriumRepository.getReferenceById(1000L)).thenReturn(updatedAuditorium);

        AuditoriumDto expected = new AuditoriumDto(1000L, departmentData, "101", Set.of());
        AuditoriumDto actual = auditoriumService.updateDepartment(1000L, updateAuditoriumRequest);
        assertEquals(expected, actual);
        verify(auditoriumRepository).save(updatedAuditorium);
    }

    @Test
    void updateDepartment_should_ThrowEntityNotFoundException_when_AuditoriumByIdDoesNotExist() {
        UpdateAuditoriumRequest updateAuditoriumRequest = new UpdateAuditoriumRequest("Humanitarian", null);
        when(auditoriumRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> auditoriumService.updateDepartment(9999L, updateAuditoriumRequest));
    }

    @Test
    void updateDepartment_should_ThrowEntityNotFoundException_when_DepartmentByNameDoesNotExist() {
        UpdateAuditoriumRequest updateAuditoriumRequest = new UpdateAuditoriumRequest("Psychological", null);
        Auditorium auditoriumData = new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of());
        when(auditoriumRepository.findById(1000L)).thenReturn(Optional.of(auditoriumData));
        when(departmentRepository.findByName(updateAuditoriumRequest.getDepartmentName())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> auditoriumService.updateDepartment(1000L, updateAuditoriumRequest));
    }

    @Test
    void updateName_should_UpdateNameOfAuditorium_ReturnUpdatedAuditoriumDto_when_AuditoriumByIdExists() {
        UpdateAuditoriumRequest updateAuditoriumRequest = new UpdateAuditoriumRequest(null, "201");
        Auditorium updatedAuditorium = new Auditorium(1000L, DEPARTMENT_DATA, "201", Set.of());
        when(!auditoriumRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(auditoriumRepository.getReferenceById(1000L)).thenReturn(updatedAuditorium);

        AuditoriumDto expected = new AuditoriumDto(1000L, DEPARTMENT_DATA, "201", Set.of());
        AuditoriumDto actual = auditoriumService.updateName(1000L, updateAuditoriumRequest);
        assertEquals(expected, actual);
        verify(auditoriumRepository).updateName(1000L, updateAuditoriumRequest.getName());
    }

    @Test
    void updateName_should_ThrowEntityNotFoundException_when_AuditoriumByIdDoesNotExist() {
        UpdateAuditoriumRequest updateAuditoriumRequest = new UpdateAuditoriumRequest(null, "201");
        when(!auditoriumRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> auditoriumService.updateName(9999L, updateAuditoriumRequest));
    }

    @Test
    void deleteById_should_DeleteAuditoriumById_when_AuditoriumByIdExists() {
        when(!auditoriumRepository.existsById(1000L)).thenReturn(Boolean.TRUE);

        auditoriumService.deleteById(1000L);
        verify(auditoriumRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_AuditoriumByIdDoesNotExist() {
        when(!auditoriumRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> auditoriumService.deleteById(9999L));
    }
}
