package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.DepartmentDto;
import com.tsymbals.universitycms.models.dto.requests.CreateDepartmentRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateDepartmentRequest;
import com.tsymbals.universitycms.models.entities.Department;
import com.tsymbals.universitycms.models.entities.University;
import com.tsymbals.universitycms.repositories.DepartmentRepository;
import com.tsymbals.universitycms.repositories.UniversityRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = DepartmentServiceImpl.class)
class DepartmentServiceImplTest {

    static final University UNIVERSITY_DATA = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());

    @MockBean
    DepartmentRepository departmentRepository;

    @MockBean
    UniversityRepository universityRepository;

    @Autowired
    DepartmentServiceImpl departmentService;

    @Test
    void save_should_SaveNewDepartment_ReturnDepartmentDto_when_CreateDepartmentRequestIsValid_UniversityByAbbreviationNameExists() {
        CreateDepartmentRequest createDepartmentRequest = new CreateDepartmentRequest("TNU", "Technical");
        Department departmentData = new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        when(universityRepository.findByAbbreviationName(createDepartmentRequest.getUniversityAbbreviationName())).thenReturn(Optional.of(UNIVERSITY_DATA));
        when(departmentRepository.save(any(Department.class))).thenReturn(departmentData);

        DepartmentDto expected = new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        DepartmentDto actual = departmentService.save(createDepartmentRequest);
        assertEquals(expected, actual);
    }

    @Test
    void save_should_ThrowEntityNotFoundException_when_CreateDepartmentRequestIsValid_UniversityByAbbreviationNameDoesNotExist() {
        CreateDepartmentRequest createDepartmentRequest = new CreateDepartmentRequest("FNU", "Technical");
        when(universityRepository.findByAbbreviationName(createDepartmentRequest.getUniversityAbbreviationName())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> departmentService.save(createDepartmentRequest));
    }

    @Test
    void getAll_should_ReturnListOfDepartmentDto_when_DepartmentsExist() {
        List<Department> departmentDataList = List.of(new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of()));
        when(departmentRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(departmentDataList);

        List<DepartmentDto> expected = List.of(new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of()));
        List<DepartmentDto> actual = departmentService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfDepartmentDto_when_DepartmentsDoNotExist() {
        List<Department> departmentDataList = List.of();
        when(departmentRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(departmentDataList);

        List<DepartmentDto> expected = List.of();
        List<DepartmentDto> actual = departmentService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_should_ReturnNoneMatchingListOfDepartmentDto_when_DepartmentsExist() {
        List<Department> departmentDataList = List.of(new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of()));
        Department comparisonDepartment = new Department(1001L, UNIVERSITY_DATA, "Humanitarian", Set.of());
        when(departmentRepository.findAllByIdNotOrderByIdAsc(comparisonDepartment.getId())).thenReturn(departmentDataList);

        List<DepartmentDto> expected = List.of(new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of()));
        List<DepartmentDto> actual = departmentService.getAllNoneMatching(comparisonDepartment);
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_should_ReturnEmptyListOfDepartmentDto_when_DepartmentsDoNotExist() {
        List<Department> departmentDataList = List.of();
        Department comparisonDepartment = new Department(1001L, UNIVERSITY_DATA, "Humanitarian", Set.of());
        when(departmentRepository.findAllByIdNotOrderByIdAsc(comparisonDepartment.getId())).thenReturn(departmentDataList);

        List<DepartmentDto> expected = List.of();
        List<DepartmentDto> actual = departmentService.getAllNoneMatching(comparisonDepartment);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ReturnDepartmentDto_when_DepartmentByIdExists() {
        Department departmentData = new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        when(departmentRepository.findById(1000L)).thenReturn(Optional.of(departmentData));

        DepartmentDto expected = new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        DepartmentDto actual = departmentService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_DepartmentByIdDoesNotExist() {
        when(departmentRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> departmentService.getById(9999L));
    }

    @Test
    void getByName_should_ReturnDepartmentDto_when_DepartmentByNameExists() {
        Department departmentData = new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        when(departmentRepository.findByName("Technical")).thenReturn(Optional.of(departmentData));

        DepartmentDto expected = new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        DepartmentDto actual = departmentService.getByName("Technical");
        assertEquals(expected, actual);
    }

    @Test
    void getByName_should_ThrowEntityNotFoundException_when_DepartmentByNameDoesNotExist() {
        when(departmentRepository.findByName("Financial")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> departmentService.getByName("Financial"));
    }

    @Test
    void updateUniversity_should_UpdateUniversityOfDepartment_ReturnUpdatedDepartmentDto_when_DepartmentByIdExists_UniversityByAbbreviationNameExists() {
        UpdateDepartmentRequest updateDepartmentRequest = new UpdateDepartmentRequest("HNU", null);
        Department departmentData = new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        University universityData = new University(1001L, "Humanitarian National University", "HNU", "San Andreas", "+380(98)7654321", Set.of());
        Department updatedDepartment = new Department(1000L, universityData, "Technical", Set.of());
        when(departmentRepository.findById(1000L)).thenReturn(Optional.of(departmentData));
        when(universityRepository.findByAbbreviationName(updateDepartmentRequest.getUniversityAbbreviationName())).thenReturn(Optional.of(universityData));
        when(departmentRepository.getReferenceById(1000L)).thenReturn(updatedDepartment);

        DepartmentDto expected = new DepartmentDto(1000L, universityData, "Technical", Set.of());
        DepartmentDto actual = departmentService.updateUniversity(1000L, updateDepartmentRequest);
        assertEquals(expected, actual);
        verify(departmentRepository).save(updatedDepartment);
    }

    @Test
    void updateUniversity_should_ThrowEntityNotFoundException_when_DepartmentByIdDoesNotExist() {
        UpdateDepartmentRequest updateDepartmentRequest = new UpdateDepartmentRequest("HNU", null);
        when(departmentRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> departmentService.updateUniversity(9999L, updateDepartmentRequest));
    }

    @Test
    void updateUniversity_should_ThrowEntityNotFoundException_when_UniversityByAbbreviationNameDoesNotExist() {
        UpdateDepartmentRequest updateDepartmentRequest = new UpdateDepartmentRequest("FNU", null);
        Department departmentData = new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        when(departmentRepository.findById(1000L)).thenReturn(Optional.of(departmentData));
        when(universityRepository.findByAbbreviationName(updateDepartmentRequest.getUniversityAbbreviationName())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> departmentService.updateUniversity(1000L, updateDepartmentRequest));
    }

    @Test
    void updateName_should_UpdateNameOfDepartment_ReturnUpdatedDepartmentDto_when_DepartmentByIdExists() {
        UpdateDepartmentRequest updateDepartmentRequest = new UpdateDepartmentRequest(null, "Financial");
        Department updatedDepartment = new Department(1000L, UNIVERSITY_DATA, "Financial", Set.of());
        when(!departmentRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(departmentRepository.getReferenceById(1000L)).thenReturn(updatedDepartment);

        DepartmentDto expected = new DepartmentDto(1000L, UNIVERSITY_DATA, "Financial", Set.of());
        DepartmentDto actual = departmentService.updateName(1000L, updateDepartmentRequest);
        assertEquals(expected, actual);
        verify(departmentRepository).updateName(1000L, updateDepartmentRequest.getName());
    }

    @Test
    void updateName_should_ThrowEntityNotFoundException_when_DepartmentByIdDoesNotExist() {
        UpdateDepartmentRequest updateDepartmentRequest = new UpdateDepartmentRequest(null, "Financial");
        when(!departmentRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> departmentService.updateName(9999L, updateDepartmentRequest));
    }

    @Test
    void deleteById_should_DeleteDepartmentById_when_DepartmentByIdExists() {
        when(!departmentRepository.existsById(1000L)).thenReturn(Boolean.TRUE);

        departmentService.deleteById(1000L);
        verify(departmentRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_DepartmentByIdDoesNotExist() {
        when(!departmentRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> departmentService.deleteById(9999L));
    }
}
