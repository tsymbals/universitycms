package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.CourseDto;
import com.tsymbals.universitycms.models.dto.requests.CreateCourseRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateCourseRequest;
import com.tsymbals.universitycms.models.entities.Course;
import com.tsymbals.universitycms.repositories.CourseRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = CourseServiceImpl.class)
class CourseServiceImplTest {

    @MockBean
    CourseRepository courseRepository;

    @Autowired
    CourseServiceImpl courseService;

    @Test
    void save_should_SaveNewCourse_ReturnCourseDto_when_CreateCourseRequestIsValid() {
        CreateCourseRequest createCourseRequest = new CreateCourseRequest("English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain");
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        when(courseRepository.save(any(Course.class))).thenReturn(courseData);

        CourseDto expected = new CourseDto(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        CourseDto actual = courseService.save(createCourseRequest);
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnListOfCourseDto_when_CoursesExist() {
        List<Course> courseDataList = List.of(new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        when(courseRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(courseDataList);

        List<CourseDto> expected = List.of(new CourseDto(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        List<CourseDto> actual = courseService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfCourseDto_when_CoursesDoNotExist() {
        List<Course> courseDataList = List.of();
        when(courseRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(courseDataList);

        List<CourseDto> expected = List.of();
        List<CourseDto> actual = courseService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_ComparisonCourse_should_ReturnNoneMatchingListOfCourseDto_when_CoursesExist_ComparisonCourseIsNotNull() {
        List<Course> courseDataList = List.of(new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        Course comparisonCourse = new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of());
        when(courseRepository.findAllByIdNotOrderByIdAsc(comparisonCourse.getId())).thenReturn(courseDataList);

        List<CourseDto> expected = List.of(new CourseDto(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        List<CourseDto> actual = courseService.getAllNoneMatching(comparisonCourse);
        assertEquals(expected, actual);
        verify(courseRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(courseRepository).findAllByIdNotOrderByIdAsc(comparisonCourse.getId());
    }

    @Test
    void getAllNoneMatching_ComparisonCourse_should_ReturnListOfCourseDto_when_CoursesExist_ComparisonCourseIsNull() {
        List<Course> courseDataList = List.of(new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        when(courseRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(courseDataList);

        List<CourseDto> expected = List.of(new CourseDto(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        List<CourseDto> actual = courseService.getAllNoneMatching((Course) null);
        assertEquals(expected, actual);
        verify(courseRepository).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(courseRepository, never()).findAllByIdNotOrderByIdAsc(anyLong());
    }

    @Test
    void getAllNoneMatching_ComparisonCourse_should_ReturnEmptyListOfCourseDto_when_CoursesDoNotExist() {
        List<Course> courseDataList = List.of();
        Course comparisonCourse = new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of());
        when(courseRepository.findAllByIdNotOrderByIdAsc(comparisonCourse.getId())).thenReturn(courseDataList);

        List<CourseDto> expected = List.of();
        List<CourseDto> actual = courseService.getAllNoneMatching(comparisonCourse);
        assertEquals(expected, actual);
        verify(courseRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(courseRepository).findAllByIdNotOrderByIdAsc(comparisonCourse.getId());
    }

    @Test
    void getAllNoneMatching_ComparisonCourses_should_ReturnNoneMatchingListOfCourseDto_when_CoursesExist_ComparisonSetOfCourseIsNotEmpty() {
        List<Course> courseDataList = List.of(new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        Set<Course> comparisonCourses = Set.of(new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()));
        when(courseRepository.findAllByIdNotInOrderByIdAsc(Set.of(1001L))).thenReturn(courseDataList);

        List<CourseDto> expected = List.of(new CourseDto(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        List<CourseDto> actual = courseService.getAllNoneMatching(comparisonCourses);
        assertEquals(expected, actual);
        verify(courseRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(courseRepository).findAllByIdNotInOrderByIdAsc(Set.of(1001L));
    }

    @Test
    void getAllNoneMatching_ComparisonCourses_should_ReturnListOfCourseDto_when_CoursesExist_ComparisonSetOfCourseIsEmpty() {
        List<Course> courseDataList = List.of(new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        Set<Course> comparisonCourses = Set.of();
        when(courseRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(courseDataList);

        List<CourseDto> expected = List.of(new CourseDto(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        List<CourseDto> actual = courseService.getAllNoneMatching(comparisonCourses);
        assertEquals(expected, actual);
        verify(courseRepository).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(courseRepository, never()).findAllByIdNotInOrderByIdAsc(anySet());
    }

    @Test
    void getAllNoneMatching_ComparisonCourses_should_ReturnEmptyListOfCourseDto_when_CoursesDoNotExist() {
        List<Course> courseDataList = List.of();
        Set<Course> comparisonCourses = Set.of(new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()));
        when(courseRepository.findAllByIdNotInOrderByIdAsc(Set.of(1001L))).thenReturn(courseDataList);

        List<CourseDto> expected = List.of();
        List<CourseDto> actual = courseService.getAllNoneMatching(comparisonCourses);
        assertEquals(expected, actual);
        verify(courseRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(courseRepository).findAllByIdNotInOrderByIdAsc(Set.of(1001L));
    }

    @Test
    void getById_should_ReturnCourseDto_when_CourseByIdExists() {
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        when(courseRepository.findById(1000L)).thenReturn(Optional.of(courseData));

        CourseDto expected = new CourseDto(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        CourseDto actual = courseService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_CourseByIdDoesNotExist() {
        when(courseRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> courseService.getById(9999L));
    }

    @Test
    void getByName_should_ReturnCourseDto_when_CourseByNameExists() {
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        when(courseRepository.findByName("English")).thenReturn(Optional.of(courseData));

        CourseDto expected = new CourseDto(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        CourseDto actual = courseService.getByName("English");
        assertEquals(expected, actual);
    }

    @Test
    void getByName_should_ThrowEntityNotFoundException_when_CourseByNameDoesNotExist() {
        when(courseRepository.findByName("Algebra")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> courseService.getByName("Algebra"));
    }

    @Test
    void updateName_should_UpdateNameOfCourse_ReturnUpdatedCourseDto_when_CourseByIdExists() {
        UpdateCourseRequest updateCourseRequest = new UpdateCourseRequest("Speak English", null);
        Course updatedCourse = new Course(1000L, "Speak English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        when(!courseRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(courseRepository.getReferenceById(1000L)).thenReturn(updatedCourse);

        CourseDto expected = new CourseDto(1000L, "Speak English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        CourseDto actual = courseService.updateName(1000L, updateCourseRequest);
        assertEquals(expected, actual);
        verify(courseRepository).updateName(1000L, updateCourseRequest.getName());
    }

    @Test
    void updateName_should_ThrowEntityNotFoundException_when_CourseByIdDoesNotExist() {
        UpdateCourseRequest updateCourseRequest = new UpdateCourseRequest("Speak English", null);
        when(!courseRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> courseService.updateName(9999L, updateCourseRequest));
    }

    @Test
    void updateDescription_should_UpdateDescriptionOfCourse_ReturnUpdatedCourseDto_when_CourseByIdExists() {
        UpdateCourseRequest updateCourseRequest = new UpdateCourseRequest(null, "Speaking practice to help you learn useful language for everyday communication");
        Course updatedCourse = new Course(1000L, "English", "Speaking practice to help you learn useful language for everyday communication", Set.of(), Set.of());
        when(!courseRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(courseRepository.getReferenceById(1000L)).thenReturn(updatedCourse);

        CourseDto expected = new CourseDto(1000L, "English", "Speaking practice to help you learn useful language for everyday communication", Set.of(), Set.of());
        CourseDto actual = courseService.updateDescription(1000L, updateCourseRequest);
        assertEquals(expected, actual);
        verify(courseRepository).updateDescription(1000L, updateCourseRequest.getDescription());
    }

    @Test
    void updateDescription_should_ThrowEntityNotFoundException_when_CourseByIdDoesNotExist() {
        UpdateCourseRequest updateCourseRequest = new UpdateCourseRequest(null, "Speaking practice to help you learn useful language for everyday communication");
        when(!courseRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> courseService.updateDescription(9999L, updateCourseRequest));
    }

    @Test
    void deleteById_should_DeleteCourseById_when_CourseByIdExists() {
        when(!courseRepository.existsById(1000L)).thenReturn(Boolean.TRUE);

        courseService.deleteById(1000L);
        verify(courseRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_CourseByIdDoesNotExist() {
        when(!courseRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> courseService.deleteById(9999L));
    }
}
