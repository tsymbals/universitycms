package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.UniversityDto;
import com.tsymbals.universitycms.models.dto.requests.CreateUniversityRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateUniversityRequest;
import com.tsymbals.universitycms.models.entities.University;
import com.tsymbals.universitycms.repositories.UniversityRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = UniversityServiceImpl.class)
class UniversityServiceImplTest {

    @MockBean
    UniversityRepository universityRepository;

    @Autowired
    UniversityServiceImpl universityService;

    @Test
    void save_should_SaveNewUniversity_ReturnUniversityDto_when_CreateUniversityRequestIsValid() {
        CreateUniversityRequest createUniversityRequest = new CreateUniversityRequest("Technical National University", "TNU", "Vice City", "+380(12)3456789");
        University universityData = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        when(universityRepository.save(any(University.class))).thenReturn(universityData);

        UniversityDto expected = new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        UniversityDto actual = universityService.save(createUniversityRequest);
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnListOfUniversityDto_when_UniversitiesExist() {
        List<University> universityDataList = List.of(new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of()));
        when(universityRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(universityDataList);

        List<UniversityDto> expected = List.of(new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of()));
        List<UniversityDto> actual = universityService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfUniversityDto_when_UniversitiesDoNotExist() {
        List<University> universityDataList = List.of();
        when(universityRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(universityDataList);

        List<UniversityDto> expected = List.of();
        List<UniversityDto> actual = universityService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_should_ReturnNoneMatchingListOfUniversityDto_when_UniversitiesExist() {
        List<University> universityDataList = List.of(new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of()));
        University comparisonUniversity = new University(1001L, "Humanitarian National University", "HNU", "San Andreas", "+380(98)7654321", Set.of());
        when(universityRepository.findAllByIdNotOrderByIdAsc(comparisonUniversity.getId())).thenReturn(universityDataList);

        List<UniversityDto> expected = List.of(new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of()));
        List<UniversityDto> actual = universityService.getAllNoneMatching(comparisonUniversity);
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_should_ReturnEmptyListOfUniversityDto_when_UniversitiesDoNotExist() {
        List<University> universityDataList = List.of();
        University comparisonUniversity = new University(1001L, "Humanitarian National University", "HNU", "San Andreas", "+380(98)7654321", Set.of());
        when(universityRepository.findAllByIdNotOrderByIdAsc(comparisonUniversity.getId())).thenReturn(universityDataList);

        List<UniversityDto> expected = List.of();
        List<UniversityDto> actual = universityService.getAllNoneMatching(comparisonUniversity);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ReturnUniversityDto_when_UniversityByIdExists() {
        University universityData = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        when(universityRepository.findById(1000L)).thenReturn(Optional.of(universityData));

        UniversityDto expected = new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        UniversityDto actual = universityService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_UniversityByIdDoesNotExist() {
        when(universityRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> universityService.getById(9999L));
    }

    @Test
    void getByAbbreviationName_should_ReturnUniversityDto_when_UniversityByAbbreviationNameExists() {
        University universityData = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        when(universityRepository.findByAbbreviationName("TNU")).thenReturn(Optional.of(universityData));

        UniversityDto expected = new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        UniversityDto actual = universityService.getByAbbreviationName("TNU");
        assertEquals(expected, actual);
    }

    @Test
    void getByAbbreviationName_should_ThrowEntityNotFoundException_when_UniversityByAbbreviationNameDoesNotExist() {
        when(universityRepository.findByAbbreviationName("FNU")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> universityService.getByAbbreviationName("FNU"));
    }

    @Test
    void updateName_should_UpdateNameOfUniversity_ReturnUpdatedUniversityDto_when_UniversityByIdExists() {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest("National Technical University", null, null, null);
        University updatedUniversity = new University(1000L, "National Technical University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        when(!universityRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(universityRepository.getReferenceById(1000L)).thenReturn(updatedUniversity);

        UniversityDto expected = new UniversityDto(1000L, "National Technical University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        UniversityDto actual = universityService.updateName(1000L, updateUniversityRequest);
        assertEquals(expected, actual);
        verify(universityRepository).updateName(1000L, updateUniversityRequest.getName());
    }

    @Test
    void updateName_should_ThrowEntityNotFoundException_when_UniversityByIdDoesNotExist() {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest("National Technical University", null, null, null);
        when(!universityRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> universityService.updateName(9999L, updateUniversityRequest));
    }

    @Test
    void updateAbbreviationName_should_UpdateAbbreviationNameOfUniversity_ReturnUpdatedUniversityDto_when_UniversityByIdExists() {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest(null, "NTU", null, null);
        University updatedUniversity = new University(1000L, "Technical National University", "NTU", "Vice City", "+380(12)3456789", Set.of());
        when(!universityRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(universityRepository.getReferenceById(1000L)).thenReturn(updatedUniversity);

        UniversityDto expected = new UniversityDto(1000L, "Technical National University", "NTU", "Vice City", "+380(12)3456789", Set.of());
        UniversityDto actual = universityService.updateAbbreviationName(1000L, updateUniversityRequest);
        assertEquals(expected, actual);
        verify(universityRepository).updateAbbreviationName(1000L, updateUniversityRequest.getAbbreviationName());
    }

    @Test
    void updateAbbreviationName_should_ThrowEntityNotFoundException_when_UniversityByIdDoesNotExist() {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest(null, "NTU", null, null);
        when(!universityRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> universityService.updateAbbreviationName(9999L, updateUniversityRequest));
    }

    @Test
    void updateCity_should_UpdateCityOfUniversity_ReturnUpdatedUniversityDto_when_UniversityByIdExists() {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest(null, null, "Liberty City", null);
        University updatedUniversity = new University(1000L, "Technical National University", "TNU", "Liberty City", "+380(12)3456789", Set.of());
        when(!universityRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(universityRepository.getReferenceById(1000L)).thenReturn(updatedUniversity);

        UniversityDto expected = new UniversityDto(1000L, "Technical National University", "TNU", "Liberty City", "+380(12)3456789", Set.of());
        UniversityDto actual = universityService.updateCity(1000L, updateUniversityRequest);
        assertEquals(expected, actual);
        verify(universityRepository).updateCity(1000L, updateUniversityRequest.getCity());
    }

    @Test
    void updateCity_should_ThrowEntityNotFoundException_when_UniversityByIdDoesNotExist() {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest(null, null, "Liberty City", null);
        when(!universityRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> universityService.updateCity(9999L, updateUniversityRequest));
    }

    @Test
    void updatePhoneNumber_should_UpdatePhoneNumberOfUniversity_ReturnUpdatedUniversityDto_when_UniversityByIdExists() {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest(null, null, null, "+380(98)7654321");
        University updatedUniversity = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(98)7654321", Set.of());
        when(!universityRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(universityRepository.getReferenceById(1000L)).thenReturn(updatedUniversity);

        UniversityDto expected = new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(98)7654321", Set.of());
        UniversityDto actual = universityService.updatePhoneNumber(1000L, updateUniversityRequest);
        assertEquals(expected, actual);
        verify(universityRepository).updatePhoneNumber(1000L, updateUniversityRequest.getPhoneNumber());
    }

    @Test
    void updatePhoneNumber_should_ThrowEntityNotFoundException_when_UniversityByIdDoesNotExist() {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest(null, null, null, "+380(98)7654321");
        when(!universityRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> universityService.updatePhoneNumber(9999L, updateUniversityRequest));
    }

    @Test
    void deleteById_should_DeleteUniversityById_when_UniversityByIdExists() {
        when(!universityRepository.existsById(1000L)).thenReturn(Boolean.TRUE);

        universityService.deleteById(1000L);
        verify(universityRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_UniversityByIdDoesNotExist() {
        when(!universityRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> universityService.deleteById(9999L));
    }
}
