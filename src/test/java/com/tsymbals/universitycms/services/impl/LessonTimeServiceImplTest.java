package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.LessonTimeDto;
import com.tsymbals.universitycms.models.dto.requests.CreateLessonTimeRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateLessonTimeRequest;
import com.tsymbals.universitycms.models.entities.LessonTime;
import com.tsymbals.universitycms.repositories.LessonTimeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = LessonTimeServiceImpl.class)
class LessonTimeServiceImplTest {

    @MockBean
    LessonTimeRepository lessonTimeRepository;

    @Autowired
    LessonTimeServiceImpl lessonTimeService;

    @Test
    void save_should_SaveNewLessonTime_ReturnLessonTimeDto_when_CreateLessonTimeRequestIsValid() {
        CreateLessonTimeRequest createLessonTimeRequest = new CreateLessonTimeRequest(LocalTime.of(8, 15), LocalTime.of(9, 0));
        LessonTime lessonTimeData = new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
        when(lessonTimeRepository.save(any(LessonTime.class))).thenReturn(lessonTimeData);

        LessonTimeDto expected = new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
        LessonTimeDto actual = lessonTimeService.save(createLessonTimeRequest);
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnListOfLessonTimeDto_when_LessonTimesExist() {
        List<LessonTime> lessonTimeDataList = List.of(new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of()));
        when(lessonTimeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(lessonTimeDataList);

        List<LessonTimeDto> expected = List.of(new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of()));
        List<LessonTimeDto> actual = lessonTimeService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfLessonTimeDto_when_LessonTimesDoNotExist() {
        List<LessonTime> lessonTimeDataList = List.of();
        when(lessonTimeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(lessonTimeDataList);

        List<LessonTimeDto> expected = List.of();
        List<LessonTimeDto> actual = lessonTimeService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_should_ReturnNoneMatchingListOfLessonTimeDto_when_LessonTimesExist() {
        List<LessonTime> lessonTimeDataList = List.of(new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of()));
        LessonTime comparisonLessonTime = new LessonTime(1001L, LocalTime.of(9, 15), LocalTime.of(10, 0), Set.of());
        when(lessonTimeRepository.findAllByIdNotOrderByIdAsc(comparisonLessonTime.getId())).thenReturn(lessonTimeDataList);

        List<LessonTimeDto> expected = List.of(new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of()));
        List<LessonTimeDto> actual = lessonTimeService.getAllNoneMatching(comparisonLessonTime);
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_should_ReturnEmptyListOfLessonTimeDto_when_LessonTimesDoNotExist() {
        List<LessonTime> lessonTimeDataList = List.of();
        LessonTime comparisonLessonTime = new LessonTime(1001L, LocalTime.of(9, 15), LocalTime.of(10, 0), Set.of());
        when(lessonTimeRepository.findAllByIdNotOrderByIdAsc(comparisonLessonTime.getId())).thenReturn(lessonTimeDataList);

        List<LessonTimeDto> expected = List.of();
        List<LessonTimeDto> actual = lessonTimeService.getAllNoneMatching(comparisonLessonTime);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ReturnLessonTimeDto_when_LessonTimeByIdExists() {
        LessonTime lessonTimeData = new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
        when(lessonTimeRepository.findById(1000L)).thenReturn(Optional.of(lessonTimeData));

        LessonTimeDto expected = new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
        LessonTimeDto actual = lessonTimeService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_LessonTimeByIdDoesNotExist() {
        when(lessonTimeRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonTimeService.getById(9999L));
    }

    @Test
    void getByStartTimeAndEndTime_should_ReturnLessonTimeDto_when_LessonTimeByStartTimeAndEndTimeExists() {
        LessonTime lessonTimeData = new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
        when(lessonTimeRepository.findByStartTimeAndEndTime(LocalTime.of(8, 15), LocalTime.of(9, 0))).thenReturn(Optional.of(lessonTimeData));

        LessonTimeDto expected = new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
        LessonTimeDto actual = lessonTimeService.getByStartTimeAndEndTime(LocalTime.of(8, 15), LocalTime.of(9, 0));
        assertEquals(expected, actual);
    }

    @Test
    void getByStartTimeAndEndTime_should_ThrowEntityNotFoundException_when_LessonTimeByStartTimeAndEndTimeDoesNotExist() {
        LocalTime startTime = LocalTime.of(9, 15);
        LocalTime endTime = LocalTime.of(10, 0);
        when(lessonTimeRepository.findByStartTimeAndEndTime(startTime, endTime)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonTimeService.getByStartTimeAndEndTime(startTime, endTime));
    }

    @Test
    void updateStartTime_should_UpdateStartTimeOfLessonTime_ReturnUpdatedLessonTimeDto_when_LessonTimeByIdExists() {
        UpdateLessonTimeRequest updateLessonTimeRequest = new UpdateLessonTimeRequest(LocalTime.of(9, 15), null);
        LessonTime updatedLessonTime = new LessonTime(1000L, LocalTime.of(9, 15), LocalTime.of(9, 0), Set.of());
        when(!lessonTimeRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(lessonTimeRepository.getReferenceById(1000L)).thenReturn(updatedLessonTime);

        LessonTimeDto expected = new LessonTimeDto(1000L, LocalTime.of(9, 15), LocalTime.of(9, 0), Set.of());
        LessonTimeDto actual = lessonTimeService.updateStartTime(1000L, updateLessonTimeRequest);
        assertEquals(expected, actual);
        verify(lessonTimeRepository).updateStartTime(1000L, updateLessonTimeRequest.getStartTime());
    }

    @Test
    void updateStartTime_should_ThrowEntityNotFoundException_when_LessonTimeByIdDoesNotExist() {
        UpdateLessonTimeRequest updateLessonTimeRequest = new UpdateLessonTimeRequest(LocalTime.of(9, 15), null);
        when(!lessonTimeRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> lessonTimeService.updateStartTime(9999L, updateLessonTimeRequest));
    }

    @Test
    void updateEndTime_should_UpdateEndTimeOfLessonTime_ReturnUpdatedLessonTimeDto_when_LessonTimeByIdExists() {
        UpdateLessonTimeRequest updateLessonTimeRequest = new UpdateLessonTimeRequest(null, LocalTime.of(10, 0));
        LessonTime updatedLessonTime = new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(10, 0), Set.of());
        when(!lessonTimeRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(lessonTimeRepository.getReferenceById(1000L)).thenReturn(updatedLessonTime);

        LessonTimeDto expected = new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(10, 0), Set.of());
        LessonTimeDto actual = lessonTimeService.updateEndTime(1000L, updateLessonTimeRequest);
        assertEquals(expected, actual);
        verify(lessonTimeRepository).updateEndTime(1000L, updateLessonTimeRequest.getEndTime());
    }

    @Test
    void updateEndTime_should_ThrowEntityNotFoundException_when_LessonTimeByIdDoesNotExist() {
        UpdateLessonTimeRequest updateLessonTimeRequest = new UpdateLessonTimeRequest(null, LocalTime.of(10, 0));
        when(!lessonTimeRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> lessonTimeService.updateEndTime(9999L, updateLessonTimeRequest));
    }

    @Test
    void deleteById_should_DeleteLessonTimeById_when_LessonTimeByIdExists() {
        when(!lessonTimeRepository.existsById(1000L)).thenReturn(Boolean.TRUE);

        lessonTimeService.deleteById(1000L);
        verify(lessonTimeRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_LessonTimeByIdDoesNotExist() {
        when(!lessonTimeRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> lessonTimeService.deleteById(9999L));
    }
}
