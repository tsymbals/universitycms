package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.GroupDto;
import com.tsymbals.universitycms.models.dto.requests.CreateGroupRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateGroupRequest;
import com.tsymbals.universitycms.models.entities.Group;
import com.tsymbals.universitycms.repositories.GroupRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = GroupServiceImpl.class)
class GroupServiceImplTest {

    @MockBean
    GroupRepository groupRepository;

    @Autowired
    GroupServiceImpl groupService;

    @Test
    void save_should_SaveNewGroup_ReturnGroupDto_when_CreateGroupRequestIsValid() {
        CreateGroupRequest createGroupRequest = new CreateGroupRequest("1SE-24");
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        when(groupRepository.save(any(Group.class))).thenReturn(groupData);

        GroupDto expected = new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        GroupDto actual = groupService.save(createGroupRequest);
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnListOfGroupDto_when_GroupsExist() {
        List<Group> groupDataList = List.of(new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        when(groupRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(groupDataList);

        List<GroupDto> expected = List.of(new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        List<GroupDto> actual = groupService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfGroupDto_when_GroupsDoNotExist() {
        List<Group> groupDataList = List.of();
        when(groupRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(groupDataList);

        List<GroupDto> expected = List.of();
        List<GroupDto> actual = groupService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_ComparisonGroup_should_ReturnNoneMatchingListOfGroupDto_when_GroupsExist_ComparisonGroupIsNotNull() {
        List<Group> groupDataList = List.of(new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        Group comparisonGroup = new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of());
        when(groupRepository.findAllByIdNotOrderByIdAsc(comparisonGroup.getId())).thenReturn(groupDataList);

        List<GroupDto> expected = List.of(new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        List<GroupDto> actual = groupService.getAllNoneMatching(comparisonGroup);
        assertEquals(expected, actual);
        verify(groupRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(groupRepository).findAllByIdNotOrderByIdAsc(comparisonGroup.getId());
    }

    @Test
    void getAllNoneMatching_ComparisonGroup_should_ReturnListOfGroupDto_when_GroupsExist_ComparisonGroupIsNull() {
        List<Group> groupDataList = List.of(new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        when(groupRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(groupDataList);

        List<GroupDto> expected = List.of(new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        List<GroupDto> actual = groupService.getAllNoneMatching((Group) null);
        assertEquals(expected, actual);
        verify(groupRepository).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(groupRepository, never()).findAllByIdNotOrderByIdAsc(anyLong());
    }

    @Test
    void getAllNoneMatching_ComparisonGroup_should_ReturnEmptyListOfGroupDto_when_GroupsDoNotExist() {
        List<Group> groupDataList = List.of();
        Group comparisonGroup = new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of());
        when(groupRepository.findAllByIdNotOrderByIdAsc(comparisonGroup.getId())).thenReturn(groupDataList);

        List<GroupDto> expected = List.of();
        List<GroupDto> actual = groupService.getAllNoneMatching(comparisonGroup);
        assertEquals(expected, actual);
        verify(groupRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(groupRepository).findAllByIdNotOrderByIdAsc(comparisonGroup.getId());
    }

    @Test
    void getAllNoneMatching_ComparisonGroups_should_ReturnNoneMatchingListOfGroupDto_when_GroupsExist_ComparisonSetOfGroupIsNotEmpty() {
        List<Group> groupDataList = List.of(new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        Set<Group> comparisonGroups = Set.of(new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of()));
        when(groupRepository.findAllByIdNotInOrderByIdAsc(Set.of(1001L))).thenReturn(groupDataList);

        List<GroupDto> expected = List.of(new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        List<GroupDto> actual = groupService.getAllNoneMatching(comparisonGroups);
        assertEquals(expected, actual);
        verify(groupRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(groupRepository).findAllByIdNotInOrderByIdAsc(Set.of(1001L));
    }

    @Test
    void getAllNoneMatching_ComparisonGroups_should_ReturnListOfGroupDto_when_GroupsExist_ComparisonSetOfGroupIsEmpty() {
        List<Group> groupDataList = List.of(new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        Set<Group> comparisonGroups = Set.of();
        when(groupRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(groupDataList);

        List<GroupDto> expected = List.of(new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        List<GroupDto> actual = groupService.getAllNoneMatching(comparisonGroups);
        assertEquals(expected, actual);
        verify(groupRepository).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(groupRepository, never()).findAllByIdNotInOrderByIdAsc(anySet());
    }

    @Test
    void getAllNoneMatching_ComparisonGroups_should_ReturnEmptyListOfGroupDto_when_GroupsDoNotExist() {
        List<Group> groupDataList = List.of();
        Set<Group> comparisonGroups = Set.of(new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of()));
        when(groupRepository.findAllByIdNotInOrderByIdAsc(Set.of(1001L))).thenReturn(groupDataList);

        List<GroupDto> expected = List.of();
        List<GroupDto> actual = groupService.getAllNoneMatching(comparisonGroups);
        assertEquals(expected, actual);
        verify(groupRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(groupRepository).findAllByIdNotInOrderByIdAsc(Set.of(1001L));
    }

    @Test
    void getById_should_ReturnGroupDto_when_GroupByIdExists() {
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        when(groupRepository.findById(1000L)).thenReturn(Optional.of(groupData));

        GroupDto expected = new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        GroupDto actual = groupService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_GroupByIdDoesNotExist() {
        when(groupRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> groupService.getById(9999L));
    }

    @Test
    void getByName_should_ReturnGroupDto_when_GroupByNameExists() {
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        when(groupRepository.findByName("1SE-24")).thenReturn(Optional.of(groupData));

        GroupDto expected = new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        GroupDto actual = groupService.getByName("1SE-24");
        assertEquals(expected, actual);
    }

    @Test
    void getByName_should_ThrowEntityNotFoundException_when_GroupByNameDoesNotExist() {
        when(groupRepository.findByName("9YY-99")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> groupService.getByName("9YY-99"));
    }

    @Test
    void updateName_should_UpdateNameOfGroup_ReturnUpdatedGroupDto_when_GroupByIdExists() {
        UpdateGroupRequest updateGroupRequest = new UpdateGroupRequest("2SE-24");
        Group updatedGroup = new Group(1000L, "2SE-24", Set.of(), Set.of(), Set.of());
        when(!groupRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(groupRepository.getReferenceById(1000L)).thenReturn(updatedGroup);

        GroupDto expected = new GroupDto(1000L, "2SE-24", Set.of(), Set.of(), Set.of());
        GroupDto actual = groupService.updateName(1000L, updateGroupRequest);
        assertEquals(expected, actual);
        verify(groupRepository).updateName(1000L, updateGroupRequest.getName());
    }

    @Test
    void updateName_should_ThrowEntityNotFoundException_when_GroupByIdDoesNotExist() {
        UpdateGroupRequest updateGroupRequest = new UpdateGroupRequest("2SE-24");
        when(!groupRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> groupService.updateName(9999L, updateGroupRequest));
    }

    @Test
    void deleteById_should_DeleteGroupById_when_GroupByIdExists() {
        when(!groupRepository.existsById(1000L)).thenReturn(Boolean.TRUE);

        groupService.deleteById(1000L);
        verify(groupRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_GroupByIdDoesNotExist() {
        when(!groupRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> groupService.deleteById(9999L));
    }
}
