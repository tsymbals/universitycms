package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.LessonDto;
import com.tsymbals.universitycms.models.dto.StudentDto;
import com.tsymbals.universitycms.models.dto.TeacherDto;
import com.tsymbals.universitycms.models.dto.requests.CreateLessonRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateLessonRequest;
import com.tsymbals.universitycms.models.entities.*;
import com.tsymbals.universitycms.repositories.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = LessonServiceImpl.class)
class LessonServiceImplTest {

    static final University UNIVERSITY_DATA = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
    static final Department DEPARTMENT_DATA = new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of());
    static final LessonTime LESSON_TIME_DATA = new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
    static final Auditorium AUDITORIUM_DATA = new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of());
    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final Role TEACHER_ROLE_DATA = new Role(1000L, "ROLE_TEACHER");
    static final Role STUDENT_ROLE_DATA = new Role(1001L, "ROLE_STUDENT");
    static final UserEntity TEACHER_USER_DATA = new UserEntity(1000L, "teacher1", ENCODED_PASSWORD, true, false, Set.of(TEACHER_ROLE_DATA));
    static final UserEntity STUDENT_USER_DATA = new UserEntity(1001L, "student1", ENCODED_PASSWORD, true, false, Set.of(STUDENT_ROLE_DATA));
    static final Teacher TEACHER_DATA = new Teacher(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
    static final Group GROUP_DATA = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());

    @MockBean
    LessonRepository lessonRepository;

    @MockBean
    LessonTimeRepository lessonTimeRepository;

    @MockBean
    AuditoriumRepository auditoriumRepository;

    @MockBean
    CourseRepository courseRepository;

    @MockBean
    TeacherRepository teacherRepository;

    @MockBean
    GroupRepository groupRepository;

    @Autowired
    LessonServiceImpl lessonService;

    @Test
    void save_should_SaveNewLesson_ReturnLessonDto_when_CreateLessonRequestIsValid_LessonTimeByIdExists_AuditoriumByNameExists() {
        CreateLessonRequest createLessonRequest = new CreateLessonRequest(LocalDate.of(2024, 1, 1), 1000L, "101");
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonTimeRepository.findById(createLessonRequest.getLessonTimeId())).thenReturn(Optional.of(LESSON_TIME_DATA));
        when(auditoriumRepository.findByName(createLessonRequest.getAuditoriumName())).thenReturn(Optional.of(AUDITORIUM_DATA));
        when(lessonRepository.save(any(Lesson.class))).thenReturn(lessonData);

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        LessonDto actual = lessonService.save(createLessonRequest);
        assertEquals(expected, actual);
    }

    @Test
    void save_should_ThrowEntityNotFoundException_when_CreateLessonRequestIsValid_LessonTimeByIdDoesNotExist() {
        CreateLessonRequest createLessonRequest = new CreateLessonRequest(LocalDate.of(2024, 1, 1), 1001L, "101");
        when(lessonTimeRepository.findById(createLessonRequest.getLessonTimeId())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.save(createLessonRequest));
    }

    @Test
    void save_should_ThrowEntityNotFoundException_when_CreateLessonRequestIsValid_AuditoriumByNameDoesNotExist() {
        CreateLessonRequest createLessonRequest = new CreateLessonRequest(LocalDate.of(2024, 1, 1), 1000L, "102");
        when(lessonTimeRepository.findById(createLessonRequest.getLessonTimeId())).thenReturn(Optional.of(LESSON_TIME_DATA));
        when(auditoriumRepository.findByName(createLessonRequest.getAuditoriumName())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.save(createLessonRequest));
    }

    @Test
    void getAll_should_ReturnListOfLessonDto_when_LessonsExist() {
        List<Lesson> lessonDataList = List.of(new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of()));
        when(lessonRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of()));
        List<LessonDto> actual = lessonService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfLessonDto_when_LessonsDoNotExist() {
        List<Lesson> lessonDataList = List.of();
        when(lessonRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of();
        List<LessonDto> actual = lessonService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAllByDate_should_ReturnListOfLessonDto_when_LessonsExist_DateOfLessonEqualsSelectedDate() {
        List<Lesson> lessonDataList = List.of(new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of()));
        when(lessonRepository.findAllByDateOrderByIdAsc(LocalDate.of(2024, 1, 1))).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of()));
        List<LessonDto> actual = lessonService.getAllByDate(LocalDate.of(2024, 1, 1));
        assertEquals(expected, actual);
    }

    @Test
    void getAllByDate_should_ReturnEmptyListOfLessonDto_when_LessonsDoNotExist_Or_DateOfLessonDoesNotEqualsSelectedDate() {
        List<Lesson> lessonDataList = List.of();
        when(lessonRepository.findAllByDateOrderByIdAsc(LocalDate.of(2024, 1, 2))).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of();
        List<LessonDto> actual = lessonService.getAllByDate(LocalDate.of(2024, 1, 2));
        assertEquals(expected, actual);
    }

    @Test
    void getAllByDate_TeacherDto_should_ReturnListOfLessonDto_when_LessonsExist_TeacherOfLessonIsNotNull_DateOfLessonEqualsSelectedDate() {
        List<Lesson> lessonDataList = List.of(new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, TEACHER_DATA, Set.of()));
        when(lessonRepository.findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc(LocalDate.of(2024, 1, 1), 1000L)).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, TEACHER_DATA, Set.of()));
        List<LessonDto> actual = lessonService.getAllByDate(LocalDate.of(2024, 1, 1), new TeacherDto(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        assertEquals(expected, actual);
    }

    @Test
    void getAllByDate_TeacherDto_should_ReturnEmptyListOfLessonDto_when_LessonsDoNotExist_Or_TeacherOfLessonIsNull_Or_DateOfLessonDoesNotEqualsSelectedDate() {
        List<Lesson> lessonDataList = List.of();
        when(lessonRepository.findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc(LocalDate.of(2024, 1, 2), 1000L)).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of();
        List<LessonDto> actual = lessonService.getAllByDate(LocalDate.of(2024, 1, 2), new TeacherDto(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        assertEquals(expected, actual);
    }

    @Test
    void getAllByDate_StudentDto_should_ReturnListOfLessonDto_when_LessonsExist_GroupsOfLessonAreNotEmpty_GroupOfStudentIsNotNull_DateOfLessonEqualsSelectedDate() {
        List<Lesson> lessonDataList = List.of(new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of(GROUP_DATA)));
        when(lessonRepository.findAllByDateAndGroups_NameOrderByIdAsc(LocalDate.of(2024, 1, 1), "1SE-24")).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of(GROUP_DATA)));
        List<LessonDto> actual = lessonService.getAllByDate(LocalDate.of(2024, 1, 1), new StudentDto(1000L, STUDENT_USER_DATA, GROUP_DATA, "Emily", "Hensley", Set.of()));
        assertEquals(expected, actual);
    }

    @Test
    void getAllByDate_StudentDto_should_ReturnEmptyListOfLessonDto_when_LessonsDoNotExist_Or_GroupsOfLessonAreEmpty_Or_GroupOfStudentIsNull_Or_DateOfLessonDoesNotEqualsSelectedDate() {
        List<Lesson> lessonDataList = List.of();
        when(lessonRepository.findAllByDateAndGroups_NameOrderByIdAsc(LocalDate.of(2024, 1, 2), "1SE-24")).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of();
        List<LessonDto> actual = lessonService.getAllByDate(LocalDate.of(2024, 1, 2), new StudentDto(1000L, STUDENT_USER_DATA, null, "Emily", "Hensley", Set.of()));
        assertEquals(expected, actual);
    }

    @Test
    void getAllByWeek_TeacherDto_should_ReturnListOfLessonDto_when_LessonsExist_TeacherOfLessonsIsNotNull_DatesOfLessonsContainInWeekRange() {
        List<Lesson> lessonDataList = List.of(new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, TEACHER_DATA, Set.of()));
        when(lessonRepository.findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc(LocalDate.of(2024, 1, 1), LocalDate.of(2024, 1, 7), 1000L)).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, TEACHER_DATA, Set.of()));
        List<LessonDto> actual = lessonService.getAllByWeek(LocalDate.of(2024, 1, 1), LocalDate.of(2024, 1, 7), new TeacherDto(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        assertEquals(expected, actual);
    }

    @Test
    void getAllByWeek_TeacherDto_should_ReturnEmptyListOfLessonDto_when_LessonsDoNotExist_Or_TeacherOfLessonsIsNull_DatesOfLessonsDoNotContainInWeekRange() {
        List<Lesson> lessonDataList = List.of();
        when(lessonRepository.findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc(LocalDate.of(2024, 1, 1), LocalDate.of(2024, 1, 7), 1000L)).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of();
        List<LessonDto> actual = lessonService.getAllByWeek(LocalDate.of(2024, 1, 1), LocalDate.of(2024, 1, 7), new TeacherDto(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        assertEquals(expected, actual);
    }

    @Test
    void getAllByWeek_StudentDto_should_ReturnListOfLessonDto_when_LessonsExist_GroupsOfLessonsAreNotEmpty_GroupOfStudentIsNotNull_DatesOfLessonsContainInWeekRange() {
        List<Lesson> lessonDataList = List.of(new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of(GROUP_DATA)));
        when(lessonRepository.findAllByDateBetweenAndGroups_NameOrderByIdAsc(LocalDate.of(2024, 1, 1), LocalDate.of(2024, 1, 7), "1SE-24")).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of(GROUP_DATA)));
        List<LessonDto> actual = lessonService.getAllByWeek(LocalDate.of(2024, 1, 1), LocalDate.of(2024, 1, 7), new StudentDto(1000L, STUDENT_USER_DATA, GROUP_DATA, "Emily", "Hensley", Set.of()));
        assertEquals(expected, actual);
    }

    @Test
    void getAllByWeek_StudentDto_should_ReturnEmptyListOfLessonDto_when_LessonsDoNotExist_Or_GroupsOfLessonsAreEmpty_Or_GroupOfStudentIsNull_Or_DatesOfLessonsDoNotContainInWeekRange() {
        List<Lesson> lessonDataList = List.of();
        when(lessonRepository.findAllByDateBetweenAndGroups_NameOrderByIdAsc(LocalDate.of(2024, 1, 1), LocalDate.of(2024, 1, 7), "1SE-24")).thenReturn(lessonDataList);

        List<LessonDto> expected = List.of();
        List<LessonDto> actual = lessonService.getAllByWeek(LocalDate.of(2024, 1, 1), LocalDate.of(2024, 1, 7), new StudentDto(1000L, STUDENT_USER_DATA, null, "Emily", "Hensley", Set.of()));
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ReturnLessonDto_when_LessonByIdExists() {
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        LessonDto actual = lessonService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        when(lessonRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.getById(9999L));
    }

    @Test
    void getByDateAndStartTimeAndEndTime_should_ReturnLessonDto_when_LessonByDateAndStartTimeAndEndTimeExists() {
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findByDateAndLessonTime_StartTimeAndLessonTime_EndTime(LocalDate.of(2024, 1, 1), LocalTime.of(8, 15), LocalTime.of(9, 0))).thenReturn(Optional.of(lessonData));

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        LessonDto actual = lessonService.getByDateAndStartTimeAndEndTime(LocalDate.of(2024, 1, 1), LocalTime.of(8, 15), LocalTime.of(9, 0));
        assertEquals(expected, actual);
    }

    @Test
    void getByDateAndStartTimeAndEndTime_should_ThrowEntityNotFoundException_when_LessonByDateAndStartTimeAndEndTimeDoesNotExist() {
        LocalDate date = LocalDate.of(2026, 1, 1);
        LocalTime startTime = LocalTime.of(22, 59);
        LocalTime endTime = LocalTime.of(23, 59);
        when(lessonRepository.findByDateAndLessonTime_StartTimeAndLessonTime_EndTime(date, startTime, endTime)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.getByDateAndStartTimeAndEndTime(date, startTime, endTime));
    }

    @Test
    void updateDate_should_UpdateDateOfLesson_ReturnUpdatedLessonDto_when_LessonByIdExists() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(LocalDate.of(2025, 1, 1), null, null, null, null, null, null, null, null);
        Lesson updatedLesson = new Lesson(1000L, LocalDate.of(2025, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(!lessonRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(lessonRepository.getReferenceById(1000L)).thenReturn(updatedLesson);

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2025, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        LessonDto actual = lessonService.updateDate(1000L, updateLessonRequest);
        assertEquals(expected, actual);
        verify(lessonRepository).updateDate(1000L, updateLessonRequest.getDate());
    }

    @Test
    void updateDate_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(LocalDate.of(2025, 1, 1), null, null, null, null, null, null, null, null);
        when(!lessonRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> lessonService.updateDate(9999L, updateLessonRequest));
    }

    @Test
    void updateLessonTime_should_UpdateLessonTimeOfLesson_ReturnUpdatedLessonDto_when_LessonByIdExists_LessonTimeByIdExists() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, 1001L, null, null, null, null, null, null, null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        LessonTime lessonTimeData = new LessonTime(1001L, LocalTime.of(9, 15), LocalTime.of(10, 0), Set.of());
        Lesson updatedLesson = new Lesson(1000L, LocalDate.of(2024, 1, 1), lessonTimeData, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(lessonTimeRepository.findById(updateLessonRequest.getLessonTimeId())).thenReturn(Optional.of(lessonTimeData));
        when(lessonRepository.getReferenceById(1000L)).thenReturn(updatedLesson);

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), lessonTimeData, AUDITORIUM_DATA, null, null, Set.of());
        LessonDto actual = lessonService.updateLessonTime(1000L, updateLessonRequest);
        assertEquals(expected, actual);
        verify(lessonRepository).save(updatedLesson);
    }

    @Test
    void updateLessonTime_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, 1001L, null, null, null, null, null, null, null);
        when(lessonRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.updateLessonTime(9999L, updateLessonRequest));
    }

    @Test
    void updateLessonTime_should_ThrowEntityNotFoundException_when_LessonTimeByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, 9999L, null, null, null, null, null, null, null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(lessonTimeRepository.findById(updateLessonRequest.getLessonTimeId())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.updateLessonTime(1000L, updateLessonRequest));
    }

    @Test
    void updateAuditorium_should_UpdateAuditoriumOfLesson_ReturnUpdatedLessonDto_when_LessonByIdExists_AuditoriumByNameExists() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, "201", null, null, null, null, null, null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        Auditorium auditoriumData = new Auditorium(1001L, DEPARTMENT_DATA, "201", Set.of());
        Lesson updatedLesson = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, auditoriumData, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(auditoriumRepository.findByName(updateLessonRequest.getAuditoriumName())).thenReturn(Optional.of(auditoriumData));
        when(lessonRepository.getReferenceById(1000L)).thenReturn(updatedLesson);

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, auditoriumData, null, null, Set.of());
        LessonDto actual = lessonService.updateAuditorium(1000L, updateLessonRequest);
        assertEquals(expected, actual);
        verify(lessonRepository).save(updatedLesson);
    }

    @Test
    void updateAuditorium_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, "201", null, null, null, null, null, null);
        when(lessonRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.updateAuditorium(9999L, updateLessonRequest));
    }

    @Test
    void updateAuditorium_should_ThrowEntityNotFoundException_when_AuditoriumByNameDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, "999", null, null, null, null, null, null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(auditoriumRepository.findByName(updateLessonRequest.getAuditoriumName())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.updateAuditorium(1000L, updateLessonRequest));
    }

    @Test
    void updateCourse_should_UpdateCourseOfLesson_ReturnUpdatedLessonDto_when_LessonByIdExists_CourseByNameExists() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, "English", null, null, null, null, null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        Lesson updatedLesson = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, courseData, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(courseRepository.findByName(updateLessonRequest.getCourseNameToUpdate())).thenReturn(Optional.of(courseData));
        when(lessonRepository.getReferenceById(1000L)).thenReturn(updatedLesson);

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, courseData, null, Set.of());
        LessonDto actual = lessonService.updateCourse(1000L, updateLessonRequest);
        assertEquals(expected, actual);
        verify(lessonRepository).save(updatedLesson);
    }

    @Test
    void updateCourse_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, "English", null, null, null, null, null);
        when(lessonRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.updateCourse(9999L, updateLessonRequest));
    }

    @Test
    void updateCourse_should_ThrowEntityNotFoundException_when_CourseByNameDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, "Mathematics", null, null, null, null, null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(courseRepository.findByName(updateLessonRequest.getCourseNameToUpdate())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.updateCourse(1000L, updateLessonRequest));
    }

    @Test
    void removeCourse_should_RemoveCourseFromLesson_ReturnUpdatedLessonDto_when_LessonByIdExists_CourseOfLessonIsNotNull_CourseByNameExists_LessonContainsExistingCourse() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, "English", null, null, null, null);
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, courseData, null, Set.of());
        Lesson updatedLesson = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(courseRepository.findByName(updateLessonRequest.getCourseNameToRemove())).thenReturn(Optional.of(courseData));
        when(lessonRepository.getReferenceById(1000L)).thenReturn(updatedLesson);

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        LessonDto actual = lessonService.removeCourse(1000L, updateLessonRequest);
        assertEquals(expected, actual);
        verify(lessonRepository).save(updatedLesson);
    }

    @Test
    void removeCourse_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, "English", null, null, null, null);
        when(lessonRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.removeCourse(9999L, updateLessonRequest));
    }

    @Test
    void removeCourse_should_ThrowIllegalArgumentException_when_CourseOfLessonIsNull() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, "English", null, null, null, null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));

        assertThrows(IllegalArgumentException.class, () -> lessonService.removeCourse(1000L, updateLessonRequest));
    }

    @Test
    void removeCourse_should_ThrowEntityNotFoundException_when_CourseByNameDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, "Mathematics", null, null, null, null);
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, courseData, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(courseRepository.findByName(updateLessonRequest.getCourseNameToRemove())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.removeCourse(1000L, updateLessonRequest));
    }

    @Test
    void removeCourse_should_ThrowIllegalArgumentException_when_LessonDoesNotContainExistingCourse() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, "English", null, null, null, null);
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        Course lessonCourseData = new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of());
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, lessonCourseData, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(courseRepository.findByName(updateLessonRequest.getCourseNameToRemove())).thenReturn(Optional.of(courseData));

        assertThrows(IllegalArgumentException.class, () -> lessonService.removeCourse(1000L, updateLessonRequest));
    }

    @Test
    void updateTeacher_should_UpdateTeacherOfLesson_ReturnUpdatedLessonDto_when_LessonByIdExists_TeacherByIdExists() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, 1000L, null, null, null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        Teacher teacherData = new Teacher(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        Lesson updatedLesson = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, teacherData, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(teacherRepository.findById(updateLessonRequest.getTeacherIdToUpdate())).thenReturn(Optional.of(teacherData));
        when(lessonRepository.getReferenceById(1000L)).thenReturn(updatedLesson);

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, teacherData, Set.of());
        LessonDto actual = lessonService.updateTeacher(1000L, updateLessonRequest);
        assertEquals(expected, actual);
        verify(lessonRepository).save(updatedLesson);
    }

    @Test
    void updateTeacher_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, 1000L, null, null, null);
        when(lessonRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.updateTeacher(9999L, updateLessonRequest));
    }

    @Test
    void updateTeacher_should_ThrowEntityNotFoundException_when_TeacherByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, 9999L, null, null, null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(teacherRepository.findById(updateLessonRequest.getTeacherIdToUpdate())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.updateTeacher(1000L, updateLessonRequest));
    }

    @Test
    void removeTeacher_should_RemoveTeacherFromLesson_ReturnUpdatedLessonDto_when_LessonByIdExists_TeacherOfLessonIsNotNull_TeacherByIdExists_LessonContainsExistingTeacher() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, 1000L, null, null);
        Teacher teacherData = new Teacher(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, teacherData, Set.of());
        Lesson updatedLesson = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(teacherRepository.findById(updateLessonRequest.getTeacherIdToRemove())).thenReturn(Optional.of(teacherData));
        when(lessonRepository.getReferenceById(1000L)).thenReturn(updatedLesson);

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        LessonDto actual = lessonService.removeTeacher(1000L, updateLessonRequest);
        assertEquals(expected, actual);
        verify(lessonRepository).save(updatedLesson);
    }

    @Test
    void removeTeacher_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, 1000L, null, null);
        when(lessonRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.removeTeacher(9999L, updateLessonRequest));
    }

    @Test
    void removeTeacher_should_ThrowIllegalArgumentException_when_TeacherOfLessonIsNull() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, 1000L, null, null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));

        assertThrows(IllegalArgumentException.class, () -> lessonService.removeTeacher(1000L, updateLessonRequest));
    }

    @Test
    void removeTeacher_should_ThrowEntityNotFoundException_when_TeacherByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, 9999L, null, null);
        Teacher teacherData = new Teacher(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, teacherData, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(teacherRepository.findById(updateLessonRequest.getTeacherIdToRemove())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.removeTeacher(1000L, updateLessonRequest));
    }

    @Test
    void removeTeacher_should_ThrowIllegalArgumentException_when_LessonDoesNotContainExistingTeacher() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, 1000L, null, null);
        Teacher teacherData = new Teacher(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        Teacher lessonTeacherData = new Teacher(1001L, TEACHER_USER_DATA, "Guy", "Griffin", Set.of(), Set.of());
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, lessonTeacherData, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(teacherRepository.findById(updateLessonRequest.getTeacherIdToRemove())).thenReturn(Optional.of(teacherData));

        assertThrows(IllegalArgumentException.class, () -> lessonService.removeTeacher(1000L, updateLessonRequest));
    }

    @Test
    void addGroup_should_AddGroupToLesson_ReturnUpdatedLessonDto_when_LessonByIdExists_GroupByNameExists_LessonDoesNotContainExistingGroup() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, null, "1SE-24", null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, new HashSet<>());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        Lesson updatedLesson = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of(groupData));
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(groupRepository.findByName(updateLessonRequest.getGroupNameToAdd())).thenReturn(Optional.of(groupData));
        when(lessonRepository.getReferenceById(1000L)).thenReturn(updatedLesson);

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of(groupData));
        LessonDto actual = lessonService.addGroup(1000L, updateLessonRequest);
        assertEquals(expected, actual);
        verify(lessonRepository).save(updatedLesson);
    }

    @Test
    void addGroup_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, null, "1SE-24", null);
        when(lessonRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.addGroup(9999L, updateLessonRequest));
    }

    @Test
    void addGroup_should_ThrowEntityNotFoundException_when_GroupByNameDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, null, "9YY-99", null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, new HashSet<>());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(groupRepository.findByName(updateLessonRequest.getGroupNameToAdd())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.addGroup(1000L, updateLessonRequest));
    }

    @Test
    void addGroup_should_ThrowIllegalArgumentException_when_LessonContainsExistingGroup() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, null, "1SE-24", null);
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, new HashSet<>());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        lessonData.getGroups().add(groupData);
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(groupRepository.findByName(updateLessonRequest.getGroupNameToAdd())).thenReturn(Optional.of(groupData));

        assertThrows(IllegalArgumentException.class, () -> lessonService.addGroup(1000L, updateLessonRequest));
    }

    @Test
    void removeGroup_should_RemoveGroupFromLesson_ReturnUpdatedLessonDto_when_LessonByIdExists_GroupByNameExists_LessonContainsExistingGroup() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, null, null, "1SE-24");
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, new HashSet<>());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        lessonData.getGroups().add(groupData);
        Lesson updatedLesson = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(groupRepository.findByName(updateLessonRequest.getGroupNameToRemove())).thenReturn(Optional.of(groupData));
        when(lessonRepository.getReferenceById(1000L)).thenReturn(updatedLesson);

        LessonDto expected = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        LessonDto actual = lessonService.removeGroup(1000L, updateLessonRequest);
        assertEquals(expected, actual);
        verify(lessonRepository).save(updatedLesson);
    }

    @Test
    void removeGroup_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, null, null, "1SE-24");
        when(lessonRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.removeGroup(9999L, updateLessonRequest));
    }

    @Test
    void removeGroup_should_ThrowEntityNotFoundException_when_GroupByNameDoesNotExist() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, null, null, "9YY-99");
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, new HashSet<>());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(groupRepository.findByName(updateLessonRequest.getGroupNameToRemove())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> lessonService.removeGroup(1000L, updateLessonRequest));
    }

    @Test
    void removeGroup_should_ThrowIllegalArgumentException_when_LessonDoesNotContainExistingGroup() {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(null, null, null, null, null, null, null, null, "1SE-24");
        Lesson lessonData = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, new HashSet<>());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        when(lessonRepository.findById(1000L)).thenReturn(Optional.of(lessonData));
        when(groupRepository.findByName(updateLessonRequest.getGroupNameToRemove())).thenReturn(Optional.of(groupData));

        assertThrows(IllegalArgumentException.class, () -> lessonService.removeGroup(1000L, updateLessonRequest));
    }

    @Test
    void deleteById_should_DeleteLessonById_when_LessonByIdExists() {
        when(!lessonRepository.existsById(1000L)).thenReturn(Boolean.TRUE);

        lessonService.deleteById(1000L);
        verify(lessonRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_LessonByIdDoesNotExist() {
        when(!lessonRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> lessonService.deleteById(9999L));
    }
}
