package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateUserRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateUserRequest;
import com.tsymbals.universitycms.models.entities.Role;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.repositories.RoleRepository;
import com.tsymbals.universitycms.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = UserServiceImpl.class)
class UserServiceImplTest {

    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final String NEW_ENCODED_PASSWORD = "$2a$10$V/fQ2QxuVvkbur/SItrICe2XE8ArgR5Ks639aH66Dm9QVsWVi81QO";
    static final Role ADMIN_ROLE_DATA = new Role(1000L, "ROLE_ADMIN");
    static final Role STAFF_ROLE_DATA = new Role(1001L, "ROLE_STAFF");
    static final Role TEACHER_ROLE_DATA = new Role(1002L, "ROLE_TEACHER");
    static final Role STUDENT_ROLE_DATA = new Role(1003L, "ROLE_STUDENT");

    @MockBean
    UserRepository  userRepository;

    @MockBean
    RoleRepository roleRepository;

    @MockBean
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    UserServiceImpl userService;

    @Test
    void save_should_SaveNewUser_ReturnUserDto_when_CreateUserRequestIsValid() {
        CreateUserRequest createUserRequest = new CreateUserRequest("username1", "Password1");
        UserEntity userData = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of());
        when(bCryptPasswordEncoder.encode(createUserRequest.getPassword())).thenReturn(userData.getPassword());
        when(userRepository.save(any(UserEntity.class))).thenReturn(userData);

        UserDto expected = new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of());
        UserDto actual = userService.save(createUserRequest);
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnListOfUserDto_when_UsersExist() {
        List<UserEntity> userDataList = List.of(new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of()));
        when(userRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(userDataList);

        List<UserDto> expected = List.of(new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of()));
        List<UserDto> actual = userService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfUserDto_when_UsersDoNotExist() {
        List<UserEntity> userDataList = List.of();
        when(userRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(userDataList);

        List<UserDto> expected = List.of();
        List<UserDto> actual = userService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAllFreeAdminUsers_should_ReturnListOfUserDto_when_FreeAdminUsersExist() {
        List<UserEntity> userDataList = List.of(new UserEntity(1001L, "admin1", ENCODED_PASSWORD, true, true, Set.of(ADMIN_ROLE_DATA)));
        when(userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc("role_admin")).thenReturn(userDataList);

        List<UserDto> expected = List.of(new UserDto(1001L, "admin1", ENCODED_PASSWORD, true, true, Set.of(ADMIN_ROLE_DATA)));
        List<UserDto> actual = userService.getAllFreeAdminUsers();
        assertEquals(expected, actual);
    }

    @Test
    void getAllFreeAdminUsers_should_ReturnEmptyListOfUserDto_when_FreeAdminUsersDoNotExist() {
        List<UserEntity> userDataList = List.of();
        when(userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc("role_admin")).thenReturn(userDataList);

        List<UserDto> expected = List.of();
        List<UserDto> actual = userService.getAllFreeAdminUsers();
        assertEquals(expected, actual);
    }

    @Test
    void getAllFreeEmployeeUsers_should_ReturnListOfUserDto_when_FreeEmployeeUsersExist() {
        List<UserEntity> userDataList = List.of(new UserEntity(1002L, "employee1", ENCODED_PASSWORD, true, true, Set.of(STAFF_ROLE_DATA)));
        when(userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc("role_staff")).thenReturn(userDataList);

        List<UserDto> expected = List.of(new UserDto(1002L, "employee1", ENCODED_PASSWORD, true, true, Set.of(STAFF_ROLE_DATA)));
        List<UserDto> actual = userService.getAllFreeEmployeeUsers();
        assertEquals(expected, actual);
    }

    @Test
    void getAllFreeEmployeeUsers_should_ReturnEmptyListOfUserDto_when_FreeEmployeeUsersDoNotExist() {
        List<UserEntity> userDataList = List.of();
        when(userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc("role_staff")).thenReturn(userDataList);

        List<UserDto> expected = List.of();
        List<UserDto> actual = userService.getAllFreeEmployeeUsers();
        assertEquals(expected, actual);
    }

    @Test
    void getAllFreeTeacherUsers_should_ReturnListOfUserDto_when_FreeTeacherUsersExist() {
        List<UserEntity> userDataList = List.of(new UserEntity(1003L, "teacher1", ENCODED_PASSWORD, true, true, Set.of(TEACHER_ROLE_DATA)));
        when(userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc("role_teacher")).thenReturn(userDataList);

        List<UserDto> expected = List.of(new UserDto(1003L, "teacher1", ENCODED_PASSWORD, true, true, Set.of(TEACHER_ROLE_DATA)));
        List<UserDto> actual = userService.getAllFreeTeacherUsers();
        assertEquals(expected, actual);
    }

    @Test
    void getAllFreeTeacherUsers_should_ReturnEmptyListOfUserDto_when_FreeTeacherUsersDoNotExist() {
        List<UserEntity> userDataList = List.of();
        when(userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc("role_teacher")).thenReturn(userDataList);

        List<UserDto> expected = List.of();
        List<UserDto> actual = userService.getAllFreeTeacherUsers();
        assertEquals(expected, actual);
    }

    @Test
    void getAllFreeStudentUsers_should_ReturnListOfUserDto_when_FreeStudentUsersExist() {
        List<UserEntity> userDataList = List.of(new UserEntity(1004L, "student1", ENCODED_PASSWORD, true, true, Set.of(STUDENT_ROLE_DATA)));
        when(userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc("role_student")).thenReturn(userDataList);

        List<UserDto> expected = List.of(new UserDto(1004L, "student1", ENCODED_PASSWORD, true, true, Set.of(STUDENT_ROLE_DATA)));
        List<UserDto> actual = userService.getAllFreeStudentUsers();
        assertEquals(expected, actual);
    }

    @Test
    void getAllFreeStudentUsers_should_ReturnEmptyListOfUserDto_when_FreeStudentUsersDoNotExist() {
        List<UserEntity> userDataList = List.of();
        when(userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc("role_student")).thenReturn(userDataList);

        List<UserDto> expected = List.of();
        List<UserDto> actual = userService.getAllFreeStudentUsers();
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ReturnUserDto_when_UserByIdExists() {
        UserEntity userData = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of());
        when(userRepository.findById(1000L)).thenReturn(Optional.of(userData));

        UserDto expected = new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of());
        UserDto actual = userService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_UserByIdDoesNotExist() {
        when(userRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.getById(9999L));
    }

    @Test
    void getByUsername_should_ReturnUserDto_when_UserByUsernameExists() {
        UserEntity userData = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of());
        when(userRepository.findByUsername("username1")).thenReturn(Optional.of(userData));

        UserDto expected = new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of());
        UserDto actual = userService.getByUsername("username1");
        assertEquals(expected, actual);
    }

    @Test
    void getByUsername_should_ThrowEntityNotFoundException_when_UserByUsernameDoesNotExist() {
        when(userRepository.findByUsername("name1")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.getByUsername("name1"));
    }

    @Test
    void updateUsername_should_UpdateUsernameOfUser_ReturnUpdatedUserDto_when_UserByIdExists() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest("username2", null, null, null, null);
        UserEntity updatedUser = new UserEntity(1000L, "username2", ENCODED_PASSWORD, true, true, Set.of());
        when(!userRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(userRepository.getReferenceById(1000L)).thenReturn(updatedUser);

        UserDto expected = new UserDto(1000L, "username2", ENCODED_PASSWORD, true, true, Set.of());
        UserDto actual = userService.updateUsername(1000L, updateUserRequest);
        assertEquals(expected, actual);
        verify(userRepository).updateUsername(1000L, updateUserRequest.getUsername());
    }

    @Test
    void updateUsername_should_ThrowEntityNotFoundException_when_UserByIdDoesNotExist() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest("username2", null, null, null, null);
        when(!userRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> userService.updateUsername(9999L, updateUserRequest));
    }

    @Test
    void updatePassword_should_UpdatePasswordOfUser_ReturnUpdatedUserDto_when_UserByIdExists() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, "Password2", null, null, null);
        UserEntity updatedUser = new UserEntity(1000L, "username1", NEW_ENCODED_PASSWORD, true, true, Set.of());
        when(!userRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(bCryptPasswordEncoder.encode(updateUserRequest.getPassword())).thenReturn(updatedUser.getPassword());
        when(userRepository.getReferenceById(1000L)).thenReturn(updatedUser);

        UserDto expected = new UserDto(1000L, "username1", NEW_ENCODED_PASSWORD, true, true, Set.of());
        UserDto actual = userService.updatePassword(1000L, updateUserRequest);
        assertEquals(expected, actual);
        verify(userRepository).updatePassword(1000L, bCryptPasswordEncoder.encode(updateUserRequest.getPassword()));
    }

    @Test
    void updatePassword_should_ThrowEntityNotFoundException_when_UserByIdDoesNotExist() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, "Password2", null, null, null);
        when(!userRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> userService.updatePassword(9999L, updateUserRequest));
    }

    @Test
    void updateEnabledStatus_should_UpdateEnabledStatusOfUser_ReturnUpdatedUserDto_when_UserByIdExists() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, null, false, null, null);
        UserEntity updatedUser = new UserEntity(1000L, "username1", ENCODED_PASSWORD, false, true, Set.of());
        when(!userRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(userRepository.getReferenceById(1000L)).thenReturn(updatedUser);

        UserDto expected = new UserDto(1000L, "username1", ENCODED_PASSWORD, false, true, Set.of());
        UserDto actual = userService.updateEnabledStatus(1000L, updateUserRequest);
        assertEquals(expected, actual);
        verify(userRepository).updateEnabledStatus(1000L, updateUserRequest.getEnabledStatus());
    }

    @Test
    void updateEnabledStatus_should_ThrowEntityNotFoundException_when_UserByIdDoesNotExist() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, null, false, null, null);
        when(!userRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> userService.updateEnabledStatus(9999L, updateUserRequest));
    }

    @Test
    void addRole_should_AddRoleToUser_ReturnUpdatedUserDto_when_UserByIdExists_RoleByNameExists_UserDoesNotContainExistingRole() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, null, null, "ROLE_ADMIN", null);
        UserEntity userData = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, new HashSet<>());
        Role roleData = new Role(1000L, "ROLE_ADMIN");
        UserEntity updatedUser = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of(roleData));
        when(userRepository.findById(1000L)).thenReturn(Optional.of(userData));
        when(roleRepository.findByName(updateUserRequest.getRoleNameToAdd())).thenReturn(Optional.of(roleData));
        when(userRepository.getReferenceById(1000L)).thenReturn(updatedUser);

        UserDto expected = new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of(roleData));
        UserDto actual = userService.addRole(1000L, updateUserRequest);
        assertEquals(expected, actual);
        verify(userRepository).save(updatedUser);
    }

    @Test
    void addRole_should_ThrowEntityNotFoundException_when_UserByIdDoesNotExist() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, null, null, "ROLE_ADMIN", null);
        when(userRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.addRole(9999L, updateUserRequest));
    }

    @Test
    void addRole_should_ThrowEntityNotFoundException_when_RoleByNameDoesNotExist() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, null, null, "ROLE_USER", null);
        UserEntity userData = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, new HashSet<>());
        when(userRepository.findById(1000L)).thenReturn(Optional.of(userData));
        when(roleRepository.findByName(updateUserRequest.getRoleNameToAdd())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.addRole(1000L, updateUserRequest));
    }

    @Test
    void addRole_should_ThrowIllegalArgumentException_when_UserContainsExistingRole() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, null, null, "ROLE_ADMIN", null);
        UserEntity userData = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, new HashSet<>());
        Role roleData = new Role(1000L, "ROLE_ADMIN");
        userData.getRoles().add(roleData);
        when(userRepository.findById(1000L)).thenReturn(Optional.of(userData));
        when(roleRepository.findByName(updateUserRequest.getRoleNameToAdd())).thenReturn(Optional.of(roleData));

        assertThrows(IllegalArgumentException.class, () -> userService.addRole(1000L, updateUserRequest));
    }

    @Test
    void removeRole_should_RemoveRoleFromUser_ReturnUpdatedUserDto_when_UserByIdExists_RoleByNameExists_UserContainsExistingRole() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, null, null, null, "ROLE_ADMIN");
        UserEntity userData = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, new HashSet<>());
        Role roleData = new Role(1000L, "ROLE_ADMIN");
        userData.getRoles().add(roleData);
        UserEntity updatedUser = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of());
        when(userRepository.findById(1000L)).thenReturn(Optional.of(userData));
        when(roleRepository.findByName(updateUserRequest.getRoleNameToRemove())).thenReturn(Optional.of(roleData));
        when(userRepository.getReferenceById(1000L)).thenReturn(updatedUser);

        UserDto expected = new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of());
        UserDto actual = userService.removeRole(1000L, updateUserRequest);
        assertEquals(expected, actual);
        verify(userRepository).save(updatedUser);
    }

    @Test
    void removeRole_should_ThrowEntityNotFoundException_when_UserByIdDoesNotExist() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, null, null, null, "ROLE_ADMIN");
        when(userRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.removeRole(9999L, updateUserRequest));
    }

    @Test
    void removeRole_should_ThrowEntityNotFoundException_when_RoleByNameDoesNotExist() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, null, null, null, "ROLE_USER");
        UserEntity userData = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, new HashSet<>());
        when(userRepository.findById(1000L)).thenReturn(Optional.of(userData));
        when(roleRepository.findByName(updateUserRequest.getRoleNameToRemove())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> userService.removeRole(1000L, updateUserRequest));
    }

    @Test
    void removeRole_should_ThrowIllegalArgumentException_when_UserDoesNotContainExistingRole() {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest(null, null, null, null, "ROLE_ADMIN");
        UserEntity userData = new UserEntity(1000L, "username1", ENCODED_PASSWORD, true, true, new HashSet<>());
        Role roleData = new Role(1000L, "ROLE_ADMIN");
        when(userRepository.findById(1000L)).thenReturn(Optional.of(userData));
        when(roleRepository.findByName(updateUserRequest.getRoleNameToRemove())).thenReturn(Optional.of(roleData));

        assertThrows(IllegalArgumentException.class, () -> userService.removeRole(1000L, updateUserRequest));
    }

    @Test
    void deleteById_should_DeleteUserById_when_UserByIdExists() {
        when(!userRepository.existsById(1000L)).thenReturn(Boolean.TRUE);

        userService.deleteById(1000L);
        verify(userRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_UserByIdDoesNotExist() {
        when(!userRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> userService.deleteById(9999L));
    }
}
