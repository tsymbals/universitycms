package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.RoleDto;
import com.tsymbals.universitycms.models.dto.requests.CreateRoleRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateRoleRequest;
import com.tsymbals.universitycms.models.entities.Role;
import com.tsymbals.universitycms.repositories.RoleRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = RoleServiceImpl.class)
class RoleServiceImplTest {

    @MockBean
    RoleRepository roleRepository;

    @Autowired
    RoleServiceImpl roleService;

    @Test
    void save_should_SaveNewRole_ReturnRoleDto_when_CreateRoleRequestIsValid() {
        CreateRoleRequest createRoleRequest = new CreateRoleRequest("ROLE_ADMIN");
        Role roleData = new Role(1000L, "ROLE_ADMIN");
        when(roleRepository.save(any(Role.class))).thenReturn(roleData);

        RoleDto expected = new RoleDto(1000L, "ROLE_ADMIN");
        RoleDto actual = roleService.save(createRoleRequest);
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnListOfRoleDto_when_RolesExist() {
        List<Role> roleDataList = List.of(new Role(1000L, "ROLE_ADMIN"));
        when(roleRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(roleDataList);

        List<RoleDto> expected = List.of(new RoleDto(1000L, "ROLE_ADMIN"));
        List<RoleDto> actual = roleService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfRoleDto_when_RolesDoNotExist() {
        List<Role> roleDataList = List.of();
        when(roleRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(roleDataList);

        List<RoleDto> expected = List.of();
        List<RoleDto> actual = roleService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_should_ReturnNoneMatchingListOfRoleDto_when_RolesExist_ComparisonSetOfRoleIsNotEmpty() {
        List<Role> roleDataList = List.of(new Role(1000L, "ROLE_ADMIN"));
        Set<Role> comparisonRoles = Set.of(new Role(1001L, "ROLE_STAFF"));
        when(roleRepository.findAllByIdNotInOrderByIdAsc(Set.of(1001L))).thenReturn(roleDataList);

        List<RoleDto> expected = List.of(new RoleDto(1000L, "ROLE_ADMIN"));
        List<RoleDto> actual = roleService.getAllNoneMatching(comparisonRoles);
        assertEquals(expected, actual);
        verify(roleRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(roleRepository).findAllByIdNotInOrderByIdAsc(Set.of(1001L));
    }

    @Test
    void getAllNoneMatching_should_ReturnListOfRoleDto_when_RolesExist_ComparisonSetOfRoleIsEmpty() {
        List<Role> roleDataList = List.of(new Role(1000L, "ROLE_ADMIN"));
        Set<Role> comparisonRoles = Set.of();
        when(roleRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(roleDataList);

        List<RoleDto> expected = List.of(new RoleDto(1000L, "ROLE_ADMIN"));
        List<RoleDto> actual = roleService.getAllNoneMatching(comparisonRoles);
        assertEquals(expected, actual);
        verify(roleRepository).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(roleRepository, never()).findAllByIdNotInOrderByIdAsc(anySet());
    }

    @Test
    void getAllNoneMatching_should_ReturnEmptyListOfRoleDto_when_RolesDoNotExist() {
        List<Role> roleDataList = List.of();
        Set<Role> comparisonRoles = Set.of(new Role(1001L, "ROLE_STAFF"));
        when(roleRepository.findAllByIdNotInOrderByIdAsc(Set.of(1001L))).thenReturn(roleDataList);

        List<RoleDto> expected = List.of();
        List<RoleDto> actual = roleService.getAllNoneMatching(comparisonRoles);
        assertEquals(expected, actual);
        verify(roleRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(roleRepository).findAllByIdNotInOrderByIdAsc(Set.of(1001L));
    }

    @Test
    void getById_should_ReturnRoleDto_when_RoleByIdExists() {
        Role roleData = new Role(1000L, "ROLE_ADMIN");
        when(roleRepository.findById(1000L)).thenReturn(Optional.of(roleData));

        RoleDto expected = new RoleDto(1000L, "ROLE_ADMIN");
        RoleDto actual = roleService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_RoleByIdDoesNotExist() {
        when(roleRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> roleService.getById(9999L));
    }

    @Test
    void getByName_should_ReturnRoleDto_when_RoleByNameExists() {
        Role roleData = new Role(1000L, "ROLE_ADMIN");
        when(roleRepository.findByName("ROLE_ADMIN")).thenReturn(Optional.of(roleData));

        RoleDto expected = new RoleDto(1000L, "ROLE_ADMIN");
        RoleDto actual = roleService.getByName("ROLE_ADMIN");
        assertEquals(expected, actual);
    }

    @Test
    void getByName_should_ThrowEntityNotFoundException_when_RoleByNameDoesNotExist() {
        when(roleRepository.findByName("ROLE_YYYY")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> roleService.getByName("ROLE_YYYY"));
    }

    @Test
    void updateName_should_UpdateNameOfRole_ReturnUpdatedRoleDto_when_RoleByIdExists() {
        UpdateRoleRequest updateRoleRequest = new UpdateRoleRequest("ROLE_STAFF");
        Role updatedRole = new Role(1000L, "ROLE_STAFF");
        when(!roleRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(roleRepository.getReferenceById(1000L)).thenReturn(updatedRole);

        RoleDto expected = new RoleDto(1000L, "ROLE_STAFF");
        RoleDto actual = roleService.updateName(1000L, updateRoleRequest);
        assertEquals(expected, actual);
        verify(roleRepository).updateName(1000L, updateRoleRequest.getName());
    }

    @Test
    void updateName_should_ThrowEntityNotFoundException_when_RoleByIdDoesNotExist() {
        UpdateRoleRequest updateRoleRequest = new UpdateRoleRequest("ROLE_STAFF");
        when(!roleRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> roleService.updateName(9999L, updateRoleRequest));
    }

    @Test
    void deleteById_should_DeleteRoleById_when_RoleByIdExists() {
        when(!roleRepository.existsById(1000L)).thenReturn(Boolean.TRUE);

        roleService.deleteById(1000L);
        verify(roleRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_RoleByIdDoesNotExist() {
        when(!roleRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> roleService.deleteById(9999L));
    }
}
