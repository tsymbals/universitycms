package com.tsymbals.universitycms.services.impl;

import com.tsymbals.universitycms.exceptions.EntityNotFoundException;
import com.tsymbals.universitycms.models.dto.TeacherDto;
import com.tsymbals.universitycms.models.dto.requests.CreateTeacherRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateTeacherRequest;
import com.tsymbals.universitycms.models.entities.Course;
import com.tsymbals.universitycms.models.entities.Group;
import com.tsymbals.universitycms.models.entities.Teacher;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.repositories.CourseRepository;
import com.tsymbals.universitycms.repositories.GroupRepository;
import com.tsymbals.universitycms.repositories.TeacherRepository;
import com.tsymbals.universitycms.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = TeacherServiceImpl.class)
class TeacherServiceImplTest {

    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final UserEntity USER_DATA = new UserEntity(1000L, "teacher1", ENCODED_PASSWORD, true, true, Set.of());

    @MockBean
    UserRepository userRepository;

    @MockBean
    TeacherRepository teacherRepository;

    @MockBean
    CourseRepository courseRepository;

    @MockBean
    GroupRepository groupRepository;

    @Autowired
    TeacherServiceImpl teacherService;

    @Test
    void save_should_UpdateUserFreeStatus_SaveNewTeacher_ReturnTeacherDto_when_CreateTeacherRequestIsValid_UserByUsernameExists() {
        CreateTeacherRequest createTeacherRequest = new CreateTeacherRequest("teacher1", "Luz", "Carson");
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        when(userRepository.findByUsername(createTeacherRequest.getUsername())).thenReturn(Optional.of(USER_DATA));
        when(teacherRepository.save(any(Teacher.class))).thenReturn(teacherData);

        TeacherDto expected = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        TeacherDto actual = teacherService.save(createTeacherRequest);
        assertEquals(expected, actual);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.FALSE);
    }

    @Test
    void save_should_ThrowEntityNotFoundException_when_CreateTeacherRequestIsValid_UserByUsernameDoesNotExist() {
        CreateTeacherRequest createTeacherRequest = new CreateTeacherRequest("student", "Luz", "Carson");
        when(userRepository.findByUsername(createTeacherRequest.getUsername())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.save(createTeacherRequest));
    }

    @Test
    void getAll_should_ReturnListOfTeacherDto_when_TeachersExist() {
        List<Teacher> teacherDataList = List.of(new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        when(teacherRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(teacherDataList);

        List<TeacherDto> expected = List.of(new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        List<TeacherDto> actual = teacherService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAll_should_ReturnEmptyListOfTeacherDto_when_TeachersDoNotExist() {
        List<Teacher> teacherDataList = List.of();
        when(teacherRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(teacherDataList);

        List<TeacherDto> expected = List.of();
        List<TeacherDto> actual = teacherService.getAll();
        assertEquals(expected, actual);
    }

    @Test
    void getAllNoneMatching_should_ReturnNoneMatchingListOfTeacherDto_when_TeachersExist_ComparisonTeacherIsNotNull() {
        List<Teacher> teacherDataList = List.of(new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        UserEntity userData = new UserEntity(1001L, "teacher2", ENCODED_PASSWORD, true, true, Set.of());
        Teacher comparisonTeacher = new Teacher(1001L, userData, "Guy", "Griffin", Set.of(), Set.of());
        when(teacherRepository.findAllByIdNotOrderByIdAsc(comparisonTeacher.getId())).thenReturn(teacherDataList);

        List<TeacherDto> expected = List.of(new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        List<TeacherDto> actual = teacherService.getAllNoneMatching(comparisonTeacher);
        assertEquals(expected, actual);
        verify(teacherRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(teacherRepository).findAllByIdNotOrderByIdAsc(comparisonTeacher.getId());
    }

    @Test
    void getAllNoneMatching_should_ReturnListOfTeacherDto_when_TeachersExist_ComparisonTeacherIsNull() {
        List<Teacher> teacherDataList = List.of(new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        when(teacherRepository.findAll(Sort.by(Sort.Direction.ASC, "id"))).thenReturn(teacherDataList);

        List<TeacherDto> expected = List.of(new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        List<TeacherDto> actual = teacherService.getAllNoneMatching(null);
        assertEquals(expected, actual);
        verify(teacherRepository).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(teacherRepository, never()).findAllByIdNotOrderByIdAsc(anyLong());
    }

    @Test
    void getAllNoneMatching_should_ReturnEmptyListOfTeacherDto_when_TeachersDoNotExist() {
        List<Teacher> teacherDataList = List.of();
        UserEntity userData = new UserEntity(1001L, "teacher2", ENCODED_PASSWORD, true, true, Set.of());
        Teacher comparisonTeacher = new Teacher(1001L, userData, "Guy", "Griffin", Set.of(), Set.of());
        when(teacherRepository.findAllByIdNotOrderByIdAsc(comparisonTeacher.getId())).thenReturn(teacherDataList);

        List<TeacherDto> expected = List.of();
        List<TeacherDto> actual = teacherService.getAllNoneMatching(comparisonTeacher);
        assertEquals(expected, actual);
        verify(teacherRepository, never()).findAll(Sort.by(Sort.Direction.ASC, "id"));
        verify(teacherRepository).findAllByIdNotOrderByIdAsc(comparisonTeacher.getId());
    }

    @Test
    void getById_should_ReturnTeacherDto_when_TeacherByIdExists() {
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));

        TeacherDto expected = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        TeacherDto actual = teacherService.getById(1000L);
        assertEquals(expected, actual);
    }

    @Test
    void getById_should_ThrowEntityNotFoundException_when_TeacherByIdDoesNotExist() {
        when(teacherRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.getById(9999L));
    }

    @Test
    void getByUsername_should_ReturnTeacherDto_when_TeacherByUsernameExists() {
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        when(teacherRepository.findByUser_Username("teacher1")).thenReturn(Optional.of(teacherData));

        TeacherDto expected = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        TeacherDto actual = teacherService.getByUsername("teacher1");
        assertEquals(expected, actual);
    }

    @Test
    void getByUsername_should_ThrowEntityNotFoundException_when_TeacherByUsernameDoesNotExist() {
        when(teacherRepository.findByUser_Username("student")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.getByUsername("student"));
    }

    @Test
    void getByFirstNameAndLastName_should_ReturnTeacherDto_when_TeacherByFirstNameAndLastNameExists() {
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        when(teacherRepository.findByFirstNameAndLastName("Luz", "Carson")).thenReturn(Optional.of(teacherData));

        TeacherDto expected = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        TeacherDto actual = teacherService.getByFirstNameAndLastName("Luz", "Carson");
        assertEquals(expected, actual);
    }

    @Test
    void getByFirstNameAndLastName_should_ThrowEntityNotFoundException_when_TeacherByFirstNameAndLastNameDoesNotExist() {
        when(teacherRepository.findByFirstNameAndLastName("Alexander", "Lawson")).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.getByFirstNameAndLastName("Alexander", "Lawson"));
    }

    @Test
    void updateUser_should_UpdateUserOfTeacher_ReturnUpdatedTeacherDto_when_TeacherByIdExists_UserByUsernameExists() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest("teacher2", null, null, null, null, null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        UserEntity userData = new UserEntity(1001L, "teacher2", ENCODED_PASSWORD, true, true, Set.of());
        Teacher updatedTeacher = new Teacher(1000L, userData, "Luz", "Carson", Set.of(), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(userRepository.findByUsername(updateTeacherRequest.getUsername())).thenReturn(Optional.of(userData));
        when(teacherRepository.getReferenceById(1000L)).thenReturn(updatedTeacher);

        TeacherDto expected = new TeacherDto(1000L, userData, "Luz", "Carson", Set.of(), Set.of());
        TeacherDto actual = teacherService.updateUser(1000L, updateTeacherRequest);
        assertEquals(expected, actual);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.TRUE);
        verify(userRepository).updateFreeStatus(userData.getId(), Boolean.FALSE);
        verify(teacherRepository).save(updatedTeacher);
    }

    @Test
    void updateUser_should_ThrowEntityNotFoundException_when_TeacherByIdDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest("teacher2", null, null, null, null, null);
        when(teacherRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.updateUser(9999L, updateTeacherRequest));
    }

    @Test
    void updateUser_should_ThrowEntityNotFoundException_when_UserByUsernameDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest("teacher3", null, null, null, null, null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(userRepository.findByUsername(updateTeacherRequest.getUsername())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.updateUser(1000L, updateTeacherRequest));
    }

    @Test
    void updateLastName_should_UpdateLastNameOfTeacher_ReturnUpdatedTeacherDto_when_TeacherByIdExists() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, "Foster", null, null, null, null);
        Teacher updatedTeacher = new Teacher(1000L, USER_DATA, "Luz", "Foster", Set.of(), Set.of());
        when(!teacherRepository.existsById(1000L)).thenReturn(Boolean.TRUE);
        when(teacherRepository.getReferenceById(1000L)).thenReturn(updatedTeacher);

        TeacherDto expected = new TeacherDto(1000L, USER_DATA, "Luz", "Foster", Set.of(), Set.of());
        TeacherDto actual = teacherService.updateLastName(1000L, updateTeacherRequest);
        assertEquals(expected, actual);
        verify(teacherRepository).updateLastName(1000L, updateTeacherRequest.getLastName());
    }

    @Test
    void updateLastName_should_ThrowEntityNotFoundException_when_TeacherByIdDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, "Foster", null, null, null, null);
        when(!teacherRepository.existsById(9999L)).thenReturn(Boolean.FALSE);

        assertThrows(EntityNotFoundException.class, () -> teacherService.updateLastName(9999L, updateTeacherRequest));
    }

    @Test
    void addCourse_should_AddCourseToTeacher_ReturnUpdatedTeacherDto_when_TeacherByIdExists_CourseByNameExists_TeacherDoesNotContainExistingCourse() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, "English", null, null, null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", new HashSet<>(), Set.of());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        Teacher updatedTeacher = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(courseData), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(courseRepository.findByName(updateTeacherRequest.getCourseNameToAdd())).thenReturn(Optional.of(courseData));
        when(teacherRepository.getReferenceById(1000L)).thenReturn(updatedTeacher);

        TeacherDto expected = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(courseData), Set.of());
        TeacherDto actual = teacherService.addCourse(1000L, updateTeacherRequest);
        assertEquals(expected, actual);
        verify(teacherRepository).save(updatedTeacher);
    }

    @Test
    void addCourse_should_ThrowEntityNotFoundException_when_TeacherByIdDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, "English", null, null, null);
        when(teacherRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.addCourse(9999L, updateTeacherRequest));
    }

    @Test
    void addCourse_should_ThrowEntityNotFoundException_when_CourseByNameDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, "Algebra", null, null, null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", new HashSet<>(), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(courseRepository.findByName(updateTeacherRequest.getCourseNameToAdd())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.addCourse(1000L, updateTeacherRequest));
    }

    @Test
    void addCourse_should_ThrowIllegalArgumentException_when_TeacherContainsExistingCourse() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, "English", null, null, null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", new HashSet<>(), Set.of());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        teacherData.getCourses().add(courseData);
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(courseRepository.findByName(updateTeacherRequest.getCourseNameToAdd())).thenReturn(Optional.of(courseData));

        assertThrows(IllegalArgumentException.class, () -> teacherService.addCourse(1000L, updateTeacherRequest));
    }

    @Test
    void removeCourse_should_RemoveCourseFromTeacher_ReturnUpdatedTeacherDto_when_TeacherByIdExists_CourseByNameExists_TeacherContainsExistingCourse() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, "English", null, null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", new HashSet<>(), Set.of());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        teacherData.getCourses().add(courseData);
        Teacher updatedTeacher = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(courseRepository.findByName(updateTeacherRequest.getCourseNameToRemove())).thenReturn(Optional.of(courseData));
        when(teacherRepository.getReferenceById(1000L)).thenReturn(updatedTeacher);

        TeacherDto expected = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        TeacherDto actual = teacherService.removeCourse(1000L, updateTeacherRequest);
        assertEquals(expected, actual);
        verify(teacherRepository).save(updatedTeacher);
    }

    @Test
    void removeCourse_should_ThrowEntityNotFoundException_when_TeacherByIdDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, "English", null, null);
        when(teacherRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.removeCourse(9999L, updateTeacherRequest));
    }

    @Test
    void removeCourse_should_ThrowEntityNotFoundException_when_CourseByNameDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, "Algebra", null, null, null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", new HashSet<>(), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(courseRepository.findByName(updateTeacherRequest.getCourseNameToRemove())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.removeCourse(1000L, updateTeacherRequest));
    }

    @Test
    void removeCourse_should_ThrowIllegalArgumentException_when_TeacherDoesNotContainExistingCourse() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, "English", null, null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", new HashSet<>(), Set.of());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(courseRepository.findByName(updateTeacherRequest.getCourseNameToRemove())).thenReturn(Optional.of(courseData));

        assertThrows(IllegalArgumentException.class, () -> teacherService.removeCourse(1000L, updateTeacherRequest));
    }

    @Test
    void addGroup_should_AddGroupToTeacher_ReturnUpdatedTeacherDto_when_TeacherByIdExists_GroupByNameExists_TeacherDoesNotContainExistingGroup() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, null, "1SE-24", null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), new HashSet<>());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        Teacher updatedTeacher = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of(groupData));
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(groupRepository.findByName(updateTeacherRequest.getGroupNameToAdd())).thenReturn(Optional.of(groupData));
        when(teacherRepository.getReferenceById(1000L)).thenReturn(updatedTeacher);

        TeacherDto expected = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of(groupData));
        TeacherDto actual = teacherService.addGroup(1000L, updateTeacherRequest);
        assertEquals(expected, actual);
        verify(teacherRepository).save(updatedTeacher);
    }

    @Test
    void addGroup_should_ThrowEntityNotFoundException_when_TeacherByIdDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, null, "1SE-24", null);
        when(teacherRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.addGroup(9999L, updateTeacherRequest));
    }

    @Test
    void addGroup_should_ThrowEntityNotFoundException_when_GroupByNameDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, null, "9YY-99", null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), new HashSet<>());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(groupRepository.findByName(updateTeacherRequest.getGroupNameToAdd())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.addGroup(1000L, updateTeacherRequest));
    }

    @Test
    void addGroup_should_ThrowIllegalArgumentException_when_TeacherContainsExistingGroup() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, null, "1SE-24", null);
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), new HashSet<>());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        teacherData.getGroups().add(groupData);
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(groupRepository.findByName(updateTeacherRequest.getGroupNameToAdd())).thenReturn(Optional.of(groupData));

        assertThrows(IllegalArgumentException.class, () -> teacherService.addGroup(1000L, updateTeacherRequest));
    }

    @Test
    void removeGroup_should_RemoveGroupFromTeacher_ReturnUpdatedTeacherDto_when_TeacherByIdExists_GroupByNameExists_TeacherContainsExistingGroup() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, null, null, "1SE-24");
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), new HashSet<>());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        teacherData.getGroups().add(groupData);
        Teacher updatedTeacher = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(groupRepository.findByName(updateTeacherRequest.getGroupNameToRemove())).thenReturn(Optional.of(groupData));
        when(teacherRepository.getReferenceById(1000L)).thenReturn(updatedTeacher);

        TeacherDto expected = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        TeacherDto actual = teacherService.removeGroup(1000L, updateTeacherRequest);
        assertEquals(expected, actual);
        verify(teacherRepository).save(updatedTeacher);
    }

    @Test
    void removeGroup_should_ThrowEntityNotFoundException_when_TeacherByIdDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, null, null, "1SE-24");
        when(teacherRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.removeGroup(9999L, updateTeacherRequest));
    }

    @Test
    void removeGroup_should_ThrowEntityNotFoundException_when_GroupByNameDoesNotExist() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, null, null, "9YY-99");
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), new HashSet<>());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(groupRepository.findByName(updateTeacherRequest.getGroupNameToRemove())).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.removeGroup(1000L, updateTeacherRequest));
    }

    @Test
    void removeGroup_should_ThrowIllegalArgumentException_when_TeacherDoesNotContainExistingGroup() {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest(null, null, null, null, null, "1SE-24");
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), new HashSet<>());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));
        when(groupRepository.findByName(updateTeacherRequest.getGroupNameToRemove())).thenReturn(Optional.of(groupData));

        assertThrows(IllegalArgumentException.class, () -> teacherService.removeGroup(1000L, updateTeacherRequest));
    }

    @Test
    void deleteById_should_UpdateUserFreeStatus_DeleteTeacherById_when_TeacherByIdExists() {
        Teacher teacherData = new Teacher(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        when(teacherRepository.findById(1000L)).thenReturn(Optional.of(teacherData));

        teacherService.deleteById(1000L);
        verify(userRepository).updateFreeStatus(USER_DATA.getId(), Boolean.TRUE);
        verify(teacherRepository).deleteById(1000L);
    }

    @Test
    void deleteById_should_ThrowEntityNotFoundException_when_TeacherByIdDoesNotExist() {
        when(teacherRepository.findById(9999L)).thenReturn(Optional.empty());

        assertThrows(EntityNotFoundException.class, () -> teacherService.deleteById(9999L));
    }
}
