package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Admin;
import com.tsymbals.universitycms.models.entities.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = AdminRepositoryTest.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class AdminRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AdminRepository adminRepository;

    @Test
    void findAll_should_FindAllAdmins_SortByIdInAscendingOrder_when_AdminsExist() {
        Optional<UserEntity> user1 = userRepository.findById(1000L);
        Optional<UserEntity> user2 = userRepository.findById(1001L);
        Optional<UserEntity> user3 = userRepository.findById(1002L);
        Optional<UserEntity> user4 = userRepository.findById(1003L);
        assertTrue(user1.isPresent());
        assertTrue(user2.isPresent());
        assertTrue(user3.isPresent());
        assertTrue(user4.isPresent());

        List<Admin> expected = List.of(
                new Admin(1000L, user1.get()),
                new Admin(1001L, user2.get()),
                new Admin(1002L, user3.get()),
                new Admin(1003L, user4.get())
        );
        List<Admin> actual = adminRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }
}
