package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = RoleRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class RoleRepositoryTest {

    @Autowired
    RoleRepository roleRepository;

    @Test
    void findAll_should_FindAllRoles_SortByIdInAscendingOrder_when_RolesExist() {
        List<Role> expected = List.of(
                new Role(1000L, "ROLE_ADMIN"),
                new Role(1001L, "ROLE_STAFF"),
                new Role(1002L, "ROLE_TEACHER"),
                new Role(1003L, "ROLE_STUDENT")
        );
        List<Role> actual = roleRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotInOrderByIdAsc_should_FindAllRolesExceptComparisonRoles_SortByIdInAscendingOrder_when_ComparisonRolesByIdExist() {
        Optional<Role> role1 = roleRepository.findById(1002L);
        Optional<Role> role2 = roleRepository.findById(1003L);
        assertTrue(role1.isPresent());
        assertTrue(role2.isPresent());

        List<Role> expected = List.of(
                new Role(1000L, "ROLE_ADMIN"),
                new Role(1001L, "ROLE_STAFF")
        );
        List<Role> actual = roleRepository.findAllByIdNotInOrderByIdAsc(Set.of(1002L, 1003L));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotInOrderByIdAsc_should_FindAllRolesExceptComparisonRoles_SortByIdInAscendingOrder_when_SomeComparisonRolesByIdDoNotExist() {
        Optional<Role> role1 = roleRepository.findById(1003L);
        Optional<Role> role2 = roleRepository.findById(9999L);
        assertTrue(role1.isPresent());
        assertFalse(role2.isPresent());

        List<Role> expected = List.of(
                new Role(1000L, "ROLE_ADMIN"),
                new Role(1001L, "ROLE_STAFF"),
                new Role(1002L, "ROLE_TEACHER")
        );
        List<Role> actual = roleRepository.findAllByIdNotInOrderByIdAsc(Set.of(1003L, 9999L));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotInOrderByIdAsc_should_FindAllRoles_SortByIdInAscendingOrder_when_ComparisonRolesByIdDoNotExist() {
        Optional<Role> role1 = roleRepository.findById(9998L);
        Optional<Role> role2 = roleRepository.findById(9999L);
        assertFalse(role1.isPresent());
        assertFalse(role2.isPresent());

        List<Role> expected = List.of(
                new Role(1000L, "ROLE_ADMIN"),
                new Role(1001L, "ROLE_STAFF"),
                new Role(1002L, "ROLE_TEACHER"),
                new Role(1003L, "ROLE_STUDENT")
        );
        List<Role> actual = roleRepository.findAllByIdNotInOrderByIdAsc(Set.of(9998L, 9999L));
        assertEquals(expected, actual);
    }

    @Test
    void updateName_should_UpdateNameOfRole_when_RoleByIdExists() {
        roleRepository.updateName(1000L, "ROLE_ADMINS");
        Role expected = new Role(1000L, "ROLE_ADMINS");
        Optional<Role> actual = roleRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
