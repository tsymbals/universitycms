package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Course;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = CourseRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class CourseRepositoryTest {

    @Autowired
    CourseRepository courseRepository;

    @Test
    void findAll_should_FindAllCourses_SortByIdInAscendingOrder_when_CoursesExist() {
        List<Course> expected = List.of(
                new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()),
                new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()),
                new Course(1002L, "Physics", "Physics is the scientific study of matter, its fundamental constituents, its motion and behavior through space and time, and the related entities of energy and force", Set.of(), Set.of()),
                new Course(1003L, "Chemistry", "Chemistry is a branch of natural science that deals principally with the properties of substances, the changes they undergo, and the natural laws that describe these changes", Set.of(), Set.of())
        );
        List<Course> actual = courseRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllCoursesExceptComparisonOne_SortByIdInAscendingOrder_when_ComparisonCourseByIdExists() {
        Optional<Course> course = courseRepository.findById(1003L);
        assertTrue(course.isPresent());

        List<Course> expected = List.of(
                new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()),
                new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()),
                new Course(1002L, "Physics", "Physics is the scientific study of matter, its fundamental constituents, its motion and behavior through space and time, and the related entities of energy and force", Set.of(), Set.of())
        );
        List<Course> actual = courseRepository.findAllByIdNotOrderByIdAsc(1003L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllCourses_SortByIdInAscendingOrder_when_ComparisonCourseByIdDoesNotExist() {
        Optional<Course> course = courseRepository.findById(9999L);
        assertFalse(course.isPresent());

        List<Course> expected = List.of(
                new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()),
                new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()),
                new Course(1002L, "Physics", "Physics is the scientific study of matter, its fundamental constituents, its motion and behavior through space and time, and the related entities of energy and force", Set.of(), Set.of()),
                new Course(1003L, "Chemistry", "Chemistry is a branch of natural science that deals principally with the properties of substances, the changes they undergo, and the natural laws that describe these changes", Set.of(), Set.of())
        );
        List<Course> actual = courseRepository.findAllByIdNotOrderByIdAsc(9999L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotInOrderByIdAsc_should_FindAllCoursesExceptComparisonCourses_SortByIdInAscendingOrder_when_ComparisonCoursesByIdExist() {
        Optional<Course> course1 = courseRepository.findById(1002L);
        Optional<Course> course2 = courseRepository.findById(1003L);
        assertTrue(course1.isPresent());
        assertTrue(course2.isPresent());

        List<Course> expected = List.of(
                new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()),
                new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of())
        );
        List<Course> actual = courseRepository.findAllByIdNotInOrderByIdAsc(Set.of(1002L, 1003L));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotInOrderByIdAsc_should_FindAllCoursesExceptComparisonCourses_SortByIdInAscendingOrder_when_SomeComparisonCoursesByIdDoNotExist() {
        Optional<Course> course1 = courseRepository.findById(1003L);
        Optional<Course> course2 = courseRepository.findById(9999L);
        assertTrue(course1.isPresent());
        assertFalse(course2.isPresent());

        List<Course> expected = List.of(
                new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()),
                new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()),
                new Course(1002L, "Physics", "Physics is the scientific study of matter, its fundamental constituents, its motion and behavior through space and time, and the related entities of energy and force", Set.of(), Set.of())
        );
        List<Course> actual = courseRepository.findAllByIdNotInOrderByIdAsc(Set.of(1003L, 9999L));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotInOrderByIdAsc_should_FindAllCourses_SortByIdInAscendingOrder_when_ComparisonCoursesByIdDoNotExist() {
        Optional<Course> course1 = courseRepository.findById(9998L);
        Optional<Course> course2 = courseRepository.findById(9999L);
        assertFalse(course1.isPresent());
        assertFalse(course2.isPresent());

        List<Course> expected = List.of(
                new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of()),
                new Course(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()),
                new Course(1002L, "Physics", "Physics is the scientific study of matter, its fundamental constituents, its motion and behavior through space and time, and the related entities of energy and force", Set.of(), Set.of()),
                new Course(1003L, "Chemistry", "Chemistry is a branch of natural science that deals principally with the properties of substances, the changes they undergo, and the natural laws that describe these changes", Set.of(), Set.of())
        );
        List<Course> actual = courseRepository.findAllByIdNotInOrderByIdAsc(Set.of(9998L, 9999L));
        assertEquals(expected, actual);
    }

    @Test
    void updateName_should_UpdateNameOfCourse_when_CourseByIdExists() {
        courseRepository.updateName(1000L, "Speak English");
        Course expected = new Course(1000L, "Speak English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        Optional<Course> actual = courseRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void updateDescription_should_UpdateDescriptionOfCourse_when_CourseByIdExists() {
        courseRepository.updateDescription(1000L, "Speaking practice to help you learn useful language for everyday communication");
        Course expected = new Course(1000L, "English", "Speaking practice to help you learn useful language for everyday communication", Set.of(), Set.of());
        Optional<Course> actual = courseRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
