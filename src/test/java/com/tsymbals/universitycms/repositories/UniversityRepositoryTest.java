package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.University;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = UniversityRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class UniversityRepositoryTest {

    @Autowired
    UniversityRepository universityRepository;

    @Test
    void findAll_should_FindAllUniversities_SortByIdInAscendingOrder_when_UniversitiesExist() {
        List<University> expected = List.of(
                new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of()),
                new University(1001L, "Humanitarian National University", "HNU", "San Andreas", "+380(98)7654321", Set.of()),
                new University(1002L, "Psychological National University", "PNU", "Los Santos", "+380(55)5555555", Set.of())
        );
        List<University> actual = universityRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllUniversitiesExceptComparisonOne_SortByIdInAscendingOrder_when_ComparisonUniversityByIdExists() {
        Optional<University> university = universityRepository.findById(1002L);
        assertTrue(university.isPresent());

        List<University> expected = List.of(
                new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of()),
                new University(1001L, "Humanitarian National University", "HNU", "San Andreas", "+380(98)7654321", Set.of())
        );
        List<University> actual = universityRepository.findAllByIdNotOrderByIdAsc(1002L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllUniversities_SortByIdInAscendingOrder_when_ComparisonUniversityByIdDoesNotExist() {
        Optional<University> university = universityRepository.findById(9999L);
        assertFalse(university.isPresent());

        List<University> expected = List.of(
                new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of()),
                new University(1001L, "Humanitarian National University", "HNU", "San Andreas", "+380(98)7654321", Set.of()),
                new University(1002L, "Psychological National University", "PNU", "Los Santos", "+380(55)5555555", Set.of())
        );
        List<University> actual = universityRepository.findAllByIdNotOrderByIdAsc(9999L);
        assertEquals(expected, actual);
    }

    @Test
    void findByAbbreviationName_should_FindUniversity_when_UniversityByAbbreviationNameExists() {
        University expected = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        Optional<University> actual = universityRepository.findByAbbreviationName("TNU");
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void findByAbbreviationName_should_NotFindUniversity_when_UniversityByAbbreviationNameDoesNotExist() {
        Optional<University> actual = universityRepository.findByAbbreviationName("FNU");
        assertFalse(actual.isPresent());
    }

    @Test
    void updateName_should_UpdateNameOfUniversity_when_UniversityByIdExists() {
        universityRepository.updateName(1000L, "National Technical University");
        University expected = new University(1000L, "National Technical University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        Optional<University> actual = universityRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void updateAbbreviationName_should_UpdateAbbreviationNameOfUniversity_when_UniversityByIdExists() {
        universityRepository.updateAbbreviationName(1000L, "NTU");
        University expected = new University(1000L, "Technical National University", "NTU", "Vice City", "+380(12)3456789", Set.of());
        Optional<University> actual = universityRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void updateCity_should_UpdateCityOfUniversity_when_UniversityByIdExists() {
        universityRepository.updateCity(1000L, "Liberty City");
        University expected = new University(1000L, "Technical National University", "TNU", "Liberty City", "+380(12)3456789", Set.of());
        Optional<University> actual = universityRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void updatePhoneNumber_should_UpdatePhoneNumberOfUniversity_when_UniversityByIdExists() {
        universityRepository.updatePhoneNumber(1000L, "+380(98)7654321");
        University expected = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(98)7654321", Set.of());
        Optional<University> actual = universityRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
