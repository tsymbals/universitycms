package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.LessonTime;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = LessonTimeRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class LessonTimeRepositoryTest {

    @Autowired
    LessonTimeRepository lessonTimeRepository;

    @Test
    void findAll_should_FindAllLessonTimes_SortByIdInAscendingOrder_when_LessonTimesExist() {
        List<LessonTime> expected = List.of(
                new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of()),
                new LessonTime(1001L, LocalTime.of(9, 15), LocalTime.of(10, 0), Set.of()),
                new LessonTime(1002L, LocalTime.of(10, 15), LocalTime.of(11, 0), Set.of()),
                new LessonTime(1003L, LocalTime.of(11, 15), LocalTime.of(12, 0), Set.of()),
                new LessonTime(1004L, LocalTime.of(12, 15), LocalTime.of(13, 0), Set.of()),
                new LessonTime(1005L, LocalTime.of(13, 15), LocalTime.of(14, 0), Set.of())
        );
        List<LessonTime> actual = lessonTimeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllLessonTimesExceptComparisonOne_SortByIdInAscendingOrder_when_ComparisonLessonTimeByIdExists() {
        Optional<LessonTime> lessonTime = lessonTimeRepository.findById(1002L);
        assertTrue(lessonTime.isPresent());

        List<LessonTime> expected = List.of(
                new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of()),
                new LessonTime(1001L, LocalTime.of(9, 15), LocalTime.of(10, 0), Set.of()),
                new LessonTime(1003L, LocalTime.of(11, 15), LocalTime.of(12, 0), Set.of()),
                new LessonTime(1004L, LocalTime.of(12, 15), LocalTime.of(13, 0), Set.of()),
                new LessonTime(1005L, LocalTime.of(13, 15), LocalTime.of(14, 0), Set.of())
        );
        List<LessonTime> actual = lessonTimeRepository.findAllByIdNotOrderByIdAsc(1002L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllLessonTimes_SortByIdInAscendingOrder_when_ComparisonLessonTimeByIdDoesNotExist() {
        Optional<LessonTime> lessonTime = lessonTimeRepository.findById(9999L);
        assertFalse(lessonTime.isPresent());

        List<LessonTime> expected = List.of(
                new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of()),
                new LessonTime(1001L, LocalTime.of(9, 15), LocalTime.of(10, 0), Set.of()),
                new LessonTime(1002L, LocalTime.of(10, 15), LocalTime.of(11, 0), Set.of()),
                new LessonTime(1003L, LocalTime.of(11, 15), LocalTime.of(12, 0), Set.of()),
                new LessonTime(1004L, LocalTime.of(12, 15), LocalTime.of(13, 0), Set.of()),
                new LessonTime(1005L, LocalTime.of(13, 15), LocalTime.of(14, 0), Set.of())
        );
        List<LessonTime> actual = lessonTimeRepository.findAllByIdNotOrderByIdAsc(9999L);
        assertEquals(expected, actual);
    }

    @Test
    void findByStartTimeAndEndTime_should_FindLessonTime_when_LessonTimeByStartTimeAndEndTimeExists() {
        LessonTime expected = new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
        Optional<LessonTime> actual = lessonTimeRepository.findByStartTimeAndEndTime(LocalTime.of(8, 15), LocalTime.of(9, 0));
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void findByStartTimeAndEndTime_should_NotFindLessonTime_when_LessonTimeByStartTimeAndEndTimeDoesNotExist() {
        Optional<LessonTime> actual = lessonTimeRepository.findByStartTimeAndEndTime(LocalTime.of(22, 59), LocalTime.of(23, 59));
        assertFalse(actual.isPresent());
    }

    @Test
    void updateStartTime_should_UpdateStartTimeOfLessonTime_when_LessonTimeByIdExists() {
        lessonTimeRepository.updateStartTime(1000L, LocalTime.of(14, 30));
        LessonTime expected = new LessonTime(1000L, LocalTime.of(14, 30), LocalTime.of(9, 0), Set.of());
        Optional<LessonTime> actual = lessonTimeRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void updateEndTime_should_UpdateEndTimeOfLessonTime_when_LessonTimeByIdExists() {
        lessonTimeRepository.updateEndTime(1000L, LocalTime.of(15, 15));
        LessonTime expected = new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(15, 15), Set.of());
        Optional<LessonTime> actual = lessonTimeRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
