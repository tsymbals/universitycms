package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = LessonRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class LessonRepositoryTest {

    @Autowired
    LessonTimeRepository lessonTimeRepository;

    @Autowired
    AuditoriumRepository auditoriumRepository;

    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    GroupRepository groupRepository;

    @Autowired
    LessonRepository lessonRepository;

    @Test
    void findAll_should_FindAllLessons_SortByIdInAscendingOrder_when_LessonsExist() {
        Optional<LessonTime> lessonTime1 = lessonTimeRepository.findById(1000L);
        Optional<LessonTime> lessonTime2 = lessonTimeRepository.findById(1001L);
        Optional<LessonTime> lessonTime3 = lessonTimeRepository.findById(1002L);
        Optional<LessonTime> lessonTime4 = lessonTimeRepository.findById(1003L);
        Optional<Auditorium> auditorium1 = auditoriumRepository.findById(1000L);
        Optional<Auditorium> auditorium2 = auditoriumRepository.findById(1001L);
        Optional<Teacher> teacher1 = teacherRepository.findById(1000L);
        Optional<Teacher> teacher2 = teacherRepository.findById(1001L);
        Optional<Group> group1 = groupRepository.findById(1000L);
        Optional<Group> group2 = groupRepository.findById(1001L);
        assertTrue(lessonTime1.isPresent());
        assertTrue(lessonTime2.isPresent());
        assertTrue(lessonTime3.isPresent());
        assertTrue(lessonTime4.isPresent());
        assertTrue(auditorium1.isPresent());
        assertTrue(auditorium2.isPresent());
        assertTrue(teacher1.isPresent());
        assertTrue(teacher2.isPresent());
        assertTrue(group1.isPresent());
        assertTrue(group2.isPresent());

        List<Lesson> expected = List.of(
                new Lesson(1000L, LocalDate.of(2025, 1, 6), lessonTime1.get(), auditorium1.get(), null, null, Set.of()),
                new Lesson(1001L, LocalDate.of(2025, 1, 6), lessonTime2.get(), auditorium1.get(), null, null, Set.of()),
                new Lesson(1002L, LocalDate.of(2025, 1, 7), lessonTime1.get(), auditorium1.get(), null, null, Set.of()),
                new Lesson(1003L, LocalDate.of(2025, 1, 8), lessonTime1.get(), auditorium1.get(), null, teacher1.get(), Set.of()),
                new Lesson(1004L, LocalDate.of(2025, 1, 8), lessonTime2.get(), auditorium1.get(), null, teacher1.get(), Set.of()),
                new Lesson(1005L, LocalDate.of(2025, 1, 8), lessonTime3.get(), auditorium1.get(), null, teacher2.get(), Set.of()),
                new Lesson(1006L, LocalDate.of(2025, 1, 8), lessonTime4.get(), auditorium1.get(), null, null, Set.of()),
                new Lesson(1007L, LocalDate.of(2025, 1, 9), lessonTime1.get(), auditorium1.get(), null, teacher1.get(), Set.of()),
                new Lesson(1008L, LocalDate.of(2025, 1, 10), lessonTime1.get(), auditorium1.get(), null, null, Set.of(group1.get())),
                new Lesson(1009L, LocalDate.of(2025, 1, 10), lessonTime2.get(), auditorium1.get(), null, null, Set.of(group1.get())),
                new Lesson(1010L, LocalDate.of(2025, 1, 10), lessonTime1.get(), auditorium2.get(), null, null, Set.of(group2.get())),
                new Lesson(1011L, LocalDate.of(2025, 1, 10), lessonTime2.get(), auditorium2.get(), null, null, Set.of(group2.get())),
                new Lesson(1012L, LocalDate.of(2025, 1, 13), lessonTime1.get(), auditorium1.get(), null, null, Set.of()),
                new Lesson(1013L, LocalDate.of(2025, 1, 13), lessonTime2.get(), auditorium1.get(), null, null, Set.of()),
                new Lesson(1014L, LocalDate.of(2025, 1, 20), lessonTime1.get(), auditorium1.get(), null, teacher1.get(), Set.of()),
                new Lesson(1015L, LocalDate.of(2025, 1, 20), lessonTime2.get(), auditorium1.get(), null, teacher1.get(), Set.of()),
                new Lesson(1016L, LocalDate.of(2025, 1, 24), lessonTime1.get(), auditorium1.get(), null, teacher1.get(), Set.of()),
                new Lesson(1017L, LocalDate.of(2025, 1, 24), lessonTime2.get(), auditorium1.get(), null, teacher1.get(), Set.of()),
                new Lesson(1018L, LocalDate.of(2025, 1, 24), lessonTime3.get(), auditorium1.get(), null, teacher2.get(), Set.of()),
                new Lesson(1019L, LocalDate.of(2025, 1, 24), lessonTime4.get(), auditorium1.get(), null, null, Set.of()),
                new Lesson(1020L, LocalDate.of(2025, 1, 27), lessonTime1.get(), auditorium1.get(), null, teacher1.get(), Set.of()),
                new Lesson(1021L, LocalDate.of(2025, 1, 27), lessonTime1.get(), auditorium1.get(), null, null, Set.of(group1.get())),
                new Lesson(1022L, LocalDate.of(2025, 1, 27), lessonTime2.get(), auditorium1.get(), null, null, Set.of(group1.get())),
                new Lesson(1023L, LocalDate.of(2025, 1, 31), lessonTime1.get(), auditorium1.get(), null, null, Set.of(group1.get())),
                new Lesson(1024L, LocalDate.of(2025, 1, 31), lessonTime2.get(), auditorium1.get(), null, null, Set.of(group1.get())),
                new Lesson(1025L, LocalDate.of(2025, 1, 31), lessonTime1.get(), auditorium2.get(), null, null, Set.of(group2.get())),
                new Lesson(1026L, LocalDate.of(2025, 1, 31), lessonTime2.get(), auditorium2.get(), null, null, Set.of(group2.get())),
                new Lesson(1027L, LocalDate.of(2025, 2, 3), lessonTime1.get(), auditorium1.get(), null, null, Set.of()),
                new Lesson(1028L, LocalDate.of(2025, 2, 3), lessonTime2.get(), auditorium1.get(), null, null, Set.of())
        );
        List<Lesson> actual = lessonRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateOrderByIdAsc_should_FindAllLessonsByDate_SortByIdInAscendingOrder_when_LessonsByDateExist() {
        Optional<LessonTime> lessonTime1 = lessonTimeRepository.findById(1000L);
        Optional<LessonTime> lessonTime2 = lessonTimeRepository.findById(1001L);
        Optional<Auditorium> auditorium = auditoriumRepository.findById(1000L);
        assertTrue(lessonTime1.isPresent());
        assertTrue(lessonTime2.isPresent());
        assertTrue(auditorium.isPresent());

        List<Lesson> expected = List.of(
                new Lesson(1000L, LocalDate.of(2025, 1, 6), lessonTime1.get(), auditorium.get(), null, null, Set.of()),
                new Lesson(1001L, LocalDate.of(2025, 1, 6), lessonTime2.get(), auditorium.get(), null, null, Set.of())
        );
        List<Lesson> actual = lessonRepository.findAllByDateOrderByIdAsc(LocalDate.of(2025, 1, 6));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateOrderByIdAsc_should_NotFindAnyLessonsByDate_when_LessonsByDateDoNotExist() {
        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByDateOrderByIdAsc(LocalDate.of(2026, 1, 6));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc_should_FindAllLessonsByDate_SortByIdInAscendingOrder_when_TeacherOfLessonsIsNotNull_LessonsByDateExist_TeacherByIdExists() {
        Optional<LessonTime> lessonTime1 = lessonTimeRepository.findById(1000L);
        Optional<LessonTime> lessonTime2 = lessonTimeRepository.findById(1001L);
        Optional<Auditorium> auditorium = auditoriumRepository.findById(1000L);
        Optional<Teacher> teacher = teacherRepository.findById(1000L);
        assertTrue(lessonTime1.isPresent());
        assertTrue(lessonTime2.isPresent());
        assertTrue(auditorium.isPresent());
        assertTrue(teacher.isPresent());

        List<Lesson> expected = List.of(
                new Lesson(1003L, LocalDate.of(2025, 1, 8), lessonTime1.get(), auditorium.get(), null, teacher.get(), Set.of()),
                new Lesson(1004L, LocalDate.of(2025, 1, 8), lessonTime2.get(), auditorium.get(), null, teacher.get(), Set.of())
        );
        List<Lesson> actual = lessonRepository.findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc(LocalDate.of(2025, 1, 8), 1000L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc_should_NotFindAnyLessonsByDate_when_LessonsByDateDoNotExist_TeacherByIdExists() {
        Optional<Teacher> teacher = teacherRepository.findById(1000L);
        assertTrue(teacher.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc(LocalDate.of(2026, 1, 8), 1000L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc_should_NotFindAnyLessonsByDate_when_LessonsByDateExist_TeacherByIdDoesNotExist() {
        Optional<Teacher> teacher = teacherRepository.findById(9999L);
        assertFalse(teacher.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc(LocalDate.of(2025, 1, 8), 9999L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc_should_NotFindAnyLessonsByDate_when_LessonsByDateDoNotExist_TeacherByIdDoesNotExist() {
        Optional<Teacher> teacher = teacherRepository.findById(9999L);
        assertFalse(teacher.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc(LocalDate.of(2026, 1, 8), 9999L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateAndGroups_NameOrderByIdAsc_should_FindAllLessonsByDate_SortByIdInAscendingOrder_when_LessonsByDateExist_GroupByNameExists_LessonsContainExistingGroup() {
        Optional<LessonTime> lessonTime1 = lessonTimeRepository.findById(1000L);
        Optional<LessonTime> lessonTime2 = lessonTimeRepository.findById(1001L);
        Optional<Auditorium> auditorium = auditoriumRepository.findById(1000L);
        Optional<Group> group = groupRepository.findByName("1SE-24");
        assertTrue(lessonTime1.isPresent());
        assertTrue(lessonTime2.isPresent());
        assertTrue(auditorium.isPresent());
        assertTrue(group.isPresent());

        List<Lesson> expected = List.of(
                new Lesson(1008L, LocalDate.of(2025, 1, 10), lessonTime1.get(), auditorium.get(), null, null, Set.of(group.get())),
                new Lesson(1009L, LocalDate.of(2025, 1, 10), lessonTime2.get(), auditorium.get(), null, null, Set.of(group.get()))
        );
        List<Lesson> actual = lessonRepository.findAllByDateAndGroups_NameOrderByIdAsc(LocalDate.of(2025, 1, 10), "1SE-24");
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateAndGroups_NameOrderByIdAsc_should_NotFindAnyLessonsByDate_when_LessonsByDateDoNotExist_GroupByNameExists() {
        Optional<Group> group = groupRepository.findByName("1SE-24");
        assertTrue(group.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByDateAndGroups_NameOrderByIdAsc(LocalDate.of(2026, 1, 10), "1SE-24");
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateAndGroups_NameOrderByIdAsc_should_NotFindAnyLessonsByDate_when_LessonsByDateExist_GroupByNameDoesNotExist() {
        Optional<Group> group = groupRepository.findByName("9YY-99");
        assertFalse(group.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByDateAndGroups_NameOrderByIdAsc(LocalDate.of(2025, 1, 10), "9YY-99");
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateAndGroups_NameOrderByIdAsc_should_NotFindAnyLessonsByDate_when_LessonsByDateDoNotExist_GroupByNameDoesNotExist() {
        Optional<Group> group = groupRepository.findByName("9YY-99");
        assertFalse(group.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByDateAndGroups_NameOrderByIdAsc(LocalDate.of(2026, 1, 10), "9YY-99");
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateAndGroups_NameOrderByIdAsc_should_NotFindAnyLessonsByDate_when_LessonsByDateExist_GroupByNameExist_LessonsDoNotContainExistingGroup() {
        Optional<Group> group = groupRepository.findByName("1SE-24");
        assertTrue(group.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByDateAndGroups_NameOrderByIdAsc(LocalDate.of(2025, 1, 13), "1SE-24");
        assertEquals(expected, actual);
    }

    @Test
    void findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc_should_FindAllLessonsByWeek_SortByIdInAscendingOrder_when_TeacherOfLessonsIsNotNull_LessonsByWeekExist_TeacherByIdExists() {
        Optional<LessonTime> lessonTime1 = lessonTimeRepository.findById(1000L);
        Optional<LessonTime> lessonTime2 = lessonTimeRepository.findById(1001L);
        Optional<Auditorium> auditorium = auditoriumRepository.findById(1000L);
        Optional<Teacher> teacher = teacherRepository.findById(1000L);
        assertTrue(lessonTime1.isPresent());
        assertTrue(lessonTime2.isPresent());
        assertTrue(auditorium.isPresent());
        assertTrue(teacher.isPresent());

        List<Lesson> expected = List.of(
                new Lesson(1014L, LocalDate.of(2025, 1, 20), lessonTime1.get(), auditorium.get(), null, teacher.get(), Set.of()),
                new Lesson(1015L, LocalDate.of(2025, 1, 20), lessonTime2.get(), auditorium.get(), null, teacher.get(), Set.of()),
                new Lesson(1016L, LocalDate.of(2025, 1, 24), lessonTime1.get(), auditorium.get(), null, teacher.get(), Set.of()),
                new Lesson(1017L, LocalDate.of(2025, 1, 24), lessonTime2.get(), auditorium.get(), null, teacher.get(), Set.of())
        );
        List<Lesson> actual = lessonRepository.findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc(LocalDate.of(2025, 1, 20), LocalDate.of(2025, 1, 26), 1000L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc_should_NotFindAnyLessonsByWeek_when_LessonsByWeekDoNotExist_TeacherByIdExists() {
        Optional<Teacher> teacher = teacherRepository.findById(1000L);
        assertTrue(teacher.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc(LocalDate.of(2026, 1, 20), LocalDate.of(2026, 1, 26), 1000L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc_should_NotFindAnyLessonsByDate_when_LessonsByWeekExist_TeacherByIdDoesNotExist() {
        Optional<Teacher> teacher = teacherRepository.findById(9999L);
        assertFalse(teacher.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc(LocalDate.of(2025, 1, 20), LocalDate.of(2025, 1, 26), 9999L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc_should_NotFindAnyLessonsByDate_when_LessonsByWeekDoNotExist_TeacherByIdDoesNotExist() {
        Optional<Teacher> teacher = teacherRepository.findById(9999L);
        assertFalse(teacher.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc(LocalDate.of(2026, 1, 20), LocalDate.of(2026, 1, 26), 9999L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateBetweenAndGroups_NameOrderByIdAsc_should_FindAllLessonsByWeek_SortByIdInAscendingOrder_when_LessonsByWeekExist_GroupByNameExists_LessonsContainExistingGroup() {
        Optional<LessonTime> lessonTime1 = lessonTimeRepository.findById(1000L);
        Optional<LessonTime> lessonTime2 = lessonTimeRepository.findById(1001L);
        Optional<Auditorium> auditorium = auditoriumRepository.findById(1000L);
        Optional<Group> group = groupRepository.findByName("1SE-24");
        assertTrue(lessonTime1.isPresent());
        assertTrue(lessonTime2.isPresent());
        assertTrue(auditorium.isPresent());
        assertTrue(group.isPresent());

        List<Lesson> expected = List.of(
                new Lesson(1021L, LocalDate.of(2025, 1, 27), lessonTime1.get(), auditorium.get(), null, null, Set.of(group.get())),
                new Lesson(1022L, LocalDate.of(2025, 1, 27), lessonTime2.get(), auditorium.get(), null, null, Set.of(group.get())),
                new Lesson(1023L, LocalDate.of(2025, 1, 31), lessonTime1.get(), auditorium.get(), null, null, Set.of(group.get())),
                new Lesson(1024L, LocalDate.of(2025, 1, 31), lessonTime2.get(), auditorium.get(), null, null, Set.of(group.get()))
        );
        List<Lesson> actual = lessonRepository.findAllByDateBetweenAndGroups_NameOrderByIdAsc(LocalDate.of(2025, 1, 27), LocalDate.of(2025, 2, 2), "1SE-24");
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateBetweenAndGroups_NameOrderByIdAsc_should_NotFindAnyLessonsByWeek_when_LessonsByWeekDoNotExist_GroupByNameExists() {
        Optional<Group> group = groupRepository.findByName("1SE-24");
        assertTrue(group.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByDateBetweenAndGroups_NameOrderByIdAsc(LocalDate.of(2026, 1, 27), LocalDate.of(2026, 2, 2), "1SE-24");
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateBetweenAndGroups_NameOrderByIdAsc_should_NotFindAnyLessonsByWeek_when_LessonsByWeekExist_GroupByNameDoesNotExist() {
        Optional<Group> group = groupRepository.findByName("9YY-99");
        assertFalse(group.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByDateBetweenAndGroups_NameOrderByIdAsc(LocalDate.of(2025, 1, 27), LocalDate.of(2025, 2, 2), "9YY-99");
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateBetweenAndGroups_NameOrderByIdAsc_should_NotFindAnyLessonsByWeek_when_LessonsByWeekDoNotExist_GroupByNameDoesNotExist() {
        Optional<Group> group = groupRepository.findByName("9YY-99");
        assertFalse(group.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByDateBetweenAndGroups_NameOrderByIdAsc(LocalDate.of(2026, 1, 27), LocalDate.of(2026, 2, 2), "9YY-99");
        assertEquals(expected, actual);
    }

    @Test
    void findAllByDateBetweenAndGroups_NameOrderByIdAsc_should_NotFindAnyLessonsByWeek_when_LessonsByWeekExist_GroupByNameExist_LessonsDoNotContainExistingGroup() {
        Optional<Group> group = groupRepository.findByName("1SE-24");
        assertTrue(group.isPresent());

        List<Lesson> expected = List.of();
        List<Lesson> actual = lessonRepository.findAllByDateBetweenAndGroups_NameOrderByIdAsc(LocalDate.of(2025, 2, 3), LocalDate.of(2025, 2, 9), "1SE-24");
        assertEquals(expected, actual);
    }

    @Test
    void findByDateAndLessonTime_StartTimeAndLessonTime_EndTime_should_FindLesson_when_LessonByDateAndStartTimeAndEndTimeExists() {
        Optional<LessonTime> lessonTime = lessonTimeRepository.findById(1000L);
        Optional<Auditorium> auditorium = auditoriumRepository.findById(1000L);
        assertTrue(lessonTime.isPresent());
        assertTrue(auditorium.isPresent());

        Lesson expected = new Lesson(1000L, LocalDate.of(2025, 1, 6), lessonTime.get(), auditorium.get(), null, null, Set.of());
        Optional<Lesson> actual = lessonRepository.findByDateAndLessonTime_StartTimeAndLessonTime_EndTime(LocalDate.of(2025, 1, 6), LocalTime.of(8, 15), LocalTime.of(9, 0));
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void findByDateAndLessonTime_StartTimeAndLessonTime_EndTime_should_NotFindLesson_when_LessonByDateAndStartTimeAndEndTimeDoesNotExist() {
        Optional<Lesson> actual = lessonRepository.findByDateAndLessonTime_StartTimeAndLessonTime_EndTime(LocalDate.of(2026, 1, 6), LocalTime.of(0, 0), LocalTime.of(1, 0));
        assertFalse(actual.isPresent());
    }

    @Test
    void updateDate_should_UpdateDateOfLesson_when_LessonByIdExists() {
        Optional<LessonTime> lessonTime = lessonTimeRepository.findById(1000L);
        Optional<Auditorium> auditorium = auditoriumRepository.findById(1000L);
        assertTrue(lessonTime.isPresent());
        assertTrue(auditorium.isPresent());

        lessonRepository.updateDate(1000L, LocalDate.of(2025, 1, 7));
        Lesson expected = new Lesson(1000L, LocalDate.of(2025, 1, 7), lessonTime.get(), auditorium.get(), null, null, Set.of());
        Optional<Lesson> actual = lessonRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
