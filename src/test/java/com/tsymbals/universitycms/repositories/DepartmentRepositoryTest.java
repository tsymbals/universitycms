package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Department;
import com.tsymbals.universitycms.models.entities.University;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = DepartmentRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class DepartmentRepositoryTest {

    @Autowired
    UniversityRepository universityRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Test
    void findAll_should_FindAllDepartments_SortByIdInAscendingOrder_when_DepartmentsExist() {
        Optional<University> university = universityRepository.findById(1000L);
        assertTrue(university.isPresent());

        List<Department> expected = List.of(
                new Department(1000L, university.get(), "Technical", Set.of()),
                new Department(1001L, university.get(), "Humanitarian", Set.of()),
                new Department(1002L, university.get(), "Psychological", Set.of())
        );
        List<Department> actual = departmentRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllDepartmentsExceptComparisonOne_SortByIdInAscendingOrder_when_ComparisonDepartmentByIdExists() {
        Optional<University> university = universityRepository.findById(1000L);
        Optional<Department> department = departmentRepository.findById(1002L);
        assertTrue(university.isPresent());
        assertTrue(department.isPresent());

        List<Department> expected = List.of(
                new Department(1000L, university.get(), "Technical", Set.of()),
                new Department(1001L, university.get(), "Humanitarian", Set.of())
        );
        List<Department> actual = departmentRepository.findAllByIdNotOrderByIdAsc(1002L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllDepartments_SortByIdInAscendingOrder_when_ComparisonDepartmentByIdDoesNotExist() {
        Optional<University> university = universityRepository.findById(1000L);
        Optional<Department> department = departmentRepository.findById(9999L);
        assertTrue(university.isPresent());
        assertFalse(department.isPresent());

        List<Department> expected = List.of(
                new Department(1000L, university.get(), "Technical", Set.of()),
                new Department(1001L, university.get(), "Humanitarian", Set.of()),
                new Department(1002L, university.get(), "Psychological", Set.of())
        );
        List<Department> actual = departmentRepository.findAllByIdNotOrderByIdAsc(9999L);
        assertEquals(expected, actual);
    }

    @Test
    void updateName_should_UpdateNameOfDepartment_when_DepartmentByIdExists() {
        Optional<University> university = universityRepository.findById(1000L);
        assertTrue(university.isPresent());

        departmentRepository.updateName(1000L, "Financial");
        Department expected = new Department(1000L, university.get(), "Financial", Set.of());
        Optional<Department> actual = departmentRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
