package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Group;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = GroupRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class GroupRepositoryTest {

    @Autowired
    GroupRepository groupRepository;

    @Test
    void findAll_should_FindAllGroups_SortByIdInAscendingOrder_when_GroupsExist() {
        List<Group> expected = List.of(
                new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()),
                new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of()),
                new Group(1002L, "1SS-24", Set.of(), Set.of(), Set.of()),
                new Group(1003L, "1PE-24", Set.of(), Set.of(), Set.of())
        );
        List<Group> actual = groupRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllGroupsExceptComparisonOne_SortByIdInAscendingOrder_when_ComparisonGroupByIdExists() {
        Optional<Group> group = groupRepository.findById(1003L);
        assertTrue(group.isPresent());

        List<Group> expected = List.of(
                new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()),
                new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of()),
                new Group(1002L, "1SS-24", Set.of(), Set.of(), Set.of())
        );
        List<Group> actual = groupRepository.findAllByIdNotOrderByIdAsc(1003L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllGroups_SortByIdInAscendingOrder_when_ComparisonGroupByIdDoesNotExist() {
        Optional<Group> group = groupRepository.findById(9999L);
        assertFalse(group.isPresent());

        List<Group> expected = List.of(
                new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()),
                new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of()),
                new Group(1002L, "1SS-24", Set.of(), Set.of(), Set.of()),
                new Group(1003L, "1PE-24", Set.of(), Set.of(), Set.of())
        );
        List<Group> actual = groupRepository.findAllByIdNotOrderByIdAsc(9999L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotInOrderByIdAsc_should_FindAllGroupsExceptComparisonGroups_SortByIdInAscendingOrder_when_ComparisonGroupsByIdExist() {
        Optional<Group> group1 = groupRepository.findById(1002L);
        Optional<Group> group2 = groupRepository.findById(1003L);
        assertTrue(group1.isPresent());
        assertTrue(group2.isPresent());

        List<Group> expected = List.of(
                new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()),
                new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of())
        );
        List<Group> actual = groupRepository.findAllByIdNotInOrderByIdAsc(Set.of(1002L, 1003L));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotInOrderByIdAsc_should_FindAllGroupsExceptComparisonGroups_SortByIdInAscendingOrder_when_SomeComparisonGroupsByIdDoNotExist() {
        Optional<Group> group1 = groupRepository.findById(1003L);
        Optional<Group> group2 = groupRepository.findById(9999L);
        assertTrue(group1.isPresent());
        assertFalse(group2.isPresent());

        List<Group> expected = List.of(
                new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()),
                new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of()),
                new Group(1002L, "1SS-24", Set.of(), Set.of(), Set.of())
        );
        List<Group> actual = groupRepository.findAllByIdNotInOrderByIdAsc(Set.of(1003L, 9999L));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotInOrderByIdAsc_should_FindAllGroups_SortByIdInAscendingOrder_when_ComparisonGroupsByIdDoNotExist() {
        Optional<Group> group1 = groupRepository.findById(9998L);
        Optional<Group> group2 = groupRepository.findById(9999L);
        assertFalse(group1.isPresent());
        assertFalse(group2.isPresent());

        List<Group> expected = List.of(
                new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of()),
                new Group(1001L, "1CE-24", Set.of(), Set.of(), Set.of()),
                new Group(1002L, "1SS-24", Set.of(), Set.of(), Set.of()),
                new Group(1003L, "1PE-24", Set.of(), Set.of(), Set.of())
        );
        List<Group> actual = groupRepository.findAllByIdNotInOrderByIdAsc(Set.of(9998L, 9999L));
        assertEquals(expected, actual);
    }

    @Test
    void updateName_should_UpdateNameOfGroup_when_GroupByIdExists() {
        groupRepository.updateName(1000L, "2SE-24");
        Group expected = new Group(1000L, "2SE-24", Set.of(), Set.of(), Set.of());
        Optional<Group> actual = groupRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
