package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Teacher;
import com.tsymbals.universitycms.models.entities.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = TeacherRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class TeacherRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    TeacherRepository teacherRepository;

    @Test
    void findAll_should_FindAllTeachers_SortByIdInAscendingOrder_when_TeachersExist() {
        Optional<UserEntity> user1 = userRepository.findById(1008L);
        Optional<UserEntity> user2 = userRepository.findById(1009L);
        Optional<UserEntity> user3 = userRepository.findById(1010L);
        Optional<UserEntity> user4 = userRepository.findById(1011L);
        assertTrue(user1.isPresent());
        assertTrue(user2.isPresent());
        assertTrue(user3.isPresent());
        assertTrue(user4.isPresent());

        List<Teacher> expected = List.of(
                new Teacher(1000L, user1.get(), "Luz", "Carson", Set.of(), Set.of()),
                new Teacher(1001L, user2.get(), "Guy", "Griffin", Set.of(), Set.of()),
                new Teacher(1002L, user3.get(), "Cynthia", "Newman", Set.of(), Set.of()),
                new Teacher(1003L, user4.get(), "Gerardo", "Andersen", Set.of(), Set.of())
        );
        List<Teacher> actual = teacherRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllTeachersExceptComparisonOne_SortByIdInAscendingOrder_when_ComparisonTeacherByIdExists() {
        Optional<UserEntity> user1 = userRepository.findById(1008L);
        Optional<UserEntity> user2 = userRepository.findById(1009L);
        Optional<UserEntity> user3 = userRepository.findById(1010L);
        Optional<Teacher> teacher = teacherRepository.findById(1003L);
        assertTrue(user1.isPresent());
        assertTrue(user2.isPresent());
        assertTrue(user3.isPresent());
        assertTrue(teacher.isPresent());

        List<Teacher> expected = List.of(
                new Teacher(1000L, user1.get(), "Luz", "Carson", Set.of(), Set.of()),
                new Teacher(1001L, user2.get(), "Guy", "Griffin", Set.of(), Set.of()),
                new Teacher(1002L, user3.get(), "Cynthia", "Newman", Set.of(), Set.of())
        );
        List<Teacher> actual = teacherRepository.findAllByIdNotOrderByIdAsc(1003L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllTeachers_SortByIdInAscendingOrder_when_ComparisonTeacherByIdDoesNotExist() {
        Optional<UserEntity> user1 = userRepository.findById(1008L);
        Optional<UserEntity> user2 = userRepository.findById(1009L);
        Optional<UserEntity> user3 = userRepository.findById(1010L);
        Optional<UserEntity> user4 = userRepository.findById(1011L);
        Optional<Teacher> teacher = teacherRepository.findById(9999L);
        assertTrue(user1.isPresent());
        assertTrue(user2.isPresent());
        assertTrue(user3.isPresent());
        assertTrue(user4.isPresent());
        assertFalse(teacher.isPresent());

        List<Teacher> expected = List.of(
                new Teacher(1000L, user1.get(), "Luz", "Carson", Set.of(), Set.of()),
                new Teacher(1001L, user2.get(), "Guy", "Griffin", Set.of(), Set.of()),
                new Teacher(1002L, user3.get(), "Cynthia", "Newman", Set.of(), Set.of()),
                new Teacher(1003L, user4.get(), "Gerardo", "Andersen", Set.of(), Set.of())
        );
        List<Teacher> actual = teacherRepository.findAllByIdNotOrderByIdAsc(9999L);
        assertEquals(expected, actual);
    }

    @Test
    void findByUser_Username_should_FindTeacher_when_TeacherByUsernameExists() {
        Optional<UserEntity> user = userRepository.findByUsername("teacher1");
        assertTrue(user.isPresent());

        Teacher expected = new Teacher(1000L, user.get(), "Luz", "Carson", Set.of(), Set.of());
        Optional<Teacher> actual = teacherRepository.findByUser_Username("teacher1");
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void findByUser_Username_should_NotFindTeacher_when_TeacherByUsernameDoesNotExist() {
        Optional<Teacher> actual = teacherRepository.findByUser_Username("student");
        assertFalse(actual.isPresent());
    }

    @Test
    void findByFirstNameAndLastName_should_FindTeacher_when_TeacherByFirstNameAndLastNameExists() {
        Optional<UserEntity> user = userRepository.findById(1008L);
        assertTrue(user.isPresent());

        Teacher expected = new Teacher(1000L, user.get(), "Luz", "Carson", Set.of(), Set.of());
        Optional<Teacher> actual = teacherRepository.findByFirstNameAndLastName("Luz", "Carson");
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void findByFirstNameAndLastName_should_NotFindTeacher_when_TeacherByFirstNameAndLastNameDoesNotExist() {
        Optional<Teacher> actual = teacherRepository.findByFirstNameAndLastName("Brendan", "Slaughter");
        assertFalse(actual.isPresent());
    }

    @Test
    void updateLastName_should_UpdateLastNameOfTeacher_when_TeacherByIdExists() {
        Optional<UserEntity> user = userRepository.findById(1008L);
        assertTrue(user.isPresent());

        teacherRepository.updateLastName(1000L, "Jacobs");
        Teacher expected = new Teacher(1000L, user.get(), "Luz", "Jacobs", Set.of(), Set.of());
        Optional<Teacher> actual = teacherRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
