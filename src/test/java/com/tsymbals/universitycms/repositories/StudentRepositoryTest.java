package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Student;
import com.tsymbals.universitycms.models.entities.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = StudentRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class StudentRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    StudentRepository studentRepository;

    @Test
    void findAll_should_FindAllStudents_SortByIdInAscendingOrder_when_StudentsExist() {
        Optional<UserEntity> user1 = userRepository.findById(1012L);
        Optional<UserEntity> user2 = userRepository.findById(1013L);
        Optional<UserEntity> user3 = userRepository.findById(1014L);
        Optional<UserEntity> user4 = userRepository.findById(1015L);
        assertTrue(user1.isPresent());
        assertTrue(user2.isPresent());
        assertTrue(user3.isPresent());
        assertTrue(user4.isPresent());

        List<Student> expected = List.of(
                new Student(1000L, user1.get(), null, "Emily", "Hensley", Set.of()),
                new Student(1001L, user2.get(), null, "Marcella", "Johnson", Set.of()),
                new Student(1002L, user3.get(), null, "Katie", "Matthews", Set.of()),
                new Student(1003L, user4.get(), null, "Stuart", "Snyder", Set.of())
        );
        List<Student> actual = studentRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findByUser_Username_should_FindStudent_when_StudentByUsernameExists() {
        Optional<UserEntity> user = userRepository.findByUsername("student1");
        assertTrue(user.isPresent());

        Student expected = new Student(1000L, user.get(), null, "Emily", "Hensley", Set.of());
        Optional<Student> actual = studentRepository.findByUser_Username("student1");
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void findByUser_Username_should_NotFindStudent_when_StudentByUsernameDoesNotExist() {
        Optional<Student> actual = studentRepository.findByUser_Username("admin");
        assertFalse(actual.isPresent());
    }

    @Test
    void findByFirstNameAndLastName_should_FindStudent_when_StudentByFirstNameAndLastNameExists() {
        Optional<UserEntity> user = userRepository.findById(1012L);
        assertTrue(user.isPresent());

        Student expected = new Student(1000L, user.get(), null, "Emily", "Hensley", Set.of());
        Optional<Student> actual = studentRepository.findByFirstNameAndLastName("Emily", "Hensley");
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void findByFirstNameAndLastName_should_NotFindStudent_when_StudentByFirstNameAndLastNameDoesNotExist() {
        Optional<Student> actual = studentRepository.findByFirstNameAndLastName("Katrina", "Harris");
        assertFalse(actual.isPresent());
    }

    @Test
    void updateLastName_should_UpdateLastNameOfStudent_when_StudentByIdExists() {
        Optional<UserEntity> user = userRepository.findById(1012L);
        assertTrue(user.isPresent());

        studentRepository.updateLastName(1000L, "Holmes");
        Student expected = new Student(1000L, user.get(), null, "Emily", "Holmes", Set.of());
        Optional<Student> actual = studentRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
