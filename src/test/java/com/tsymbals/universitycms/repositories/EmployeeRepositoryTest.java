package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Employee;
import com.tsymbals.universitycms.models.entities.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = EmployeeRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class EmployeeRepositoryTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Test
    void findAll_should_FindAllEmployees_SortByIdInAscendingOrder_when_EmployeesExist() {
        Optional<UserEntity> user1 = userRepository.findById(1004L);
        Optional<UserEntity> user2 = userRepository.findById(1005L);
        Optional<UserEntity> user3 = userRepository.findById(1006L);
        Optional<UserEntity> user4 = userRepository.findById(1007L);
        assertTrue(user1.isPresent());
        assertTrue(user2.isPresent());
        assertTrue(user3.isPresent());
        assertTrue(user4.isPresent());

        List<Employee> expected = List.of(
                new Employee(1000L, user1.get(), "Tasha", "Garcia"),
                new Employee(1001L, user2.get(), "Barry", "McAllister"),
                new Employee(1002L, user3.get(), "Gretchen", "Boyer"),
                new Employee(1003L, user4.get(), "Alison", "Riggs")
        );
        List<Employee> actual = employeeRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findByFirstNameAndLastName_should_FindEmployee_when_EmployeeByFirstNameAndLastNameExists() {
        Optional<UserEntity> user = userRepository.findById(1004L);
        assertTrue(user.isPresent());

        Employee expected = new Employee(1000L, user.get(), "Tasha", "Garcia");
        Optional<Employee> actual = employeeRepository.findByFirstNameAndLastName("Tasha", "Garcia");
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void findByFirstNameAndLastName_should_NotFindEmployee_when_EmployeeByFirstNameAndLastNameDoesNotExist() {
        Optional<Employee> actual = employeeRepository.findByFirstNameAndLastName("Sidney", "Wallace");
        assertFalse(actual.isPresent());
    }

    @Test
    void updateLastName_should_UpdateLastNameOfEmployee_when_EmployeeByIdExists() {
        Optional<UserEntity> user = userRepository.findById(1004L);
        assertTrue(user.isPresent());

        employeeRepository.updateLastName(1000L, "Parrish");
        Employee expected = new Employee(1000L, user.get(), "Tasha", "Parrish");
        Optional<Employee> actual = employeeRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
