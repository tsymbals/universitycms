package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Auditorium;
import com.tsymbals.universitycms.models.entities.Department;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = AuditoriumRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class AuditoriumRepositoryTest {

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    AuditoriumRepository auditoriumRepository;

    @Test
    void findAll_should_FindAllAuditoriums_SortByIdInAscendingOrder_when_AuditoriumsExist() {
        Optional<Department> department = departmentRepository.findById(1000L);
        assertTrue(department.isPresent());

        List<Auditorium> expected = List.of(
                new Auditorium(1000L, department.get(), "101", Set.of()),
                new Auditorium(1001L, department.get(), "102", Set.of()),
                new Auditorium(1002L, department.get(), "103", Set.of())
        );
        List<Auditorium> actual = auditoriumRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllAuditoriumsExceptComparisonOne_SortByIdInAscendingOrder_when_ComparisonAuditoriumByIdExists() {
        Optional<Department> department = departmentRepository.findById(1000L);
        Optional<Auditorium> auditorium = auditoriumRepository.findById(1002L);
        assertTrue(department.isPresent());
        assertTrue(auditorium.isPresent());

        List<Auditorium> expected = List.of(
                new Auditorium(1000L, department.get(), "101", Set.of()),
                new Auditorium(1001L, department.get(), "102", Set.of())
        );
        List<Auditorium> actual = auditoriumRepository.findAllByIdNotOrderByIdAsc(1002L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByIdNotOrderByIdAsc_should_FindAllAuditoriums_SortByIdInAscendingOrder_when_ComparisonAuditoriumByIdDoesNotExist() {
        Optional<Department> department = departmentRepository.findById(1000L);
        Optional<Auditorium> auditorium = auditoriumRepository.findById(9999L);
        assertTrue(department.isPresent());
        assertFalse(auditorium.isPresent());

        List<Auditorium> expected = List.of(
                new Auditorium(1000L, department.get(), "101", Set.of()),
                new Auditorium(1001L, department.get(), "102", Set.of()),
                new Auditorium(1002L, department.get(), "103", Set.of())
        );
        List<Auditorium> actual = auditoriumRepository.findAllByIdNotOrderByIdAsc(9999L);
        assertEquals(expected, actual);
    }

    @Test
    void findAllByLessonsIsNotEmptyOrderByIdAsc_should_FindAllAuditoriums_SortByIdInAscendingOrder_when_LessonsOfAuditoriumsAreNotEmpty() {
        Optional<Department> department = departmentRepository.findById(1000L);
        assertTrue(department.isPresent());

        List<Auditorium> expected = List.of(
                new Auditorium(1000L, department.get(), "101", Set.of()),
                new Auditorium(1001L, department.get(), "102", Set.of())
        );
        List<Auditorium> actual = auditoriumRepository.findAllByLessonsIsNotEmptyOrderByIdAsc();
        assertEquals(expected, actual);
    }

    @Test
    void updateName_should_UpdateNameOfAuditorium_when_AuditoriumByIdExists() {
        Optional<Department> department = departmentRepository.findById(1000L);
        assertTrue(department.isPresent());

        auditoriumRepository.updateName(1000L, "201");
        Auditorium expected = new Auditorium(1000L, department.get(), "201", Set.of());
        Optional<Auditorium> actual = auditoriumRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
