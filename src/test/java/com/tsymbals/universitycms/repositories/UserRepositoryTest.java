package com.tsymbals.universitycms.repositories;

import com.tsymbals.universitycms.models.entities.Role;
import com.tsymbals.universitycms.models.entities.UserEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest(includeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = UserRepository.class))
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Sql(scripts = {"classpath:sql/clear_all_tables_script.sql", "classpath:sql/insert_sample_data_script.sql"})
class UserRepositoryTest {

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @Test
    void findAll_should_FindAllUsers_SortByIdInAscendingOrder_when_UsersExist() {
        Optional<Role> role1 = roleRepository.findByName("ROLE_ADMIN");
        Optional<Role> role2 = roleRepository.findByName("ROLE_STAFF");
        Optional<Role> role3 = roleRepository.findByName("ROLE_TEACHER");
        Optional<Role> role4 = roleRepository.findByName("ROLE_STUDENT");
        assertTrue(role1.isPresent());
        assertTrue(role2.isPresent());
        assertTrue(role3.isPresent());
        assertTrue(role4.isPresent());

        List<UserEntity> expected = List.of(
                new UserEntity(1000L, "admin1", "Admin001", true, false, Set.of(role1.get())),
                new UserEntity(1001L, "admin2", "Admin002", true, true, Set.of(role1.get())),
                new UserEntity(1002L, "admin3", "Admin003", true, true, Set.of(role1.get())),
                new UserEntity(1003L, "admin4", "Admin004", true, true, Set.of()),
                new UserEntity(1004L, "employee1", "Employee1", true, false, Set.of(role2.get())),
                new UserEntity(1005L, "employee2", "Employee2", true, true, Set.of(role2.get())),
                new UserEntity(1006L, "employee3", "Employee3", true, true, Set.of(role2.get())),
                new UserEntity(1007L, "employee4", "Employee4", true, true, Set.of()),
                new UserEntity(1008L, "teacher1", "Teacher1", true, false, Set.of(role3.get())),
                new UserEntity(1009L, "teacher2", "Teacher2", true, true, Set.of(role3.get())),
                new UserEntity(1010L, "teacher3", "Teacher3", true, true, Set.of(role3.get())),
                new UserEntity(1011L, "teacher4", "Teacher4", true, true, Set.of()),
                new UserEntity(1012L, "student1", "Student1", true, false, Set.of(role4.get())),
                new UserEntity(1013L, "student2", "Student2", true, true, Set.of(role4.get())),
                new UserEntity(1014L, "student3", "Student3", true, true, Set.of(role4.get())),
                new UserEntity(1015L, "student4", "Student4", true, true, Set.of())
        );
        List<UserEntity> actual = userRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
        assertEquals(expected, actual);
    }

    @Test
    void findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc_should_FindAllUsersWithFreeStatus_SortByIdInAscendingOrder_when_UsersWithRoleNameExist() {
        Optional<Role> role = roleRepository.findByName("ROLE_ADMIN");
        assertTrue(role.isPresent());

        List<UserEntity> expected = List.of(
                new UserEntity(1001L, "admin2", "Admin002", true, true, Set.of(role.get())),
                new UserEntity(1002L, "admin3", "Admin003", true, true, Set.of(role.get()))
        );
        List<UserEntity> actual = userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc("role_admin");
        assertEquals(expected, actual);
    }

    @Test
    void findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc_should_NotFindAnyUsers_when_UsersWithRoleNameDoNotExist() {
        Optional<Role> firstRole = roleRepository.findByName("ROLE_USER");
        assertFalse(firstRole.isPresent());

        List<UserEntity> expected = List.of();
        List<UserEntity> actual = userRepository.findAllByFreeStatusTrueAndRoles_NameIgnoreCaseOrderByIdAsc("role_user");
        assertEquals(expected, actual);
    }

    @Test
    void findByUsername_should_FindUser_when_UserByUsernameExists() {
        Optional<Role> role = roleRepository.findByName("ROLE_ADMIN");
        assertTrue(role.isPresent());

        UserEntity expected = new UserEntity(1000L, "admin1", "Admin001", true, false, Set.of(role.get()));
        Optional<UserEntity> actual = userRepository.findByUsername("admin1");
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void findByUsername_should_NotFindUser_when_UserByUsernameDoesNotExist() {
        Optional<UserEntity> actual = userRepository.findByUsername("username");
        assertFalse(actual.isPresent());
    }

    @Test
    void updateUsername_should_UpdateUsernameOfUser_when_UserByIdExists() {
        Optional<Role> role = roleRepository.findByName("ROLE_ADMIN");
        assertTrue(role.isPresent());

        userRepository.updateUsername(1000L, "admin5");
        UserEntity expected = new UserEntity(1000L, "admin5", "Admin001", true, false, Set.of(role.get()));
        Optional<UserEntity> actual = userRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void updatePassword_should_UpdatePasswordOfUser_when_UserByIdExists() {
        Optional<Role> role = roleRepository.findByName("ROLE_ADMIN");
        assertTrue(role.isPresent());

        userRepository.updatePassword(1000L, "Admin005");
        UserEntity expected = new UserEntity(1000L, "admin1", "Admin005", true, false, Set.of(role.get()));
        Optional<UserEntity> actual = userRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void updateEnabledStatus_should_UpdateEnabledStatusOfUser_when_UserByIdExists() {
        Optional<Role> role = roleRepository.findByName("ROLE_ADMIN");
        assertTrue(role.isPresent());

        userRepository.updateEnabledStatus(1000L, false);
        UserEntity expected = new UserEntity(1000L, "admin1", "Admin001", false, false, Set.of(role.get()));
        Optional<UserEntity> actual = userRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }

    @Test
    void updateFreeStatus_should_UpdateFreeStatusOfUser_when_UserByIdExists() {
        Optional<Role> role = roleRepository.findByName("ROLE_ADMIN");
        assertTrue(role.isPresent());

        userRepository.updateFreeStatus(1000L, true);
        UserEntity expected = new UserEntity(1000L, "admin1", "Admin001", true, true, Set.of(role.get()));
        Optional<UserEntity> actual = userRepository.findById(1000L);
        assertTrue(actual.isPresent());
        assertEquals(expected, actual.get());
    }
}
