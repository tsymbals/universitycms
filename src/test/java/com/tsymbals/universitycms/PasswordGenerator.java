package com.tsymbals.universitycms;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordGenerator {

    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String password = "Student30";
        String encodedPassword = encoder.encode(password);

        System.out.println(encodedPassword);
    }
}
