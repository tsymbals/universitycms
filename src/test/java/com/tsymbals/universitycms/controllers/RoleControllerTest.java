package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.RoleDto;
import com.tsymbals.universitycms.models.dto.requests.CreateRoleRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateRoleRequest;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.RoleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(RoleController.class)
@Import(SecurityConfig.class)
class RoleControllerTest {

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    RoleService roleService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateRoleForm_should_ReturnCreateRoleForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateRoleRequest createRoleRequest = new CreateRoleRequest();

        mockMvc.perform(get("/roles/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/roles/create-role"))
                .andExpect(model().attribute("createRoleRequest", createRoleRequest));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveRole_should_SaveNewRole_RedirectToRolesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateRoleRequest createRoleRequest = new CreateRoleRequest("ROLE_ADMIN");

        mockMvc.perform(post("/roles/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", createRoleRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/roles"));
        verify(roleService).save(createRoleRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveRole_should_ReturnCreateRoleForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateRoleRequest createRoleRequest = new CreateRoleRequest();

        mockMvc.perform(post("/roles/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", createRoleRequest.getName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/roles/create-role"))
                .andExpect(model().attribute("createRoleRequest", createRoleRequest));
        verify(roleService, never()).save(createRoleRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getAllRoles_should_ReturnAllRolesPage_when_LoggedInUserIsAdmin() throws Exception {
        List<RoleDto> roleDataList = List.of(new RoleDto(1000L, "ROLE_ADMIN"));
        when(roleService.getAll()).thenReturn(roleDataList);

        mockMvc.perform(get("/roles"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/roles/all-roles"))
                .andExpect(model().attribute("roles", roleDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getRoleById_should_ReturnOneRolePage_when_LoggedInUserIsAdmin() throws Exception {
        RoleDto roleData = new RoleDto(1000L, "ROLE_ADMIN");
        when(roleService.getById(1000L)).thenReturn(roleData);

        mockMvc.perform(get("/roles/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/roles/one-role"))
                .andExpect(model().attribute("role", roleData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateRoleForm_should_ReturnUpdateRoleForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateRoleRequest updateRoleRequest = new UpdateRoleRequest("ROLE_ADMIN");
        RoleDto roleData = new RoleDto(1000L, "ROLE_ADMIN");
        when(roleService.getById(1000L)).thenReturn(roleData);

        mockMvc.perform(get("/roles/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/roles/update-role"))
                .andExpect(model().attribute("updateRoleRequest", updateRoleRequest))
                .andExpect(model().attribute("role", roleData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateRole_should_UpdateStateOfRole_RedirectToRolesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateRoleRequest updateRoleRequest = new UpdateRoleRequest("ROLE_STAFF");

        mockMvc.perform(post("/roles/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateRoleRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/roles"));
        verify(roleService).updateName(1000L, updateRoleRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateRole_should_UpdateStateOfRole_RedirectToRolesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateRoleRequest updateRoleRequest = new UpdateRoleRequest("ROLE_ADMIN");

        mockMvc.perform(post("/roles/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateRoleRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/roles"));
        verify(roleService).updateName(1000L, updateRoleRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateRole_should_ReturnUpdateRoleForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateRoleRequest updateRoleRequest = new UpdateRoleRequest();
        RoleDto roleData = new RoleDto(1000L, "ROLE_ADMIN");
        when(roleService.getById(1000L)).thenReturn(roleData);

        mockMvc.perform(post("/roles/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateRoleRequest.getName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/roles/update-role"))
                .andExpect(model().attribute("updateRoleRequest", updateRoleRequest))
                .andExpect(model().attribute("role", roleData));
        verify(roleService, never()).updateName(1000L, updateRoleRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteRole_should_DeleteRole_RedirectToRolesUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/roles/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/roles"));
        verify(roleService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/roles".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/roles/create", "/roles", "/roles/1000", "/roles/1000/update", "/roles/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/roles/create", "/roles/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
