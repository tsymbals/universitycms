package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.*;
import com.tsymbals.universitycms.models.dto.requests.CreateLessonRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateLessonRequest;
import com.tsymbals.universitycms.models.entities.*;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.TemporalAdjusters;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(LessonController.class)
@Import(SecurityConfig.class)
class LessonControllerTest {

    static final University UNIVERSITY_DATA = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
    static final Department DEPARTMENT_DATA = new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of());
    static final LessonTime LESSON_TIME_DATA = new LessonTime(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
    static final Auditorium AUDITORIUM_DATA = new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of());
    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final Role TEACHER_ROLE_DATA = new Role(1000L, "ROLE_TEACHER");
    static final Role STUDENT_ROLE_DATA = new Role(1001L, "ROLE_STUDENT");
    static final UserEntity TEACHER_USER_DATA = new UserEntity(1000L, "teacher1", ENCODED_PASSWORD, true, false, Set.of(TEACHER_ROLE_DATA));
    static final UserEntity STUDENT_USER_DATA = new UserEntity(1001L, "student1", ENCODED_PASSWORD, true, false, Set.of(STUDENT_ROLE_DATA));
    static final Teacher TEACHER_DATA = new Teacher(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
    static final Group GROUP_DATA = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    LessonTimeService lessonTimeService;

    @MockBean
    AuditoriumService auditoriumService;

    @MockBean
    LessonService lessonService;

    @MockBean
    UserService userService;

    @MockBean
    TeacherService teacherService;

    @MockBean
    StudentService studentService;

    @MockBean
    CourseService courseService;

    @MockBean
    GroupService groupService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateLessonForm_should_ReturnCreateLessonForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateLessonRequest createLessonRequest = new CreateLessonRequest();
        List<LessonTimeDto> lessonTimeDataList = List.of(new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of()));
        List<AuditoriumDto> auditoriumDataList = List.of(new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of()));
        when(lessonTimeService.getAll()).thenReturn(lessonTimeDataList);
        when(auditoriumService.getAll()).thenReturn(auditoriumDataList);

        mockMvc.perform(get("/lessons/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/create-lesson"))
                .andExpect(model().attribute("createLessonRequest", createLessonRequest))
                .andExpect(model().attribute("lessonTimes", lessonTimeDataList))
                .andExpect(model().attribute("auditoriums", auditoriumDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveLesson_should_SaveNewLesson_RedirectToLessonsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateLessonRequest createLessonRequest = new CreateLessonRequest(LocalDate.of(2024, 1, 1), 1000L, "101");

        mockMvc.perform(post("/lessons/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("date", String.valueOf(createLessonRequest.getDate()))
                        .param("lessonTimeId", String.valueOf(createLessonRequest.getLessonTimeId()))
                        .param("auditoriumName", createLessonRequest.getAuditoriumName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/lessons"));
        verify(lessonService).save(createLessonRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveLesson_should_ReturnCreateLessonForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateLessonRequest createLessonRequest = new CreateLessonRequest();
        List<LessonTimeDto> lessonTimeDataList = List.of(new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of()));
        List<AuditoriumDto> auditoriumDataList = List.of(new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of()));
        when(lessonTimeService.getAll()).thenReturn(lessonTimeDataList);
        when(auditoriumService.getAll()).thenReturn(auditoriumDataList);

        mockMvc.perform(post("/lessons/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("date", String.valueOf(createLessonRequest.getDate()))
                        .param("lessonTimeId", String.valueOf(createLessonRequest.getLessonTimeId()))
                        .param("auditoriumName", createLessonRequest.getAuditoriumName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/create-lesson"))
                .andExpect(model().attribute("createLessonRequest", createLessonRequest))
                .andExpect(model().attribute("lessonTimes", lessonTimeDataList))
                .andExpect(model().attribute("auditoriums", auditoriumDataList));
        verify(lessonService, never()).save(createLessonRequest);
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getAllLessons_should_ReturnAllLessonsPage_when_LoggedInUserIsAdminOrTeacherOrStudent() throws Exception {
        List<LessonDto> lessonDataList = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of()));
        when(lessonService.getAll()).thenReturn(lessonDataList);

        mockMvc.perform(get("/lessons"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/all-lessons"))
                .andExpect(model().attribute("lessons", lessonDataList));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getLessonById_should_ReturnOneLessonPage_when_LoggedInUserIsAdminOrTeacherOrStudent() throws Exception {
        LessonDto lessonData = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());
        when(lessonService.getById(1000L)).thenReturn(lessonData);

        mockMvc.perform(get("/lessons/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/one-lesson"))
                .andExpect(model().attribute("lesson", lessonData));
    }

    @Test
    @WithMockUser(username = "teacher1", authorities = ROLE_TEACHER)
    void getDateLessonsSchedule_should_ReturnDateLessonsSchedulePage_when_LoggedInUserIsTeacher_LessonsBySelectedDateExist() throws Exception {
        LocalDate scheduleDate = LocalDate.of(2024, 1, 1);
        UserDto userData = new UserDto(1000L, "teacher1", ENCODED_PASSWORD, true, false, Set.of(TEACHER_ROLE_DATA));
        TeacherDto teacherData = new TeacherDto(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        List<LessonDto> lessonDataList = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, TEACHER_DATA, Set.of()));
        when(userService.getByUsername("teacher1")).thenReturn(userData);
        when(teacherService.getByUsername("teacher1")).thenReturn(teacherData);
        when(lessonService.getAllByDate(scheduleDate, teacherData)).thenReturn(lessonDataList);

        mockMvc.perform(get("/lessons/{username}/date-schedule", "teacher1")
                        .param("selectedDate", "2024-01-01"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/date-lessons-schedule"))
                .andExpect(model().attribute("selectedDate", scheduleDate))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("lessons", lessonDataList));
    }

    @Test
    @WithMockUser(username = "teacher1", authorities = ROLE_TEACHER)
    void getDateLessonsSchedule_should_ReturnDateLessonsSchedulePage_when_LoggedInUserIsTeacher_LessonsBySelectedDateDoNotExist() throws Exception {
        LocalDate scheduleDate = LocalDate.now();
        UserDto userData = new UserDto(1000L, "teacher1", ENCODED_PASSWORD, true, false, Set.of(TEACHER_ROLE_DATA));
        TeacherDto teacherData = new TeacherDto(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        List<LessonDto> lessonDataList = List.of();
        when(userService.getByUsername("teacher1")).thenReturn(userData);
        when(teacherService.getByUsername("teacher1")).thenReturn(teacherData);
        when(lessonService.getAllByDate(scheduleDate, teacherData)).thenReturn(lessonDataList);

        mockMvc.perform(get("/lessons/{username}/date-schedule", "teacher1"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/date-lessons-schedule"))
                .andExpect(model().attribute("selectedDate", scheduleDate))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("lessons", lessonDataList));
    }

    @Test
    @WithMockUser(username = "student1", authorities = ROLE_STUDENT)
    void getDateLessonsSchedule_should_ReturnDateLessonsSchedulePage_when_LoggedInUserIsStudent_LessonsBySelectedDateExist() throws Exception {
        LocalDate scheduleDate = LocalDate.of(2024, 1, 1);
        UserDto userData = new UserDto(1001L, "student1", ENCODED_PASSWORD, true, false, Set.of(STUDENT_ROLE_DATA));
        StudentDto studentData = new StudentDto(1001L, STUDENT_USER_DATA, GROUP_DATA, "Emily", "Hensley", Set.of());
        List<LessonDto> lessonDataList = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of(GROUP_DATA)));
        when(userService.getByUsername("student1")).thenReturn(userData);
        when(studentService.getByUsername("student1")).thenReturn(studentData);
        when(lessonService.getAllByDate(scheduleDate, studentData)).thenReturn(lessonDataList);

        mockMvc.perform(get("/lessons/{username}/date-schedule", "student1")
                        .param("selectedDate", "2024-01-01"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/date-lessons-schedule"))
                .andExpect(model().attribute("selectedDate", scheduleDate))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("lessons", lessonDataList));
    }

    @Test
    @WithMockUser(username = "student1", authorities = ROLE_STUDENT)
    void getDateLessonsSchedule_should_ReturnDateLessonsSchedulePage_when_LoggedInUserIsStudent_LessonsBySelectedDateDoNotExist() throws Exception {
        LocalDate scheduleDate = LocalDate.now();
        UserDto userData = new UserDto(1001L, "student1", ENCODED_PASSWORD, true, false, Set.of(STUDENT_ROLE_DATA));
        StudentDto studentData = new StudentDto(1001L, STUDENT_USER_DATA, GROUP_DATA, "Emily", "Hensley", Set.of());
        List<LessonDto> lessonDataList = List.of();
        when(userService.getByUsername("student1")).thenReturn(userData);
        when(studentService.getByUsername("student1")).thenReturn(studentData);
        when(lessonService.getAllByDate(scheduleDate, studentData)).thenReturn(lessonDataList);

        mockMvc.perform(get("/lessons/{username}/date-schedule", "student1"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/date-lessons-schedule"))
                .andExpect(model().attribute("selectedDate", scheduleDate))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("lessons", lessonDataList));
    }

    @Test
    @WithMockUser(username = "student1", authorities = {ROLE_TEACHER, ROLE_STUDENT})
    void getDateLessonsSchedule_should_RedirectToHomeUrl_when_LoggedInUserIsTeacherOrStudent_UserDoesNotHaveTeacherOrStudentRole() throws Exception {
        UserDto userData = new UserDto(1001L, "student1", ENCODED_PASSWORD, true, false, Set.of());
        when(userService.getByUsername("student1")).thenReturn(userData);

        mockMvc.perform(get("/lessons/{username}/date-schedule", "student1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    @WithMockUser(username = "teacher1", authorities = {ROLE_TEACHER, ROLE_STUDENT})
    void getDateLessonsSchedule_should_RedirectToHomeUrl_when_UsernameDoesNotMatchAuthenticatedUser() throws Exception {
        UserDto userData = new UserDto(1001L, "student1", ENCODED_PASSWORD, true, false, Set.of(STUDENT_ROLE_DATA));
        when(userService.getByUsername("teacher1")).thenReturn(userData);

        mockMvc.perform(get("/lessons/{username}/date-schedule", "teacher1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    @WithMockUser(username = "username", authorities = {ROLE_ADMIN, ROLE_STAFF})
    void getDateLessonsSchedule_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsAdminOrEmployee_UrlIsCorrect() throws Exception {
        mockMvc.perform(get("/lessons/{username}/date-schedule", "username"))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @Test
    @WithMockUser(username = "teacher1", authorities = ROLE_TEACHER)
    void getWeekLessonsSchedule_should_ReturnWeekLessonsSchedulePage_when_LoggedInUserIsTeacher_LessonsBySelectedWeekExist() throws Exception {
        LocalDate scheduleDate = LocalDate.of(2024, 1, 1);
        LocalDate startWeekScheduleDate = LocalDate.of(2024, 1, 1);
        LocalDate endWeekScheduleDate = LocalDate.of(2024, 1, 7);
        UserDto userData = new UserDto(1000L, "teacher1", ENCODED_PASSWORD, true, false, Set.of(TEACHER_ROLE_DATA));
        TeacherDto teacherData = new TeacherDto(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        List<LessonDto> lessonDataList = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, TEACHER_DATA, Set.of()));
        when(userService.getByUsername("teacher1")).thenReturn(userData);
        when(teacherService.getByUsername("teacher1")).thenReturn(teacherData);
        when(lessonService.getAllByWeek(startWeekScheduleDate, endWeekScheduleDate, teacherData)).thenReturn(lessonDataList);

        mockMvc.perform(get("/lessons/{username}/week-schedule", "teacher1")
                        .param("selectedDate", "2024-01-01"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/week-lessons-schedule"))
                .andExpect(model().attribute("selectedDate", scheduleDate))
                .andExpect(model().attribute("startWeekScheduleDate", startWeekScheduleDate))
                .andExpect(model().attribute("endWeekScheduleDate", endWeekScheduleDate))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("lessons", lessonDataList));
    }

    @Test
    @WithMockUser(username = "teacher1", authorities = ROLE_TEACHER)
    void getWeekLessonsSchedule_should_ReturnWeekLessonsSchedulePage_when_LoggedInUserIsTeacher_LessonsBySelectedWeekDoNotExist() throws Exception {
        LocalDate scheduleDate = LocalDate.now();
        LocalDate startWeekScheduleDate = scheduleDate.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate endWeekScheduleDate = startWeekScheduleDate.plusDays(DayOfWeek.SATURDAY.getValue());
        UserDto userData = new UserDto(1000L, "teacher1", ENCODED_PASSWORD, true, false, Set.of(TEACHER_ROLE_DATA));
        TeacherDto teacherData = new TeacherDto(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        List<LessonDto> lessonDataList = List.of();
        when(userService.getByUsername("teacher1")).thenReturn(userData);
        when(teacherService.getByUsername("teacher1")).thenReturn(teacherData);
        when(lessonService.getAllByWeek(startWeekScheduleDate, endWeekScheduleDate, teacherData)).thenReturn(lessonDataList);

        mockMvc.perform(get("/lessons/{username}/week-schedule", "teacher1"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/week-lessons-schedule"))
                .andExpect(model().attribute("selectedDate", scheduleDate))
                .andExpect(model().attribute("startWeekScheduleDate", startWeekScheduleDate))
                .andExpect(model().attribute("endWeekScheduleDate", endWeekScheduleDate))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("lessons", lessonDataList));
    }

    @Test
    @WithMockUser(username = "student1", authorities = ROLE_STUDENT)
    void getWeekLessonsSchedule_should_ReturnWeekLessonsSchedulePage_when_LoggedInUserIsStudent_LessonsBySelectedWeekExist() throws Exception {
        LocalDate scheduleDate = LocalDate.of(2024, 1, 1);
        LocalDate startWeekScheduleDate = LocalDate.of(2024, 1, 1);
        LocalDate endWeekScheduleDate = LocalDate.of(2024, 1, 7);
        UserDto userData = new UserDto(1001L, "student1", ENCODED_PASSWORD, true, false, Set.of(STUDENT_ROLE_DATA));
        StudentDto studentData = new StudentDto(1001L, STUDENT_USER_DATA, GROUP_DATA, "Emily", "Hensley", Set.of());
        List<LessonDto> lessonDataList = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of(GROUP_DATA)));
        when(userService.getByUsername("student1")).thenReturn(userData);
        when(studentService.getByUsername("student1")).thenReturn(studentData);
        when(lessonService.getAllByWeek(startWeekScheduleDate, endWeekScheduleDate, studentData)).thenReturn(lessonDataList);

        mockMvc.perform(get("/lessons/{username}/week-schedule", "student1")
                        .param("selectedDate", "2024-01-01"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/week-lessons-schedule"))
                .andExpect(model().attribute("selectedDate", scheduleDate))
                .andExpect(model().attribute("startWeekScheduleDate", startWeekScheduleDate))
                .andExpect(model().attribute("endWeekScheduleDate", endWeekScheduleDate))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("lessons", lessonDataList));
    }

    @Test
    @WithMockUser(username = "student1", authorities = ROLE_STUDENT)
    void getWeekLessonsSchedule_should_ReturnWeekLessonsSchedulePage_when_LoggedInUserIsStudent_LessonsBySelectedWeekDoNotExist() throws Exception {
        LocalDate scheduleDate = LocalDate.now();
        LocalDate startWeekScheduleDate = scheduleDate.with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate endWeekScheduleDate = startWeekScheduleDate.plusDays(DayOfWeek.SATURDAY.getValue());
        UserDto userData = new UserDto(1001L, "student1", ENCODED_PASSWORD, true, false, Set.of(STUDENT_ROLE_DATA));
        StudentDto studentData = new StudentDto(1001L, STUDENT_USER_DATA, GROUP_DATA, "Emily", "Hensley", Set.of());
        List<LessonDto> lessonDataList = List.of();
        when(userService.getByUsername("student1")).thenReturn(userData);
        when(studentService.getByUsername("student1")).thenReturn(studentData);
        when(lessonService.getAllByWeek(startWeekScheduleDate, endWeekScheduleDate, studentData)).thenReturn(lessonDataList);

        mockMvc.perform(get("/lessons/{username}/week-schedule", "student1"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/week-lessons-schedule"))
                .andExpect(model().attribute("selectedDate", scheduleDate))
                .andExpect(model().attribute("startWeekScheduleDate", startWeekScheduleDate))
                .andExpect(model().attribute("endWeekScheduleDate", endWeekScheduleDate))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("lessons", lessonDataList));
    }

    @Test
    @WithMockUser(username = "student1", authorities = {ROLE_TEACHER, ROLE_STUDENT})
    void getWeekLessonsSchedule_should_RedirectToHomeUrl_when_LoggedInUserIsTeacherOrStudent_UserDoesNotHaveTeacherOrStudentRole() throws Exception {
        UserDto userData = new UserDto(1001L, "student1", ENCODED_PASSWORD, true, false, Set.of());
        when(userService.getByUsername("student1")).thenReturn(userData);

        mockMvc.perform(get("/lessons/{username}/week-schedule", "student1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    @WithMockUser(username = "teacher1", authorities = {ROLE_TEACHER, ROLE_STUDENT})
    void getWeekLessonsSchedule_should_RedirectToHomeUrl_when_UsernameDoesNotMatchAuthenticatedUser() throws Exception {
        UserDto userData = new UserDto(1001L, "student1", ENCODED_PASSWORD, true, false, Set.of(STUDENT_ROLE_DATA));
        when(userService.getByUsername("teacher1")).thenReturn(userData);

        mockMvc.perform(get("/lessons/{username}/week-schedule", "teacher1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    @WithMockUser(username = "username", authorities = {ROLE_ADMIN, ROLE_STAFF})
    void getWeekLessonsSchedule_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsAdminOrEmployee_UrlIsCorrect() throws Exception {
        mockMvc.perform(get("/lessons/{username}/week-schedule", "username"))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateLessonForm_should_ReturnUpdateLessonForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(LocalDate.of(2024, 1, 1));
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo European language family whose speakers called Anglophones originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        Teacher teacherData = new Teacher(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        LessonDto lessonData = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, courseData, teacherData, new HashSet<>());
        lessonData.getGroups().add(groupData);
        List<LessonTimeDto> lessonTimeDataList = List.of(new LessonTimeDto(1001L, LocalTime.of(9, 15), LocalTime.of(10, 0), Set.of()));
        List<AuditoriumDto> auditoriumDataList = List.of(new AuditoriumDto(1001L, DEPARTMENT_DATA, "102", Set.of()));
        List<CourseDto> courseDataList = List.of(new CourseDto(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()));
        List<TeacherDto> teacherDataList = List.of(new TeacherDto(1001L, TEACHER_USER_DATA, "Guy", "Griffin", Set.of(), Set.of()));
        List<GroupDto> groupDataList = List.of(new GroupDto(1001L, "1CE-24", Set.of(), Set.of(), Set.of()));
        when(lessonService.getById(1000L)).thenReturn(lessonData);
        when(lessonTimeService.getAllNoneMatching(lessonData.getLessonTime())).thenReturn(lessonTimeDataList);
        when(auditoriumService.getAllNoneMatching(lessonData.getAuditorium())).thenReturn(auditoriumDataList);
        when(courseService.getAllNoneMatching(lessonData.getCourse())).thenReturn(courseDataList);
        when(teacherService.getAllNoneMatching(lessonData.getTeacher())).thenReturn(teacherDataList);
        when(groupService.getAllNoneMatching(lessonData.getGroups())).thenReturn(groupDataList);

        mockMvc.perform(get("/lessons/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/update-lesson"))
                .andExpect(model().attribute("updateLessonRequest", updateLessonRequest))
                .andExpect(model().attribute("lesson", lessonData))
                .andExpect(model().attribute("lessonTimes", lessonTimeDataList))
                .andExpect(model().attribute("auditoriums", auditoriumDataList))
                .andExpect(model().attribute("coursesToUpdate", courseDataList))
                .andExpect(model().attribute("courseToRemove", lessonData.getCourse()))
                .andExpect(model().attribute("teachersToUpdate", teacherDataList))
                .andExpect(model().attribute("teacherToRemove", lessonData.getTeacher()))
                .andExpect(model().attribute("groupsToAdd", groupDataList))
                .andExpect(model().attribute("groupsToRemove", lessonData.getGroups()));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateLesson_should_UpdateStateOfLesson_RedirectToLessonsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(LocalDate.of(2025, 1, 1), 1001L, "201", "Mathematics", "English", 1001L, 1000L, "1CE-24", "1SE-24");

        mockMvc.perform(post("/lessons/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("date", String.valueOf(updateLessonRequest.getDate()))
                        .param("lessonTimeId", String.valueOf(updateLessonRequest.getLessonTimeId()))
                        .param("auditoriumName", updateLessonRequest.getAuditoriumName())
                        .param("courseNameToUpdate", updateLessonRequest.getCourseNameToUpdate())
                        .param("courseNameToRemove", updateLessonRequest.getCourseNameToRemove())
                        .param("teacherIdToUpdate", String.valueOf(updateLessonRequest.getTeacherIdToUpdate()))
                        .param("teacherIdToRemove", String.valueOf(updateLessonRequest.getTeacherIdToRemove()))
                        .param("groupNameToAdd", updateLessonRequest.getGroupNameToAdd())
                        .param("groupNameToRemove", updateLessonRequest.getGroupNameToRemove()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/lessons"));
        verify(lessonService).updateDate(1000L, updateLessonRequest);
        verify(lessonService).updateLessonTime(1000L, updateLessonRequest);
        verify(lessonService).updateAuditorium(1000L, updateLessonRequest);
        verify(lessonService).removeCourse(1000L, updateLessonRequest);
        verify(lessonService).updateCourse(1000L, updateLessonRequest);
        verify(lessonService).removeTeacher(1000L, updateLessonRequest);
        verify(lessonService).updateTeacher(1000L, updateLessonRequest);
        verify(lessonService).addGroup(1000L, updateLessonRequest);
        verify(lessonService).removeGroup(1000L, updateLessonRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateLesson_should_UpdateStateOfLesson_RedirectToLessonsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest(LocalDate.of(2024, 1, 1), 1000L, "101", "", "", Long.MIN_VALUE, Long.MIN_VALUE, "", "");

        mockMvc.perform(post("/lessons/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("date", String.valueOf(updateLessonRequest.getDate()))
                        .param("lessonTimeId", String.valueOf(updateLessonRequest.getLessonTimeId()))
                        .param("auditoriumName", updateLessonRequest.getAuditoriumName())
                        .param("courseNameToUpdate", updateLessonRequest.getCourseNameToUpdate())
                        .param("courseNameToRemove", updateLessonRequest.getCourseNameToRemove())
                        .param("teacherIdToUpdate", String.valueOf(updateLessonRequest.getTeacherIdToUpdate()))
                        .param("teacherIdToRemove", String.valueOf(updateLessonRequest.getTeacherIdToRemove()))
                        .param("groupNameToAdd", updateLessonRequest.getGroupNameToAdd())
                        .param("groupNameToRemove", updateLessonRequest.getGroupNameToRemove()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/lessons"));
        verify(lessonService).updateDate(1000L, updateLessonRequest);
        verify(lessonService).updateLessonTime(1000L, updateLessonRequest);
        verify(lessonService).updateAuditorium(1000L, updateLessonRequest);
        verify(lessonService, never()).removeCourse(1000L, updateLessonRequest);
        verify(lessonService, never()).updateCourse(1000L, updateLessonRequest);
        verify(lessonService, never()).removeTeacher(1000L, updateLessonRequest);
        verify(lessonService, never()).updateTeacher(1000L, updateLessonRequest);
        verify(lessonService, never()).addGroup(1000L, updateLessonRequest);
        verify(lessonService, never()).removeGroup(1000L, updateLessonRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateLesson_should_ReturnUpdateLessonForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateLessonRequest updateLessonRequest = new UpdateLessonRequest();
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo European language family whose speakers called Anglophones originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        Teacher teacherData = new Teacher(1000L, TEACHER_USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        LessonDto lessonData = new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, courseData, teacherData, new HashSet<>());
        lessonData.getGroups().add(groupData);
        List<LessonTimeDto> lessonTimeDataList = List.of(new LessonTimeDto(1001L, LocalTime.of(9, 15), LocalTime.of(10, 0), Set.of()));
        List<AuditoriumDto> auditoriumDataList = List.of(new AuditoriumDto(1001L, DEPARTMENT_DATA, "102", Set.of()));
        List<CourseDto> courseDataList = List.of(new CourseDto(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()));
        List<TeacherDto> teacherDataList = List.of(new TeacherDto(1001L, TEACHER_USER_DATA, "Guy", "Griffin", Set.of(), Set.of()));
        List<GroupDto> groupDataList = List.of(new GroupDto(1001L, "1CE-24", Set.of(), Set.of(), Set.of()));
        when(lessonService.getById(1000L)).thenReturn(lessonData);
        when(lessonTimeService.getAllNoneMatching(lessonData.getLessonTime())).thenReturn(lessonTimeDataList);
        when(auditoriumService.getAllNoneMatching(lessonData.getAuditorium())).thenReturn(auditoriumDataList);
        when(courseService.getAllNoneMatching(lessonData.getCourse())).thenReturn(courseDataList);
        when(teacherService.getAllNoneMatching(lessonData.getTeacher())).thenReturn(teacherDataList);
        when(groupService.getAllNoneMatching(lessonData.getGroups())).thenReturn(groupDataList);

        mockMvc.perform(post("/lessons/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("date", String.valueOf(updateLessonRequest.getDate()))
                        .param("lessonTimeId", String.valueOf(updateLessonRequest.getLessonTimeId()))
                        .param("auditoriumName", updateLessonRequest.getAuditoriumName())
                        .param("courseNameToUpdate", updateLessonRequest.getCourseNameToUpdate())
                        .param("courseNameToRemove", updateLessonRequest.getCourseNameToRemove())
                        .param("teacherIdToUpdate", String.valueOf(updateLessonRequest.getTeacherIdToUpdate()))
                        .param("teacherIdToRemove", String.valueOf(updateLessonRequest.getTeacherIdToRemove()))
                        .param("groupNameToAdd", updateLessonRequest.getGroupNameToAdd())
                        .param("groupNameToRemove", updateLessonRequest.getGroupNameToRemove()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lessons/update-lesson"))
                .andExpect(model().attribute("updateLessonRequest", updateLessonRequest))
                .andExpect(model().attribute("lesson", lessonData))
                .andExpect(model().attribute("lessonTimes", lessonTimeDataList))
                .andExpect(model().attribute("auditoriums", auditoriumDataList))
                .andExpect(model().attribute("coursesToUpdate", courseDataList))
                .andExpect(model().attribute("courseToRemove", lessonData.getCourse()))
                .andExpect(model().attribute("teachersToUpdate", teacherDataList))
                .andExpect(model().attribute("teacherToRemove", lessonData.getTeacher()))
                .andExpect(model().attribute("groupsToAdd", groupDataList))
                .andExpect(model().attribute("groupsToRemove", lessonData.getGroups()));
        verify(lessonService, never()).updateDate(1000L, updateLessonRequest);
        verify(lessonService, never()).updateLessonTime(1000L, updateLessonRequest);
        verify(lessonService, never()).updateAuditorium(1000L, updateLessonRequest);
        verify(lessonService, never()).removeCourse(1000L, updateLessonRequest);
        verify(lessonService, never()).updateCourse(1000L, updateLessonRequest);
        verify(lessonService, never()).removeTeacher(1000L, updateLessonRequest);
        verify(lessonService, never()).updateTeacher(1000L, updateLessonRequest);
        verify(lessonService, never()).addGroup(1000L, updateLessonRequest);
        verify(lessonService, never()).removeGroup(1000L, updateLessonRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteLesson_should_DeleteLesson_RedirectToLessonsUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/lessons/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/lessons"));
        verify(lessonService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/lessons".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/lessons/create", "/lessons", "/lessons/1000", "/lessons/1000/update", "/lessons/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/lessons/create", "/lessons/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
