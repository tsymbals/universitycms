package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.LessonTimeDto;
import com.tsymbals.universitycms.models.dto.requests.CreateLessonTimeRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateLessonTimeRequest;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.LessonTimeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(LessonTimeController.class)
@Import(SecurityConfig.class)
class LessonTimeControllerTest {

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    LessonTimeService lessonTimeService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateLessonTimeForm_should_ReturnCreateLessonTimeForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateLessonTimeRequest createLessonTimeRequest = new CreateLessonTimeRequest();

        mockMvc.perform(get("/lesson-times/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lesson-times/create-lesson-time"))
                .andExpect(model().attribute("createLessonTimeRequest", createLessonTimeRequest));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveLessonTime_should_SaveNewLessonTime_RedirectToLessonTimesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateLessonTimeRequest createLessonTimeRequest = new CreateLessonTimeRequest(LocalTime.of(8, 15), LocalTime.of(9, 0));

        mockMvc.perform(post("/lesson-times/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("startTime", String.valueOf(createLessonTimeRequest.getStartTime()))
                        .param("endTime", String.valueOf(createLessonTimeRequest.getEndTime())))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/lesson-times"));
        verify(lessonTimeService).save(createLessonTimeRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveLessonTime_should_ReturnCreateLessonTimeForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateLessonTimeRequest createLessonTimeRequest = new CreateLessonTimeRequest();

        mockMvc.perform(post("/lesson-times/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("startTime", String.valueOf(createLessonTimeRequest.getStartTime()))
                        .param("endTime", String.valueOf(createLessonTimeRequest.getEndTime())))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lesson-times/create-lesson-time"))
                .andExpect(model().attribute("createLessonTimeRequest", createLessonTimeRequest));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void getAllLessonTimes_should_ReturnAllLessonTimesPage_when_LoggedInUserIsAdminOrEmployeeOrTeacherOrStudent() throws Exception {
        List<LessonTimeDto> lessonTimeDataList = List.of(new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of()));
        when(lessonTimeService.getAll()).thenReturn(lessonTimeDataList);

        mockMvc.perform(get("/lesson-times"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lesson-times/all-lesson-times"))
                .andExpect(model().attribute("lessonTimes", lessonTimeDataList));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void getLessonTimeById_should_ReturnOneLessonTimePage_when_LoggedInUserIsAdminOrEmployeeOrTeacherOrStudent() throws Exception {
        LessonTimeDto lessonTimeData = new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
        when(lessonTimeService.getById(1000L)).thenReturn(lessonTimeData);

        mockMvc.perform(get("/lesson-times/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lesson-times/one-lesson-time"))
                .andExpect(model().attribute("lessonTime", lessonTimeData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateLessonTimeForm_should_ReturnUpdateLessonTimeForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateLessonTimeRequest updateLessonTimeRequest = new UpdateLessonTimeRequest(LocalTime.of(8, 15), LocalTime.of(9, 0));
        LessonTimeDto lessonTimeData = new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
        when(lessonTimeService.getById(1000L)).thenReturn(lessonTimeData);

        mockMvc.perform(get("/lesson-times/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lesson-times/update-lesson-time"))
                .andExpect(model().attribute("updateLessonTimeRequest", updateLessonTimeRequest))
                .andExpect(model().attribute("lessonTime", lessonTimeData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateLessonTime_should_UpdateStateOfLessonTime_RedirectToLessonTimesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateLessonTimeRequest updateLessonTimeRequest = new UpdateLessonTimeRequest(LocalTime.of(9, 15), LocalTime.of(10, 0));

        mockMvc.perform(post("/lesson-times/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("startTime", String.valueOf(updateLessonTimeRequest.getStartTime()))
                        .param("endTime", String.valueOf(updateLessonTimeRequest.getEndTime())))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/lesson-times"));
        verify(lessonTimeService).updateStartTime(1000L, updateLessonTimeRequest);
        verify(lessonTimeService).updateEndTime(1000L, updateLessonTimeRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateLessonTime_should_UpdateStateOfLessonTime_RedirectToLessonTimesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateLessonTimeRequest updateLessonTimeRequest = new UpdateLessonTimeRequest(LocalTime.of(8, 15), LocalTime.of(9, 0));

        mockMvc.perform(post("/lesson-times/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("startTime", String.valueOf(updateLessonTimeRequest.getStartTime()))
                        .param("endTime", String.valueOf(updateLessonTimeRequest.getEndTime())))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/lesson-times"));
        verify(lessonTimeService).updateStartTime(1000L, updateLessonTimeRequest);
        verify(lessonTimeService).updateEndTime(1000L, updateLessonTimeRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateLessonTime_should_ReturnUpdateLessonTimeForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateLessonTimeRequest updateLessonTimeRequest = new UpdateLessonTimeRequest();
        LessonTimeDto lessonTimeData = new LessonTimeDto(1000L, LocalTime.of(8, 15), LocalTime.of(9, 0), Set.of());
        when(lessonTimeService.getById(1000L)).thenReturn(lessonTimeData);

        mockMvc.perform(post("/lesson-times/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("startTime", String.valueOf(updateLessonTimeRequest.getStartTime()))
                        .param("endTime", String.valueOf(updateLessonTimeRequest.getEndTime())))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/lesson-times/update-lesson-time"))
                .andExpect(model().attribute("updateLessonTimeRequest", updateLessonTimeRequest))
                .andExpect(model().attribute("lessonTime", lessonTimeData));
        verify(lessonTimeService, never()).updateStartTime(1000L, updateLessonTimeRequest);
        verify(lessonTimeService, never()).updateEndTime(1000L, updateLessonTimeRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteLessonTime_should_DeleteLessonTime_RedirectToLessonTimesUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/lesson-times/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/lesson-times"));
        verify(lessonTimeService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/lesson-times".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/lesson-times/create", "/lesson-times/1000/update", "/lesson-times/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/lesson-times/create", "/lesson-times/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
