package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.CourseDto;
import com.tsymbals.universitycms.models.dto.GroupDto;
import com.tsymbals.universitycms.models.dto.TeacherDto;
import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateTeacherRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateTeacherRequest;
import com.tsymbals.universitycms.models.entities.Course;
import com.tsymbals.universitycms.models.entities.Group;
import com.tsymbals.universitycms.models.entities.Role;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.CourseService;
import com.tsymbals.universitycms.services.GroupService;
import com.tsymbals.universitycms.services.TeacherService;
import com.tsymbals.universitycms.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TeacherController.class)
@Import(SecurityConfig.class)
class TeacherControllerTest {

    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final Role TEACHER_ROLE_DATA = new Role(1000L, "ROLE_TEACHER");
    static final UserEntity USER_DATA = new UserEntity(1000L, "teacher1", ENCODED_PASSWORD, true, false, Set.of(TEACHER_ROLE_DATA));

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    UserService userService;

    @MockBean
    TeacherService teacherService;

    @MockBean
    CourseService courseService;

    @MockBean
    GroupService groupService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateTeacherForm_should_ReturnCreateTeacherForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateTeacherRequest createTeacherRequest = new CreateTeacherRequest();
        List<UserDto> userDataList = List.of(new UserDto(1000L, "teacher1", ENCODED_PASSWORD, true, true, Set.of(TEACHER_ROLE_DATA)));
        when(userService.getAllFreeTeacherUsers()).thenReturn(userDataList);

        mockMvc.perform(get("/teachers/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/teachers/create-teacher"))
                .andExpect(model().attribute("createTeacherRequest", createTeacherRequest))
                .andExpect(model().attribute("users", userDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveTeacher_should_SaveNewTeacher_RedirectToTeachersUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateTeacherRequest createTeacherRequest = new CreateTeacherRequest("teacher1", "Luz", "Carson");

        mockMvc.perform(post("/teachers/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", createTeacherRequest.getUsername())
                        .param("firstName", createTeacherRequest.getFirstName())
                        .param("lastName", createTeacherRequest.getLastName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/teachers"));
        verify(teacherService).save(createTeacherRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveTeacher_should_ReturnCreateTeacherForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateTeacherRequest createTeacherRequest = new CreateTeacherRequest();
        List<UserDto> userDataList = List.of(new UserDto(1000L, "teacher1", ENCODED_PASSWORD, true, true, Set.of(TEACHER_ROLE_DATA)));
        when(userService.getAllFreeTeacherUsers()).thenReturn(userDataList);

        mockMvc.perform(post("/teachers/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", createTeacherRequest.getUsername())
                        .param("firstName", createTeacherRequest.getFirstName())
                        .param("lastName", createTeacherRequest.getLastName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/teachers/create-teacher"))
                .andExpect(model().attribute("createTeacherRequest", createTeacherRequest))
                .andExpect(model().attribute("users", userDataList));
        verify(teacherService, never()).save(createTeacherRequest);
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getAllTeachers_should_ReturnAllTeachersPage_when_LoggedInUserIsAdminOrTeacherOrStudent() throws Exception {
        List<TeacherDto> teacherDataList = List.of(new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of()));
        when(teacherService.getAll()).thenReturn(teacherDataList);

        mockMvc.perform(get("/teachers"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/teachers/all-teachers"))
                .andExpect(model().attribute("teachers", teacherDataList));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getTeacherById_should_ReturnOneTeacherPage_when_LoggedInUserIsAdminOrTeacherOrStudent() throws Exception {
        TeacherDto teacherData = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", Set.of(), Set.of());
        when(teacherService.getById(1000L)).thenReturn(teacherData);

        mockMvc.perform(get("/teachers/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/teachers/one-teacher"))
                .andExpect(model().attribute("teacher", teacherData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateTeacherForm_should_ReturnUpdateTeacherForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest("Carson");
        TeacherDto teacherData = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", new HashSet<>(), new HashSet<>());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        teacherData.getCourses().add(courseData);
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        teacherData.getGroups().add(groupData);
        List<UserDto> userDataList = List.of(new UserDto(1001L, "teacher2", ENCODED_PASSWORD, true, true, Set.of(TEACHER_ROLE_DATA)));
        List<CourseDto> courseDataList = List.of(new CourseDto(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()));
        List<GroupDto> groupDataList = List.of(new GroupDto(1001L, "1CE-24", Set.of(), Set.of(), Set.of()));
        when(teacherService.getById(1000L)).thenReturn(teacherData);
        when(userService.getAllFreeTeacherUsers()).thenReturn(userDataList);
        when(courseService.getAllNoneMatching(teacherData.getCourses())).thenReturn(courseDataList);
        when(groupService.getAllNoneMatching(teacherData.getGroups())).thenReturn(groupDataList);

        mockMvc.perform(get("/teachers/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/teachers/update-teacher"))
                .andExpect(model().attribute("updateTeacherRequest", updateTeacherRequest))
                .andExpect(model().attribute("teacher", teacherData))
                .andExpect(model().attribute("users", userDataList))
                .andExpect(model().attribute("coursesToAdd", courseDataList))
                .andExpect(model().attribute("coursesToRemove", teacherData.getCourses()))
                .andExpect(model().attribute("groupsToAdd", groupDataList))
                .andExpect(model().attribute("groupsToRemove", teacherData.getGroups()));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateTeacher_should_UpdateStateOfTeacher_RedirectToTeachersUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest("teacher2", "Snyder", "Mathematics", "English", "1CE-24", "1SE-24");

        mockMvc.perform(post("/teachers/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateTeacherRequest.getUsername())
                        .param("lastName", updateTeacherRequest.getLastName())
                        .param("courseNameToAdd", updateTeacherRequest.getCourseNameToAdd())
                        .param("courseNameToRemove", updateTeacherRequest.getCourseNameToRemove())
                        .param("groupNameToAdd", updateTeacherRequest.getGroupNameToAdd())
                        .param("groupNameToRemove", updateTeacherRequest.getGroupNameToRemove()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/teachers"));
        verify(teacherService).updateUser(1000L, updateTeacherRequest);
        verify(teacherService).updateLastName(1000L, updateTeacherRequest);
        verify(teacherService).addCourse(1000L, updateTeacherRequest);
        verify(teacherService).removeCourse(1000L, updateTeacherRequest);
        verify(teacherService).addGroup(1000L, updateTeacherRequest);
        verify(teacherService).removeGroup(1000L, updateTeacherRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateTeacher_should_UpdateStateOfTeacher_RedirectToTeachersUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest("teacher1", "Carson", "", "", "", "");

        mockMvc.perform(post("/teachers/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateTeacherRequest.getUsername())
                        .param("lastName", updateTeacherRequest.getLastName())
                        .param("courseNameToAdd", updateTeacherRequest.getCourseNameToAdd())
                        .param("courseNameToRemove", updateTeacherRequest.getCourseNameToRemove())
                        .param("groupNameToAdd", updateTeacherRequest.getGroupNameToAdd())
                        .param("groupNameToRemove", updateTeacherRequest.getGroupNameToRemove()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/teachers"));
        verify(teacherService).updateUser(1000L, updateTeacherRequest);
        verify(teacherService).updateLastName(1000L, updateTeacherRequest);
        verify(teacherService, never()).addCourse(1000L, updateTeacherRequest);
        verify(teacherService, never()).removeCourse(1000L, updateTeacherRequest);
        verify(teacherService, never()).addGroup(1000L, updateTeacherRequest);
        verify(teacherService, never()).removeGroup(1000L, updateTeacherRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateTeacher_should_ReturnUpdateTeacherForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateTeacherRequest updateTeacherRequest = new UpdateTeacherRequest();
        TeacherDto teacherData = new TeacherDto(1000L, USER_DATA, "Luz", "Carson", new HashSet<>(), new HashSet<>());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        teacherData.getCourses().add(courseData);
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        teacherData.getGroups().add(groupData);
        List<UserDto> userDataList = List.of(new UserDto(1001L, "teacher2", ENCODED_PASSWORD, true, true, Set.of(TEACHER_ROLE_DATA)));
        List<CourseDto> courseDataList = List.of(new CourseDto(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()));
        List<GroupDto> groupDataList = List.of(new GroupDto(1001L, "1CE-24", Set.of(), Set.of(), Set.of()));
        when(teacherService.getById(1000L)).thenReturn(teacherData);
        when(userService.getAllFreeTeacherUsers()).thenReturn(userDataList);
        when(courseService.getAllNoneMatching(teacherData.getCourses())).thenReturn(courseDataList);
        when(groupService.getAllNoneMatching(teacherData.getGroups())).thenReturn(groupDataList);

        mockMvc.perform(post("/teachers/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateTeacherRequest.getUsername())
                        .param("lastName", updateTeacherRequest.getLastName())
                        .param("courseNameToAdd", updateTeacherRequest.getCourseNameToAdd())
                        .param("courseNameToRemove", updateTeacherRequest.getCourseNameToRemove())
                        .param("groupNameToAdd", updateTeacherRequest.getGroupNameToAdd())
                        .param("groupNameToRemove", updateTeacherRequest.getGroupNameToRemove()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/teachers/update-teacher"))
                .andExpect(model().attribute("updateTeacherRequest", updateTeacherRequest))
                .andExpect(model().attribute("teacher", teacherData))
                .andExpect(model().attribute("users", userDataList))
                .andExpect(model().attribute("coursesToAdd", courseDataList))
                .andExpect(model().attribute("coursesToRemove", teacherData.getCourses()))
                .andExpect(model().attribute("groupsToAdd", groupDataList))
                .andExpect(model().attribute("groupsToRemove", teacherData.getGroups()));
        verify(teacherService, never()).updateUser(1000L, updateTeacherRequest);
        verify(teacherService, never()).updateLastName(1000L, updateTeacherRequest);
        verify(teacherService, never()).addCourse(1000L, updateTeacherRequest);
        verify(teacherService, never()).removeCourse(1000L, updateTeacherRequest);
        verify(teacherService, never()).addGroup(1000L, updateTeacherRequest);
        verify(teacherService, never()).removeGroup(1000L, updateTeacherRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteTeacher_should_DeleteTeacher_RedirectToTeachersUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/teachers/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/teachers"));
        verify(teacherService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/teachers".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/teachers/create", "/teachers/1000/update", "/teachers/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/teachers", "/teachers/1000"})
    @WithMockUser(authorities = ROLE_STAFF)
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsEmployee_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/teachers/create", "/teachers/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
