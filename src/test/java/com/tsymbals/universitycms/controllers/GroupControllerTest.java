package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.GroupDto;
import com.tsymbals.universitycms.models.dto.requests.CreateGroupRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateGroupRequest;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.GroupService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(GroupController.class)
@Import(SecurityConfig.class)
class GroupControllerTest {

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    GroupService groupService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateGroupForm_should_ReturnCreateGroupForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateGroupRequest createGroupRequest = new CreateGroupRequest();

        mockMvc.perform(get("/groups/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/groups/create-group"))
                .andExpect(model().attribute("createGroupRequest", createGroupRequest));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveGroup_should_SaveNewGroup_RedirectToGroupsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateGroupRequest createGroupRequest = new CreateGroupRequest("1SE-24");

        mockMvc.perform(post("/groups/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", createGroupRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/groups"));
        verify(groupService).save(createGroupRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveGroup_should_ReturnCreateGroupForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateGroupRequest createGroupRequest = new CreateGroupRequest();

        mockMvc.perform(post("/groups/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", createGroupRequest.getName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/groups/create-group"))
                .andExpect(model().attribute("createGroupRequest", createGroupRequest));
        verify(groupService, never()).save(createGroupRequest);
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getAllGroups_should_ReturnAllGroupsPage_when_LoggedInUserIsAdminOrTeacherOrStudent() throws Exception {
        List<GroupDto> groupDataList = List.of(new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of()));
        when(groupService.getAll()).thenReturn(groupDataList);

        mockMvc.perform(get("/groups"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/groups/all-groups"))
                .andExpect(model().attribute("groups", groupDataList));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getGroupById_should_ReturnOneGroupPage_when_LoggedInUserIsAdminOrTeacherOrStudent() throws Exception {
        GroupDto groupData = new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        when(groupService.getById(1000L)).thenReturn(groupData);

        mockMvc.perform(get("/groups/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/groups/one-group"))
                .andExpect(model().attribute("group", groupData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateGroupForm_should_ReturnUpdateGroupForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateGroupRequest updateGroupRequest = new UpdateGroupRequest("1SE-24");
        GroupDto groupData = new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        when(groupService.getById(1000L)).thenReturn(groupData);

        mockMvc.perform(get("/groups/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/groups/update-group"))
                .andExpect(model().attribute("updateGroupRequest", updateGroupRequest))
                .andExpect(model().attribute("group", groupData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateGroup_should_UpdateStateOfGroup_RedirectToGroupsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateGroupRequest updateGroupRequest = new UpdateGroupRequest("1CE-24");

        mockMvc.perform(post("/groups/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateGroupRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/groups"));
        verify(groupService).updateName(1000L, updateGroupRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateGroup_should_UpdateStateOfGroup_RedirectToGroupsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateGroupRequest updateGroupRequest = new UpdateGroupRequest("1SE-24");

        mockMvc.perform(post("/groups/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateGroupRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/groups"));
        verify(groupService).updateName(1000L, updateGroupRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateGroup_should_ReturnUpdateGroupForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateGroupRequest updateGroupRequest = new UpdateGroupRequest();
        GroupDto groupData = new GroupDto(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        when(groupService.getById(1000L)).thenReturn(groupData);

        mockMvc.perform(post("/groups/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateGroupRequest.getName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/groups/update-group"))
                .andExpect(model().attribute("updateGroupRequest", updateGroupRequest))
                .andExpect(model().attribute("group", groupData));
        verify(groupService, never()).updateName(1000L, updateGroupRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteGroup_should_DeleteGroup_RedirectToGroupsUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/groups/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/groups"));
        verify(groupService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/groups".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/groups/create", "/groups/1000/update", "/groups/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/groups", "/groups/1000"})
    @WithMockUser(authorities = ROLE_STAFF)
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsEmployee_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/groups/create", "/groups/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
