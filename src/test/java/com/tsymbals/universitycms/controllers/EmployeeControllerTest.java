package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.EmployeeDto;
import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateEmployeeRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateEmployeeRequest;
import com.tsymbals.universitycms.models.entities.Role;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.EmployeeService;
import com.tsymbals.universitycms.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(EmployeeController.class)
@Import(SecurityConfig.class)
class EmployeeControllerTest {

    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final Role STAFF_ROLE_DATA = new Role(1000L, "ROLE_STAFF");
    static final UserEntity USER_DATA = new UserEntity(1000L, "employee1", ENCODED_PASSWORD, true, false, Set.of(STAFF_ROLE_DATA));

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    UserService userService;

    @MockBean
    EmployeeService employeeService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateEmployeeForm_should_ReturnCreateEmployeeForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateEmployeeRequest createEmployeeRequest = new CreateEmployeeRequest();
        List<UserDto> userDataList = List.of(new UserDto(1000L, "employee1", ENCODED_PASSWORD, true, true, Set.of(STAFF_ROLE_DATA)));
        when(userService.getAllFreeEmployeeUsers()).thenReturn(userDataList);

        mockMvc.perform(get("/employees/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/employees/create-employee"))
                .andExpect(model().attribute("createEmployeeRequest", createEmployeeRequest))
                .andExpect(model().attribute("users", userDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveEmployee_should_SaveNewEmployee_RedirectToEmployeesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateEmployeeRequest createEmployeeRequest = new CreateEmployeeRequest("employee1", "Tasha", "Garcia");

        mockMvc.perform(post("/employees/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", createEmployeeRequest.getUsername())
                        .param("firstName", createEmployeeRequest.getFirstName())
                        .param("lastName", createEmployeeRequest.getLastName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/employees"));
        verify(employeeService).save(createEmployeeRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveEmployee_should_ReturnCreateEmployeeForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateEmployeeRequest createEmployeeRequest = new CreateEmployeeRequest();
        List<UserDto> userDataList = List.of(new UserDto(1000L, "employee1", ENCODED_PASSWORD, true, true, Set.of(STAFF_ROLE_DATA)));
        when(userService.getAllFreeEmployeeUsers()).thenReturn(userDataList);

        mockMvc.perform(post("/employees/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", createEmployeeRequest.getUsername())
                        .param("firstName", createEmployeeRequest.getFirstName())
                        .param("lastName", createEmployeeRequest.getLastName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/employees/create-employee"))
                .andExpect(model().attribute("createEmployeeRequest", createEmployeeRequest))
                .andExpect(model().attribute("users", userDataList));
        verify(employeeService, never()).save(createEmployeeRequest);
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF})
    void getAllEmployees_should_ReturnAllEmployeesPage_when_LoggedInUserIsAdminOrEmployee() throws Exception {
        List<EmployeeDto> employeeDataList = List.of(new EmployeeDto(1000L, USER_DATA, "Tasha", "Garcia"));
        when(employeeService.getAll()).thenReturn(employeeDataList);

        mockMvc.perform(get("/employees"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/employees/all-employees"))
                .andExpect(model().attribute("employees", employeeDataList));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF})
    void getEmployeeById_should_ReturnOneEmployeePage_when_LoggedInUserIsAdminOrEmployee() throws Exception {
        EmployeeDto employeeData = new EmployeeDto(1000L, USER_DATA, "Tasha", "Garcia");
        when(employeeService.getById(1000L)).thenReturn(employeeData);

        mockMvc.perform(get("/employees/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/employees/one-employee"))
                .andExpect(model().attribute("employee", employeeData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateEmployeeForm_should_ReturnUpdateEmployeeForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest("Garcia");
        EmployeeDto employeeData = new EmployeeDto(1000L, USER_DATA, "Tasha", "Garcia");
        List<UserDto> userDataList = List.of(new UserDto(1001L, "employee2", ENCODED_PASSWORD, true, true, Set.of(STAFF_ROLE_DATA)));
        when(employeeService.getById(1000L)).thenReturn(employeeData);
        when(userService.getAllFreeEmployeeUsers()).thenReturn(userDataList);

        mockMvc.perform(get("/employees/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/employees/update-employee"))
                .andExpect(model().attribute("updateEmployeeRequest", updateEmployeeRequest))
                .andExpect(model().attribute("employee", employeeData))
                .andExpect(model().attribute("users", userDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateEmployee_should_UpdateStateOfEmployee_RedirectToEmployeesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest("employee2", "Stout");

        mockMvc.perform(post("/employees/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateEmployeeRequest.getUsername())
                        .param("lastName", updateEmployeeRequest.getLastName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/employees"));
        verify(employeeService).updateUser(1000L, updateEmployeeRequest);
        verify(employeeService).updateLastName(1000L, updateEmployeeRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateEmployee_should_UpdateStateOfEmployee_RedirectToEmployeesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest("employee1", "Garcia");

        mockMvc.perform(post("/employees/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateEmployeeRequest.getUsername())
                        .param("lastName", updateEmployeeRequest.getLastName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/employees"));
        verify(employeeService).updateUser(1000L, updateEmployeeRequest);
        verify(employeeService).updateLastName(1000L, updateEmployeeRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateEmployee_should_ReturnUpdateEmployeeForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateEmployeeRequest updateEmployeeRequest = new UpdateEmployeeRequest();
        EmployeeDto employeeData = new EmployeeDto(1000L, USER_DATA, "Tasha", "Garcia");
        List<UserDto> userDataList = List.of(new UserDto(1001L, "employee2", ENCODED_PASSWORD, true, true, Set.of(STAFF_ROLE_DATA)));
        when(employeeService.getById(1000L)).thenReturn(employeeData);
        when(userService.getAllFreeEmployeeUsers()).thenReturn(userDataList);

        mockMvc.perform(post("/employees/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateEmployeeRequest.getUsername())
                        .param("lastName", updateEmployeeRequest.getLastName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/employees/update-employee"))
                .andExpect(model().attribute("updateEmployeeRequest", updateEmployeeRequest))
                .andExpect(model().attribute("employee", employeeData))
                .andExpect(model().attribute("users", userDataList));
        verify(employeeService, never()).updateUser(1000L, updateEmployeeRequest);
        verify(employeeService, never()).updateLastName(1000L, updateEmployeeRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteEmployee_should_DeleteEmployee_RedirectToEmployeesUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/employees/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/employees"));
        verify(employeeService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/employees".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/employees/create", "/employees/1000/update", "/employees/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/employees", "/employees/1000"})
    @WithMockUser(authorities = {ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdminOrEmployee_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/employees/create", "/employees/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
