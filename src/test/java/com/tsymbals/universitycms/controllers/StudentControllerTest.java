package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.CourseDto;
import com.tsymbals.universitycms.models.dto.GroupDto;
import com.tsymbals.universitycms.models.dto.StudentDto;
import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateStudentRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateStudentRequest;
import com.tsymbals.universitycms.models.entities.Course;
import com.tsymbals.universitycms.models.entities.Group;
import com.tsymbals.universitycms.models.entities.Role;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.CourseService;
import com.tsymbals.universitycms.services.GroupService;
import com.tsymbals.universitycms.services.StudentService;
import com.tsymbals.universitycms.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(StudentController.class)
@Import(SecurityConfig.class)
class StudentControllerTest {

    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final Role STUDENT_ROLE_DATA = new Role(1000L, "ROLE_STUDENT");
    static final UserEntity USER_DATA = new UserEntity(1000L, "student1", ENCODED_PASSWORD, true, false, Set.of(STUDENT_ROLE_DATA));

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    UserService userService;

    @MockBean
    StudentService studentService;

    @MockBean
    GroupService groupService;

    @MockBean
    CourseService courseService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateStudentForm_should_ReturnCreateStudentForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateStudentRequest createStudentRequest = new CreateStudentRequest();
        List<UserDto> userDataList = List.of(new UserDto(1000L, "student1", ENCODED_PASSWORD, true, true, Set.of(STUDENT_ROLE_DATA)));
        when(userService.getAllFreeStudentUsers()).thenReturn(userDataList);

        mockMvc.perform(get("/students/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/students/create-student"))
                .andExpect(model().attribute("createStudentRequest", createStudentRequest))
                .andExpect(model().attribute("users", userDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveStudent_should_SaveNewStudent_RedirectToStudentsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateStudentRequest createStudentRequest = new CreateStudentRequest("student1", "Emily", "Hensley");

        mockMvc.perform(post("/students/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", createStudentRequest.getUsername())
                        .param("firstName", createStudentRequest.getFirstName())
                        .param("lastName", createStudentRequest.getLastName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/students"));
        verify(studentService).save(createStudentRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveStudent_should_ReturnCreateStudentForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateStudentRequest createStudentRequest = new CreateStudentRequest();
        List<UserDto> userDataList = List.of(new UserDto(1000L, "student1", ENCODED_PASSWORD, true, true, Set.of(STUDENT_ROLE_DATA)));
        when(userService.getAllFreeStudentUsers()).thenReturn(userDataList);

        mockMvc.perform(post("/students/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", createStudentRequest.getUsername())
                        .param("firstName", createStudentRequest.getFirstName())
                        .param("lastName", createStudentRequest.getLastName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/students/create-student"))
                .andExpect(model().attribute("createStudentRequest", createStudentRequest))
                .andExpect(model().attribute("users", userDataList));
        verify(studentService, never()).save(createStudentRequest);
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getAllStudents_should_ReturnAllStudentsPage_when_LoggedInUserIsAdminOrTeacherOrStudent() throws Exception {
        List<StudentDto> studentDataList = List.of(new StudentDto(1000L, USER_DATA, null, "Emily", "Hensley", Set.of()));
        when(studentService.getAll()).thenReturn(studentDataList);

        mockMvc.perform(get("/students"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/students/all-students"))
                .andExpect(model().attribute("students", studentDataList));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getStudentById_should_ReturnOneStudentPage_when_LoggedInUserIsAdminOrTeacherOrStudent() throws Exception {
        StudentDto studentData = new StudentDto(1000L, USER_DATA, null, "Emily", "Hensley", Set.of());
        when(studentService.getById(1000L)).thenReturn(studentData);

        mockMvc.perform(get("/students/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/students/one-student"))
                .andExpect(model().attribute("student", studentData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateStudentForm_should_ReturnUpdateStudentForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest("Hensley");
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        StudentDto studentData = new StudentDto(1000L, USER_DATA, groupData, "Emily", "Hensley", new HashSet<>());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        studentData.getCourses().add(courseData);
        List<UserDto> userDataList = List.of(new UserDto(1001L, "student2", ENCODED_PASSWORD, true, true, Set.of(STUDENT_ROLE_DATA)));
        List<GroupDto> groupDataList = List.of(new GroupDto(1001L, "1CE-24", Set.of(), Set.of(), Set.of()));
        List<CourseDto> courseDataList = List.of(new CourseDto(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()));
        when(studentService.getById(1000L)).thenReturn(studentData);
        when(userService.getAllFreeStudentUsers()).thenReturn(userDataList);
        when(groupService.getAllNoneMatching(studentData.getGroup())).thenReturn(groupDataList);
        when(courseService.getAllNoneMatching(studentData.getCourses())).thenReturn(courseDataList);

        mockMvc.perform(get("/students/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/students/update-student"))
                .andExpect(model().attribute("updateStudentRequest", updateStudentRequest))
                .andExpect(model().attribute("student", studentData))
                .andExpect(model().attribute("users", userDataList))
                .andExpect(model().attribute("groupsToUpdate", groupDataList))
                .andExpect(model().attribute("groupToRemove", studentData.getGroup()))
                .andExpect(model().attribute("coursesToAdd", courseDataList))
                .andExpect(model().attribute("coursesToRemove", studentData.getCourses()));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateStudent_should_UpdateStateOfStudent_RedirectToStudentsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest("student2", "1CE-24", "1SE-24", "Hunter", "Mathematics", "English");

        mockMvc.perform(post("/students/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateStudentRequest.getUsername())
                        .param("groupNameToUpdate", updateStudentRequest.getGroupNameToUpdate())
                        .param("groupNameToRemove", updateStudentRequest.getGroupNameToRemove())
                        .param("lastName", updateStudentRequest.getLastName())
                        .param("courseNameToAdd", updateStudentRequest.getCourseNameToAdd())
                        .param("courseNameToRemove", updateStudentRequest.getCourseNameToRemove()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/students"));
        verify(studentService).updateUser(1000L, updateStudentRequest);
        verify(studentService).removeGroup(1000L, updateStudentRequest);
        verify(studentService).updateGroup(1000L, updateStudentRequest);
        verify(studentService).updateLastName(1000L, updateStudentRequest);
        verify(studentService).addCourse(1000L, updateStudentRequest);
        verify(studentService).removeCourse(1000L, updateStudentRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateStudent_should_UpdateStateOfStudent_RedirectToStudentsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest("student1", "", "", "Hensley", "", "");

        mockMvc.perform(post("/students/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateStudentRequest.getUsername())
                        .param("groupNameToUpdate", updateStudentRequest.getGroupNameToUpdate())
                        .param("groupNameToRemove", updateStudentRequest.getGroupNameToRemove())
                        .param("lastName", updateStudentRequest.getLastName())
                        .param("courseNameToAdd", updateStudentRequest.getCourseNameToAdd())
                        .param("courseNameToRemove", updateStudentRequest.getCourseNameToRemove()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/students"));
        verify(studentService).updateUser(1000L, updateStudentRequest);
        verify(studentService, never()).removeGroup(1000L, updateStudentRequest);
        verify(studentService, never()).updateGroup(1000L, updateStudentRequest);
        verify(studentService).updateLastName(1000L, updateStudentRequest);
        verify(studentService, never()).addCourse(1000L, updateStudentRequest);
        verify(studentService, never()).removeCourse(1000L, updateStudentRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateStudent_should_ReturnUpdateStudentForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateStudentRequest updateStudentRequest = new UpdateStudentRequest();
        Group groupData = new Group(1000L, "1SE-24", Set.of(), Set.of(), Set.of());
        StudentDto studentData = new StudentDto(1000L, USER_DATA, groupData, "Emily", "Hensley", new HashSet<>());
        Course courseData = new Course(1000L, "English", "English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        studentData.getCourses().add(courseData);
        List<UserDto> userDataList = List.of(new UserDto(1001L, "student2", ENCODED_PASSWORD, true, true, Set.of(STUDENT_ROLE_DATA)));
        List<GroupDto> groupDataList = List.of(new GroupDto(1001L, "1CE-24", Set.of(), Set.of(), Set.of()));
        List<CourseDto> courseDataList = List.of(new CourseDto(1001L, "Mathematics", "Mathematics is the science and study of quality, structure, space, and change", Set.of(), Set.of()));
        when(studentService.getById(1000L)).thenReturn(studentData);
        when(userService.getAllFreeStudentUsers()).thenReturn(userDataList);
        when(groupService.getAllNoneMatching(studentData.getGroup())).thenReturn(groupDataList);
        when(courseService.getAllNoneMatching(studentData.getCourses())).thenReturn(courseDataList);

        mockMvc.perform(post("/students/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateStudentRequest.getUsername())
                        .param("groupNameToUpdate", updateStudentRequest.getGroupNameToUpdate())
                        .param("groupNameToRemove", updateStudentRequest.getGroupNameToRemove())
                        .param("lastName", updateStudentRequest.getLastName())
                        .param("courseNameToAdd", updateStudentRequest.getCourseNameToAdd())
                        .param("courseNameToRemove", updateStudentRequest.getCourseNameToRemove()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/students/update-student"))
                .andExpect(model().attribute("updateStudentRequest", updateStudentRequest))
                .andExpect(model().attribute("student", studentData))
                .andExpect(model().attribute("users", userDataList))
                .andExpect(model().attribute("groupsToUpdate", groupDataList))
                .andExpect(model().attribute("groupToRemove", studentData.getGroup()))
                .andExpect(model().attribute("coursesToAdd", courseDataList))
                .andExpect(model().attribute("coursesToRemove", studentData.getCourses()));
        verify(studentService, never()).updateUser(1000L, updateStudentRequest);
        verify(studentService, never()).removeGroup(1000L, updateStudentRequest);
        verify(studentService, never()).updateGroup(1000L, updateStudentRequest);
        verify(studentService, never()).updateLastName(1000L, updateStudentRequest);
        verify(studentService, never()).addCourse(1000L, updateStudentRequest);
        verify(studentService, never()).removeCourse(1000L, updateStudentRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteStudent_should_DeleteStudent_RedirectToStudentsUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/students/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/students"));
        verify(studentService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/students".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/students/create", "/students/1000/update", "/students/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/students", "/students/1000"})
    @WithMockUser(authorities = ROLE_STAFF)
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsEmployee_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/students/create", "/students/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
