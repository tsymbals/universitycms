package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.CourseDto;
import com.tsymbals.universitycms.models.dto.requests.CreateCourseRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateCourseRequest;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.CourseService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CourseController.class)
@Import(SecurityConfig.class)
class CourseControllerTest {

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    CourseService courseService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateCourseForm_should_ReturnCreateCourseForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateCourseRequest createCourseRequest = new CreateCourseRequest();

        mockMvc.perform(get("/courses/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/courses/create-course"))
                .andExpect(model().attribute("createCourseRequest", createCourseRequest));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveCourse_should_SaveNewCourse_RedirectToCoursesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateCourseRequest createCourseRequest = new CreateCourseRequest("English", "English is a West Germanic language in the Indo European language family whose speakers called Anglophones originated in early medieval England on the island of Great Britain");

        mockMvc.perform(post("/courses/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", createCourseRequest.getName())
                        .param("description", createCourseRequest.getDescription()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/courses"));
        verify(courseService).save(createCourseRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveCourse_should_ReturnCreateCourseForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateCourseRequest createCourseRequest = new CreateCourseRequest();

        mockMvc.perform(post("/courses/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", createCourseRequest.getName())
                        .param("description", createCourseRequest.getDescription()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/courses/create-course"))
                .andExpect(model().attribute("createCourseRequest", createCourseRequest));
        verify(courseService, never()).save(createCourseRequest);
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getAllCourses_should_ReturnAllCoursesPage_when_LoggedInUserIsAdminOrTeacherOrStudent() throws Exception {
        List<CourseDto> courseDataList = List.of(new CourseDto(1000L, "English", "English is a West Germanic language in the Indo European language family whose speakers called Anglophones originated in early medieval England on the island of Great Britain", Set.of(), Set.of()));
        when(courseService.getAll()).thenReturn(courseDataList);

        mockMvc.perform(get("/courses"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/courses/all-courses"))
                .andExpect(model().attribute("courses", courseDataList));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getCourseById_should_ReturnOneCoursePage_when_LoggedInUserIsAdminOrTeacherOrStudent() throws Exception {
        CourseDto courseData = new CourseDto(1000L, "English", "English is a West Germanic language in the Indo European language family whose speakers called Anglophones originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        when(courseService.getById(1000L)).thenReturn(courseData);

        mockMvc.perform(get("/courses/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/courses/one-course"))
                .andExpect(model().attribute("course", courseData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateCourseForm_should_ReturnUpdateCourseForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateCourseRequest updateCourseRequest = new UpdateCourseRequest("English", "English is a West Germanic language in the Indo European language family whose speakers called Anglophones originated in early medieval England on the island of Great Britain");
        CourseDto courseData = new CourseDto(1000L, "English", "English is a West Germanic language in the Indo European language family whose speakers called Anglophones originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        when(courseService.getById(1000L)).thenReturn(courseData);

        mockMvc.perform(get("/courses/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/courses/update-course"))
                .andExpect(model().attribute("updateCourseRequest", updateCourseRequest))
                .andExpect(model().attribute("course", courseData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateCourse_should_UpdateStateOfCourse_RedirectToCoursesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateCourseRequest updateCourseRequest = new UpdateCourseRequest("Speak English", "Speaking practice to help you learn useful language for everyday communication");

        mockMvc.perform(post("/courses/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateCourseRequest.getName())
                        .param("description", updateCourseRequest.getDescription()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/courses"));
        verify(courseService).updateName(1000L, updateCourseRequest);
        verify(courseService).updateDescription(1000L, updateCourseRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateCourse_should_UpdateStateOfCourse_RedirectToCoursesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateCourseRequest updateCourseRequest = new UpdateCourseRequest("English", "English is a West Germanic language in the Indo European language family whose speakers called Anglophones originated in early medieval England on the island of Great Britain");

        mockMvc.perform(post("/courses/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateCourseRequest.getName())
                        .param("description", updateCourseRequest.getDescription()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/courses"));
        verify(courseService).updateName(1000L, updateCourseRequest);
        verify(courseService).updateDescription(1000L, updateCourseRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateCourse_should_ReturnUpdateCourseForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateCourseRequest updateCourseRequest = new UpdateCourseRequest();
        CourseDto courseData = new CourseDto(1000L, "English", "English is a West Germanic language in the Indo European language family whose speakers called Anglophones originated in early medieval England on the island of Great Britain", Set.of(), Set.of());
        when(courseService.getById(1000L)).thenReturn(courseData);

        mockMvc.perform(post("/courses/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateCourseRequest.getName())
                        .param("description", updateCourseRequest.getDescription()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/courses/update-course"))
                .andExpect(model().attribute("updateCourseRequest", updateCourseRequest))
                .andExpect(model().attribute("course", courseData));
        verify(courseService, never()).updateName(1000L, updateCourseRequest);
        verify(courseService, never()).updateDescription(1000L, updateCourseRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteCourse_should_DeleteCourse_RedirectToCoursesUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/courses/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/courses"));
        verify(courseService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/courses".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/courses/create", "/courses/1000/update", "/courses/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/courses", "/courses/1000"})
    @WithMockUser(authorities = ROLE_STAFF)
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsEmployee_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/courses/create", "/courses/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
