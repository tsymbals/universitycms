package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.UniversityDto;
import com.tsymbals.universitycms.models.dto.requests.CreateUniversityRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateUniversityRequest;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.UniversityService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UniversityController.class)
@Import(SecurityConfig.class)
class UniversityControllerTest {

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    UniversityService universityService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateUniversityForm_should_ReturnCreateUniversityForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateUniversityRequest createUniversityRequest = new CreateUniversityRequest();

        mockMvc.perform(get("/universities/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/universities/create-university"))
                .andExpect(model().attribute("createUniversityRequest", createUniversityRequest));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveUniversity_should_SaveNewUniversity_RedirectToUniversitiesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateUniversityRequest createUniversityRequest = new CreateUniversityRequest("Technical National University", "TNU", "Vice City", "+380(12)3456789");

        mockMvc.perform(post("/universities/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", createUniversityRequest.getName())
                        .param("abbreviationName", createUniversityRequest.getAbbreviationName())
                        .param("city", createUniversityRequest.getCity())
                        .param("phoneNumber", createUniversityRequest.getPhoneNumber()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/universities"));
        verify(universityService).save(createUniversityRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveUniversity_should_ReturnCreateUniversityForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateUniversityRequest createUniversityRequest = new CreateUniversityRequest();

        mockMvc.perform(post("/universities/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", createUniversityRequest.getName())
                        .param("abbreviationName", createUniversityRequest.getAbbreviationName())
                        .param("city", createUniversityRequest.getCity())
                        .param("phoneNumber", createUniversityRequest.getPhoneNumber()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/universities/create-university"))
                .andExpect(model().attribute("createUniversityRequest", createUniversityRequest));
        verify(universityService, never()).save(createUniversityRequest);
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void getAllUniversities_should_ReturnAllUniversitiesPage_when_LoggedInUserIsAdminOrEmployeeOrTeacherOrStudent() throws Exception {
        List<UniversityDto> universityDataList = List.of(new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of()));
        when(universityService.getAll()).thenReturn(universityDataList);

        mockMvc.perform(get("/universities"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/universities/all-universities"))
                .andExpect(model().attribute("universities", universityDataList));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void getUniversityById_should_ReturnOneUniversityPage_when_LoggedInUserIsAdminOrEmployeeOrTeacherOrStudent() throws Exception {
        UniversityDto universityData = new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        when(universityService.getById(1000L)).thenReturn(universityData);

        mockMvc.perform(get("/universities/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/universities/one-university"))
                .andExpect(model().attribute("university", universityData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateUniversityForm_should_ReturnUpdateUniversityForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest("Technical National University", "TNU", "Vice City", "+380(12)3456789");
        UniversityDto universityData = new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        when(universityService.getById(1000L)).thenReturn(universityData);

        mockMvc.perform(get("/universities/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/universities/update-university"))
                .andExpect(model().attribute("updateUniversityRequest", updateUniversityRequest))
                .andExpect(model().attribute("university", universityData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateUniversity_should_UpdateStateOfUniversity_RedirectToUniversitiesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest("National Technical University", "NTU", "Liberty City", "+380(98)7654321");

        mockMvc.perform(post("/universities/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateUniversityRequest.getName())
                        .param("abbreviationName", updateUniversityRequest.getAbbreviationName())
                        .param("city", updateUniversityRequest.getCity())
                        .param("phoneNumber", updateUniversityRequest.getPhoneNumber()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/universities"));
        verify(universityService).updateName(1000L, updateUniversityRequest);
        verify(universityService).updateAbbreviationName(1000L, updateUniversityRequest);
        verify(universityService).updateCity(1000L, updateUniversityRequest);
        verify(universityService).updatePhoneNumber(1000L, updateUniversityRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateUniversity_should_UpdateStateOfUniversity_RedirectToUniversitiesUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest("Technical National University", "TNU", "Vice City", "+380(12)3456789");

        mockMvc.perform(post("/universities/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateUniversityRequest.getName())
                        .param("abbreviationName", updateUniversityRequest.getAbbreviationName())
                        .param("city", updateUniversityRequest.getCity())
                        .param("phoneNumber", updateUniversityRequest.getPhoneNumber()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/universities"));
        verify(universityService).updateName(1000L, updateUniversityRequest);
        verify(universityService).updateAbbreviationName(1000L, updateUniversityRequest);
        verify(universityService).updateCity(1000L, updateUniversityRequest);
        verify(universityService).updatePhoneNumber(1000L, updateUniversityRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateUniversity_should_ReturnUpdateUniversityForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateUniversityRequest updateUniversityRequest = new UpdateUniversityRequest();
        UniversityDto universityData = new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
        when(universityService.getById(1000L)).thenReturn(universityData);

        mockMvc.perform(post("/universities/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("name", updateUniversityRequest.getName())
                        .param("abbreviationName", updateUniversityRequest.getAbbreviationName())
                        .param("city", updateUniversityRequest.getCity())
                        .param("phoneNumber", updateUniversityRequest.getPhoneNumber()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/universities/update-university"))
                .andExpect(model().attribute("updateUniversityRequest", updateUniversityRequest))
                .andExpect(model().attribute("university", universityData));
        verify(universityService, never()).updateName(1000L, updateUniversityRequest);
        verify(universityService, never()).updateAbbreviationName(1000L, updateUniversityRequest);
        verify(universityService, never()).updateCity(1000L, updateUniversityRequest);
        verify(universityService, never()).updatePhoneNumber(1000L, updateUniversityRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteUniversity_should_DeleteUniversity_RedirectToUniversitiesUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/universities/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/universities"));
        verify(universityService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/universities".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/universities/create", "/universities/1000/update", "/universities/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/universities/create", "/universities/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
