package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.DepartmentDto;
import com.tsymbals.universitycms.models.dto.UniversityDto;
import com.tsymbals.universitycms.models.dto.requests.CreateDepartmentRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateDepartmentRequest;
import com.tsymbals.universitycms.models.entities.University;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.DepartmentService;
import com.tsymbals.universitycms.services.UniversityService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(DepartmentController.class)
@Import(SecurityConfig.class)
class DepartmentControllerTest {

    static final University UNIVERSITY_DATA = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    UniversityService universityService;

    @MockBean
    DepartmentService departmentService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateDepartmentForm_should_ReturnCreateDepartmentForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateDepartmentRequest createDepartmentRequest = new CreateDepartmentRequest();
        List<UniversityDto> universityDataList = List.of(new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of()));
        when(universityService.getAll()).thenReturn(universityDataList);

        mockMvc.perform(get("/departments/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/departments/create-department"))
                .andExpect(model().attribute("createDepartmentRequest", createDepartmentRequest))
                .andExpect(model().attribute("universities", universityDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveDepartment_should_SaveNewDepartment_RedirectToDepartmentsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateDepartmentRequest createDepartmentRequest = new CreateDepartmentRequest("TNU", "Technical");

        mockMvc.perform(post("/departments/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("universityAbbreviationName", createDepartmentRequest.getUniversityAbbreviationName())
                        .param("name", createDepartmentRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/departments"));
        verify(departmentService).save(createDepartmentRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveDepartment_should_ReturnCreateDepartmentForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateDepartmentRequest createDepartmentRequest = new CreateDepartmentRequest();
        List<UniversityDto> universityDataList = List.of(new UniversityDto(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of()));
        when(universityService.getAll()).thenReturn(universityDataList);

        mockMvc.perform(post("/departments/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("universityAbbreviationName", createDepartmentRequest.getUniversityAbbreviationName())
                        .param("name", createDepartmentRequest.getName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/departments/create-department"))
                .andExpect(model().attribute("createDepartmentRequest", createDepartmentRequest))
                .andExpect(model().attribute("universities", universityDataList));
        verify(departmentService, never()).save(createDepartmentRequest);
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void getAllDepartments_should_ReturnAllDepartmentsPage_when_LoggedInUserIsAdminOrEmployeeOrTeacherOrStudent() throws Exception {
        List<DepartmentDto> departmentDataList = List.of(new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of()));
        when(departmentService.getAll()).thenReturn(departmentDataList);

        mockMvc.perform(get("/departments"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/departments/all-departments"))
                .andExpect(model().attribute("departments", departmentDataList));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void getDepartmentById_should_ReturnOneDepartmentPage_when_LoggedInUserIsAdminOrEmployeeOrTeacherOrStudent() throws Exception {
        DepartmentDto departmentData = new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        when(departmentService.getById(1000L)).thenReturn(departmentData);

        mockMvc.perform(get("/departments/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/departments/one-department"))
                .andExpect(model().attribute("department", departmentData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateDepartmentForm_should_ReturnUpdateDepartmentForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateDepartmentRequest updateDepartmentRequest = new UpdateDepartmentRequest("Technical");
        DepartmentDto departmentData = new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        List<UniversityDto> universityDataList = List.of(new UniversityDto(1001L, "Humanitarian National University", "HNU", "San Andreas", "+380(98)7654321", Set.of()));
        when(departmentService.getById(1000L)).thenReturn(departmentData);
        when(universityService.getAllNoneMatching(departmentData.getUniversity())).thenReturn(universityDataList);

        mockMvc.perform(get("/departments/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/departments/update-department"))
                .andExpect(model().attribute("updateDepartmentRequest", updateDepartmentRequest))
                .andExpect(model().attribute("department", departmentData))
                .andExpect(model().attribute("universities", universityDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateDepartment_should_UpdateStateOfDepartment_RedirectToDepartmentsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateDepartmentRequest updateDepartmentRequest = new UpdateDepartmentRequest("HNU", "Humanitarian");

        mockMvc.perform(post("/departments/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("universityAbbreviationName", updateDepartmentRequest.getUniversityAbbreviationName())
                        .param("name", updateDepartmentRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/departments"));
        verify(departmentService).updateUniversity(1000L, updateDepartmentRequest);
        verify(departmentService).updateName(1000L, updateDepartmentRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateDepartment_should_UpdateStateOfDepartment_RedirectToDepartmentsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateDepartmentRequest updateDepartmentRequest = new UpdateDepartmentRequest("TNU", "Technical");

        mockMvc.perform(post("/departments/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("universityAbbreviationName", updateDepartmentRequest.getUniversityAbbreviationName())
                        .param("name", updateDepartmentRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/departments"));
        verify(departmentService).updateUniversity(1000L, updateDepartmentRequest);
        verify(departmentService).updateName(1000L, updateDepartmentRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateDepartment_should_ReturnUpdateDepartmentForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateDepartmentRequest updateDepartmentRequest = new UpdateDepartmentRequest();
        DepartmentDto departmentData = new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of());
        List<UniversityDto> universityDataList = List.of(new UniversityDto(1001L, "Humanitarian National University", "HNU", "San Andreas", "+380(98)7654321", Set.of()));
        when(departmentService.getById(1000L)).thenReturn(departmentData);
        when(universityService.getAllNoneMatching(departmentData.getUniversity())).thenReturn(universityDataList);

        mockMvc.perform(post("/departments/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("universityAbbreviationName", updateDepartmentRequest.getUniversityAbbreviationName())
                        .param("name", updateDepartmentRequest.getName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/departments/update-department"))
                .andExpect(model().attribute("updateDepartmentRequest", updateDepartmentRequest))
                .andExpect(model().attribute("department", departmentData))
                .andExpect(model().attribute("universities", universityDataList));
        verify(departmentService, never()).updateUniversity(1000L, updateDepartmentRequest);
        verify(departmentService, never()).updateName(1000L, updateDepartmentRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteDepartment_should_DeleteDepartment_RedirectToDepartmentsUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/departments/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/departments"));
        verify(departmentService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/departments".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/departments/create", "/departments/1000/update", "/departments/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/departments/create", "/departments/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
