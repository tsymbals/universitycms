package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.AdminDto;
import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateAdminRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateAdminRequest;
import com.tsymbals.universitycms.models.entities.Role;
import com.tsymbals.universitycms.models.entities.UserEntity;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.AdminService;
import com.tsymbals.universitycms.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AdminController.class)
@Import(SecurityConfig.class)
class AdminControllerTest {

    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final Role ADMIN_ROLE_DATA = new Role(1000L, "ROLE_ADMIN");
    static final UserEntity USER_DATA = new UserEntity(1000L, "admin1", ENCODED_PASSWORD, true, false, Set.of(ADMIN_ROLE_DATA));

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    UserService userService;

    @MockBean
    AdminService adminService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateAdminForm_should_ReturnCreateAdminForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateAdminRequest createAdminRequest = new CreateAdminRequest();
        List<UserDto> userDataList = List.of(new UserDto(1000L, "admin1", ENCODED_PASSWORD, true, true, Set.of(ADMIN_ROLE_DATA)));
        when(userService.getAllFreeAdminUsers()).thenReturn(userDataList);

        mockMvc.perform(get("/admins/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/admins/create-admin"))
                .andExpect(model().attribute("createAdminRequest", createAdminRequest))
                .andExpect(model().attribute("users", userDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveAdmin_should_SaveNewAdmin_RedirectToAdminsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateAdminRequest createAdminRequest = new CreateAdminRequest("admin1");

        mockMvc.perform(post("/admins/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", createAdminRequest.getUsername()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/admins"));
        verify(adminService).save(createAdminRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveAdmin_should_ReturnCreateAdminForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateAdminRequest createAdminRequest = new CreateAdminRequest();
        List<UserDto> userDataList = List.of(new UserDto(1000L, "admin1", ENCODED_PASSWORD, true, true, Set.of(ADMIN_ROLE_DATA)));
        when(userService.getAllFreeAdminUsers()).thenReturn(userDataList);

        mockMvc.perform(post("/admins/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", createAdminRequest.getUsername()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/admins/create-admin"))
                .andExpect(model().attribute("createAdminRequest", createAdminRequest))
                .andExpect(model().attribute("users", userDataList));
        verify(adminService, never()).save(createAdminRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getAllAdmins_should_ReturnAllAdminsPage_when_LoggedInUserIsAdmin() throws Exception {
        List<AdminDto> adminDataList = List.of(new AdminDto(1000L, USER_DATA));
        when(adminService.getAll()).thenReturn(adminDataList);

        mockMvc.perform(get("/admins"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/admins/all-admins"))
                .andExpect(model().attribute("admins", adminDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getAdminById_should_ReturnOneAdminPage_when_LoggedInUserIsAdmin() throws Exception {
        AdminDto adminData = new AdminDto(1000L, USER_DATA);
        when(adminService.getById(1000L)).thenReturn(adminData);

        mockMvc.perform(get("/admins/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/admins/one-admin"))
                .andExpect(model().attribute("admin", adminData));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateAdminForm_should_ReturnUpdateAdminForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateAdminRequest updateAdminRequest = new UpdateAdminRequest();
        AdminDto adminData = new AdminDto(1000L, USER_DATA);
        List<UserDto> userDataList = List.of(new UserDto(1001L, "admin2", ENCODED_PASSWORD, true, true, Set.of(ADMIN_ROLE_DATA)));
        when(adminService.getById(1000L)).thenReturn(adminData);
        when(userService.getAllFreeAdminUsers()).thenReturn(userDataList);

        mockMvc.perform(get("/admins/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/admins/update-admin"))
                .andExpect(model().attribute("updateAdminRequest", updateAdminRequest))
                .andExpect(model().attribute("admin", adminData))
                .andExpect(model().attribute("users", userDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateAdmin_should_UpdateStateOfAdmin_RedirectToAdminsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateAdminRequest updateAdminRequest = new UpdateAdminRequest("admin2");

        mockMvc.perform(post("/admins/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateAdminRequest.getUsername()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/admins"));
        verify(adminService).updateUser(1000L, updateAdminRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateAdmin_should_UpdateStateOfAdmin_RedirectToAdminsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateAdminRequest updateAdminRequest = new UpdateAdminRequest("admin1");

        mockMvc.perform(post("/admins/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateAdminRequest.getUsername()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/admins"));
        verify(adminService).updateUser(1000L, updateAdminRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteAdmin_should_DeleteAdmin_RedirectToAdminsUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/admins/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/admins"));
        verify(adminService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/admins".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/admins/create", "/admins", "/admins/1000", "/admins/1000/update", "/admins/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/admins/create", "/admins/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
