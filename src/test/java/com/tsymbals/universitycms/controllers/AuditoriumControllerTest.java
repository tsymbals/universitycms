package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.AuditoriumDto;
import com.tsymbals.universitycms.models.dto.DepartmentDto;
import com.tsymbals.universitycms.models.dto.LessonDto;
import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateAuditoriumRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateAuditoriumRequest;
import com.tsymbals.universitycms.models.entities.*;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.AuditoriumService;
import com.tsymbals.universitycms.services.DepartmentService;
import com.tsymbals.universitycms.services.LessonService;
import com.tsymbals.universitycms.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AuditoriumController.class)
@Import(SecurityConfig.class)
class AuditoriumControllerTest {

    static final University UNIVERSITY_DATA = new University(1000L, "Technical National University", "TNU", "Vice City", "+380(12)3456789", Set.of());
    static final Department DEPARTMENT_DATA = new Department(1000L, UNIVERSITY_DATA, "Technical", Set.of());
    static final LessonTime LESSON_TIME_DATA = new LessonTime(1000L, LocalTime.of(0, 0), LocalTime.of(1, 0), Set.of());
    static final Auditorium AUDITORIUM_DATA = new Auditorium(1000L, DEPARTMENT_DATA, "101", Set.of());
    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";
    static final Role STAFF_ROLE_DATA = new Role(1000L, "ROLE_STAFF");
    static final Lesson LESSON_DATA = new Lesson(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of());

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    DepartmentService departmentService;

    @MockBean
    AuditoriumService auditoriumService;

    @MockBean
    UserService userService;

    @MockBean
    LessonService lessonService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateAuditoriumForm_should_ReturnCreateAuditoriumForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateAuditoriumRequest createAuditoriumRequest = new CreateAuditoriumRequest();
        List<DepartmentDto> departmentDataList = List.of(new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of()));
        when(departmentService.getAll()).thenReturn(departmentDataList);

        mockMvc.perform(get("/auditoriums/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/auditoriums/create-auditorium"))
                .andExpect(model().attribute("createAuditoriumRequest", createAuditoriumRequest))
                .andExpect(model().attribute("departments", departmentDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveAuditorium_should_SaveNewAuditorium_RedirectToAuditoriumsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateAuditoriumRequest createAuditoriumRequest = new CreateAuditoriumRequest("Technical", "101");

        mockMvc.perform(post("/auditoriums/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("departmentName", createAuditoriumRequest.getDepartmentName())
                        .param("name", createAuditoriumRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/auditoriums"));
        verify(auditoriumService).save(createAuditoriumRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveAuditorium_should_ReturnCreateAuditoriumForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateAuditoriumRequest createAuditoriumRequest = new CreateAuditoriumRequest();
        List<DepartmentDto> departmentDataList = List.of(new DepartmentDto(1000L, UNIVERSITY_DATA, "Technical", Set.of()));
        when(departmentService.getAll()).thenReturn(departmentDataList);

        mockMvc.perform(post("/auditoriums/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("departmentName", createAuditoriumRequest.getDepartmentName())
                        .param("name", createAuditoriumRequest.getName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/auditoriums/create-auditorium"))
                .andExpect(model().attribute("createAuditoriumRequest", createAuditoriumRequest))
                .andExpect(model().attribute("departments", departmentDataList));
        verify(auditoriumService, never()).save(createAuditoriumRequest);
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void getAllAuditoriums_should_ReturnAllAuditoriumsPage_when_LoggedInUserIsAdminOrEmployeeOrTeacherOrStudent() throws Exception {
        List<AuditoriumDto> auditoriumDataList = List.of(new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of()));
        when(auditoriumService.getAll()).thenReturn(auditoriumDataList);

        mockMvc.perform(get("/auditoriums"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/auditoriums/all-auditoriums"))
                .andExpect(model().attribute("auditoriums", auditoriumDataList));
    }

    @Test
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void getAuditoriumById_should_ReturnOneAuditoriumPage_when_LoggedInUserIsAdminOrEmployeeOrTeacherOrStudent() throws Exception {
        AuditoriumDto auditoriumData = new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of());
        when(auditoriumService.getById(1000L)).thenReturn(auditoriumData);

        mockMvc.perform(get("/auditoriums/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/auditoriums/one-auditorium"))
                .andExpect(model().attribute("auditorium", auditoriumData));
    }

    @Test
    @WithMockUser(username = "employee1", authorities = ROLE_STAFF)
    void getDateAuditoriumsSchedule_should_ReturnDateAuditoriumsSchedulePage_when_LoggedInUserIsEmployee_LessonsBySelectedDateExist() throws Exception {
        LocalDate scheduleDate = LocalDate.of(2024, 1, 1);
        UserDto userData = new UserDto(1000L, "employee1", ENCODED_PASSWORD, true, false, Set.of(STAFF_ROLE_DATA));
        List<LessonDto> lessonDataList = List.of(new LessonDto(1000L, LocalDate.of(2024, 1, 1), LESSON_TIME_DATA, AUDITORIUM_DATA, null, null, Set.of()));
        List<AuditoriumDto> auditoriumDataList = List.of(new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of(LESSON_DATA)));
        when(userService.getByUsername("employee1")).thenReturn(userData);
        when(lessonService.getAllByDate(scheduleDate)).thenReturn(lessonDataList);
        when(auditoriumService.getAllByDate(lessonDataList)).thenReturn(auditoriumDataList);

        mockMvc.perform(get("/auditoriums/{username}/date-schedule", "employee1")
                        .param("selectedDate", "2024-01-01"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/auditoriums/date-auditoriums-schedule"))
                .andExpect(model().attribute("selectedDate", scheduleDate))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("auditoriums", auditoriumDataList));
    }

    @Test
    @WithMockUser(username = "employee1", authorities = ROLE_STAFF)
    void getDateAuditoriumsSchedule_should_ReturnDateAuditoriumsSchedulePage_when_LoggedInUserIsEmployee_LessonsBySelectedDateDoNotExist() throws Exception {
        LocalDate scheduleDate = LocalDate.now();
        UserDto userData = new UserDto(1000L, "employee1", ENCODED_PASSWORD, true, false, Set.of(STAFF_ROLE_DATA));
        List<LessonDto> lessonDataList = List.of();
        List<AuditoriumDto> auditoriumDataList = List.of(new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of()));
        when(userService.getByUsername("employee1")).thenReturn(userData);
        when(lessonService.getAllByDate(scheduleDate)).thenReturn(lessonDataList);
        when(auditoriumService.getAllByDate(lessonDataList)).thenReturn(auditoriumDataList);

        mockMvc.perform(get("/auditoriums/{username}/date-schedule", "employee1"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/auditoriums/date-auditoriums-schedule"))
                .andExpect(model().attribute("selectedDate", scheduleDate))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("auditoriums", auditoriumDataList));
    }

    @Test
    @WithMockUser(username = "employee1", authorities = ROLE_STAFF)
    void getDateAuditoriumsSchedule_should_RedirectToHomeUrl_when_UsernameDoesNotMatchAuthenticatedUser() throws Exception {
        UserDto userData = new UserDto(1001L, "employee2", ENCODED_PASSWORD, true, false, Set.of(STAFF_ROLE_DATA));
        when(userService.getByUsername("employee1")).thenReturn(userData);

        mockMvc.perform(get("/auditoriums/{username}/date-schedule", "employee1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    @WithMockUser(username = "username", authorities = {ROLE_ADMIN, ROLE_TEACHER, ROLE_STUDENT})
    void getDateAuditoriumsSchedule_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotEmployee_UrlIsCorrect() throws Exception {
        mockMvc.perform(get("/auditoriums/{username}/date-schedule", "username"))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateAuditoriumForm_should_ReturnUpdateAuditoriumForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateAuditoriumRequest updateAuditoriumRequest = new UpdateAuditoriumRequest("101");
        AuditoriumDto auditoriumData = new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of());
        List<DepartmentDto> departmentDataList = List.of(new DepartmentDto(1001L, UNIVERSITY_DATA, "Humanitarian", Set.of()));
        when(auditoriumService.getById(1000L)).thenReturn(auditoriumData);
        when(departmentService.getAllNoneMatching(auditoriumData.getDepartment())).thenReturn(departmentDataList);

        mockMvc.perform(get("/auditoriums/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/auditoriums/update-auditorium"))
                .andExpect(model().attribute("updateAuditoriumRequest", updateAuditoriumRequest))
                .andExpect(model().attribute("auditorium", auditoriumData))
                .andExpect(model().attribute("departments", departmentDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateAuditorium_should_UpdateStateOfAuditorium_RedirectToAuditoriumsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateAuditoriumRequest updateAuditoriumRequest = new UpdateAuditoriumRequest("Humanitarian", "102");

        mockMvc.perform(post("/auditoriums/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("departmentName", updateAuditoriumRequest.getDepartmentName())
                        .param("name", updateAuditoriumRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/auditoriums"));
        verify(auditoriumService).updateDepartment(1000L, updateAuditoriumRequest);
        verify(auditoriumService).updateName(1000L, updateAuditoriumRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateAuditorium_should_UpdateStateOfAuditorium_RedirectToAuditoriumsUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateAuditoriumRequest updateAuditoriumRequest = new UpdateAuditoriumRequest("Technical", "101");

        mockMvc.perform(post("/auditoriums/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("departmentName", updateAuditoriumRequest.getDepartmentName())
                        .param("name", updateAuditoriumRequest.getName()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/auditoriums"));
        verify(auditoriumService).updateDepartment(1000L, updateAuditoriumRequest);
        verify(auditoriumService).updateName(1000L, updateAuditoriumRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateAuditorium_should_ReturnUpdateAuditoriumForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateAuditoriumRequest updateAuditoriumRequest = new UpdateAuditoriumRequest();
        AuditoriumDto auditoriumData = new AuditoriumDto(1000L, DEPARTMENT_DATA, "101", Set.of());
        List<DepartmentDto> departmentDataList = List.of(new DepartmentDto(1001L, UNIVERSITY_DATA, "Humanitarian", Set.of()));
        when(auditoriumService.getById(1000L)).thenReturn(auditoriumData);
        when(departmentService.getAllNoneMatching(auditoriumData.getDepartment())).thenReturn(departmentDataList);

        mockMvc.perform(post("/auditoriums/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("departmentName", updateAuditoriumRequest.getDepartmentName())
                        .param("name", updateAuditoriumRequest.getName()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/auditoriums/update-auditorium"))
                .andExpect(model().attribute("updateAuditoriumRequest", updateAuditoriumRequest))
                .andExpect(model().attribute("auditorium", auditoriumData))
                .andExpect(model().attribute("departments", departmentDataList));
        verify(auditoriumService, never()).updateDepartment(1000L, updateAuditoriumRequest);
        verify(auditoriumService, never()).updateName(1000L, updateAuditoriumRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteAuditorium_should_DeleteAuditorium_RedirectToAuditoriumsUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/auditoriums/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/auditoriums"));
        verify(auditoriumService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/auditoriums".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/auditoriums/create", "/auditoriums/1000/update", "/auditoriums/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/auditoriums/create", "/auditoriums/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
