package com.tsymbals.universitycms.controllers;

import com.tsymbals.universitycms.configs.SecurityConfig;
import com.tsymbals.universitycms.models.dto.RoleDto;
import com.tsymbals.universitycms.models.dto.UserDto;
import com.tsymbals.universitycms.models.dto.requests.CreateUserRequest;
import com.tsymbals.universitycms.models.dto.requests.UpdateUserRequest;
import com.tsymbals.universitycms.models.entities.Role;
import com.tsymbals.universitycms.security.UserDetailsServiceImpl;
import com.tsymbals.universitycms.services.RoleService;
import com.tsymbals.universitycms.services.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
@Import(SecurityConfig.class)
class UserControllerTest {

    static final String ENCODED_PASSWORD = "$2a$10$BbrTGgJHihGawY2ATkvbYup91Ef4Q1uJWX.MdSTnGDrGNpj3x.l1m";

    static final String ROLE_ADMIN = "ROLE_ADMIN";
    static final String ROLE_STAFF = "ROLE_STAFF";
    static final String ROLE_TEACHER = "ROLE_TEACHER";
    static final String ROLE_STUDENT = "ROLE_STUDENT";

    @MockBean
    UserService userService;

    @MockBean
    RoleService roleService;

    @MockBean
    UserDetailsServiceImpl userDetailsService;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getCreateUserForm_should_ReturnCreateUserForm_when_LoggedInUserIsAdmin() throws Exception {
        CreateUserRequest createUserRequest = new CreateUserRequest();

        mockMvc.perform(get("/users/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/users/create-user"))
                .andExpect(model().attribute("createUserRequest", createUserRequest));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveUser_should_SaveNewUser_RedirectToUsersUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors() throws Exception {
        CreateUserRequest createUserRequest = new CreateUserRequest("username1", "Password1");

        mockMvc.perform(post("/users/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", createUserRequest.getUsername())
                        .param("password", createUserRequest.getPassword()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/users"));
        verify(userService).save(createUserRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void saveUser_should_ReturnCreateUserForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        CreateUserRequest createUserRequest = new CreateUserRequest();

        mockMvc.perform(post("/users/create")
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", createUserRequest.getUsername())
                        .param("password", createUserRequest.getPassword()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/users/create-user"))
                .andExpect(model().attribute("createUserRequest", createUserRequest));
        verify(userService, never()).save(createUserRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getAllUsers_should_ReturnAllUsersPage_when_LoggedInUserIsAdmin() throws Exception {
        List<UserDto> userDataList = List.of(new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of()));
        when(userService.getAll()).thenReturn(userDataList);

        mockMvc.perform(get("/users"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/users/all-users"))
                .andExpect(model().attribute("users", userDataList));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUserById_should_ReturnOneUserPage_when_LoggedInUserIsAdmin() throws Exception {
        UserDto userData = new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of());
        when(userService.getById(1000L)).thenReturn(userData);

        mockMvc.perform(get("/users/{id}", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/users/one-user"))
                .andExpect(model().attribute("user", userData));
    }

    @Test
    @WithMockUser(username = "username1", authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void getLoggedInUserProfile_should_ReturnOneUserPage_when_UsernameMatchesAuthenticatedUser() throws Exception {
        UserDto userData = new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, Set.of());
        when(userService.getByUsername("username1")).thenReturn(userData);

        mockMvc.perform(get("/users/{username}/profile", "username1"))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/users/one-user"))
                .andExpect(model().attribute("user", userData));
    }

    @Test
    @WithMockUser(username = "username1", authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void getLoggedInUserProfile_should_RedirectToHomeUrl_when_UsernameDoesNotMatchAuthenticatedUser() throws Exception {
        UserDto userData = new UserDto(1000L, "user", ENCODED_PASSWORD, true, true, Set.of());
        when(userService.getByUsername("username1")).thenReturn(userData);

        mockMvc.perform(get("/users/{username}/profile", "username1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/"));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void getUpdateUserForm_should_ReturnUpdateUserForm_when_LoggedInUserIsAdmin() throws Exception {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest("username1");
        UserDto userData = new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, new HashSet<>());
        Role roleData = new Role(1000L, "ROLE_ADMIN");
        userData.getRoles().add(roleData);
        List<RoleDto> roleDataList = List.of(new RoleDto(1001L, "ROLE_STAFF"));
        when(userService.getById(1000L)).thenReturn(userData);
        when(roleService.getAllNoneMatching(userData.getRoles())).thenReturn(roleDataList);

        mockMvc.perform(get("/users/{id}/update", 1000L))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/users/update-user"))
                .andExpect(model().attribute("updateUserRequest", updateUserRequest))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("rolesToAdd", roleDataList))
                .andExpect(model().attribute("rolesToRemove", userData.getRoles()));
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateUser_should_UpdateStateOfUser_RedirectToUsersUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreEnteredAndSelected() throws Exception {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest("username2", "Password2", false, "ROLE_STAFF", "ROLE_ADMIN");

        mockMvc.perform(post("/users/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateUserRequest.getUsername())
                        .param("password", updateUserRequest.getPassword())
                        .param("enabledStatus", String.valueOf(updateUserRequest.getEnabledStatus()))
                        .param("roleNameToAdd", updateUserRequest.getRoleNameToAdd())
                        .param("roleNameToRemove", updateUserRequest.getRoleNameToRemove()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/users"));
        verify(userService).updateUsername(1000L, updateUserRequest);
        verify(userService).updatePassword(1000L, updateUserRequest);
        verify(userService).updateEnabledStatus(1000L, updateUserRequest);
        verify(userService).addRole(1000L, updateUserRequest);
        verify(userService).removeRole(1000L, updateUserRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateUser_should_UpdateStateOfUser_RedirectToUsersUrl_when_LoggedInUserIsAdmin_BindingResultDoesNotHaveAnyErrors_AllAttributesAreNotEnteredAndNotSelected() throws Exception {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest("username1", "Password1", true, "", "");

        mockMvc.perform(post("/users/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateUserRequest.getUsername())
                        .param("password", updateUserRequest.getPassword())
                        .param("enabledStatus", String.valueOf(updateUserRequest.getEnabledStatus()))
                        .param("roleNameToAdd", updateUserRequest.getRoleNameToAdd())
                        .param("roleNameToRemove", updateUserRequest.getRoleNameToRemove()))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/users"));
        verify(userService).updateUsername(1000L, updateUserRequest);
        verify(userService).updatePassword(1000L, updateUserRequest);
        verify(userService).updateEnabledStatus(1000L, updateUserRequest);
        verify(userService, never()).addRole(1000L, updateUserRequest);
        verify(userService, never()).removeRole(1000L, updateUserRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void updateUser_should_ReturnUpdateUserForm_when_LoggedInUserIsAdmin_BindingResultHasErrors() throws Exception {
        UpdateUserRequest updateUserRequest = new UpdateUserRequest();
        UserDto userData = new UserDto(1000L, "username1", ENCODED_PASSWORD, true, true, new HashSet<>());
        Role roleData = new Role(1000L, "ROLE_ADMIN");
        userData.getRoles().add(roleData);
        List<RoleDto> roleDataList = List.of(new RoleDto(1001L, "ROLE_STAFF"));
        when(userService.getById(1000L)).thenReturn(userData);
        when(roleService.getAllNoneMatching(userData.getRoles())).thenReturn(roleDataList);

        mockMvc.perform(post("/users/{id}/update", 1000L)
                        .with(csrf())
                        .contentType(APPLICATION_FORM_URLENCODED)
                        .param("username", updateUserRequest.getUsername())
                        .param("password", updateUserRequest.getPassword())
                        .param("enabledStatus", String.valueOf(updateUserRequest.getEnabledStatus()))
                        .param("roleNameToAdd", updateUserRequest.getRoleNameToAdd())
                        .param("roleNameToRemove", updateUserRequest.getRoleNameToRemove()))
                .andExpect(status().isOk())
                .andExpect(view().name("pages/users/update-user"))
                .andExpect(model().attribute("updateUserRequest", updateUserRequest))
                .andExpect(model().attribute("user", userData))
                .andExpect(model().attribute("rolesToAdd", roleDataList))
                .andExpect(model().attribute("rolesToRemove", userData.getRoles()));
        verify(userService, never()).updateUsername(1000L, updateUserRequest);
        verify(userService, never()).updatePassword(1000L, updateUserRequest);
        verify(userService, never()).updateEnabledStatus(1000L, updateUserRequest);
        verify(userService, never()).addRole(1000L, updateUserRequest);
        verify(userService, never()).removeRole(1000L, updateUserRequest);
    }

    @Test
    @WithMockUser(authorities = ROLE_ADMIN)
    void deleteUser_should_DeleteUser_RedirectToUsersUrl_when_LoggedInUserIsAdmin() throws Exception {
        mockMvc.perform(get("/users/{id}/delete", 1000L))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/users"));
        verify(userService).deleteById(1000L);
    }

    @ParameterizedTest
    @ValueSource(strings = {"/Create", "/CREATE", "/creat", "/1000/Update", "/1000/UPDATE", "/1000/updat", "/1000/Delete", "/1000/DELETE", "/1000/delet"})
    @WithMockUser(authorities = {ROLE_ADMIN, ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void should_Return4xxStatus_when_UserIsLoggedIn_UrlIsWrong(String wrongUrl) throws Exception {
        mockMvc.perform(get("/users".concat(wrongUrl)))
                .andExpect(status().is4xxClientError());
    }

    @ParameterizedTest
    @ValueSource(strings = {"/users/create", "/users", "/users/1000", "/users/1000/update", "/users/1000/delete"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void GET_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(get(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }

    @ParameterizedTest
    @ValueSource(strings = {"/users/create", "/users/1000/update"})
    @WithMockUser(authorities = {ROLE_STAFF, ROLE_TEACHER, ROLE_STUDENT})
    void POST_should_ForwardToAccessDeniedUrl_when_LoggedInUserIsNotAdmin_UrlIsCorrect(String correctUrl) throws Exception {
        mockMvc.perform(post(correctUrl))
                .andExpect(status().isForbidden())
                .andExpect(forwardedUrl("/access-denied"));
    }
}
