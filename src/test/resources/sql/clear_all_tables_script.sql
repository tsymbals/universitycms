DELETE FROM universities;
DELETE FROM departments;
DELETE FROM auditoriums;
DELETE FROM lesson_times;
DELETE FROM courses;
DELETE FROM groups;
DELETE FROM roles;
DELETE FROM users;
DELETE FROM admins;
DELETE FROM employees;
DELETE FROM teachers;
DELETE FROM students;
DELETE FROM lessons;

DELETE FROM roles_users;
DELETE FROM groups_lessons;

ALTER SEQUENCE universities_id_seq RESTART WITH 1;
ALTER SEQUENCE departments_id_seq RESTART WITH 1;
ALTER SEQUENCE auditoriums_id_seq RESTART WITH 1;
ALTER SEQUENCE lesson_times_id_seq RESTART WITH 1;
ALTER SEQUENCE courses_id_seq RESTART WITH 1;
ALTER SEQUENCE groups_id_seq RESTART WITH 1;
ALTER SEQUENCE roles_id_seq RESTART WITH 1;
ALTER SEQUENCE users_id_seq RESTART WITH 1;
ALTER SEQUENCE admins_id_seq RESTART WITH 1;
ALTER SEQUENCE employees_id_seq RESTART WITH 1;
ALTER SEQUENCE teachers_id_seq RESTART WITH 1;
ALTER SEQUENCE students_id_seq RESTART WITH 1;
ALTER SEQUENCE lessons_id_seq RESTART WITH 1;
