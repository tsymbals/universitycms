INSERT INTO universities (id, name, abbreviation_name, city, phone_number) VALUES (1000, 'Technical National University', 'TNU', 'Vice City', '+380(12)3456789');
INSERT INTO universities (id, name, abbreviation_name, city, phone_number) VALUES (1001, 'Humanitarian National University', 'HNU', 'San Andreas', '+380(98)7654321');
INSERT INTO universities (id, name, abbreviation_name, city, phone_number) VALUES (1002, 'Psychological National University', 'PNU', 'Los Santos', '+380(55)5555555');

INSERT INTO departments (id, university_id, name) VALUES (1000, 1000, 'Technical');
INSERT INTO departments (id, university_id, name) VALUES (1001, 1000, 'Humanitarian');
INSERT INTO departments (id, university_id, name) VALUES (1002, 1000, 'Psychological');

INSERT INTO auditoriums (id, department_id, name) VALUES (1000, 1000, '101');
INSERT INTO auditoriums (id, department_id, name) VALUES (1001, 1000, '102');
INSERT INTO auditoriums (id, department_id, name) VALUES (1002, 1000, '103');

INSERT INTO lesson_times (id, start_time, end_time) VALUES (1000, '08:15', '09:00');
INSERT INTO lesson_times (id, start_time, end_time) VALUES (1001, '09:15', '10:00');
INSERT INTO lesson_times (id, start_time, end_time) VALUES (1002, '10:15', '11:00');
INSERT INTO lesson_times (id, start_time, end_time) VALUES (1003, '11:15', '12:00');
INSERT INTO lesson_times (id, start_time, end_time) VALUES (1004, '12:15', '13:00');
INSERT INTO lesson_times (id, start_time, end_time) VALUES (1005, '13:15', '14:00');

INSERT INTO courses (id, name, description) VALUES (1000, 'English', 'English is a West Germanic language in the Indo-European language family, whose speakers, called Anglophones, originated in early medieval England on the island of Great Britain');
INSERT INTO courses (id, name, description) VALUES (1001, 'Mathematics', 'Mathematics is the science and study of quality, structure, space, and change');
INSERT INTO courses (id, name, description) VALUES (1002, 'Physics', 'Physics is the scientific study of matter, its fundamental constituents, its motion and behavior through space and time, and the related entities of energy and force');
INSERT INTO courses (id, name, description) VALUES (1003, 'Chemistry', 'Chemistry is a branch of natural science that deals principally with the properties of substances, the changes they undergo, and the natural laws that describe these changes');

INSERT INTO groups (id, name) VALUES (1000, '1SE-24');
INSERT INTO groups (id, name) VALUES (1001, '1CE-24');
INSERT INTO groups (id, name) VALUES (1002, '1SS-24');
INSERT INTO groups (id, name) VALUES (1003, '1PE-24');

INSERT INTO roles (id, name) VALUES (1000, 'ROLE_ADMIN');
INSERT INTO roles (id, name) VALUES (1001, 'ROLE_STAFF');
INSERT INTO roles (id, name) VALUES (1002, 'ROLE_TEACHER');
INSERT INTO roles (id, name) VALUES (1003, 'ROLE_STUDENT');

INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1000, 'admin1', 'Admin001', true, false);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1001, 'admin2', 'Admin002', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1002, 'admin3', 'Admin003', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1003, 'admin4', 'Admin004', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1004, 'employee1', 'Employee1', true, false);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1005, 'employee2', 'Employee2', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1006, 'employee3', 'Employee3', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1007, 'employee4', 'Employee4', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1008, 'teacher1', 'Teacher1', true, false);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1009, 'teacher2', 'Teacher2', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1010, 'teacher3', 'Teacher3', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1011, 'teacher4', 'Teacher4', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1012, 'student1', 'Student1', true, false);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1013, 'student2', 'Student2', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1014, 'student3', 'Student3', true, true);
INSERT INTO users (id, username, password, enabled_status, free_status) VALUES (1015, 'student4', 'Student4', true, true);

INSERT INTO roles_users(role_id, user_id) VALUES (1000, 1000);
INSERT INTO roles_users(role_id, user_id) VALUES (1000, 1001);
INSERT INTO roles_users(role_id, user_id) VALUES (1000, 1002);
INSERT INTO roles_users(role_id, user_id) VALUES (1001, 1004);
INSERT INTO roles_users(role_id, user_id) VALUES (1001, 1005);
INSERT INTO roles_users(role_id, user_id) VALUES (1001, 1006);
INSERT INTO roles_users(role_id, user_id) VALUES (1002, 1008);
INSERT INTO roles_users(role_id, user_id) VALUES (1002, 1009);
INSERT INTO roles_users(role_id, user_id) VALUES (1002, 1010);
INSERT INTO roles_users(role_id, user_id) VALUES (1003, 1012);
INSERT INTO roles_users(role_id, user_id) VALUES (1003, 1013);
INSERT INTO roles_users(role_id, user_id) VALUES (1003, 1014);

INSERT INTO admins (id, user_id) VALUES (1000, 1000);
INSERT INTO admins (id, user_id) VALUES (1001, 1001);
INSERT INTO admins (id, user_id) VALUES (1002, 1002);
INSERT INTO admins (id, user_id) VALUES (1003, 1003);

INSERT INTO employees (id, user_id, first_name, last_name) VALUES (1000, 1004, 'Tasha', 'Garcia');
INSERT INTO employees (id, user_id, first_name, last_name) VALUES (1001, 1005, 'Barry', 'McAllister');
INSERT INTO employees (id, user_id, first_name, last_name) VALUES (1002, 1006, 'Gretchen', 'Boyer');
INSERT INTO employees (id, user_id, first_name, last_name) VALUES (1003, 1007, 'Alison', 'Riggs');

INSERT INTO teachers (id, user_id, first_name, last_name) VALUES (1000, 1008, 'Luz', 'Carson');
INSERT INTO teachers (id, user_id, first_name, last_name) VALUES (1001, 1009, 'Guy', 'Griffin');
INSERT INTO teachers (id, user_id, first_name, last_name) VALUES (1002, 1010, 'Cynthia', 'Newman');
INSERT INTO teachers (id, user_id, first_name, last_name) VALUES (1003, 1011, 'Gerardo', 'Andersen');

INSERT INTO students (id, user_id, first_name, last_name) VALUES (1000, 1012, 'Emily', 'Hensley');
INSERT INTO students (id, user_id, first_name, last_name) VALUES (1001, 1013, 'Marcella', 'Johnson');
INSERT INTO students (id, user_id, first_name, last_name) VALUES (1002, 1014, 'Katie', 'Matthews');
INSERT INTO students (id, user_id, first_name, last_name) VALUES (1003, 1015, 'Stuart', 'Snyder');

-- findAllByDateOrderByIdAsc()
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1000, '2025-01-06', 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1001, '2025-01-06', 1001, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1002, '2025-01-07', 1000, 1000);
-- findAllByTeacherIsNotNullAndDateAndTeacher_IdOrderByIdAsc()
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id, teacher_id) VALUES (1003, '2025-01-08', 1000, 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id, teacher_id) VALUES (1004, '2025-01-08', 1001, 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id, teacher_id) VALUES (1005, '2025-01-08', 1002, 1000, 1001);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1006, '2025-01-08', 1003, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id, teacher_id) VALUES (1007, '2025-01-09', 1000, 1000, 1000);
-- findAllByDateAndGroups_NameOrderByIdAsc()
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1008, '2025-01-10', 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1009, '2025-01-10', 1001, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1010, '2025-01-10', 1000, 1001);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1011, '2025-01-10', 1001, 1001);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1012, '2025-01-13', 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1013, '2025-01-13', 1001, 1000);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1000, 1008);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1000, 1009);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1001, 1010);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1001, 1011);
-- findAllByTeacherIsNotNullAndDateBetweenAndTeacher_IdOrderByIdAsc()
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id, teacher_id) VALUES (1014, '2025-01-20', 1000, 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id, teacher_id) VALUES (1015, '2025-01-20', 1001, 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id, teacher_id) VALUES (1016, '2025-01-24', 1000, 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id, teacher_id) VALUES (1017, '2025-01-24', 1001, 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id, teacher_id) VALUES (1018, '2025-01-24', 1002, 1000, 1001);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1019, '2025-01-24', 1003, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id, teacher_id) VALUES (1020, '2025-01-27', 1000, 1000, 1000);
-- findAllByDateBetweenAndGroups_NameOrderByIdAsc()
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1021, '2025-01-27', 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1022, '2025-01-27', 1001, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1023, '2025-01-31', 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1024, '2025-01-31', 1001, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1025, '2025-01-31', 1000, 1001);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1026, '2025-01-31', 1001, 1001);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1027, '2025-02-03', 1000, 1000);
INSERT INTO lessons (id, date, lesson_time_id, auditorium_id) VALUES (1028, '2025-02-03', 1001, 1000);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1000, 1021);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1000, 1022);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1000, 1023);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1000, 1024);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1001, 1025);
INSERT INTO groups_lessons (group_id, lesson_id) VALUES (1001, 1026);
