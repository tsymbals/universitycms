# UniversityCMS



This is a Java Spring Web Application for managing the creation and modification of digital content for university.

### Stack

- Java 17
- Maven
- Spring Boot 3.2.5
- Docker
- PostgreSQL 15.7
- Flyway Migration
- Hibernate
- Spring Data JPA
- Spring MVC
- Thymeleaf
- Bootstrap 5.3.3
- Spring Security
- JUnit 5

### User Stories

#### Employee Schedule Viewing
**As a** Employee  
**I want to** view my auditoriums schedule  
**So that** I can plan my activities accordingly

**Scenario: Viewing Daily Schedule**
- Given I am logged in as a Employee
- When I access my account
- And I navigate to "Auditoriums schedule" menu
- Then I should see my schedule for the selected date

#### Teacher Schedule Viewing
**As a** Teacher  
**I want to** view my class timetable  
**So that** I can plan my activities accordingly

**Scenario: Viewing Daily Class Timetable**
- Given I am logged in as a Teacher
- When I access my account
- And I navigate to "Lessons schedules" dropdown menu and select "Date schedule"
- Then I should see my class timetable for the selected date

**Scenario: Viewing Weekly Class Timetable**
- Given I am logged in as a Teacher
- When I access my account
- And I navigate to "Lessons schedules" dropdown menu and select "Week schedule"
- Then I should see my class timetable for the entire week

#### Student Schedule Viewing
**As a** Student  
**I want to** view my class timetable  
**So that** I can attend my classes on time

**Scenario: Viewing Daily Class Timetable**
- Given I am logged in as a Student
- When I access my account
- And I navigate to "Lessons schedules" dropdown menu and select "Date schedule"
- Then I should see my class timetable for the selected date

**Scenario: Viewing Weekly Class Timetable**
- Given I am logged in as a Student
- When I access my account
- And I navigate to "Lessons schedules" dropdown menu and select "Week schedule"
- Then I should see my class timetable for the entire week
